﻿using EcommerceSystem.Models;
using EcommerceSystem.Web.Framework.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EcommerceSystem.Web.Framework.Helpers
{
    public class RoleHelper
    {
        #region  Notifications

        public const string NotificationListing = "Notification-Listing";
        public const string NotificationCreate = "Notification-Create";
        public const string NotificationEdit = "Notification-Edit";
        public const string NotificationDelete = "Notification-Delete";
        public const string NotificationInvisible = "Notification-Invisible";

        public static bool IsNotificationListing()
        {
            if (HttpContext.Current.User.IsInRole(NotificationListing))
                return true;

            return false;
        }

        public static bool IsNotificationCreate()
        {
            if (HttpContext.Current.User.IsInRole(NotificationCreate))
                return true;

            return false;
        }

        public static bool IsNotificationEdit()
        {
            if (HttpContext.Current.User.IsInRole(NotificationEdit))
                return true;

            return false;
        }

        public static bool IsNotificationDelete()
        {
            if (HttpContext.Current.User.IsInRole(NotificationDelete))
                return true;

            return false;
        }

        public static bool INotificationDisable()
        {
            if (HttpContext.Current.User.IsInRole(NotificationInvisible))
                return true;

            return false;
        }


        #endregion

        #region  Complaints

        public const string ComplaintListing = "Complaint-Listing";
        public const string ComplaintCreate = "Complaint-Create";
        public const string ComplaintEdit = "Complaint-Edit";
  

        public static bool IsComplaintListing()
        {
            if (HttpContext.Current.User.IsInRole(ComplaintListing))
                return true;

            return false;
        }

        public static bool IsComplaintCreate()
        {
            if (HttpContext.Current.User.IsInRole(ComplaintCreate))
                return true;

            return false;
        }

        public static bool IsComplaintEdit()
        {
            if (HttpContext.Current.User.IsInRole(ComplaintEdit))
                return true;

            return false;
        }

        #endregion

        #region  CarrierOrders

        public const string CarrierOrderListing = "CarrierOrder-Listing";
        public const string CarrierOrderCreate = "CarrierOrder-Create";
        public const string CarrierOrderEdit = "CarrierOrder-Edit";
        public const string CarrierOrderDelete = "CarrierOrder-Delete";
        public const string CarrierOrderInvisible = "CarrierOrder-Invisible";

        public static bool IsCarrierOrderListing()
        {
            if (HttpContext.Current.User.IsInRole(CarrierOrderListing))
                return true;

            return false;
        }

        public static bool IsCarrierOrderCreate()
        {
            if (HttpContext.Current.User.IsInRole(CarrierOrderCreate))
                return true;

            return false;
        }

        public static bool IsCarrierOrderEdit()
        {
            if (HttpContext.Current.User.IsInRole(CarrierOrderEdit))
                return true;

            return false;
        }

        public static bool IsCarrierOrderDelete()
        {
            if (HttpContext.Current.User.IsInRole(CarrierOrderDelete))
                return true;

            return false;
        }

        public static bool ICarrierOrderDisable()
        {
            if (HttpContext.Current.User.IsInRole(CarrierOrderInvisible))
                return true;

            return false;
        }


        #endregion

        #region  Transactions

        public const string TransactionHistories = "Transaction-History";
        public const string TransactionWithdrawal = "Transaction-Withdrawal";
        public const string TransactionEdit = "Transaction-Edit";
        public const string TransactionDelete = "Transaction-Delete";
        public const string TransactionRecharge = "Transaction-ReCharge";
        public const string TransactionRefund = "Transaction-Refund";

        public static bool IsTransactionHistories()
        {
            if (HttpContext.Current.User.IsInRole(TransactionHistories))
                return true;

            return false;
        }

        public static bool IsTransactionWithdrawal()
        {
            if (HttpContext.Current.User.IsInRole(TransactionWithdrawal))
                return true;

            return false;
        }

        public static bool IsTransactionEdit()
        {
            if (HttpContext.Current.User.IsInRole(TransactionEdit))
                return true;

            return false;
        }

        public static bool IsTransactionDelete()
        {
            if (HttpContext.Current.User.IsInRole(TransactionDelete))
                return true;

            return false;
        }

        public static bool IsTransactionRecharge()
        {
            if (HttpContext.Current.User.IsInRole(TransactionRecharge))
                return true;

            return false;
        }
        public static bool IsTransactionRefund()
        {
            if (HttpContext.Current.User.IsInRole(TransactionRefund))
                return true;

            return false;
        }

        public static bool HasRole(string roleName)
        {
            if (HttpContext.Current.User.IsInRole(roleName))
                return true;

            return false;
        }
        #endregion

        #region  Credits

        public const string CreditInfo = "Credit-Info";
        public const string CreditTransactionListing = "Credit-TransactionListing";
        public const string CreditChargeToNV = "Credit-ChargeToNV";
        public const string CreditChargeToAdmin = "Credit-ChargeToAdmin";
        public const string CreditWithdrawCreditAdmin = "Credit-WithdrawCreditAdmin";
        public const string CreditRefundUserCredit = "Credit-RefundUserCredit";
        public const string CreditTransfer = "Credit-Transfer";
        public static bool IsCreditInfo()
        {
            if (HttpContext.Current.User.IsInRole(CreditInfo))
                return true;

            return false;
        }
        public static bool IsCreditTransactionListing()
        {
            if (HttpContext.Current.User.IsInRole(CreditTransactionListing))
                return true;

            return false;
        }
        public static bool IsCreditChargeToNV()
        {
            if (HttpContext.Current.User.IsInRole(CreditChargeToNV))
                return true;

            return false;
        }

        public static bool IsCreditChargeToAdmin()
        {
            if (HttpContext.Current.User.IsInRole(CreditChargeToAdmin))
                return true;

            return false;
        }

        public static bool IsCreditWithdrawCreditAdmin()
        {
            if (HttpContext.Current.User.IsInRole(CreditWithdrawCreditAdmin))
                return true;

            return false;
        }
        public static bool IsCreditTransfer()
        {
            if (HttpContext.Current.User.IsInRole(CreditTransfer))
                return true;

            return false;
        }

        public static bool IsSuperAdmin()
        {
            var customPrincipal = HttpContext.Current.User.Identity as EcommerceSystemIdentity;
            if (customPrincipal != null)
            {
                return customPrincipal.UserCommon.IsAdmin;
            }
            return false;
        }


        public static bool IsCreditRefundUserCredit()
        {
            if (HttpContext.Current.User.IsInRole(CreditRefundUserCredit))
                return true;

            return false;
        }
        #endregion

        #region  Roles

        public const string RoleListing = "Role-Listing";
        public const string RoleCreate = "Role-Create";
        public const string RoleEdit = "Role-Edit";
        public const string RoleDelete = "Role-Delete";
        public const string RoleInvisible = "Role-Invisible";

        public static bool IsRoleListing()
        {
            if (HttpContext.Current.User.IsInRole(RoleListing))
                return true;

            return false;
        }

        public static bool IsRoleCreate()
        {
            if (HttpContext.Current.User.IsInRole(RoleCreate))
                return true;

            return false;
        }

        public static bool IsRoleEdit()
        {
            if (HttpContext.Current.User.IsInRole(RoleEdit))
                return true;

            return false;
        }

        public static bool IsRoleDelete()
        {
            if (HttpContext.Current.User.IsInRole(RoleDelete))
                return true;

            return false;
        }

        public static bool IsRoleInvisible()
        {
            if (HttpContext.Current.User.IsInRole(RoleInvisible))
                return true;

            return false;
        }


        #endregion

        #region Configurations

        public const string ConfigurationSetting = "Configuration-Setting";

        public static bool IsSetting()
        {
            if (HttpContext.Current.User.IsInRole(ConfigurationSetting))
                return true;

            return false;
        }

        #endregion

        #region  Users

        public const string UserListing = "Users-Listing";
        public const string UserCreate = "Users-Create";
        public const string UserEdit = "Users-Edit";
        public const string UserDelete = "Users-Delete";
        public const string UserInvisibe = "Users-Invisible";

        public static bool IsUserListing()
        {
            if (HttpContext.Current.User.IsInRole(UserListing))
                return true;

            return false;
        }

        public static bool IsUserCreate()
        {
            if (HttpContext.Current.User.IsInRole(UserCreate))
                return true;

            return false;
        }

        public static bool IsUserEdit()
        {
            if (HttpContext.Current.User.IsInRole(UserEdit))
                return true;

            return false;
        }

        public static bool IsUserDelete()
        {
            if (HttpContext.Current.User.IsInRole(UserDelete))
                return true;

            return false;
        }

        public static bool IsUserInvisibe()
        {
            if (HttpContext.Current.User.IsInRole(UserInvisibe))
                return true;

            return false;
        }

        #endregion

        #region  Customers

        public const string CustomerListing = "Customer-Listing";
        public const string CustomerCreate = "Customer-Create";
        public const string CustomerEdit = "Customer-Edit";
        public const string CustomerDelete = "Customer-Delete";
        public const string CustomerInvisible = "Customer-Invisible";
        public const string CustomerRemoteLogin = "Customer-RemoteLogin";
        public const string CustomerTransaction = "Customer-Transaction";

        public static bool IsCustomerListing()
        {
            if (HttpContext.Current.User.IsInRole(CustomerListing))
                return true;

            return false;
        }

        public static bool IsCustomerCreate()
        {
            if (HttpContext.Current.User.IsInRole(CustomerCreate))
                return true;

            return false;
        }

        public static bool IsCustomerEdit()
        {
            if (HttpContext.Current.User.IsInRole(CustomerEdit))
                return true;

            return false;
        }

        public static bool IsCustomerDelete()
        {
            if (HttpContext.Current.User.IsInRole(CustomerDelete))
                return true;

            return false;
        }

        public static bool IsCustomerInvisible()
        {
            if (HttpContext.Current.User.IsInRole(CustomerInvisible))
                return true;

            return false;
        }


        public static bool IsCustomerTransaction()
        {
            if (HttpContext.Current.User.IsInRole(CustomerTransaction))
                return true;

            return false;
        }

        public static bool IsCustomerRemoteLogin()
        {
            if (HttpContext.Current.User.IsInRole(CustomerRemoteLogin))
                return true;

            return false;
        }


        #endregion

        #region  Orders

    
        //public const string OrderView = "Order-View";
        public const string DepositOrder = "Order-DepositOrder";
        public const string CancelOrderDeposited = "Order-CancelOrderDeposited";
      
        public const string OrderQuote = "Order-Quote";
        public const string MergeOrder = "Order-MergeOrder";
        public const string LatchOrder = "Order-Latch";
        public const string OrderDetail = "Order-Detail";
        public const string ShopOrderDetail = "Order-ShopOrderDetail";
        public const string PurchaseOrder = "Order-PurchaseOrder";
        public const string CancelOrder = "Order-CancelOrder";
        public const string OrderPendingListing = "Order-OrderPendingListing";
        public const string OrderPendingQuoteListing = "Order-OrderPendingQuoteListing";
        public const string OrderQuoteListing = "Order-OrderQuoteListing";
        public const string OrderPurchasingListing = "Order-OrderPurchasingListing";
        public const string OrderPaidListing = "Order-OrderPaidListing";
        public const string ShopperOrderListing = "Order-ShopperOrderListing";
        public const string OrderPenddingDepositListing = "Order-PenddingDepositListing";
        public const string ImportChinaStoreListing = "Order-ImportChinaStoreListing";
        public const string ProcessImportChinaStore = "Order-ProcessImportChinaStore";
        public const string ExportChinaStoreListing = "Order-ExportChinaStoreListing";
     
        public const string ImportVietnamStoreListing = "Order-ImportVietnamStoreListing";
        public const string ProcessImportVietnamStore = "Order-ProcessImportVietnamStore";
        public const string ExportVietnamStoreListing = "Order-ExportVietnamStoreListing";
        public const string ProcessExportVietnamStore = "Order-ProcessExportVietnamStore";
        public const string ReadyExportVNStoreListing = "Order-ReadyExportVNStoreListing";
        public const string OrderDone = "Order-OrderDone";
        public const string SearchLanding = "Order-SearchLanding";
        public const string SearchShopOrder = "Order-SearchShopOrder";
        public const string ProcessOrder = "Order-ProcessOrder";
        public const string OrderTrashListing = "OrderTrash-Listing";
        public const string OrderTrashDetail = "OrderTrash-Detail";
        public const string OrderTrashRestore = "OrderTrash-Restore";
        public const string AwaitPurchaseOrder = "Order-AwaitPurchaseOrder";
        public const string RollBackToQuoteOrder = "Order-RollBackToQuoteOrder";
        public const string EditBoughtOrder = "Order-EditBoughtOrder";
        public const string UpdateShippingCode = "Order-UpdateShippingCode";

        public const string AddPackage = "Order-AddPackage";
        public const string AddLandingCodeToPackage = "Order-AddLandingCodeToPackage";
        public const string DeletePackage = "Order-DeletePackage";
        public const string DeletePackageItem = "Order-DeletePackageItem";
        public const string UpdatePackage = "Order-UpdatePackage";
        public const string ReceivePackage = "Order-ReceivePackage";

        public const string OrderChat = "Order-Chat";
        public static bool IsOrderPendingListing()
        {
            if (HttpContext.Current.User.IsInRole(OrderPendingListing))
                return true;

            return false;
        }
        public static bool IsOrderPendingQuoteListing()
        {
            if (HttpContext.Current.User.IsInRole(OrderPendingQuoteListing))
                return true;

            return false;
        }

        public static bool IsReadyExportVNStoreListing()
        {
            if (HttpContext.Current.User.IsInRole(ReadyExportVNStoreListing))
                return true;

            return false;
        }
        public static bool IsOrderQuoteListing()
        {
            if (HttpContext.Current.User.IsInRole(OrderQuoteListing))
                return true;

            return false;
        }
        public static bool IsOrderPurchasingListing()
        {
            if (HttpContext.Current.User.IsInRole(OrderPurchasingListing))
                return true;

            return false;
        }
        public static bool IsOrderPenddingDepositListing()
        {
            if (HttpContext.Current.User.IsInRole(OrderPenddingDepositListing))
                return true;

            return false;
        }
        public static bool IsOrderPaidListing()
        {
            if (HttpContext.Current.User.IsInRole(OrderPaidListing))
                return true;

            return false;
        }
        public static bool IsImportChinaStoreListing()
        {
            if (HttpContext.Current.User.IsInRole(ImportChinaStoreListing))
                return true;

            return false;
        }
        public static bool IsExportChinaStoreListing()
        {
            if (HttpContext.Current.User.IsInRole(ExportChinaStoreListing))
                return true;

            return false;
        }
        public static bool IsImportVietnamStoreListing()
        {
            if (HttpContext.Current.User.IsInRole(ImportVietnamStoreListing))
                return true;

            return false;
        }
        public static bool IsExportVietnamStoreListing()
        {
            if (HttpContext.Current.User.IsInRole(ExportVietnamStoreListing))
                return true;

            return false;
        }

        public static bool IsShopperOrderListing()
        {
            if (HttpContext.Current.User.IsInRole(ShopperOrderListing))
                return true;

            return false;
        }

        public static bool IsOrderDone()
        {
            if (HttpContext.Current.User.IsInRole(OrderDone))
                return true;

            return false;
        }
        public static bool IsDepositOrder()
        {
            if (HttpContext.Current.User.IsInRole(DepositOrder))
                return true;

            return false;
        }

        public static bool IsCancelOrderDeposited()
        {
            if (HttpContext.Current.User.IsInRole(CancelOrderDeposited))
                return true;

            return false;
        }

        public static bool IsOrderQuote()
        {
            if (HttpContext.Current.User.IsInRole(OrderQuote))
                return true;

            return false;
        }

        public static bool IsMergeOrder()
        {
            if (HttpContext.Current.User.IsInRole(MergeOrder))
                return true;

            return false;
        }
        public static bool IsLatchOrder()
        {
            if (HttpContext.Current.User.IsInRole(LatchOrder))
                return true;

            return false;
        }
        public static bool IsOrderDetail()
        {
            if (HttpContext.Current.User.IsInRole(OrderDetail))
                return true;

            return false;
        }
        public static bool IsShopOrderDetail()
        {
            if (HttpContext.Current.User.IsInRole(ShopOrderDetail))
                return true;

            return false;
        }
        public static bool IsPurchaseOrder()
        {
            if (HttpContext.Current.User.IsInRole(PurchaseOrder))
                return true;

            return false;
        }
        public static bool IsCancelOrder()
        {
            if (HttpContext.Current.User.IsInRole(CancelOrder))
                return true;

            return false;
        }
        
        public static bool IsSearchLanding()
        {
            if (HttpContext.Current.User.IsInRole(SearchLanding))
                return true;

            return false;
        }

        public static bool IsOrderTrashListing()
        {
            if (HttpContext.Current.User.IsInRole(OrderTrashListing))
                return true;

            return false;
        }

        public static string GetRoleByOrderStatus(OrderStatusType orderStatus)
        {
            switch (orderStatus)
            {
                case OrderStatusType.PenddingProcess:
                    return OrderPendingListing;
                case OrderStatusType.PenddingQuote:
                    return OrderPendingQuoteListing;
                case OrderStatusType.Quoted:
                    return OrderQuoteListing;
                case OrderStatusType.PenddingDeposit:
                    return OrderPenddingDepositListing;
                case OrderStatusType.PenddingBuy:
                    return OrderPurchasingListing;
                case OrderStatusType.Bought:
                    return OrderPaidListing;
                case OrderStatusType.AllBought:
                    return ShopperOrderListing;
                case OrderStatusType.ImportChinaStore:
                    return ImportChinaStoreListing;
                case OrderStatusType.ExportChinaStore:
                    return ExportChinaStoreListing;
                case OrderStatusType.ImportVNStore:
                    return ImportVietnamStoreListing;
                case OrderStatusType.ExportVNStore:
                    return ExportVietnamStoreListing;
                case OrderStatusType.ReadyExportVNStore:
                    return ReadyExportVNStoreListing;
                case OrderStatusType.Finished:
                    return OrderDone;
                case OrderStatusType.Cancelled:
                    return string.Empty; // no idea for this case
                default:
                    return string.Empty;
            }
        }

        #endregion

        #region Helper methods

        public static string GetDisplayActionName(string actionType)
        {
            var result = string.Empty;
            switch (actionType)
            {
                case "Listing":
                    result = "Danh sách";
                    break;
                case "Create":
                    result = "Thêm";
                    break;
                case "Edit":
                    result = "Sửa";
                    break;
                case "Delete":
                    result = "Xóa";
                    break;
                case "Invisible":
                    result = "KH/Ngừng KH";
                    break;
                case "PermissionRole":
                    result = "Phân Quyền";
                    break;
                case "Export":
                    result = "Xuất báo cáo";
                    break;
                case "SearchLanding":
                    result = "Tìm kiếm MVĐ";
                    break;
                case "OrderPendingListing":
                    result = "Ds chờ xử lý";
                    break;
                case "DepositOrder":
                    result = "Đặt cọc";
                    break;
                case "ProcessOrder":
                    result = "Xử lý ĐH";
                    break;
                case "CancelOrderDeposited":
                    result = "Hủy đơn đặt cọc";
                    break;
                case "OrderPendingQuoteListing":
                    result = "Ds chờ báo giá";
                    break;
                case "Quote":
                    result = "Báo giá";
                    break;
                case "MergeOrder":
                    result = "Gộp đơn";
                    break;
                case "Latch":
                    result = "Chốt đơn";
                    break;
                case "Detail":
                    result = "Chi tiết";
                    break;
                case "AwaitPurchaseOrder":
                    result = "Chờ mua";
                    break;
                case "ShopOrderDetail":
                    result = "Chi tiết Shop";
                    break;
                case "PurchaseOrder":
                    result = "Mua hàng";
                    break;
                case "EditBoughtOrder":
                    result = "Chỉnh sửa ĐH đã mua";
                    break;
                case "CancelOrder":
                    result = "Hủy đơn";
                    break;
                case "OrderQuoteListing":
                    result = "Ds đã báo giá";
                    break;
                case "PenddingDepositListing":
                    result = "Ds chờ đặt cọc";
                    break;
                case "ReadyExportVNStoreListing":
                    result = "Ds Chuyển cho khách";
                    break;
                case "OrderPurchasingListing":
                    result = "Ds chờ mua";
                    break;
                case "OrderPaidListing":
                    result = "Ds đã mua";
                    break;
                case "ShopperOrderListing":
                    result = "Ds đã mua theo shop";
                    break;
                case "OrderChineseInventoryReceivingListing":
                    result = "Ds nhập kho TQ";
                    break;
                case "OrderChineseInventoryDeliveringListing":
                    result = "Ds xuất kho TQ";
                    break;
                case "OrderVietnameseInventoryReceivingListing":
                    result = "Ds nhập kho VN";
                    break;
                case "OrderVietnameseInventoryDeliveringListing":
                    result = "Ds xuất kho VN";
                    break;
                case "OrderDone":
                    result = "Ds hoàn thành";
                    break;
                case "Recharge":
                    result = "Nạp tiền";
                    break;
                case "Withdrawal":
                    result = "Rút tiền";
                    break;
                case "Approve":
                    result = "Chấp thuận";
                    break;
                case "UpdateShippingCode":
                    result = "Cập nhật MVĐ";
                    break;
                case "RollBackToQuoteOrder":
                    result = "Quay lại báo giá";
                    break;
                case "Restore":
                    result = "Khôi phục";
                    break;
                case "SearchShopOrder":
                    result = "Tìm kiếm shop";
                    break;
                case "ProcessImportChinaStore":
                    result = "Nhập kho Trung Quốc";
                    break;
                case "ReceivePackage":
                    result = "Nhận kiện hàng";
                    break;
                case "ProcessImportVietnamStore":
                    result = "Nhập kho Việt Nam";
                    break;
                case "ProcessExportVietnamStore":
                    result = "Xuất kho Việt Nam";
                    break;
                case "AddPackage":
                    result = "Thêm kiện hàng";
                    break;
                case "AddLandingCodeToPackage":
                    result = "Thêm MVĐ vào kiện hàng";
                    break;
                case "DeletePackage":
                    result = "Xóa kiện hàng";
                    break;
                case "UpdatePackage":
                    result = "Cập nhật kiện hàng";
                    break;
                case "DeletePackageItem":
                    result = "Xóa MVĐ khỏi kiện hàng";
                    break;
                case "ImportChinaStoreListing":
                    result = "Ds nhập kho TQ";
                    break;
                case "ExportChinaStoreListing":
                    result = "Ds xuất kho TQ";
                    break;
                case "ImportVietNamStoreListing":
                    result = "Ds nhập kho VN";
                    break;
                case "ExportVietNamStoreListing":
                    result = "Xuất kho nhập kho VN";
                    break;
                case "Chat":
                    result= "Chat";
                    break;
                case "History":
                    result = "Lịch sử giao dịch";
                    break;
                case "Info":
                    result = "Thông tin";
                    break;
                case "TransactionListing":
                    result = "Lịch sử giao dịch";
                    break;
                case "ChargeToNV":
                    result = "Nạp tiền nhân viên";
                    break;
                case "ChargeToAdmin":
                    result = "Nạp tiền cho công ty";
                    break;
                case "WithdrawCreditAdmin":
                    result = "Rút tiền công ty";
                    break;
                case "RefundUserCredit":
                    result = "Thu hồi tiền nhân viên";
                    break;
                case "Transfer":
                    result = "Chuyển khoản";
                    break;
                case "Setting":
                    result = "Thiết lập chung";
                    break;
                case "RemoteLogin":
                    result = "Đăng nhập tk khách";
                    break;
                case "Transaction":
                    result = "Giao dịch";
                    break;
                case "Refund":
                    result = "Hoàn tiền";
                    break;
            }

            return result;
        }


        #endregion
    }
}
