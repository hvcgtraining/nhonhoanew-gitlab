﻿using EcommerceSystem.Services;
using System;
using System.Collections.Specialized;
using System.Web.Mvc;
using System.Web.Security;

namespace EcommerceSystem.Web.Framework.Security
{
    public class EcommerceSystemMembershipProvider : MembershipProvider
    {
        #region Properties

        private int _cacheTimeoutInMinutes = 30;

        #endregion

        public override void Initialize(string name, NameValueCollection config)
        {
            int val;
            if (!string.IsNullOrEmpty(config["cacheTimeoutInMinutes"]) && Int32.TryParse(config["cacheTimeoutInMinutes"], out val))
                _cacheTimeoutInMinutes = val;

            base.Initialize(name, config);
        }

        public override bool ValidateUser(string username, string password)
        {
            var userServices = DependencyResolver.Current.GetService(typeof(IUserService)) as IUserService;
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return false;

            if (userServices != null)
            {
                string message;
                var result = userServices.ValidateLogon(username, password, out message);
                if (result != null && result.UserId != 0)
                {
                    return true;
                }
            }
            return false;
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            var userServices = DependencyResolver.Current.GetService(typeof(IUserService)) as IUserService;
            //var cacheKey = string.Format("User_{0}", username);
            //if (HttpRuntime.Cache[cacheKey] != null)
            //    return (EcommerceSystemMembershipUser)HttpRuntime.Cache[cacheKey];

            //Get Data from Database
            if (userServices != null)
            {
                var userResult = userServices.GetUserByEmail(username);
                if (userResult == null || userResult.UserId == 0)
                {
                    return null;
                }

                var membershipUser = new EcommerceSystemMembershipUser(userResult);
                //Store in cache,NoSlidingExpiration : timeout
                //HttpRuntime.Cache.Insert(cacheKey, membershipUser, null, DateTime.UtcNow.AddMinutes(_cacheTimeoutInMinutes),
                //    Cache.NoSlidingExpiration);

                return membershipUser;
            }

            return null;
        }

        #region Overrides of MembershipProvider

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override string ApplicationName { get; set; }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}