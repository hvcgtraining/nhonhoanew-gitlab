﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;

namespace EcommerceSystem.Web.Framework.Security
{
    public class EcommerceSystemPrincipal : IPrincipal
    {
        #region Implementation of IPrincipal

        public bool IsInRole(string roleName)
        {
            if(EcommerceSystemIdentity.UserCommon != null && EcommerceSystemIdentity.UserCommon.IsAdmin)
            {
                return true;
            }
            var roleModuleActions = (EcommerceSystemIdentity.UserCommon != null && EcommerceSystemIdentity.UserCommon.RoleModuleActions != null)
                ? EcommerceSystemIdentity.UserCommon.RoleModuleActions : new Dictionary<string, string>();

            if (roleModuleActions.ContainsKey(roleName.ToLower()))
                return true;

            return false;
        }

        /// <summary>
        /// Gets the identity of the current principal.
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Security.Principal.IIdentity"/> object associated with the current principal.
        /// </returns>
        public IIdentity Identity { get; private set; }

        #endregion

        public EcommerceSystemIdentity EcommerceSystemIdentity { get { return (EcommerceSystemIdentity)Identity; } set { Identity = value; } }

        public EcommerceSystemPrincipal(EcommerceSystemIdentity identity)
        {
            Identity = identity;
        }
    }
}