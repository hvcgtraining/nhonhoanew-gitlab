﻿using System;
using System.Web.Security;
using EcommerceSystem.Models.User;

namespace EcommerceSystem.Web.Framework.Security
{
    public class EcommerceSystemMembershipUser : MembershipUser
    {
        #region Properties

        public UserCommon UserCommon { get; set; }
        
        #endregion

        public EcommerceSystemMembershipUser(UserCommon user)
            : base("EcommerceSystemMembershipProvider", user.UserName, user.FullName, user.Email, string.Empty, string.Empty, true, false, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow)
        {
            UserCommon = user;
        }
    }
}