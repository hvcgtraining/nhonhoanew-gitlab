﻿using Autofac;
using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;

namespace EcommerceSystem.Web.Framework.Dependency
{
    public class EFModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new RepositoryModule());

            builder.RegisterType(typeof(EcommerceSystemEntities)).InstancePerRequest();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
        }
    }
}