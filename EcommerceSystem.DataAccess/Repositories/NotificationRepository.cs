﻿using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using EcommerceSystem.Core;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface INotificationRepository : IBaseRepository<Notification>
    {
        List<Notification> Search(string textSearch, int? type, int currentPage, int pageSize, out int totalPage);
    }
    public class NotificationRepository : BaseRepository<Notification>, INotificationRepository
    {
        public NotificationRepository(EcommerceSystemEntities context)
            : base(context)
        {
        }
        public List<Notification> Search(string textSearch, int? type, int currentPage, int pageSize, out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;


            var query = Dbset.AsQueryable().Where(x=>!x.IsDeleted);
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.Title.Contains(textSearch) || c.Note.Contains(textSearch));
            }
            if(type.HasValue)
            {
                query = query.Where(x => x.Type == type);
            }
            totalPage = query.Count();

            query = query.OrderByDescending(c => c.NotificationId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
