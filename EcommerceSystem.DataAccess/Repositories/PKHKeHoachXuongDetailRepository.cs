﻿using System.Collections.Generic;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Core;
using EcommerceSystem.DataAccess.LinqExtensions;
using System.Data.Entity;
using System;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface IPKHKeHoachXuongDetailRepository: IBaseRepository<PKHKeHoachXuongDetail>
    {
        List<PKHKeHoachXuongDetail> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        List<PKHKeHoachXuongDetail> GetAllByParent(long keHoachXuongId);
    }
    public class PKHKeHoachXuongDetailRepository : BaseRepository<PKHKeHoachXuongDetail>, IPKHKeHoachXuongDetailRepository
    {
        public PKHKeHoachXuongDetailRepository(EcommerceSystemEntities context) : base(context)
        {
        }

        public List<PKHKeHoachXuongDetail> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection,
          out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            var query = Dbset.AsQueryable();
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.Title.Contains(textSearch));
            }

            totalPage = query.Count();

			if(!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
                query = query.OrderByDescending(c => c.PKHKeHoachXuongDetailId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }

        public List<PKHKeHoachXuongDetail> GetAllByParent(long keHoachXuongId)
        {
            var query = Dbset.AsQueryable().Include(c=>c.ChiTietVatTu).Include(x=>x.Kho).Where(k=>k.PKHKeHoachXuongId == keHoachXuongId);

            return query.ToList();
        }
    }
}
