﻿using EcommerceSystem.DataAccess.Common;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface IRoleModuleActionRepository : IBaseRepository<RoleModuleAction>
    {

    }
    public class RoleModuleActionRepository : BaseRepository<RoleModuleAction>, IRoleModuleActionRepository
    {
        public RoleModuleActionRepository(EcommerceSystemEntities context)
            : base(context)
        {
        }

    }
}
