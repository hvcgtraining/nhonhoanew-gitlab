﻿using System.Collections.Generic;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Core;
using EcommerceSystem.DataAccess.LinqExtensions;
using System.Data.Entity;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface ISoDuBanDauRepository: IBaseRepository<SoDuBanDau>
    {
        List<SoDuBanDau> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
    }
    public class SoDuBanDauRepository : BaseRepository<SoDuBanDau>, ISoDuBanDauRepository
    {
        public SoDuBanDauRepository(EcommerceSystemEntities context) : base(context)
        {
        }

        public List<SoDuBanDau> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection,
          out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            var query = Dbset.Include(p=>p.Kho).Include(p => p.ChiTietVatTu).AsQueryable();
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.Title.Contains(textSearch)|| c.Kho.MaKho.Contains(textSearch));
            }

            totalPage = query.Count();

			if(!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
                query = query.OrderByDescending(c => c.SoDuBanDauId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
