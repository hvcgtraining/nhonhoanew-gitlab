﻿using System.Collections.Generic;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Core;
using EcommerceSystem.DataAccess.LinqExtensions;
using System.Data.Entity;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface IPKHDuKienGiaoHangRepository: IBaseRepository<PKHDuKienGiaoHang>
    {
        List<PKHDuKienGiaoHang> Search(int currentPage, int pageSize, string textSearch, int PKHPhieuYeuCauMuaHangId, int PKHPhieuYeuCauMuaHangDetailId, string sortColumn, string sortDirection, out int totalPage);
    }
    public class PKHDuKienGiaoHangRepository : BaseRepository<PKHDuKienGiaoHang>, IPKHDuKienGiaoHangRepository
    {
        public PKHDuKienGiaoHangRepository(EcommerceSystemEntities context) : base(context)
        {
        }

        public List<PKHDuKienGiaoHang> Search(int currentPage, int pageSize, string textSearch, int PKHPhieuYeuCauMuaHangId, int PKHPhieuYeuCauMuaHangDetailId, string sortColumn, string sortDirection,
          out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            var query = Dbset.Include(c=>c.PKHPhieuYeuCauMuaHangDetail.ChiTietVatTu).AsQueryable();
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.Title.Contains(textSearch));
            }

            totalPage = query.Count();
            if(PKHPhieuYeuCauMuaHangId != 0)
            {
                query = query.Where(c => c.PKHPhieuYeuCauMuaHangId == PKHPhieuYeuCauMuaHangId);
            }
            if (PKHPhieuYeuCauMuaHangDetailId != 0)
            {
                query = query.Where(c => c.PKHPhieuYeuCauMuaHangDetailId == PKHPhieuYeuCauMuaHangDetailId);
            }
            if (!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
                query = query.OrderByDescending(c => c.PKHDuKienGiaoHangId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
