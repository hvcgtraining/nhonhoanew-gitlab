﻿using System.Collections.Generic;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Core;
using EcommerceSystem.DataAccess.LinqExtensions;
using System.Data.Entity;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface IDanhSachDieuChuyenQuyetToanRepository: IBaseRepository<DanhSachDieuChuyenQuyetToan>
    {
        List<DanhSachDieuChuyenQuyetToan> Search(int currentPage, int pageSize, string textSearch, int loaiDieuChuyen, int quyetToanId, string sortColumn, string sortDirection, out int totalPage);
    }
    public class DanhSachDieuChuyenQuyetToanRepository : BaseRepository<DanhSachDieuChuyenQuyetToan>, IDanhSachDieuChuyenQuyetToanRepository
    {
        public DanhSachDieuChuyenQuyetToanRepository(EcommerceSystemEntities context) : base(context)
        {
        }

        public List<DanhSachDieuChuyenQuyetToan> Search(int currentPage, int pageSize, string textSearch, int loaiDieuChuyen, int quyetToanId, string sortColumn, string sortDirection,
          out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            var query = Dbset.Include(c=>c.DieuChuyenQuyetToan).Include(c=>c.ChiTietVatTu).Include(c=>c.ChiTietVatTu1).AsQueryable();
            if (quyetToanId != 0)
            {
                query = query.Where(d => d.DieuChuyenQuyetToanId == quyetToanId);
            }
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.Title.Contains(textSearch));
            }

            totalPage = query.Count();

			if(!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
                query = query.OrderByDescending(c => c.DanhSachDieuChuyenQuyetToanId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
