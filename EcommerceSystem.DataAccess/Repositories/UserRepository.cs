﻿using System.Data.Entity;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using System.Collections.Generic;
using EcommerceSystem.Core;

namespace EcommerceSystem.DataAccess
{
    public interface IUserRepository : IBaseRepository<User>
    {
        User RetrieveUser(int userId);
        //List<User> SearchUser(UserSearchModel searchOption, bool includesSuperAdminAccount = false);
        List<User> SearchUser(int currentPage, int pageSize, string textSearch, out int totalPage);

        User GetUserByEmail(string email);
        User GetUserByUserName(string username);
    }
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(EcommerceSystemEntities context)
            : base(context)
        {
        }

        public User RetrieveUser(int userId)
        {
            return Dbset.Include(c => c.UserRoles).FirstOrDefault(x => x.UserId == userId);
        }

        public User GetUserByUserName(string username)
        {
            return Dbset.Include(c => c.UserRoles).FirstOrDefault(c => c.UserName.Equals(username) && (!c.IsLockedOut));
        }

        public List<User> SearchUser(int currentPage, int pageSize, string textSearch, out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;
            var query = Dbset.Include(x => x.UserRoles.Select(y => y.Role)).AsQueryable().Where(x => !x.IsSupperAdmin);
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.UserName.Contains(textSearch) || c.UserRoles.Any(y => y.Role.Name.Contains(textSearch)));
            }

            totalPage = query.Count();

            query = query.OrderBy(c => c.UserId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

        }

        public User GetUserByEmail(string email)
        {
            return Dbset.Include(ct => ct.UserRoles).FirstOrDefault(c => c.Email.Equals(email));
        }
    }
}
