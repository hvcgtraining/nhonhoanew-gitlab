﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Core;
using EcommerceSystem.DataAccess.LinqExtensions;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface IDanhSachChiTietRepository: IBaseRepository<DanhSachChiTiet>
    {
        List<DanhSachChiTiet> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        List<DanhSachChiTiet> GetAllByParentId(int dinhMucSanXuatId);
        List<DanhSachChiTiet> GetAllByDinhMucCatToleId(int dinhMucCatToleId);
    }
    public class DanhSachChiTietRepository : BaseRepository<DanhSachChiTiet>, IDanhSachChiTietRepository
    {
        public DanhSachChiTietRepository(EcommerceSystemEntities context) : base(context)
        {
        }

        public List<DanhSachChiTiet> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection,
          out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            var query = Dbset.Include(x => x.ChiTietVatTu).AsQueryable();
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.Title.Contains(textSearch));
            }

            totalPage = query.Count();

			if(!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
                query = query.OrderByDescending(c => c.DanhSachChiTietId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }

        public List<DanhSachChiTiet> GetAllByParentId(int dinhMucSanXuatId)
        {
            var query = Dbset.AsQueryable();
            query = query.Where(p => p.DinhMucSanXuatId == dinhMucSanXuatId).Include(o => o.ChiTietVatTu);

            return query.ToList();
        }

        public List<DanhSachChiTiet> GetAllByDinhMucCatToleId(int dinhMucCatToleId)
        {
            var query = Dbset.AsQueryable();
            query = query.Where(p => p.DinhMucCatToleId == dinhMucCatToleId).Include(o => o.ChiTietVatTu);

            return query.ToList();
        }
    }
}
