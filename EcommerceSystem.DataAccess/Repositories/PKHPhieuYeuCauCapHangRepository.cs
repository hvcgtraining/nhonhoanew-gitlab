﻿using System.Collections.Generic;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Core;
using EcommerceSystem.DataAccess.LinqExtensions;
using System.Data.Entity;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface IPKHPhieuYeuCauCapHangRepository: IBaseRepository<PKHPhieuYeuCauCapHang>
    {
        List<PKHPhieuYeuCauCapHang> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
    }
    public class PKHPhieuYeuCauCapHangRepository : BaseRepository<PKHPhieuYeuCauCapHang>, IPKHPhieuYeuCauCapHangRepository
    {
        public PKHPhieuYeuCauCapHangRepository(EcommerceSystemEntities context) : base(context)
        {
        }

        public List<PKHPhieuYeuCauCapHang> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection,
          out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            var query = Dbset.AsQueryable().Include(p => p.LoaiVatTu).Include(p => p.Kho); ;
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.Title.Contains(textSearch));
            }

            totalPage = query.Count();

			if(!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
                query = query.OrderByDescending(c => c.PKHPhieuYeuCauCapHangId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
