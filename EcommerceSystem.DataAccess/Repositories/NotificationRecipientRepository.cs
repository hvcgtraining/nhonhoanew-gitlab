﻿using EcommerceSystem.DataAccess.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface INotificationRecipientRepository : IBaseRepository<NotificationRecipient>
    {
    }
    public class NotificationRecipientRepository : BaseRepository<NotificationRecipient>, INotificationRecipientRepository
    {
        public NotificationRecipientRepository(EcommerceSystemEntities context)
            : base(context)
        {
        }
    }
}
