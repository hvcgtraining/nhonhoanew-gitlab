﻿using System.Collections.Generic;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Core;
using EcommerceSystem.DataAccess.LinqExtensions;
using System.Data.Entity;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface IDieuChuyenQuyetToanRepository : IBaseRepository<DieuChuyenQuyetToan>
    {
        List<DieuChuyenQuyetToan> Search(int currentPage, int pageSize, string textSearch, int loaiDieuChuyen, string sortColumn, string sortDirection, out int totalPage);
    }
    public class DieuChuyenQuyetToanRepository : BaseRepository<DieuChuyenQuyetToan>, IDieuChuyenQuyetToanRepository
    {
        public DieuChuyenQuyetToanRepository(EcommerceSystemEntities context) : base(context)
        {
        }

        public List<DieuChuyenQuyetToan> Search(int currentPage, int pageSize, string textSearch, int loaiDieuChuyen, string sortColumn, string sortDirection,
          out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            var query = Dbset.Include(c => c.Kho).Include(c=>c.Kho1).AsQueryable();
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.Title.Contains(textSearch));
            }

            if(loaiDieuChuyen != 0) //DieuChuyenQuyetToan co loaiDieuChuyen=0
            {
                query = query.Where(c => c.LoaiDieuChuyenQuyetToan == loaiDieuChuyen);
            }
            totalPage = query.Count();

            if (!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
                query = query.OrderByDescending(c => c.DieuChuyenQuyetToanId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
