﻿using System.Collections.Generic;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Core;
using EcommerceSystem.DataAccess.LinqExtensions;
using System.Data.Entity;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface ICoPhanCapXuongRepository: IBaseRepository<CoPhanCapXuong>
    {
        List<CoPhanCapXuong> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
    }
    public class CoPhanCapXuongRepository : BaseRepository<CoPhanCapXuong>, ICoPhanCapXuongRepository
    {
        public CoPhanCapXuongRepository(EcommerceSystemEntities context) : base(context)
        {
        }

        public List<CoPhanCapXuong> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection,
          out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            var query = Dbset.Include(c => c.ChiTietVatTu).Include(c => c.Kho).Include(c => c.Kho1).Include(c=>c.NhomSanPham).AsQueryable();
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.Title.Contains(textSearch));
            }

            totalPage = query.Count();

			if(!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
                query = query.OrderByDescending(c => c.CoPhanCapXuongId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
