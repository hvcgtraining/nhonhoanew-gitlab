﻿using System.Collections.Generic;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Core;
using EcommerceSystem.DataAccess.LinqExtensions;
using System.Data.Entity;
using EcommerceSystem.Models;
using System;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface IPKHKeHoachXuongRepository: IBaseRepository<PKHKeHoachXuong>
    {
        List<PKHKeHoachXuong> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        List<PKHKeHoachXuong> Search(int currentPage, int pageSize, string textSearch, DateTime? startDate, DateTime? endDate, int? xuongId, string sortColumn, string sortDirection, out int totalPage);
        List<PKHKeHoachXuong> GetAllXuongKeHoachSX(long khSXId, int xuongType);
    }
    public class PKHKeHoachXuongRepository : BaseRepository<PKHKeHoachXuong>, IPKHKeHoachXuongRepository
    {
        public PKHKeHoachXuongRepository(EcommerceSystemEntities context) : base(context)
        {
        }

        public List<PKHKeHoachXuong> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection,
          out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            var query = Dbset.AsQueryable();
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.Title.Contains(textSearch));
            }

            totalPage = query.Count();

			if(!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
                query = query.OrderByDescending(c => c.PKHKeHoachXuongId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }

        public List<PKHKeHoachXuong> Search(int currentPage, int pageSize, string textSearch, DateTime? startDate, DateTime? endDate, int? xuongId, string sortColumn, string sortDirection,
          out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            var query = Dbset.AsQueryable().Include(c=>c.PKHKeHoachSXNKXH);
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.Title.Contains(textSearch));
            }

            if (startDate.HasValue && endDate.HasValue)
            {
                query = query.Where(c => c.PKHKeHoachSXNKXH.StartDate >= startDate.Value && c.PKHKeHoachSXNKXH.EndDate <= endDate.Value);
            }
            else
            {
                if (startDate.HasValue)
                {
                    query = query.Where(c => c.PKHKeHoachSXNKXH.StartDate >= startDate.Value);
                }
                if (endDate.HasValue)
                {
                    query = query.Where(c => c.PKHKeHoachSXNKXH.EndDate <= endDate.Value);
                }
            }

            if(xuongId.HasValue)
            {
                query = query.Where(c => c.XuongId == xuongId.Value);
            }

            totalPage = query.Count();

            if (!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
                query = query.OrderByDescending(c => c.PKHKeHoachXuongId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }

        public List<PKHKeHoachXuong> GetAllXuongKeHoachSX(long khSXId, int xuongType)
        {
            var query = Dbset.AsQueryable().Include(c=>c.Xuong);
            var pKHKeHoachXuongEntities = query.Where(x => x.PKHKeHoachSXNKXHId == khSXId && x.Xuong != null && x.Xuong.Type.HasValue && x.Xuong.Type.Value == xuongType).Distinct().ToList();
            return pKHKeHoachXuongEntities;
        }
    }
}
