﻿using System.Linq;
using EcommerceSystem.DataAccess.Common;

namespace EcommerceSystem.DataAccess
{
    public interface IUserRoleRepository : IBaseRepository<UserRole>
    {
        UserRole RetrieveUserRole(int UserId);
    }
    public class UserRoleRepository : BaseRepository<UserRole>, IUserRoleRepository
    {
        public UserRoleRepository(EcommerceSystemEntities context)
            : base(context)
        {
        }

        public UserRole RetrieveUserRole(int UserId)
        {
            return Dbset.FirstOrDefault(x => x.UserId == UserId);
        }
       
    }
}
