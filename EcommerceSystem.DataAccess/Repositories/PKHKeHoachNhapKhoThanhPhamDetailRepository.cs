﻿using System.Collections.Generic;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Core;
using EcommerceSystem.DataAccess.LinqExtensions;
using System.Data.Entity;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface IPKHKeHoachNhapKhoThanhPhamDetailRepository: IBaseRepository<PKHKeHoachNhapKhoThanhPhamDetail>
    {
        List<PKHKeHoachNhapKhoThanhPhamDetail> Search(int currentPage, int pageSize, string textSearch, int keHoachXuongId, string sortColumn, string sortDirection, out int totalPage);
    }
    public class PKHKeHoachNhapKhoThanhPhamDetailRepository : BaseRepository<PKHKeHoachNhapKhoThanhPhamDetail>, IPKHKeHoachNhapKhoThanhPhamDetailRepository
    {
        public PKHKeHoachNhapKhoThanhPhamDetailRepository(EcommerceSystemEntities context) : base(context)
        {
        }

        public List<PKHKeHoachNhapKhoThanhPhamDetail> Search(int currentPage, int pageSize, string textSearch, int keHoachXuongId, string sortColumn, string sortDirection,
          out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            var query = Dbset.Include(c=>c.ChiTietVatTu).Include(c=>c.PKHKeHoachXuong.Xuong).AsQueryable();
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.Title.Contains(textSearch));
            }

            if(keHoachXuongId != 0)
            {
                query = query.Where(c => c.PKHKeHoachXuongId == keHoachXuongId);
            }
            totalPage = query.Count();

			if(!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
                query = query.OrderByDescending(c => c.PKHKeHoachNhapKhoThanhPhamDetailId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
