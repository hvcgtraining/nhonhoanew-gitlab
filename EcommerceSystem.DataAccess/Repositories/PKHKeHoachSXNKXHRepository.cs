﻿using System.Collections.Generic;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Core;
using EcommerceSystem.DataAccess.LinqExtensions;
using System;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface IPKHKeHoachSXNKXHRepository: IBaseRepository<PKHKeHoachSXNKXH>
    {
        List<PKHKeHoachSXNKXH> Search(int currentPage, int pageSize, string textSearch, DateTime? fromDate, DateTime? toDate, string sortColumn, string sortDirection, out int totalPage, bool? status = null);
    }
    public class PKHKeHoachSXNKXHRepository : BaseRepository<PKHKeHoachSXNKXH>, IPKHKeHoachSXNKXHRepository
    {
        public PKHKeHoachSXNKXHRepository(EcommerceSystemEntities context) : base(context)
        {
        }

        public List<PKHKeHoachSXNKXH> Search(int currentPage, int pageSize, string textSearch, DateTime? fromDate, DateTime? toDate, string sortColumn, string sortDirection,
          out int totalPage, bool? status = null)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            var query = Dbset.AsQueryable();
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.MaTuan.Contains(textSearch));
            }
            
            if (fromDate.HasValue && toDate.HasValue)
            {
                query = query.Where(c => c.StartDate >= fromDate.Value && c.EndDate <= toDate.Value);
            }
            else
            {
                if (fromDate.HasValue)
                {
                    query = query.Where(c => c.StartDate >= fromDate.Value);
                }
                if (toDate.HasValue)
                {
                    query = query.Where(c => c.EndDate <= toDate.Value);
                }
            }

            if(status.HasValue)
            {
                query = query.Where(s => s.Status == status.Value);
            }

            totalPage = query.Count();

			if(!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
                query = query.OrderByDescending(c => c.PKHKeHoachSXNKXHId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
