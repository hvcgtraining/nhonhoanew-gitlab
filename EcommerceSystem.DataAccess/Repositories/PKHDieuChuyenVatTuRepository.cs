﻿using System.Collections.Generic;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Core;
using EcommerceSystem.DataAccess.LinqExtensions;
using System.Data.Entity;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface IPKHDieuChuyenVatTuRepository: IBaseRepository<PKHDieuChuyenVatTu>
    {
        List<PKHDieuChuyenVatTu> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
    }
    public class PKHDieuChuyenVatTuRepository : BaseRepository<PKHDieuChuyenVatTu>, IPKHDieuChuyenVatTuRepository
    {
        public PKHDieuChuyenVatTuRepository(EcommerceSystemEntities context) : base(context)
        {
        }

        public List<PKHDieuChuyenVatTu> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection,
          out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            var query = Dbset.Include(p=>p.PKHKeHoachXuong).Include(p=>p.PKHKeHoachSXNKXH).Include(p=>p.LyDoXuat1).Include(p=>p.Kho).Include(p=>p.Kho1).Include(p=>p.Xuong).Include(p=>p.Xuong1).Include(p=>p.NguoiNhanKho).Include(p=>p.NguoiNhanKho1).AsQueryable();
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.Title.Contains(textSearch));
            }

            totalPage = query.Count();

			if(!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
                query = query.OrderByDescending(c => c.PKHDieuChuyenVatTuId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
