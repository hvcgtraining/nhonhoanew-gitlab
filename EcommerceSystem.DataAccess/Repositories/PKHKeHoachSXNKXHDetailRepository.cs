﻿using System.Collections.Generic;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Core;
using EcommerceSystem.DataAccess.LinqExtensions;
using System.Data.Entity;
using System;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface IPKHKeHoachSXNKXHDetailRepository : IBaseRepository<PKHKeHoachSXNKXHDetail>
    {
        List<PKHKeHoachSXNKXHDetail> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        List<PKHKeHoachSXNKXHDetail> GetAllByParentId(long keHoachId);
        void UpdateKHSXXuong(int parentVatTuId, long pKHKeHoachSXNKXHId, long pKHKeHoachSXNKXHDetailId, int tongNhapKho, int loaiXuong, string currentUser);
    }
    public class PKHKeHoachSXNKXHDetailRepository : BaseRepository<PKHKeHoachSXNKXHDetail>, IPKHKeHoachSXNKXHDetailRepository
    {
        public PKHKeHoachSXNKXHDetailRepository(EcommerceSystemEntities context) : base(context)
        {
        }

        public List<PKHKeHoachSXNKXHDetail> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection,
          out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            var query = Dbset.AsQueryable();
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.Title.Contains(textSearch));
            }

            totalPage = query.Count();

            if (!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
                query = query.OrderByDescending(c => c.PKHKeHoachSXNKXHDetailId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }

        public List<PKHKeHoachSXNKXHDetail> GetAllByParentId(long keHoachId)
        {
            var query = Dbset.AsQueryable();
            query = query.Where(p => p.PKHKeHoachSXNKXHId == keHoachId).Include(o => o.ChiTietVatTu);

            return query.ToList();
        }

        public void UpdateKHSXXuong(int parentVatTuId, long pKHKeHoachSXNKXHId, long pKHKeHoachSXNKXHDetailId, int tongNhapKho, int loaiXuong, string currentUser)
        {
            using (var transaction = new System.Transactions.TransactionScope())
            {
                using (var dbContext = new EcommerceSystemEntities())
                {
                    //update Xuong, Xuong detail
                    //1. Get dinh muc SX (VatTuId) --> DinhMucId --> DetailDinhMucIds --> ChiTietVatTuIds, Vatus --> ThanhPhamXuong, Xuong, CoPhanCapXuong(Kho)
                    try
                    {
                        var dmSXDetails = GetAllDinhMucDetailByParent(dbContext, parentVatTuId);
                        foreach(var dt in dmSXDetails)
                        {
                            CreateKHSXXuong(dbContext, dt, pKHKeHoachSXNKXHId, pKHKeHoachSXNKXHDetailId, tongNhapKho, currentUser);
                        }
                    }
                    catch (System.Exception ex)
                    {
                        throw ex;
                        //Log.Error("UpdateKHSXXuong repository error", ex);
                    }
                }
                transaction.Complete();
            }
        }

        private List<DanhSachChiTiet> GetAllDinhMucDetailByParent(EcommerceSystemEntities dbContext, int parentVatTuId)
        {
            List<DanhSachChiTiet> dmVTDetails = new List<DanhSachChiTiet>();
            //var dmSx = dbContext.DinhMucSanXuats.FirstOrDefault(d => d.ChiTietVatTuId == parentVatTuId);
            GetDinhMucByVatTu(dbContext, dmVTDetails, parentVatTuId);
            return dmVTDetails;
        }

        private void GetDinhMucByVatTu(EcommerceSystemEntities dbContext, List<DanhSachChiTiet> dmDetails, int vatTuId)
        {
            List<DanhSachChiTiet> details = new List<DanhSachChiTiet>();
            var dmSx = dbContext.DinhMucSanXuats.FirstOrDefault(d=>d.ChiTietVatTuId == vatTuId);
            if (dmSx == null)
                return;
            var dmSXDetails = GetDinhMucDetailsByDinhMucId(dbContext, dmSx.DinhMucSanXuatId);
            if(dmSXDetails == null || !dmSXDetails.Any())
                return;
            dmDetails.AddRange(dmSXDetails);

            foreach (var dt in dmSXDetails)
            {
                if(dt.ChiTietVatTuId.HasValue)
                {
                    GetDinhMucByVatTu(dbContext, dmDetails, dt.ChiTietVatTuId.Value);
                }
            }
        }

        private List<DanhSachChiTiet> GetDinhMucDetailsByDinhMucId(EcommerceSystemEntities dbContext, int dinhMucSanXuatId)
        {
            var dmsxDetails = dbContext.DanhSachChiTiets.Where(d => d.DinhMucSanXuatId == dinhMucSanXuatId).ToList();
            return dmsxDetails;
        }

        private bool CreateKHSXXuong(EcommerceSystemEntities dbContext, DanhSachChiTiet dinhMucChiTiet, long pKHKeHoachSXNKXHId, long pKHKeHoachSXNKXHDetailId, int tongNhapKho, string currentUser)
        {
            //Kiem tra CoPhanCapXuong --> KhoXuatId -> XuongId --> Tao ke hoach Xuong
            var coPhanXuong = dbContext.CoPhanCapXuongs.Include(c => c.Kho1).FirstOrDefault(t => t.VatTuId == dinhMucChiTiet.ChiTietVatTuId);
            if (coPhanXuong == null || coPhanXuong.Kho1 == null || !coPhanXuong.Kho1.XuongId.HasValue)
                return false;
            var khXuongChecked = dbContext.PKHKeHoachXuongs.FirstOrDefault(x => x.PKHKeHoachSXNKXHId == pKHKeHoachSXNKXHId && x.XuongId == coPhanXuong.Kho1.XuongId);
            if (khXuongChecked == null)
            {
                PKHKeHoachXuong khXuong = new PKHKeHoachXuong
                {
                    PKHKeHoachSXNKXHId = pKHKeHoachSXNKXHId,
                    XuongId = coPhanXuong.Kho1.XuongId,
                    TinhTrang = (int)Models.TrangThaiKeHoachXuong.DangCho,
                    Status = true,
                    CreatedDate = DateTime.Now,
                    CreatedBy = currentUser
                };
                var khXuongEntity = dbContext.PKHKeHoachXuongs.Add(khXuong);
                dbContext.SaveChanges();
                khXuong.PKHKeHoachXuongId = khXuongEntity.PKHKeHoachXuongId;
                khXuongChecked = khXuong;
            }

            //Kiem tra vat tu trong kehoachXuongDetail da ton tai chua, neu co thi cong them vao dinh muc, neu chua thi tao moi
            var khXuongDetailEntity = dbContext.PKHKeHoachXuongDetails.FirstOrDefault(d => d.PKHKeHoachXuongId == khXuongChecked.PKHKeHoachXuongId && d.KhoId == coPhanXuong.KhoNhapId && d.ChiTietVatTuId == dinhMucChiTiet.ChiTietVatTuId);

            if(khXuongDetailEntity == null)
            {
                PKHKeHoachXuongDetail khXuongDetail = new PKHKeHoachXuongDetail
                {
                    ChiTietVatTuId = dinhMucChiTiet.ChiTietVatTuId,
                    DinhMucSanXuatId = dinhMucChiTiet.DinhMucSanXuatId.Value,
                    KhoId = coPhanXuong.KhoNhapId,
                    PKHKeHoachSXNKXHDetailId = pKHKeHoachSXNKXHDetailId,
                    PKHKeHoachXuongId = khXuongChecked.PKHKeHoachXuongId,
                    CreatedDate = DateTime.Now,
                    CreatedBy = currentUser,
                    DinhMuc = (dinhMucChiTiet.SoLuong * tongNhapKho),
                    Status = true
                };
                dbContext.PKHKeHoachXuongDetails.Add(khXuongDetail);
            }
            else
            {
                khXuongDetailEntity.DinhMuc += (dinhMucChiTiet.SoLuong * tongNhapKho);
            }

            dbContext.SaveChanges();
            return false;
        }

        private void CreatePhieuDieuChuyenVT()
        {

        }
    }
}
