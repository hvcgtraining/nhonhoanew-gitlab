﻿using System.Collections.Generic;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Core;
using EcommerceSystem.DataAccess.LinqExtensions;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface IPKHDieuChuyenVatTuDetailRepository: IBaseRepository<PKHDieuChuyenVatTuDetail>
    {
        List<PKHDieuChuyenVatTuDetail> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
    }
    public class PKHDieuChuyenVatTuDetailRepository : BaseRepository<PKHDieuChuyenVatTuDetail>, IPKHDieuChuyenVatTuDetailRepository
    {
        public PKHDieuChuyenVatTuDetailRepository(EcommerceSystemEntities context) : base(context)
        {
        }

        public List<PKHDieuChuyenVatTuDetail> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection,
          out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            var query = Dbset.AsQueryable();
            if (!string.IsNullOrEmpty(textSearch))
            {
                query = query.Where(c => c.Title.Contains(textSearch));
            }

            totalPage = query.Count();

			if(!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
                query = query.OrderByDescending(c => c.PKHDieuChuyenVatTuDetailId);

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
