﻿using EcommerceSystem.DataAccess.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface IConfigurationRepository : IBaseRepository<Configuration>
    {

    }
    public class ConfigurationRepository : BaseRepository<Configuration>, IConfigurationRepository
    {
        public ConfigurationRepository(EcommerceSystemEntities context)
            : base(context)
        {
        }

    }
}
