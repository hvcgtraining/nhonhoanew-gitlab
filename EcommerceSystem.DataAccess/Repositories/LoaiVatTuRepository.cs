﻿using System.Collections.Generic;
using System.Linq;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.Core;
using EcommerceSystem.DataAccess.LinqExtensions;
using System;

namespace EcommerceSystem.DataAccess.Repositories
{
    public interface ILoaiVatTuRepository : IBaseRepository<LoaiVatTu>
    {
        List<LoaiVatTu> Search(int currentPage, int pageSize, string textSearch, int searchType, int cap, string sortColumn, string sortDirection, out int totalPage);
        //object Search(int currentPage, int pageSize, string textSearch, int searchType, int cap, string sortColumn, string sortDirection, out int totalPage);
    }
    public class LoaiVatTuRepository : BaseRepository<LoaiVatTu>, ILoaiVatTuRepository
    {
        public LoaiVatTuRepository(EcommerceSystemEntities context) : base(context)
        {
        }

        public List<LoaiVatTu> Search(int currentPage, int pageSize, string textSearch, int searchType, int cap, string sortColumn, string sortDirection,
          out int totalPage)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            pageSize = (pageSize <= 0) ? Constants.DefaultPageSize : pageSize;

            //query theo tên và mã Loại vật tư
            var query = Dbset.AsQueryable();
            if (!string.IsNullOrEmpty(textSearch))
            {
                switch (searchType)
                {
                    case 0:
                        query = query.Where(c => c.Title.Contains(textSearch) || c.MaLoaiVatTu.Contains(textSearch));
                        break;
                    case 1:
                        query = query.Where(c => c.Title.Contains(textSearch));
                        break;
                    case 2:
                        query = query.Where(c => c.MaLoaiVatTu.Contains(textSearch));
                        break;
                    default:
                        break;
                }
            }
            //Query theo cấp
            if (cap > 0)
            {
                switch (cap)
                {
                    case 1:
                        query = query.Where(c => c.Cap == 1);
                        break;
                    case 2:
                        query = query.Where(c => c.Cap == 2);
                        break;
                    case 3:
                        query = query.Where(c => c.Cap == 3);
                        break;
                    case 4:
                        query = query.Where(c => c.Cap == 4);
                        break;
                    default:
                        break;
                }
            }

            totalPage = query.Count();
            if (!string.IsNullOrEmpty(sortColumn))
            {
                query = query.OrderByField(sortColumn.Trim(), sortDirection);
            }
            else
            {
                query = query.OrderByDescending(c => c.LoaiVatTuId);
            }

            return query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
