//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EcommerceSystem.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class CoPhanCapXuong
    {
        public int CoPhanCapXuongId { get; set; }
        public string Title { get; set; }
        public int KhoNhapId { get; set; }
        public int KhoXuatId { get; set; }
        public Nullable<int> VatTuId { get; set; }
        public Nullable<int> NhomSanPhamId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool Status { get; set; }
    
        public virtual ChiTietVatTu ChiTietVatTu { get; set; }
        public virtual Kho Kho { get; set; }
        public virtual Kho Kho1 { get; set; }
        public virtual NhomSanPham NhomSanPham { get; set; }
    }
}
