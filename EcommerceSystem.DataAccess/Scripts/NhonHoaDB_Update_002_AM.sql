USE [EcommerceSystem_Dev]
GO
/****** Object:  Table [dbo].[ChiTietVatTu]    Script Date: 2019-07-04 4:37:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietVatTu](
	[ChiTietVatTuId] [int] IDENTITY(1,1) NOT NULL,
	[MaChiTietVatTu] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[DonVi] [int] NULL,
	[GhiChu] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_ChiTietVatTu] PRIMARY KEY CLUSTERED 
(
	[ChiTietVatTuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DanhSachChiTiet]    Script Date: 2019-07-04 4:37:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DanhSachChiTiet](
	[DanhSachChiTietId] [int] IDENTITY(1,1) NOT NULL,
	[ChiTietVatTuId] [int] NOT NULL,
	[SoLuongCoPhanId] [int] NULL,
	[SoLuong] [int] NULL,
	[GhiChu] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DanhSachChiTiet] PRIMARY KEY CLUSTERED 
(
	[DanhSachChiTietId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dechet]    Script Date: 2019-07-04 4:37:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dechet](
	[DechetId] [int] IDENTITY(1,1) NOT NULL,
	[MaCoPhan] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[GhiChu] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_Dechet] PRIMARY KEY CLUSTERED 
(
	[DechetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DinhMucCatTole]    Script Date: 2019-07-04 4:37:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DinhMucCatTole](
	[DinhMucCatToleId] [int] IDENTITY(1,1) NOT NULL,
	[SoChungTu] [nvarchar](50) NOT NULL,
	[Day] [float] NULL,
	[PhuongAn] [int] NULL,
	[ChiTietVatTuId] [int] NULL,
	[DanhSachChiTietId] [int] NULL,
	[ToleId] [int] NULL,
	[GhiChu] [nvarchar](250) NULL,
	[Loai] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DinhMucCatTole] PRIMARY KEY CLUSTERED 
(
	[DinhMucCatToleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DinhMucSanXuat]    Script Date: 2019-07-04 4:37:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DinhMucSanXuat](
	[DinhMucSanXuatId] [int] IDENTITY(1,1) NOT NULL,
	[MaDinhMucSanXuat] [nvarchar](50) NOT NULL,
	[SoChungTu] [nvarchar](50) NULL,
	[MaThanhPham] [nvarchar](50) NULL,
	[TenThanhPham] [nvarchar](250) NULL,
	[MaCapPhat] [nvarchar](50) NULL,
	[Loai] [nvarchar](50) NULL,
	[DienGiai] [nvarchar](250) NULL,
	[GhiChu] [nvarchar](250) NULL,
	[KhoQuyetToan] [int] NULL,
	[DanhSachChiTietId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DinhMucSanXuat] PRIMARY KEY CLUSTERED 
(
	[DinhMucSanXuatId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DinhMucTuan]    Script Date: 2019-07-04 4:37:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DinhMucTuan](
	[DinhMucTuanId] [int] IDENTITY(1,1) NOT NULL,
	[LoaiVatTuId] [int] NOT NULL,
	[DinhMuc] [float] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DinhMucTuan] PRIMARY KEY CLUSTERED 
(
	[DinhMucTuanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Kho]    Script Date: 2019-07-04 4:37:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kho](
	[KhoId] [int] IDENTITY(1,1) NOT NULL,
	[MaKho] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[NguoiGiao] [nvarchar](50) NULL,
	[ThongTinKho] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_Kho] PRIMARY KEY CLUSTERED 
(
	[KhoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KhoiLuongCoPhan]    Script Date: 2019-07-04 4:37:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhoiLuongCoPhan](
	[KhoiLuongCoPhanId] [int] IDENTITY(1,1) NOT NULL,
	[MaKhoiLuongCoPhan] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoiLuong] [float] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_KhoiLuongCoPhan] PRIMARY KEY CLUSTERED 
(
	[KhoiLuongCoPhanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoaiVatTu]    Script Date: 2019-07-04 4:37:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiVatTu](
	[LoaiVatTuId] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiVatTu] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[Cap] [int] NULL,
	[GhiChu] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_LoaiVatTu] PRIMARY KEY CLUSTERED 
(
	[LoaiVatTuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhuLucISO]    Script Date: 2019-07-04 4:37:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhuLucISO](
	[PhuLucIsoId] [int] IDENTITY(1,1) NOT NULL,
	[MaPhuLucIso] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[SoPhuLuc] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PhuLucISO] PRIMARY KEY CLUSTERED 
(
	[PhuLucIsoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuyDoiDinhMuc]    Script Date: 2019-07-04 4:37:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuyDoiDinhMuc](
	[QuyDoiDinhMucId] [int] IDENTITY(1,1) NOT NULL,
	[MaQuyDoiDinhMuc] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[DinhMuc] [float] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_QuyDoiDinhMuc] PRIMARY KEY CLUSTERED 
(
	[QuyDoiDinhMucId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SoDuBanDau]    Script Date: 2019-07-04 4:37:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SoDuBanDau](
	[SuDuBanDauId] [int] IDENTITY(1,1) NOT NULL,
	[KhoId] [int] NOT NULL,
	[LoaiVatTuId] [int] NULL,
	[SoDuDauKy] [float] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_SoDuBanDau] PRIMARY KEY CLUSTERED 
(
	[SuDuBanDauId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ThanhPhamXuong]    Script Date: 2019-07-04 4:37:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThanhPhamXuong](
	[ThanhPhamXuongId] [int] IDENTITY(1,1) NOT NULL,
	[XuongId] [int] NOT NULL,
	[LoaiVatTuId] [int] NOT NULL,
	[NhomSanPham] [nvarchar](50) NULL,
	[Loai] [nvarchar](50) NULL,
	[NangSuatTuan] [nvarchar](50) NULL,
	[LoaiVatLieu] [nvarchar](50) NULL,
	[QuyCach] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_ThanhPhamXuong] PRIMARY KEY CLUSTERED 
(
	[ThanhPhamXuongId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tole]    Script Date: 2019-07-04 4:37:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tole](
	[ToleId] [int] IDENTITY(1,1) NOT NULL,
	[MaTole] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[DonVi] [int] NULL,
	[Day] [float] NULL,
	[Ngang] [float] NULL,
	[Dung] [float] NULL,
	[KhoiLuong] [float] NULL,
	[DechetId] [int] NULL,
	[GhiChu] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_Tole] PRIMARY KEY CLUSTERED 
(
	[ToleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TrongLuongVatLieu]    Script Date: 2019-07-04 4:37:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrongLuongVatLieu](
	[TrongLuongVatLieuId] [int] IDENTITY(1,1) NOT NULL,
	[MaNguyenVatLieu] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KichThuoc] [nvarchar](50) NULL,
	[KhoiLuong] [float] NULL,
	[GhiChu] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_TrongLuongVatLieu] PRIMARY KEY CLUSTERED 
(
	[TrongLuongVatLieuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DanhSachChiTiet]  WITH CHECK ADD  CONSTRAINT [FK_DanhSachChiTiet_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[DanhSachChiTiet] CHECK CONSTRAINT [FK_DanhSachChiTiet_ChiTietVatTu]
GO
ALTER TABLE [dbo].[DanhSachChiTiet]  WITH CHECK ADD  CONSTRAINT [FK_DanhSachChiTiet_KhoiLuongCoPhan] FOREIGN KEY([SoLuongCoPhanId])
REFERENCES [dbo].[KhoiLuongCoPhan] ([KhoiLuongCoPhanId])
GO
ALTER TABLE [dbo].[DanhSachChiTiet] CHECK CONSTRAINT [FK_DanhSachChiTiet_KhoiLuongCoPhan]
GO
ALTER TABLE [dbo].[DinhMucCatTole]  WITH CHECK ADD  CONSTRAINT [FK_DinhMucCatTole_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[DinhMucCatTole] CHECK CONSTRAINT [FK_DinhMucCatTole_ChiTietVatTu]
GO
ALTER TABLE [dbo].[DinhMucCatTole]  WITH CHECK ADD  CONSTRAINT [FK_DinhMucCatTole_DanhSachChiTiet] FOREIGN KEY([DanhSachChiTietId])
REFERENCES [dbo].[DanhSachChiTiet] ([DanhSachChiTietId])
GO
ALTER TABLE [dbo].[DinhMucCatTole] CHECK CONSTRAINT [FK_DinhMucCatTole_DanhSachChiTiet]
GO
ALTER TABLE [dbo].[DinhMucCatTole]  WITH CHECK ADD  CONSTRAINT [FK_DinhMucCatTole_Tole] FOREIGN KEY([ToleId])
REFERENCES [dbo].[Tole] ([ToleId])
GO
ALTER TABLE [dbo].[DinhMucCatTole] CHECK CONSTRAINT [FK_DinhMucCatTole_Tole]
GO
ALTER TABLE [dbo].[DinhMucSanXuat]  WITH CHECK ADD  CONSTRAINT [FK_DinhMucSanXuat_DanhSachChiTiet] FOREIGN KEY([DanhSachChiTietId])
REFERENCES [dbo].[DanhSachChiTiet] ([DanhSachChiTietId])
GO
ALTER TABLE [dbo].[DinhMucSanXuat] CHECK CONSTRAINT [FK_DinhMucSanXuat_DanhSachChiTiet]
GO
ALTER TABLE [dbo].[DinhMucSanXuat]  WITH CHECK ADD  CONSTRAINT [FK_DinhMucSanXuat_Kho] FOREIGN KEY([KhoQuyetToan])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[DinhMucSanXuat] CHECK CONSTRAINT [FK_DinhMucSanXuat_Kho]
GO
ALTER TABLE [dbo].[DinhMucTuan]  WITH CHECK ADD  CONSTRAINT [FK_DinhMucTuan_LoaiVatTu] FOREIGN KEY([LoaiVatTuId])
REFERENCES [dbo].[LoaiVatTu] ([LoaiVatTuId])
GO
ALTER TABLE [dbo].[DinhMucTuan] CHECK CONSTRAINT [FK_DinhMucTuan_LoaiVatTu]
GO
ALTER TABLE [dbo].[SoDuBanDau]  WITH CHECK ADD  CONSTRAINT [FK_SoDuBanDau_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[SoDuBanDau] CHECK CONSTRAINT [FK_SoDuBanDau_Kho]
GO
ALTER TABLE [dbo].[SoDuBanDau]  WITH CHECK ADD  CONSTRAINT [FK_SoDuBanDau_LoaiVatTu] FOREIGN KEY([LoaiVatTuId])
REFERENCES [dbo].[LoaiVatTu] ([LoaiVatTuId])
GO
ALTER TABLE [dbo].[SoDuBanDau] CHECK CONSTRAINT [FK_SoDuBanDau_LoaiVatTu]
GO
ALTER TABLE [dbo].[ThanhPhamXuong]  WITH CHECK ADD  CONSTRAINT [FK_ThanhPhamXuong_LoaiVatTu] FOREIGN KEY([LoaiVatTuId])
REFERENCES [dbo].[LoaiVatTu] ([LoaiVatTuId])
GO
ALTER TABLE [dbo].[ThanhPhamXuong] CHECK CONSTRAINT [FK_ThanhPhamXuong_LoaiVatTu]
GO
ALTER TABLE [dbo].[Tole]  WITH CHECK ADD  CONSTRAINT [FK_Tole_Dechet] FOREIGN KEY([DechetId])
REFERENCES [dbo].[Dechet] ([DechetId])
GO
ALTER TABLE [dbo].[Tole] CHECK CONSTRAINT [FK_Tole_Dechet]
GO
