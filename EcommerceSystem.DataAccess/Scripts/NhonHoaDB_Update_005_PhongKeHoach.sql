USE [EcommerceSystem_Dev]
GO
/****** Object:  Table [dbo].[PKHDuKienGiaoHang]    Script Date: 7/23/2019 8:48:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHDuKienGiaoHang](
	[PKHDuKienGiaoHangId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHPhieuYeuCauMuaHangId] [bigint] NOT NULL,
	[PKHPhieuYeuCauMuaHangDetailId] [bigint] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[NgayDuKien] [datetime] NULL,
	[SoLuongGiaoDuKien] [int] NULL,
	[TongSoLuong] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHDuKienGiaoHang] PRIMARY KEY CLUSTERED 
(
	[PKHDuKienGiaoHangId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachNhapKhoThanhPham]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachNhapKhoThanhPham](
	[PKHKeHoachNhapKhoThanhPhamId] [int] IDENTITY(1,1) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[XuongId] [int] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[TrangThai] [tinyint] NOT NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachNhapKhoThanhPham] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachNhapKhoThanhPhamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachNhapKhoThanhPhamDetail]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachNhapKhoThanhPhamDetail](
	[PKHKeHoachNhapKhoThanhPhamDetailId] [int] IDENTITY(1,1) NOT NULL,
	[PKHKeHoachNhapKhoThanhPhamId] [int] NOT NULL,
	[LoaiVatTuId] [int] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[ThuHai] [int] NULL,
	[ThuBa] [int] NULL,
	[ThuTu] [int] NULL,
	[ThuNam] [int] NULL,
	[ThuSau] [int] NULL,
	[ThuBay] [int] NULL,
	[ChuNhat] [int] NULL,
	[TongNhapKho] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachNhapKhoThanhPhamDetail] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachNhapKhoThanhPhamDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachSX]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachSX](
	[PKHKeHoachSXId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHKeHoachSXNKXHId] [bigint] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[NgayTonKho] [datetime] NULL,
	[SoLuongTon] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[SoLuong] [int] NULL,
	[TrangThai] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachSX] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachSXId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachSXDetail]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachSXDetail](
	[PKHKeHoachSXDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHKeHoachSXId] [bigint] NOT NULL,
	[LoaiVatTuId] [int] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[SoLuongCan] [int] NULL,
	[SoLuongBaoBi] [int] NULL,
	[XuongId] [int] NOT NULL,
	[DieuChinhSoLuong] [int] NULL,
	[DieuChinhBaoBi] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachSXDetail] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachSXDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachSXNKXH]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachSXNKXH](
	[PKHKeHoachSXNKXHId] [bigint] IDENTITY(1,1) NOT NULL,
	[MaTuan] [nvarchar](10) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[DuKienXuatKho] [smallint] NOT NULL,
	[NgayTonKho] [datetime] NOT NULL,
	[GhiChu] [nvarchar](500) NULL,
	[TrangThai] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachSXNKXH] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachSXNKXHId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachSXNKXHDetail]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachSXNKXHDetail](
	[PKHKeHoachSXNKXHDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHKeHoachSXNKXHId] [bigint] NOT NULL,
	[LoaiVatTuId] [int] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[SoLuongCan] [int] NULL,
	[TongSoCan] [int] NULL,
	[TonKhoId] [int] NULL,
	[LuongTonKho] [int] NULL,
	[LuongNhapKho] [int] NULL,
	[LuongXuatHaNoi] [int] NULL,
	[LuongXuatKho] [int] NULL,
	[DaiLy] [int] NULL,
	[TongTonKho] [int] NULL,
	[NhapXuong4] [int] NULL,
	[NhapXuong5] [int] NULL,
	[NhapXuong6] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachSXNKXHDetail] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachSXNKXHDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachSXNKXHXuongDetail]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachSXNKXHXuongDetail](
	[PKHKeHoachSXNKXHXuongDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHKeHoachSXNKXHId] [bigint] NOT NULL,
	[PKHKeHoachSXNKXHDetailId] [bigint] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[XuongId] [int] NULL,
	[SoLuong] [int] NULL,
	[TongSoCan] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[GhiChuChung] [nvarchar](500) NULL,
	[OTFlag] [bit] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachSXNKXHXuongDetail] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachSXNKXHXuongDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachSXTuan]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachSXTuan](
	[PKHKeHoachSXTuanId] [bigint] IDENTITY(1,1) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[XuongId] [int] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[TrangThai] [tinyint] NOT NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachSXTuan] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachSXTuanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachSXTuanDetail]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachSXTuanDetail](
	[PKHKeHoachSXTuanDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHKeHoachSXTuanId] [bigint] NOT NULL,
	[ChiTietVatTuId] [int] NOT NULL,
	[SoLuongSanXuat] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[ThuHai] [int] NULL,
	[ThuBa] [int] NULL,
	[ThuTu] [int] NULL,
	[ThuNam] [int] NULL,
	[ThuSau] [int] NULL,
	[ThuBay] [int] NULL,
	[ChuNhat] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachSXTuanDetail] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachSXTuanDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachXCT]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachXCT](
	[PKHKeHoachXCTId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHKeHoachXLRId] [bigint] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[TrangThai] [tinyint] NOT NULL,
	[NgayTonKho] [datetime] NOT NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachXCT] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachXCTId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachXCTDetail]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachXCTDetail](
	[PKHKeHoachXCTDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHKeHoachXCTId] [bigint] NOT NULL,
	[XuongId] [int] NOT NULL,
	[ChiTietVatTuId] [int] NOT NULL,
	[KhoId] [int] NOT NULL,
	[DinhMucSanXuatId] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[SoLuongKeHoach] [int] NULL,
	[TyLe] [float] NULL,
	[SoLuongYeuCau] [int] NULL,
	[SoLuongChenhLech] [int] NULL,
	[DoUuTien] [tinyint] NULL,
	[SoLuongTon] [int] NULL,
	[ChiTietTon] [nvarchar](1000) NULL,
	[TonDuKien] [int] NULL,
	[DauKy] [datetime] NULL,
	[NgayCapHang] [datetime] NULL,
	[LoaiVatTuId] [int] NULL,
	[SoLuongCan] [int] NULL,
	[SoLuongBaoBi] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachXCTDetail] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachXCTDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachXLR]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachXLR](
	[PKHKeHoachXLRId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHKeHoachSXId] [bigint] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoId] [int] NOT NULL,
	[NgayTonKho] [datetime] NOT NULL,
	[TonKhoId] [int] NOT NULL,
	[SoLuong] [int] NULL,
	[TrangThai] [tinyint] NOT NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachXLR] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachXLRId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachXLRDetail]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachXLRDetail](
	[PKHKeHoachXLRDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHKeHoachXLRId] [bigint] NOT NULL,
	[ChiTietVatTuId] [int] NOT NULL,
	[XuongId] [int] NOT NULL,
	[KhoId] [int] NOT NULL,
	[DinhMucSanXuatId] [int] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[ThoiLuong] [float] NULL,
	[LuongYeuCau] [int] NOT NULL,
	[ChenhLech] [int] NULL,
	[DoUuTien] [tinyint] NULL,
	[TonKhoId] [int] NOT NULL,
	[LuongTonKho] [int] NULL,
	[ChiTietTon] [nvarchar](500) NULL,
	[LuongTonKhoDuKien] [int] NULL,
	[DauKy] [datetime] NOT NULL,
	[NgayCapHang] [datetime] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachXLRDetail] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachXLRDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKiemHoa]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKiemHoa](
	[PKHKiemHoaId] [bigint] IDENTITY(1,1) NOT NULL,
	[LoaiKiemHoa] [tinyint] NOT NULL,
	[SoKiemHoa] [nvarchar](50) NOT NULL,
	[NgayLapPhieu] [datetime] NULL,
	[KhoId] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[MaKiemHoa] [nvarchar](20) NULL,
	[PhieuNhap] [nvarchar](50) NULL,
	[SoHD] [int] NULL,
	[TrangThai] [tinyint] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKiemHoa] PRIMARY KEY CLUSTERED 
(
	[PKHKiemHoaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKiemHoaDetail]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKiemHoaDetail](
	[PKHKiemHoaDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHKiemHoaId] [bigint] NOT NULL,
	[MaPYC] [int] NULL,
	[CustomerId] [int] NULL,
	[ChiTietVatTuId] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[SoLuong] [int] NULL,
	[SoLuongDat] [int] NULL,
	[SoLuongKhongDat] [int] NULL,
	[CheckFlag] [bit] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKiemHoaDetail] PRIMARY KEY CLUSTERED 
(
	[PKHKiemHoaDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHPhieuNhapVatTu]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHPhieuNhapVatTu](
	[PKHPhieuNhapVatTuId] [bigint] IDENTITY(1,1) NOT NULL,
	[SoChungTu] [nvarchar](20) NULL,
	[NgayLapPhieu] [datetime] NOT NULL,
	[KhoId] [int] NOT NULL,
	[PKHKiemHoaId] [bigint] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[MaKH] [nvarchar](20) NOT NULL,
	[SoKiemHoa] [nvarchar](20) NULL,
	[SoDC] [nvarchar](10) NULL,
	[TrangThai] [tinyint] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHPhieuNhapVatTu] PRIMARY KEY CLUSTERED 
(
	[PKHPhieuNhapVatTuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHPhieuNhapVatTuDetail]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHPhieuNhapVatTuDetail](
	[PKHPhieuNhapVatTuDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHPhieuNhapVatTuId] [bigint] NOT NULL,
	[KhoId] [int] NULL,
	[CustomerId] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[ChiTietVatTuId] [int] NULL,
	[SoLuong] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHPhieuNhapVatTuDetail] PRIMARY KEY CLUSTERED 
(
	[PKHPhieuNhapVatTuDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHPhieuYeuCauMuaHang]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHPhieuYeuCauMuaHang](
	[PKHPhieuYeuCauMuaHangId] [bigint] IDENTITY(1,1) NOT NULL,
	[MaNhom] [nvarchar](20) NULL,
	[SoPR] [nvarchar](50) NULL,
	[Title] [nvarchar](250) NULL,
	[NgayYeuCau] [datetime] NOT NULL,
	[NgayDuKien] [datetime] NOT NULL,
	[NgayHoanTat] [datetime] NULL,
	[NoiDung] [nvarchar](200) NULL,
	[TrangThai] [tinyint] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHPhieuYeuCauMuaHang] PRIMARY KEY CLUSTERED 
(
	[PKHPhieuYeuCauMuaHangId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHPhieuYeuCauMuaHangDetail]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHPhieuYeuCauMuaHangDetail](
	[PKHPhieuYeuCauMuaHangDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHPhieuYeuCauMuaHangId] [bigint] NOT NULL,
	[YeuCau] [nvarchar](200) NULL,
	[DiaChi] [nvarchar](200) NULL,
	[MucDich] [nvarchar](200) NULL,
	[ChiTietVatTuId] [int] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[SoLuong] [int] NULL,
	[DonGiaThamKhao] [int] NULL,
	[QuyCach] [nvarchar](100) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[TongSoLuong] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHPhieuYeuCauMuaHangDetail] PRIMARY KEY CLUSTERED 
(
	[PKHPhieuYeuCauMuaHangDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHTheoDoiGiaoHang]    Script Date: 7/23/2019 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHTheoDoiGiaoHang](
	[PKHTheoDoiGiaoHangId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHPhieuYeuCauMuaHangId] [bigint] NOT NULL,
	[PKHPhieuYeuCauMuaHangDetailId] [bigint] NOT NULL,
	[PKHDuKienGiaoHangId] [bigint] NOT NULL,
	[PKHKiemHoaId] [bigint] NOT NULL,
	[NgayGiaoHang] [datetime] NULL,
	[ChiTietVatTuId] [int] NULL,
	[CustomerId] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[SoLuongGiaoDuKien] [int] NULL,
	[SoLuongGiaoThucTe] [int] NULL,
	[SoLuongConThieu] [int] NULL,
	[KiemHoaFlag] [bit] NOT NULL,
	[SoLuongDat] [int] NULL,
	[SoLuongKhongDat] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHTheoDoiGiaoHang] PRIMARY KEY CLUSTERED 
(
	[PKHTheoDoiGiaoHangId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[PKHDuKienGiaoHang]  WITH CHECK ADD  CONSTRAINT [FK_PKHDuKienGiaoHang_PKHPhieuYeuCauMuaHang] FOREIGN KEY([PKHPhieuYeuCauMuaHangId])
REFERENCES [dbo].[PKHPhieuYeuCauMuaHang] ([PKHPhieuYeuCauMuaHangId])
GO
ALTER TABLE [dbo].[PKHDuKienGiaoHang] CHECK CONSTRAINT [FK_PKHDuKienGiaoHang_PKHPhieuYeuCauMuaHang]
GO
ALTER TABLE [dbo].[PKHKeHoachNhapKhoThanhPhamDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachNhapKhoThanhPhamDetail_LoaiVatTu] FOREIGN KEY([LoaiVatTuId])
REFERENCES [dbo].[LoaiVatTu] ([LoaiVatTuId])
GO
ALTER TABLE [dbo].[PKHKeHoachNhapKhoThanhPhamDetail] CHECK CONSTRAINT [FK_PKHKeHoachNhapKhoThanhPhamDetail_LoaiVatTu]
GO
ALTER TABLE [dbo].[PKHKeHoachNhapKhoThanhPhamDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachNhapKhoThanhPhamDetail_PKHKeHoachNhapKhoThanhPham] FOREIGN KEY([PKHKeHoachNhapKhoThanhPhamId])
REFERENCES [dbo].[PKHKeHoachNhapKhoThanhPham] ([PKHKeHoachNhapKhoThanhPhamId])
GO
ALTER TABLE [dbo].[PKHKeHoachNhapKhoThanhPhamDetail] CHECK CONSTRAINT [FK_PKHKeHoachNhapKhoThanhPhamDetail_PKHKeHoachNhapKhoThanhPham]
GO
ALTER TABLE [dbo].[PKHKeHoachSX]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachSX_PKHKeHoachSXNKXH] FOREIGN KEY([PKHKeHoachSXNKXHId])
REFERENCES [dbo].[PKHKeHoachSXNKXH] ([PKHKeHoachSXNKXHId])
GO
ALTER TABLE [dbo].[PKHKeHoachSX] CHECK CONSTRAINT [FK_PKHKeHoachSX_PKHKeHoachSXNKXH]
GO
ALTER TABLE [dbo].[PKHKeHoachSXDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachSXDetail_LoaiVatTu] FOREIGN KEY([LoaiVatTuId])
REFERENCES [dbo].[LoaiVatTu] ([LoaiVatTuId])
GO
ALTER TABLE [dbo].[PKHKeHoachSXDetail] CHECK CONSTRAINT [FK_PKHKeHoachSXDetail_LoaiVatTu]
GO
ALTER TABLE [dbo].[PKHKeHoachSXDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachSXDetail_PKHKeHoachSX] FOREIGN KEY([PKHKeHoachSXId])
REFERENCES [dbo].[PKHKeHoachSX] ([PKHKeHoachSXId])
GO
ALTER TABLE [dbo].[PKHKeHoachSXDetail] CHECK CONSTRAINT [FK_PKHKeHoachSXDetail_PKHKeHoachSX]
GO
ALTER TABLE [dbo].[PKHKeHoachSXNKXHDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachSXNKXHDetail_LoaiVatTu] FOREIGN KEY([LoaiVatTuId])
REFERENCES [dbo].[LoaiVatTu] ([LoaiVatTuId])
GO
ALTER TABLE [dbo].[PKHKeHoachSXNKXHDetail] CHECK CONSTRAINT [FK_PKHKeHoachSXNKXHDetail_LoaiVatTu]
GO
ALTER TABLE [dbo].[PKHKeHoachSXNKXHDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachSXNKXHDetail_PKHKeHoachSXNKXH] FOREIGN KEY([PKHKeHoachSXNKXHId])
REFERENCES [dbo].[PKHKeHoachSXNKXH] ([PKHKeHoachSXNKXHId])
GO
ALTER TABLE [dbo].[PKHKeHoachSXNKXHDetail] CHECK CONSTRAINT [FK_PKHKeHoachSXNKXHDetail_PKHKeHoachSXNKXH]
GO
ALTER TABLE [dbo].[PKHKeHoachSXNKXHXuongDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachSXNKXHXuongDetail_PKHKeHoachSXNKXH] FOREIGN KEY([PKHKeHoachSXNKXHId])
REFERENCES [dbo].[PKHKeHoachSXNKXH] ([PKHKeHoachSXNKXHId])
GO
ALTER TABLE [dbo].[PKHKeHoachSXNKXHXuongDetail] CHECK CONSTRAINT [FK_PKHKeHoachSXNKXHXuongDetail_PKHKeHoachSXNKXH]
GO
ALTER TABLE [dbo].[PKHKeHoachSXNKXHXuongDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachSXNKXHXuongDetail_PKHKeHoachSXNKXHDetail] FOREIGN KEY([PKHKeHoachSXNKXHDetailId])
REFERENCES [dbo].[PKHKeHoachSXNKXHDetail] ([PKHKeHoachSXNKXHDetailId])
GO
ALTER TABLE [dbo].[PKHKeHoachSXNKXHXuongDetail] CHECK CONSTRAINT [FK_PKHKeHoachSXNKXHXuongDetail_PKHKeHoachSXNKXHDetail]
GO
ALTER TABLE [dbo].[PKHKeHoachSXTuanDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachSXTuanDetail_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[PKHKeHoachSXTuanDetail] CHECK CONSTRAINT [FK_PKHKeHoachSXTuanDetail_ChiTietVatTu]
GO
ALTER TABLE [dbo].[PKHKeHoachSXTuanDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachSXTuanDetail_PKHKeHoachSXTuan] FOREIGN KEY([PKHKeHoachSXTuanId])
REFERENCES [dbo].[PKHKeHoachSXTuan] ([PKHKeHoachSXTuanId])
GO
ALTER TABLE [dbo].[PKHKeHoachSXTuanDetail] CHECK CONSTRAINT [FK_PKHKeHoachSXTuanDetail_PKHKeHoachSXTuan]
GO
ALTER TABLE [dbo].[PKHKeHoachXCT]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXCT_PKHKeHoachXLR] FOREIGN KEY([PKHKeHoachXLRId])
REFERENCES [dbo].[PKHKeHoachXLR] ([PKHKeHoachXLRId])
GO
ALTER TABLE [dbo].[PKHKeHoachXCT] CHECK CONSTRAINT [FK_PKHKeHoachXCT_PKHKeHoachXLR]
GO
ALTER TABLE [dbo].[PKHKeHoachXCTDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXCTDetail_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[PKHKeHoachXCTDetail] CHECK CONSTRAINT [FK_PKHKeHoachXCTDetail_ChiTietVatTu]
GO
ALTER TABLE [dbo].[PKHKeHoachXCTDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXCTDetail_DinhMucSanXuat] FOREIGN KEY([DinhMucSanXuatId])
REFERENCES [dbo].[DinhMucSanXuat] ([DinhMucSanXuatId])
GO
ALTER TABLE [dbo].[PKHKeHoachXCTDetail] CHECK CONSTRAINT [FK_PKHKeHoachXCTDetail_DinhMucSanXuat]
GO
ALTER TABLE [dbo].[PKHKeHoachXCTDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXCTDetail_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[PKHKeHoachXCTDetail] CHECK CONSTRAINT [FK_PKHKeHoachXCTDetail_Kho]
GO
ALTER TABLE [dbo].[PKHKeHoachXCTDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXCTDetail_LoaiVatTu] FOREIGN KEY([LoaiVatTuId])
REFERENCES [dbo].[LoaiVatTu] ([LoaiVatTuId])
GO
ALTER TABLE [dbo].[PKHKeHoachXCTDetail] CHECK CONSTRAINT [FK_PKHKeHoachXCTDetail_LoaiVatTu]
GO
ALTER TABLE [dbo].[PKHKeHoachXLR]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXLR_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[PKHKeHoachXLR] CHECK CONSTRAINT [FK_PKHKeHoachXLR_Kho]
GO
ALTER TABLE [dbo].[PKHKeHoachXLR]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXLR_PKHKeHoachSX] FOREIGN KEY([PKHKeHoachSXId])
REFERENCES [dbo].[PKHKeHoachSX] ([PKHKeHoachSXId])
GO
ALTER TABLE [dbo].[PKHKeHoachXLR] CHECK CONSTRAINT [FK_PKHKeHoachXLR_PKHKeHoachSX]
GO
ALTER TABLE [dbo].[PKHKeHoachXLRDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXLRDetail_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[PKHKeHoachXLRDetail] CHECK CONSTRAINT [FK_PKHKeHoachXLRDetail_ChiTietVatTu]
GO
ALTER TABLE [dbo].[PKHKeHoachXLRDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXLRDetail_DinhMucSanXuat] FOREIGN KEY([DinhMucSanXuatId])
REFERENCES [dbo].[DinhMucSanXuat] ([DinhMucSanXuatId])
GO
ALTER TABLE [dbo].[PKHKeHoachXLRDetail] CHECK CONSTRAINT [FK_PKHKeHoachXLRDetail_DinhMucSanXuat]
GO
ALTER TABLE [dbo].[PKHKeHoachXLRDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXLRDetail_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[PKHKeHoachXLRDetail] CHECK CONSTRAINT [FK_PKHKeHoachXLRDetail_Kho]
GO
ALTER TABLE [dbo].[PKHKeHoachXLRDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXLRDetail_PKHKeHoachXLR1] FOREIGN KEY([PKHKeHoachXLRId])
REFERENCES [dbo].[PKHKeHoachXLR] ([PKHKeHoachXLRId])
GO
ALTER TABLE [dbo].[PKHKeHoachXLRDetail] CHECK CONSTRAINT [FK_PKHKeHoachXLRDetail_PKHKeHoachXLR1]
GO
ALTER TABLE [dbo].[PKHKiemHoa]  WITH CHECK ADD  CONSTRAINT [FK_PKHKiemHoa_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[PKHKiemHoa] CHECK CONSTRAINT [FK_PKHKiemHoa_Kho]
GO
ALTER TABLE [dbo].[PKHKiemHoaDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKiemHoaDetail_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[PKHKiemHoaDetail] CHECK CONSTRAINT [FK_PKHKiemHoaDetail_ChiTietVatTu]
GO
ALTER TABLE [dbo].[PKHKiemHoaDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKiemHoaDetail_PKHKiemHoa] FOREIGN KEY([PKHKiemHoaId])
REFERENCES [dbo].[PKHKiemHoa] ([PKHKiemHoaId])
GO
ALTER TABLE [dbo].[PKHKiemHoaDetail] CHECK CONSTRAINT [FK_PKHKiemHoaDetail_PKHKiemHoa]
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTu]  WITH CHECK ADD  CONSTRAINT [FK_PKHPhieuNhapVatTu_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTu] CHECK CONSTRAINT [FK_PKHPhieuNhapVatTu_Kho]
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTu]  WITH CHECK ADD  CONSTRAINT [FK_PKHPhieuNhapVatTu_PKHKiemHoa] FOREIGN KEY([PKHKiemHoaId])
REFERENCES [dbo].[PKHKiemHoa] ([PKHKiemHoaId])
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTu] CHECK CONSTRAINT [FK_PKHPhieuNhapVatTu_PKHKiemHoa]
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTuDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHPhieuNhapVatTuDetail_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTuDetail] CHECK CONSTRAINT [FK_PKHPhieuNhapVatTuDetail_ChiTietVatTu]
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTuDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHPhieuNhapVatTuDetail_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTuDetail] CHECK CONSTRAINT [FK_PKHPhieuNhapVatTuDetail_Kho]
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTuDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHPhieuNhapVatTuDetail_PKHPhieuNhapVatTu] FOREIGN KEY([PKHPhieuNhapVatTuId])
REFERENCES [dbo].[PKHPhieuNhapVatTu] ([PKHPhieuNhapVatTuId])
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTuDetail] CHECK CONSTRAINT [FK_PKHPhieuNhapVatTuDetail_PKHPhieuNhapVatTu]
GO
ALTER TABLE [dbo].[PKHPhieuYeuCauMuaHangDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHPhieuYeuCauMuaHangDetail_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[PKHPhieuYeuCauMuaHangDetail] CHECK CONSTRAINT [FK_PKHPhieuYeuCauMuaHangDetail_ChiTietVatTu]
GO
ALTER TABLE [dbo].[PKHPhieuYeuCauMuaHangDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHPhieuYeuCauMuaHangDetail_PKHPhieuYeuCauMuaHang] FOREIGN KEY([PKHPhieuYeuCauMuaHangId])
REFERENCES [dbo].[PKHPhieuYeuCauMuaHang] ([PKHPhieuYeuCauMuaHangId])
GO
ALTER TABLE [dbo].[PKHPhieuYeuCauMuaHangDetail] CHECK CONSTRAINT [FK_PKHPhieuYeuCauMuaHangDetail_PKHPhieuYeuCauMuaHang]
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang]  WITH CHECK ADD  CONSTRAINT [FK_PKHTheoDoiGiaoHang_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang] CHECK CONSTRAINT [FK_PKHTheoDoiGiaoHang_ChiTietVatTu]
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang]  WITH CHECK ADD  CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHDuKienGiaoHang] FOREIGN KEY([PKHDuKienGiaoHangId])
REFERENCES [dbo].[PKHDuKienGiaoHang] ([PKHDuKienGiaoHangId])
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang] CHECK CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHDuKienGiaoHang]
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang]  WITH CHECK ADD  CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHKiemHoa] FOREIGN KEY([PKHKiemHoaId])
REFERENCES [dbo].[PKHKiemHoa] ([PKHKiemHoaId])
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang] CHECK CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHKiemHoa]
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang]  WITH CHECK ADD  CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHPhieuYeuCauMuaHang] FOREIGN KEY([PKHPhieuYeuCauMuaHangId])
REFERENCES [dbo].[PKHPhieuYeuCauMuaHang] ([PKHPhieuYeuCauMuaHangId])
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang] CHECK CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHPhieuYeuCauMuaHang]
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang]  WITH CHECK ADD  CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHPhieuYeuCauMuaHangDetail] FOREIGN KEY([PKHPhieuYeuCauMuaHangDetailId])
REFERENCES [dbo].[PKHPhieuYeuCauMuaHangDetail] ([PKHPhieuYeuCauMuaHangDetailId])
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang] CHECK CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHPhieuYeuCauMuaHangDetail]
GO
