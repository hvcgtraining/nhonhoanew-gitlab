USE [EcommerceSystem_Dev]
GO

CREATE TABLE [dbo].[LoaiVatTu](
	[LoaiVatTuId] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiVatTu] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL, -- TenLoaiVatTu
	[Cap] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_LoaiVatTu] PRIMARY KEY CLUSTERED 
(
	[LoaiVatTuId] ASC
)
) ON [PRIMARY]

GO


