USE [master]
GO
/****** Object:  Database [EcommerceSystem_Dev]    Script Date: 7/3/2019 9:49:10 AM ******/
CREATE DATABASE [EcommerceSystem_Dev]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'EcommerceSystem_Dev', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\EcommerceSystem_Dev.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'EcommerceSystem_Dev_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\EcommerceSystem_Dev_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [EcommerceSystem_Dev] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EcommerceSystem_Dev].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EcommerceSystem_Dev] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET ARITHABORT OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET  DISABLE_BROKER 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET  MULTI_USER 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EcommerceSystem_Dev] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [EcommerceSystem_Dev]
GO
/****** Object:  Table [dbo].[AccountToken]    Script Date: 7/3/2019 9:49:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountToken](
	[AccountTokenId] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [int] NOT NULL,
	[IsAdminAccountSide] [bit] NULL,
	[TokenKey] [varchar](500) NULL,
	[ExpiredDate] [datetime] NULL,
	[TokenType] [int] NOT NULL,
 CONSTRAINT [PK_AccountToken] PRIMARY KEY CLUSTERED 
(
	[AccountTokenId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Configuration]    Script Date: 7/3/2019 9:49:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Configuration](
	[ConfigurationId] [int] IDENTITY(1,1) NOT NULL,
	[ExchangeRate] [money] NULL,
	[AdminAcceptanceBalancePercentage] [int] NOT NULL,
	[ClientAcceptanceBalancePercentage] [int] NOT NULL,
 CONSTRAINT [PK_Configuration] PRIMARY KEY CLUSTERED 
(
	[ConfigurationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailTemplate]    Script Date: 7/3/2019 9:49:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailTemplate](
	[EmailTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationID] [int] NOT NULL,
	[Placed] [varchar](256) NOT NULL,
	[Cancelled] [varchar](256) NOT NULL,
	[Updated] [varchar](256) NOT NULL,
	[PartialApproval] [varchar](256) NOT NULL,
	[NotApproved] [varchar](256) NOT NULL,
	[Approved] [varchar](256) NOT NULL,
	[IntoReviewOnPlaced] [varchar](256) NOT NULL,
	[Done] [varchar](256) NULL,
	[Paid] [varchar](256) NULL,
 CONSTRAINT [PK_EmailTemplate] PRIMARY KEY CLUSTERED 
(
	[EmailTemplateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ModuleAction]    Script Date: 7/3/2019 9:49:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ModuleAction](
	[ModuleActionID] [int] IDENTITY(1,1) NOT NULL,
	[Module] [varchar](50) NULL,
	[Action] [varchar](50) NULL,
	[Description] [varchar](500) NULL,
	[OrderIndex] [int] NULL,
 CONSTRAINT [PK_ModuleAction] PRIMARY KEY CLUSTERED 
(
	[ModuleActionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Notification]    Script Date: 7/3/2019 9:49:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[NotificationId] [int] IDENTITY(1,1) NOT NULL,
	[Note] [nvarchar](max) NOT NULL,
	[Type] [smallint] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Title] [nvarchar](250) NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[NotificationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NotificationRecipient]    Script Date: 7/3/2019 9:49:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationRecipient](
	[NotificationRecipientId] [int] IDENTITY(1,1) NOT NULL,
	[NotificationId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[IsRead] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_NotificationRecipient] PRIMARY KEY CLUSTERED 
(
	[NotificationRecipientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoleModuleAction]    Script Date: 7/3/2019 9:49:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleModuleAction](
	[RoleModuleActionID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NOT NULL,
	[ModuleActionID] [int] NOT NULL,
 CONSTRAINT [PK_RoleModuleAction] PRIMARY KEY CLUSTERED 
(
	[RoleModuleActionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 7/3/2019 9:49:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Note] [nvarchar](300) NULL,
	[Status] [bit] NOT NULL,
	[Type] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedByUserId] [nvarchar](50) NULL,
	[UpdatedUserId] [nvarchar](50) NULL,
 CONSTRAINT [PK_dbo.Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 7/3/2019 9:49:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Email] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[PasswordSalt] [nvarchar](max) NULL,
	[IsSupperAdmin] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[LastLoginDate] [datetime] NULL,
	[LastActivityDate] [datetime] NULL,
	[IsLockedOut] [bit] NOT NULL,
	[Avatar] [nvarchar](max) NULL,
	[Status] [bit] NOT NULL,
	[FullName] [nvarchar](150) NULL,
	[Tel] [nvarchar](50) NULL,
 CONSTRAINT [PK_dbo.User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 7/3/2019 9:49:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserRoleId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[AccountToken] ON 

INSERT [dbo].[AccountToken] ([AccountTokenId], [AccountId], [IsAdminAccountSide], [TokenKey], [ExpiredDate], [TokenType]) VALUES (30, 3, 0, N'1a7d471e86c0432f9d37d47bce07e2b9', CAST(0x0000A70000BF7B86 AS DateTime), 3)
SET IDENTITY_INSERT [dbo].[AccountToken] OFF
SET IDENTITY_INSERT [dbo].[Configuration] ON 

INSERT [dbo].[Configuration] ([ConfigurationId], [ExchangeRate], [AdminAcceptanceBalancePercentage], [ClientAcceptanceBalancePercentage]) VALUES (1, 3366.0000, 70, 90)
SET IDENTITY_INSERT [dbo].[Configuration] OFF
SET IDENTITY_INSERT [dbo].[ModuleAction] ON 

INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (1, N'User', N'Listing', N'User listing', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (2, N'User', N'Create', N'Create new a user', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (3, N'User', N'Edit', N'Update user', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (4, N'User', N'Delete', N'Delete User', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (5, N'Role', N'Create', N'Create new a role', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (6, N'Role', N'Edit', N'Update role', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (7, N'Role', N'Listing', N'Role listing', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (8, N'Role', N'Delete', N'Delete role', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (10, N'Customer', N'Edit', N'Update Customer', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (11, N'Customer', N'Listing', N'Customer listing', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (12, N'Customer', N'Delete', N'Delete Customer', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (13, N'Customer', N'Create', N'Create customer', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (15, N'Role', N'Invisible', N'Disable ', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (65, N'Customer', N'Invisible', N'Disable customer', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (66, N'User', N'Invisible', N'Delete User', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (67, N'Order', N'OrderPendingListing', N'List of pending order ', 0)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (68, N'Order', N'DepositOrder', N'Deposit order', 5)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (69, N'Order', N'CancelOrderDeposited', N'Cancel order deposited', 6)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (70, N'Order', N'OrderPendingQuoteListing', N'Order Pending Quote Listing', 2)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (71, N'Order', N'Quote', N'Quote order', 4)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (72, N'Order', N'MergeOrder', N'Merge order', 3)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (73, N'Order', N'Latch', N'Latch Order', 7)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (74, N'Order', N'Detail', N'Detail of order', 4)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (75, N'Order', N'ShopOrderDetail', N'Detail of shop order detail', 8)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (76, N'Order', N'PurchaseOrder', NULL, 8)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (77, N'Order', N'CancelOrder', N'Cancel Order', 9)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (78, N'Order', N'OrderQuoteListing', N'Order Quote Listing', 4)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (79, N'Order', N'OrderPurchasingListing', N'Order Purchasing Listing', 8)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (80, N'Order', N'OrderPaidListing', N'Order Paid Listing', 10)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (81, N'Order', N'ShopperOrderListing', N'Shopper Order Listing', 12)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (82, N'Order', N'ImportChinaStoreListing', N'Order Chinese Inventory Receiving Listing', 13)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (83, N'Order', N'ExportChinaStoreListing', N'Order Chinese Inventory Delivering Listing', 14)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (84, N'Order', N'ImportVietNamStoreListing', N'Order Vietnamese Inventory Receiving Listing', 15)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (85, N'Order', N'ExportVietNamStoreListing', N'Order Vietnamese Inventory Delivering Listing', 16)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (86, N'Order', N'OrderDone', N'Order Done', 17)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (87, N'Order', N'SearchLanding', N'Search lading', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (88, N'OrderTrash', N'Listing', N'Listing', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (89, N'OrderTrash', N'Detail', N'Detail', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (90, N'OrderTrash', N'Restore', N'Restore', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (91, N'Notification', N'Create', N'Create', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (92, N'Notification', N'Edit', N'Edit', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (93, N'Notification', N'Listing', N'Listing', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (94, N'Notification', N'Invisible', N'Disable', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (95, N'Notification', N'Delete', N'Delete', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (96, N'Transaction', N'Listing', N'Listing', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (97, N'Transaction', N'Detail', N'Detail', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (98, N'Customer', N'Transaction', N'Transaction', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (99, N'Customer', N'RemoteLogin', N'RemoteLogin', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (100, N'Complaint', N'Listing', N'Listing', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (101, N'Complaint', N'Detail', N'Detail', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (102, N'Complaint', N'Cancel', N'Cancel', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (103, N'Complaint', N'Approve', N'Aprove', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (104, N'Order', N'ProcessOrder', N'Process order', 1)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (106, N'Order', N'AwaitPurchaseOrder', N'Await Purchase Order', 8)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (107, N'Order', N'RollBackToQuoteOrder', N'RollBack To Quote Order', 7)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (108, N'Order', N'EditBoughtOrder', N'Edit Bought Order', 9)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (109, N'Order', N'UpdateShippingCode', N'UpdateShippingCode', 10)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (110, N'Order', N'SearchShopOrder', N'SearchShopOrder', 12)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (111, N'Order', N'ProcessImportChinaStore', N'ProcessImportChinaStore', 13)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (112, N'Order', N'ReceivePackage', NULL, 14)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (113, N'Order', N'ProcessImportVietnamStore', NULL, 15)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (114, N'Order', N'ProcessExportVietnamStore', NULL, 16)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (115, N'Order', N'AddPackage', NULL, 14)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (116, N'Order', N'AddLandingCodeToPackage', NULL, 14)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (117, N'Order', N'DeletePackage', NULL, 14)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (118, N'Order', N'DeletePackageItem', NULL, 14)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (119, N'Order', N'UpdatePackage', NULL, 14)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (120, N'User', N'Invisibe', N'Invisibe', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (121, N'Configuration', N'Setting', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (122, N'Notification', N'Create', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (123, N'Notification', N'Edit', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (124, N'Notification', N'Listing', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (125, N'Notification', N'Delete', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (126, N'Notification', N'Invisible', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (127, N'Role', N'Invisible', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (128, N'Transaction', N'History', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (129, N'Transaction', N'Withdrawal', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (130, N'Transaction', N'Edit', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (131, N'Transaction', N'Delete', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (132, N'Transaction', N'ReCharge', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (133, N'Transaction', N'Refund', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (134, N'Credit', N'Listing', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (135, N'Credit', N'Info', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (136, N'Credit', N'ChargeToNV', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (137, N'Credit', N'ChargeToAdmin', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (138, N'Credit', N'WithdrawCreditAdmin', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (139, N'Credit', N'RefundUserCredit', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (143, N'CarrierOrder', N'Create', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (144, N'CarrierOrder', N'Edit', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (145, N'CarrierOrder', N'Listing', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (146, N'CarrierOrder', N'Delete', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (148, N'Order', N'PenddingDepositListing', NULL, 7)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (149, N'Order', N'ReadyExportVNStoreListing', NULL, 17)
SET IDENTITY_INSERT [dbo].[ModuleAction] OFF
SET IDENTITY_INSERT [dbo].[Notification] ON 

INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (51, N'<p>Hùng Thịnh Logistics gửi bạn bảng báo giá cho đơn hàng <strong>DH4131711828</strong></p><p>Chi tiết vui lòng xem tại : <a target=''_blank'' href=''/Order''>Đơn hàng đã báo giá</a></p>', 2, 0, CAST(0x0000A6FF0179D7B6 AS DateTime), N'Báo giá đơn hàng DH4131711828', NULL, 1)
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (52, N'<p>Dear anh chị.</p>
<p>Hùng Thịnh đã mua đơn hàng DH4131711828. Anh chị kiểm tra lại đơn hàng những phần đã ghi chú lại (nếu có).</p>
<p>Khi hàng về Việt Nam bộ phận kho sẽ thông báo để anh chị làm thủ tục nhận hàng và chuyển xe đi tỉnh nếu cần.</p>
<p>Sau 7 ngày anh chị có thể gọi điện cho <strong>BP Kho : 04 39956222 </strong> để hỏi về tình trạng hàng hóa.</p>
<p><strong>Xin cảm ơn!</strong></p>

', 2, 0, CAST(0x0000A6FF017A4898 AS DateTime), N'Đã mua xong đơn hàng DH4131711828', NULL, 1)
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (53, N'<p>Hùng Thịnh Logistics gửi bạn bảng báo giá cho đơn hàng <strong>DH6131711826</strong></p><p>Chi tiết vui lòng xem tại : <a target=''_blank'' href=''/Order''>Đơn hàng đã báo giá</a></p>', 2, 0, CAST(0x0000A6FF017E517C AS DateTime), N'Báo giá đơn hàng DH6131711826', NULL, 1)
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (54, N'<p>Dear anh chị.</p>
<p>Hùng Thịnh đã mua đơn hàng DH6131711826. Anh chị kiểm tra lại đơn hàng những phần đã ghi chú lại (nếu có).</p>
<p>Khi hàng về Việt Nam bộ phận kho sẽ thông báo để anh chị làm thủ tục nhận hàng và chuyển xe đi tỉnh nếu cần.</p>
<p>Sau 7 ngày anh chị có thể gọi điện cho <strong>BP Kho : 04 39956222 </strong> để hỏi về tình trạng hàng hóa.</p>
<p><strong>Xin cảm ơn!</strong></p>

', 2, 0, CAST(0x0000A6FF017E945C AS DateTime), N'Đã mua xong đơn hàng DH6131711826', NULL, 1)
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (55, N'<p>Dear anh chị.</p>
<p>Hùng Thịnh đã mua đơn hàng DH3131711859. Anh chị kiểm tra lại đơn hàng những phần đã ghi chú lại (nếu có).</p>
<p>Khi hàng về Việt Nam bộ phận kho sẽ thông báo để anh chị làm thủ tục nhận hàng và chuyển xe đi tỉnh nếu cần.</p>
<p>Sau 7 ngày anh chị có thể gọi điện cho <strong>BP Kho : 04 39956222 </strong> để hỏi về tình trạng hàng hóa.</p>
<p><strong>Xin cảm ơn!</strong></p>

', 2, 0, CAST(0x0000A6FF017EAABB AS DateTime), N'Đã mua xong đơn hàng DH3131711859', NULL, 1)
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (56, N'<p>Hùng Thịnh Logistics gửi bạn bảng báo giá cho đơn hàng <strong>DH6131711954</strong></p><p>Chi tiết vui lòng xem tại : <a target=''_blank'' href=''/Order''>Đơn hàng đã báo giá</a></p>', 2, 0, CAST(0x0000A70000A68A4E AS DateTime), N'Báo giá đơn hàng DH6131711954', NULL, 1)
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (57, N'<p>Dear anh chị.</p>
<p>Hùng Thịnh đã mua đơn hàng DH6131711954. Anh chị kiểm tra lại đơn hàng những phần đã ghi chú lại (nếu có).</p>
<p>Khi hàng về Việt Nam bộ phận kho sẽ thông báo để anh chị làm thủ tục nhận hàng và chuyển xe đi tỉnh nếu cần.</p>
<p>Sau 7 ngày anh chị có thể gọi điện cho <strong>BP Kho : 04 39956222 </strong> để hỏi về tình trạng hàng hóa.</p>
<p><strong>Xin cảm ơn!</strong></p>

', 2, 0, CAST(0x0000A70000B9E554 AS DateTime), N'Đã mua xong đơn hàng DH6131711954', NULL, 1)
SET IDENTITY_INSERT [dbo].[Notification] OFF
SET IDENTITY_INSERT [dbo].[NotificationRecipient] ON 

INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (74, 51, 13, 1, CAST(0x0000A6FF0179D7D1 AS DateTime))
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (75, 52, 13, 0, CAST(0x0000A6FF017A48A2 AS DateTime))
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (76, 53, 13, 1, CAST(0x0000A6FF017E5185 AS DateTime))
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (77, 54, 13, 0, CAST(0x0000A6FF017E9468 AS DateTime))
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (78, 55, 13, 1, CAST(0x0000A6FF017EAAC6 AS DateTime))
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (79, 56, 13, 1, CAST(0x0000A70000A68A58 AS DateTime))
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (80, 57, 13, 1, CAST(0x0000A70000B9E55F AS DateTime))
SET IDENTITY_INSERT [dbo].[NotificationRecipient] OFF
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (16, N'R001', N'Check Đơn', N'Kiểm tra đơn hàng', 1, NULL, CAST(0x0000A6A1017B3593 AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (17, N'R002', N'Cộng tác viên', N'Cộng tác viên', 1, NULL, CAST(0x0000A6A1017B6C76 AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (18, N'R003', N'Kế toán', N'Kế toán', 1, NULL, CAST(0x0000A6A1017B9615 AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (19, N'R004', N'Kho Trung Quốc', N'Kho Trung Quốc', 1, NULL, CAST(0x0000A6A1017BB563 AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (20, N'R005', N'Kho Việt Nam', N'Kho Việt Nam', 1, NULL, CAST(0x0000A6A1017BDC4E AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (21, N'R006', N'Orders', N'Orders', 1, NULL, CAST(0x0000A6A1017BFEF8 AS DateTime), CAST(0x0000A6F3000866EA AS DateTime), N'admin@nhonhoa.vn', N'admin@nhonhoa.vn')
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (22, N'R007', N'Quản trị viên', N'Quản trị viên', 1, NULL, CAST(0x0000A6A1017C1B3A AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
SET IDENTITY_INSERT [dbo].[Roles] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (6, N'Hùng Thịnh', N'admin@nhonhoa.vn', N'1000:paqv42x0zf32cagcDdRNXcpLPTqVDovH:0+A7K0tvhPf6HV3HU2CZ2tUYYIAlTu9X', N'1000:4XwRFklHMIqnUCvhhZxx8Njut1rgzX3+:UiUrwx8Kuhyw3aRx0f2JZETaWv6PjXyw', 1, CAST(0x0000A69500000000 AS DateTime), CAST(0x0000A69500000000 AS DateTime), CAST(0x0000A69500000000 AS DateTime), CAST(0x0000A69500000000 AS DateTime), 0, NULL, 1, N'', NULL)
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (27, N'Ke Toan', N'ketoan@taobao.com.vn', N'1000:prLoKfnzcM2TLaqgUyL9hWhzwhGp260o:vyIEznpGv2JZ/W4quiEha6p8VZi1rJPv', N'1000:4XwRFklHMIqnUCvhhZxx8Njut1rgzX3+:UiUrwx8Kuhyw3aRx0f2JZETaWv6PjXyw', 0, CAST(0x0000A69500000000 AS DateTime), NULL, CAST(0x0000A69500000000 AS DateTime), CAST(0x0000A69500000000 AS DateTime), 0, NULL, 0, NULL, NULL)
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (44, N'Nguyen Van Ngu', N'ngunv@nhonhoa.vn', N'1000:Ey/r8F+D3FxlWNFkqOcnsWJgtPKl2Gtd:waDb3mz1VcVCXYwZWgETDWbmoiSpXoV9', NULL, 0, CAST(0x0000A6A2003053FB AS DateTime), CAST(0x0000A789010D9411 AS DateTime), NULL, NULL, 0, NULL, 1, NULL, NULL)
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (45, N'order', N'order@nhonhoa.vn', N'1000:Jpq5c+X4lEJ1PerLJHCPQYWDhbPjHysy:/Ki90iy696NhXzTQDVfnArvX+XvUMfSZ', NULL, 0, CAST(0x0000A6A20082C626 AS DateTime), CAST(0x0000A6A200883524 AS DateTime), NULL, NULL, 0, NULL, 1, NULL, NULL)
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (46, N'Cộng tác viên', N'congtacvien@nhonhoa.vn', N'1000:roLna4oGLzr7BL3W5aUd0AUcP6pnSils:WeFI3IC1CO3wsfcIJqOiYH6oq9nOqG3j', NULL, 0, CAST(0x0000A6A200897EA8 AS DateTime), NULL, NULL, NULL, 0, NULL, 1, NULL, NULL)
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (47, N'fdgdfg', N'sdsfsdf@gfg.cc', N'1000:2dqjQtKdF+fYLOTqys6zLwi99xRYmqDb:mKSgx9wuMx7FMSJzRWptxtqwG8Tfpsot', NULL, 0, CAST(0x0000A789011E3AE2 AS DateTime), NULL, NULL, NULL, 0, NULL, 0, NULL, NULL)
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (48, NULL, N'sâdsa', N'1000:slRi5zRLSuKv9BKnMT5zsDGXw/NL6l/+:RRL4Z6Sh9bYofEvvRtkK9JQ8a7y9bp4b', NULL, 0, CAST(0x0000AA19000C5666 AS DateTime), NULL, NULL, NULL, 0, NULL, 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[UserRole] ON 

INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (1, 27, 18, CAST(0x0000A6A100000000 AS DateTime))
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (6, 44, 19, CAST(0x0000A6A200305403 AS DateTime))
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (7, 45, 21, CAST(0x0000A6A20082C670 AS DateTime))
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (8, 46, 17, CAST(0x0000A6A20089803B AS DateTime))
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (9, 47, 16, CAST(0x0000A789011E64CE AS DateTime))
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (10, 48, 16, CAST(0x0000AA19000C567C AS DateTime))
SET IDENTITY_INSERT [dbo].[UserRole] OFF
ALTER TABLE [dbo].[NotificationRecipient]  WITH CHECK ADD  CONSTRAINT [FK_NotificationRecipient_Notification1] FOREIGN KEY([NotificationId])
REFERENCES [dbo].[Notification] ([NotificationId])
GO
ALTER TABLE [dbo].[NotificationRecipient] CHECK CONSTRAINT [FK_NotificationRecipient_Notification1]
GO
ALTER TABLE [dbo].[RoleModuleAction]  WITH CHECK ADD  CONSTRAINT [FK_RoleModuleAction_ModuleAction] FOREIGN KEY([ModuleActionID])
REFERENCES [dbo].[ModuleAction] ([ModuleActionID])
GO
ALTER TABLE [dbo].[RoleModuleAction] CHECK CONSTRAINT [FK_RoleModuleAction_ModuleAction]
GO
ALTER TABLE [dbo].[RoleModuleAction]  WITH CHECK ADD  CONSTRAINT [FK_RoleModuleAction_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[RoleModuleAction] CHECK CONSTRAINT [FK_RoleModuleAction_Roles]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Role]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_User]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AccountType = 0(User  of client side), AccountType=1 User  of admin side' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountToken', @level2type=N'COLUMN',@level2name=N'IsAdminAccountSide'
GO
USE [master]
GO
ALTER DATABASE [EcommerceSystem_Dev] SET  READ_WRITE 
GO
