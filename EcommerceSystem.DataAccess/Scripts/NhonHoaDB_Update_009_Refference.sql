USE [master]
GO
/****** Object:  Database [EcommerceSystem_Dev]    Script Date: 8/2/2019 6:30:36 PM ******/
CREATE DATABASE [EcommerceSystem_Dev]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'EcommerceSystem_Dev', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\EcommerceSystem_Dev.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'EcommerceSystem_Dev_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\EcommerceSystem_Dev_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [EcommerceSystem_Dev] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EcommerceSystem_Dev].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EcommerceSystem_Dev] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET ARITHABORT OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET  DISABLE_BROKER 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET  MULTI_USER 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EcommerceSystem_Dev] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EcommerceSystem_Dev] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [EcommerceSystem_Dev]
GO
/****** Object:  Table [dbo].[AccountToken]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountToken](
	[AccountTokenId] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [int] NOT NULL,
	[IsAdminAccountSide] [bit] NULL,
	[TokenKey] [varchar](500) NULL,
	[ExpiredDate] [datetime] NULL,
	[TokenType] [int] NOT NULL,
 CONSTRAINT [PK_AccountToken] PRIMARY KEY CLUSTERED 
(
	[AccountTokenId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChiTietVatTu]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietVatTu](
	[ChiTietVatTuId] [int] IDENTITY(1,1) NOT NULL,
	[MaChiTietVatTu] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[DonVi] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
	[LoaiVatTuId] [int] NULL,
 CONSTRAINT [PK_ChiTietVatTu] PRIMARY KEY CLUSTERED 
(
	[ChiTietVatTuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Configuration]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Configuration](
	[ConfigurationId] [int] IDENTITY(1,1) NOT NULL,
	[ExchangeRate] [money] NULL,
	[AdminAcceptanceBalancePercentage] [int] NOT NULL,
	[ClientAcceptanceBalancePercentage] [int] NOT NULL,
 CONSTRAINT [PK_Configuration] PRIMARY KEY CLUSTERED 
(
	[ConfigurationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CoPhanCapXuong]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CoPhanCapXuong](
	[CoPhanCapXuongId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoNhapId] [int] NOT NULL,
	[KhoXuatId] [int] NOT NULL,
	[VatTuId] [int] NULL,
	[NhomSanPhamId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_CoPhanCapXuong] PRIMARY KEY CLUSTERED 
(
	[CoPhanCapXuongId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DanhSachChiTiet]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DanhSachChiTiet](
	[DanhSachChiTietId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[ChiTietVatTuId] [int] NULL,
	[SoLuongCoPhanId] [int] NULL,
	[SoLuong] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[HasChild] [bit] NULL,
	[ParentId] [int] NULL,
	[DinhMucCatToleId] [int] NULL,
	[DinhMucSanXuatId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DanhSachChiTiet_1] PRIMARY KEY CLUSTERED 
(
	[DanhSachChiTietId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DanhSachDieuChuyenQuyetToan]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DanhSachDieuChuyenQuyetToan](
	[DanhSachDieuChuyenQuyetToanId] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[DieuChuyenQuyetToanId] [bigint] NOT NULL,
	[SoChungTu] [nvarchar](50) NULL,
	[VatTuDiId] [int] NULL,
	[SoLuongDi] [int] NULL,
	[VatTuVeId] [int] NULL,
	[SoLuongVeId] [int] NULL,
	[DonViTinh] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_ListChiTiet] PRIMARY KEY CLUSTERED 
(
	[DanhSachDieuChuyenQuyetToanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Dechet]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dechet](
	[DechetId] [int] IDENTITY(1,1) NOT NULL,
	[MaCoPhan] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_Dechet] PRIMARY KEY CLUSTERED 
(
	[DechetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DieuChuyenQuyetToan]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DieuChuyenQuyetToan](
	[DieuChuyenQuyetToanId] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[SoChungTu] [nvarchar](50) NOT NULL,
	[KhoNhapId] [int] NULL,
	[KhoXuatId] [int] NULL,
	[DaXem] [bit] NULL,
	[LoaiDieuChuyenQuyetToan] [int] NULL,
	[LyDoXuat] [nvarchar](350) NULL,
	[NguoiNhan] [nvarchar](50) NULL,
	[CanCu] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_TranferSettlement] PRIMARY KEY CLUSTERED 
(
	[DieuChuyenQuyetToanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DinhMucCatTole]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DinhMucCatTole](
	[DinhMucCatToleId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[SoChungTu] [nvarchar](50) NOT NULL,
	[Day] [float] NULL,
	[PhuongAn] [int] NULL,
	[ChiTietVatTuId] [int] NULL,
	[ToleId] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[Loai] [int] NULL,
	[HinhAnh] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DinhMucCatTole] PRIMARY KEY CLUSTERED 
(
	[DinhMucCatToleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DinhMucSanXuat]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DinhMucSanXuat](
	[DinhMucSanXuatId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[MaDinhMucSanXuat] [nvarchar](50) NOT NULL,
	[ChiTietVatTuId] [int] NULL,
	[SoChungTu] [nvarchar](50) NULL,
	[MaCapPhat] [nvarchar](50) NULL,
	[Loai] [nvarchar](50) NULL,
	[DienGiai] [nvarchar](500) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[KhoQuyetToanId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DinhMucSanXuat] PRIMARY KEY CLUSTERED 
(
	[DinhMucSanXuatId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DinhMucTuan]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DinhMucTuan](
	[DinhMucTuanId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[VatTuId] [int] NOT NULL,
	[DinhMuc] [float] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DinhMucTuan] PRIMARY KEY CLUSTERED 
(
	[DinhMucTuanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DoiMaChiTiet]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DoiMaChiTiet](
	[DoiMaChiTietId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[MaChiTietVatTu] [nvarchar](50) NOT NULL,
	[DonVi] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DoiMaChiTiet] PRIMARY KEY CLUSTERED 
(
	[DoiMaChiTietId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DoiMaVatTu]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DoiMaVatTu](
	[DoiMaVatTuId] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiVatTu] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[Cap] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DoiMaVatTu] PRIMARY KEY CLUSTERED 
(
	[DoiMaVatTuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailTemplate]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailTemplate](
	[EmailTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationID] [int] NOT NULL,
	[Placed] [varchar](256) NOT NULL,
	[Cancelled] [varchar](256) NOT NULL,
	[Updated] [varchar](256) NOT NULL,
	[PartialApproval] [varchar](256) NOT NULL,
	[NotApproved] [varchar](256) NOT NULL,
	[Approved] [varchar](256) NOT NULL,
	[IntoReviewOnPlaced] [varchar](256) NOT NULL,
	[Done] [varchar](256) NULL,
	[Paid] [varchar](256) NULL,
 CONSTRAINT [PK_EmailTemplate] PRIMARY KEY CLUSTERED 
(
	[EmailTemplateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GoiDau]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GoiDau](
	[GoiDauId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[VatTuId] [int] NOT NULL,
	[NgayGoiDau] [int] NULL,
	[TuanDuPhong] [int] NULL,
	[PKHKeHoachSXNKXHId] [bigint] NULL,
	[MaKeHoach] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_GoiDau] PRIMARY KEY CLUSTERED 
(
	[GoiDauId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Kho]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kho](
	[KhoId] [int] IDENTITY(1,1) NOT NULL,
	[MaKho] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[NguoiGiao] [nvarchar](50) NULL,
	[XuongId] [int] NULL,
	[ThongTinKho] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_Kho] PRIMARY KEY CLUSTERED 
(
	[KhoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[KhoiLuongCoPhan]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhoiLuongCoPhan](
	[KhoiLuongCoPhanId] [int] IDENTITY(1,1) NOT NULL,
	[MaKhoiLuongCoPhan] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoiLuong] [float] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_KhoiLuongCoPhan] PRIMARY KEY CLUSTERED 
(
	[KhoiLuongCoPhanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[KhoTinhTon]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhoTinhTon](
	[KhoTinhTonId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoId] [int] NOT NULL,
	[NhomSanPhamId] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_KhoTinhTon] PRIMARY KEY CLUSTERED 
(
	[KhoTinhTonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[KyTen]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KyTen](
	[KyTenId] [int] IDENTITY(1,1) NOT NULL,
	[MaKyTen] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[NhanVienId] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[LoaiKyTen] [tinyint] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_KyTenKho] PRIMARY KEY CLUSTERED 
(
	[KyTenId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiVatTu]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiVatTu](
	[LoaiVatTuId] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiVatTu] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[Cap] [int] NULL,
	[Type] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_LoaiVatTu] PRIMARY KEY CLUSTERED 
(
	[LoaiVatTuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LyDoXuat]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LyDoXuat](
	[LyDoXuatId] [int] IDENTITY(1,1) NOT NULL,
	[MaLyDoXuat] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_LyDoXuat] PRIMARY KEY CLUSTERED 
(
	[LyDoXuatId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ModuleAction]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ModuleAction](
	[ModuleActionID] [int] IDENTITY(1,1) NOT NULL,
	[Module] [varchar](50) NULL,
	[Action] [varchar](50) NULL,
	[Description] [varchar](500) NULL,
	[OrderIndex] [int] NULL,
 CONSTRAINT [PK_ModuleAction] PRIMARY KEY CLUSTERED 
(
	[ModuleActionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NguoiNhanKho]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NguoiNhanKho](
	[NguoiNhanKhoId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoId] [int] NOT NULL,
	[MaTen] [nvarchar](50) NULL,
	[TenGiaoNhan] [nvarchar](250) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[MaTruongDonVi] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_NguoiNhanKho] PRIMARY KEY CLUSTERED 
(
	[NguoiNhanKhoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhomSanPham]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhomSanPham](
	[NhomSanPhamId] [int] IDENTITY(1,1) NOT NULL,
	[MaNhomSanPham] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_NhomSanPham] PRIMARY KEY CLUSTERED 
(
	[NhomSanPhamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhomVatTuNhapRieng]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhomVatTuNhapRieng](
	[NhomVatTuNhapRiengId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoId] [int] NOT NULL,
	[DangMa] [nvarchar](50) NOT NULL,
	[TenNhom] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_NhomVatTuNhapRieng] PRIMARY KEY CLUSTERED 
(
	[NhomVatTuNhapRiengId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhomYeuCau]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhomYeuCau](
	[NhomYeuCauId] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_NhomYeuCau] PRIMARY KEY CLUSTERED 
(
	[NhomYeuCauId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Notification]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[NotificationId] [int] IDENTITY(1,1) NOT NULL,
	[Note] [nvarchar](max) NOT NULL,
	[Type] [smallint] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Title] [nvarchar](250) NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[NotificationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NotificationRecipient]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationRecipient](
	[NotificationRecipientId] [int] IDENTITY(1,1) NOT NULL,
	[NotificationId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[IsRead] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_NotificationRecipient] PRIMARY KEY CLUSTERED 
(
	[NotificationRecipientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PhuLucIso]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhuLucIso](
	[PhuLucIsoId] [int] IDENTITY(1,1) NOT NULL,
	[MaPhuLucIso] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[SoPhuLuc] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PhuLucISO] PRIMARY KEY CLUSTERED 
(
	[PhuLucIsoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHDuKienGiaoHang]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHDuKienGiaoHang](
	[PKHDuKienGiaoHangId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHPhieuYeuCauMuaHangId] [bigint] NOT NULL,
	[PKHPhieuYeuCauMuaHangDetailId] [bigint] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[NgayDuKien] [datetime] NULL,
	[SoLuongGiaoDuKien] [int] NULL,
	[TongSoLuong] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHDuKienGiaoHang] PRIMARY KEY CLUSTERED 
(
	[PKHDuKienGiaoHangId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachNhapKhoThanhPhamDetail]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachNhapKhoThanhPhamDetail](
	[PKHKeHoachNhapKhoThanhPhamDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHKeHoachXuongId] [bigint] NOT NULL,
	[VatTuId] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[ThuHai] [int] NULL,
	[ThuBa] [int] NULL,
	[ThuTu] [int] NULL,
	[ThuNam] [int] NULL,
	[ThuSau] [int] NULL,
	[ThuBay] [int] NULL,
	[ChuNhat] [int] NULL,
	[TongNhapKho] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachNhapKhoThanhPhamDetail] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachNhapKhoThanhPhamDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachSXNKXH]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachSXNKXH](
	[PKHKeHoachSXNKXHId] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[MaTuan] [nvarchar](10) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[NgayTonKho] [datetime] NOT NULL,
	[SoNgayXuatKho] [smallint] NULL,
	[SoLuongCan] [int] NULL,
	[SoLuongTon] [int] NULL,
	[TinhTrang] [tinyint] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachSXNKXH] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachSXNKXHId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachSXNKXHDetail]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachSXNKXHDetail](
	[PKHKeHoachSXNKXHDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHKeHoachSXNKXHId] [bigint] NOT NULL,
	[VatTuId] [int] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[TonKho] [int] NULL,
	[NhapKhoTuanTruoc] [int] NULL,
	[XuatHaNoi] [int] NULL,
	[XuatKho] [int] NULL,
	[TongTonKho] [int] NULL,
	[TongNhapKho] [int] NULL,
	[XuatDaiLy] [int] NULL,
	[DuKienXuatKho] [int] NULL,
	[DuKienTonKho] [int] NULL,
	[SoLuongCan] [int] NULL,
	[SoLuongBaoBi] [int] NULL,
	[DieuChinhCan] [int] NULL,
	[DieuChinhBaoBi] [int] NULL,
	[TongSoCan] [int] NULL,
	[Xuong4] [int] NULL,
	[Xuong5] [int] NULL,
	[Xuong6] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
	[xuongEx] [int] NULL,
 CONSTRAINT [PK_PKHKeHoachSXNKXHDetail] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachSXNKXHDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachSXTuanDetail]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachSXTuanDetail](
	[PKHKeHoachSXTuanDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[ChiTietVatTuId] [int] NOT NULL,
	[PKHKeHoachXuongId] [bigint] NOT NULL,
	[SoLuongSanXuat] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[ThuHai] [int] NULL,
	[ThuBa] [int] NULL,
	[ThuTu] [int] NULL,
	[ThuNam] [int] NULL,
	[ThuSau] [int] NULL,
	[ThuBay] [int] NULL,
	[ChuNhat] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachSXTuanDetail] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachSXTuanDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachXuong]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachXuong](
	[PKHKeHoachXuongId] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[PKHKeHoachSXNKXHId] [bigint] NOT NULL,
	[XuongId] [int] NULL,
	[TinhTrang] [tinyint] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachXuong] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachXuongId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKeHoachXuongDetail]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKeHoachXuongDetail](
	[PKHKeHoachXuongDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHKeHoachSXNKXHDetailId] [bigint] NULL,
	[ChiTietVatTuId] [int] NULL,
	[KhoId] [int] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[DinhMucSanXuatId] [int] NOT NULL,
	[PKHKeHoachXuongId] [bigint] NOT NULL,
	[ThoiLuong] [float] NULL,
	[YeuCau] [int] NULL,
	[ChenhLech] [int] NULL,
	[UuTien] [int] NULL,
	[TonKho] [int] NULL,
	[ChiTietTon] [nvarchar](500) NULL,
	[TonDuKien] [int] NULL,
	[DauKy] [datetime] NULL,
	[NgayCapHang] [datetime] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKeHoachXuongDetail] PRIMARY KEY CLUSTERED 
(
	[PKHKeHoachXuongDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKiemHoa]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKiemHoa](
	[PKHKiemHoaId] [bigint] IDENTITY(1,1) NOT NULL,
	[LoaiKiemHoa] [tinyint] NOT NULL,
	[SoKiemHoa] [nvarchar](50) NOT NULL,
	[NgayLapPhieu] [datetime] NULL,
	[KhoId] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[MaKiemHoa] [nvarchar](20) NULL,
	[PhieuNhap] [nvarchar](50) NULL,
	[SoHD] [int] NULL,
	[TrangThai] [tinyint] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKiemHoa] PRIMARY KEY CLUSTERED 
(
	[PKHKiemHoaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHKiemHoaDetail]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHKiemHoaDetail](
	[PKHKiemHoaDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHKiemHoaId] [bigint] NOT NULL,
	[MaPYC] [int] NULL,
	[CustomerId] [int] NULL,
	[ChiTietVatTuId] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[SoLuong] [int] NULL,
	[SoLuongDat] [int] NULL,
	[SoLuongKhongDat] [int] NULL,
	[CheckFlag] [bit] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHKiemHoaDetail] PRIMARY KEY CLUSTERED 
(
	[PKHKiemHoaDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHPhieuNhapVatTu]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHPhieuNhapVatTu](
	[PKHPhieuNhapVatTuId] [bigint] IDENTITY(1,1) NOT NULL,
	[SoChungTu] [nvarchar](20) NULL,
	[NgayLapPhieu] [datetime] NOT NULL,
	[KhoId] [int] NOT NULL,
	[PKHKiemHoaId] [bigint] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[MaKH] [nvarchar](20) NOT NULL,
	[SoKiemHoa] [nvarchar](20) NULL,
	[SoDC] [nvarchar](10) NULL,
	[TrangThai] [tinyint] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHPhieuNhapVatTu] PRIMARY KEY CLUSTERED 
(
	[PKHPhieuNhapVatTuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHPhieuNhapVatTuDetail]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHPhieuNhapVatTuDetail](
	[PKHPhieuNhapVatTuDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHPhieuNhapVatTuId] [bigint] NOT NULL,
	[KhoId] [int] NULL,
	[CustomerId] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[ChiTietVatTuId] [int] NULL,
	[SoLuong] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHPhieuNhapVatTuDetail] PRIMARY KEY CLUSTERED 
(
	[PKHPhieuNhapVatTuDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHPhieuYeuCauMuaHang]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHPhieuYeuCauMuaHang](
	[PKHPhieuYeuCauMuaHangId] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[NgayYeuCau] [datetime] NOT NULL,
	[NgayDuKien] [datetime] NOT NULL,
	[NgayHoanTat] [datetime] NULL,
	[TrangThai] [tinyint] NULL,
	[YeuCauKT] [nvarchar](500) NULL,
	[NoiDung] [nvarchar](500) NULL,
	[DiaDiem] [nvarchar](250) NULL,
	[MucDich] [nvarchar](500) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHPhieuYeuCauMuaHang] PRIMARY KEY CLUSTERED 
(
	[PKHPhieuYeuCauMuaHangId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHPhieuYeuCauMuaHangDetail]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHPhieuYeuCauMuaHangDetail](
	[PKHPhieuYeuCauMuaHangDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHPhieuYeuCauMuaHangId] [bigint] NOT NULL,
	[ChiTietVatTuId] [int] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[SoLuong] [int] NULL,
	[DonGiaThamKhao] [int] NOT NULL,
	[QuyCach] [nvarchar](200) NULL,
	[DonGia] [int] NULL,
	[NhomYeuCauId] [bigint] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[TongSoLuong] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHPhieuYeuCauMuaHangDetail] PRIMARY KEY CLUSTERED 
(
	[PKHPhieuYeuCauMuaHangDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PKHTheoDoiGiaoHang]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PKHTheoDoiGiaoHang](
	[PKHTheoDoiGiaoHangId] [bigint] IDENTITY(1,1) NOT NULL,
	[PKHPhieuYeuCauMuaHangId] [bigint] NOT NULL,
	[PKHPhieuYeuCauMuaHangDetailId] [bigint] NOT NULL,
	[PKHDuKienGiaoHangId] [bigint] NOT NULL,
	[PKHKiemHoaId] [bigint] NOT NULL,
	[NgayGiaoHang] [datetime] NULL,
	[ChiTietVatTuId] [int] NULL,
	[CustomerId] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[SoLuongGiaoDuKien] [int] NULL,
	[SoLuongGiaoThucTe] [int] NULL,
	[SoLuongConThieu] [int] NULL,
	[KiemHoaFlag] [bit] NOT NULL,
	[SoLuongDat] [int] NULL,
	[SoLuongKhongDat] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PKHTheoDoiGiaoHang] PRIMARY KEY CLUSTERED 
(
	[PKHTheoDoiGiaoHangId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuyDoiDinhMuc]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuyDoiDinhMuc](
	[QuyDoiDinhMucId] [int] IDENTITY(1,1) NOT NULL,
	[MaQuyDoiDinhMuc] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[DinhMuc] [float] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_QuyDoiDinhMuc] PRIMARY KEY CLUSTERED 
(
	[QuyDoiDinhMucId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoleModuleAction]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleModuleAction](
	[RoleModuleActionID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NOT NULL,
	[ModuleActionID] [int] NOT NULL,
 CONSTRAINT [PK_RoleModuleAction] PRIMARY KEY CLUSTERED 
(
	[RoleModuleActionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Note] [nvarchar](300) NULL,
	[Status] [bit] NOT NULL,
	[Type] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedByUserId] [nvarchar](50) NULL,
	[UpdatedUserId] [nvarchar](50) NULL,
 CONSTRAINT [PK_dbo.Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SoDuBanDau]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SoDuBanDau](
	[SoDuBanDauId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoId] [int] NOT NULL,
	[VatTuId] [int] NULL,
	[SoDuDauKy] [float] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_SoDuBanDau] PRIMARY KEY CLUSTERED 
(
	[SoDuBanDauId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ThanhPhamTuDong]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThanhPhamTuDong](
	[ThanhPhamTuDongId] [int] IDENTITY(1,1) NOT NULL,
	[MaCoPhan] [nvarchar](50) NULL,
	[Title] [nvarchar](250) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_ThanhPhamTuDong] PRIMARY KEY CLUSTERED 
(
	[ThanhPhamTuDongId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ThanhPhamXuong]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThanhPhamXuong](
	[ThanhPhamXuongId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[XuongId] [int] NOT NULL,
	[VatTuId] [int] NOT NULL,
	[NhomSanPhamId] [int] NULL,
	[Loai] [nvarchar](50) NULL,
	[NangSuatTuan] [nvarchar](50) NULL,
	[LoaiVatLieu] [nvarchar](50) NULL,
	[QuyCach] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_ThanhPhamXuong] PRIMARY KEY CLUSTERED 
(
	[ThanhPhamXuongId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tole]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tole](
	[ToleId] [int] IDENTITY(1,1) NOT NULL,
	[MaTole] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[DonVi] [int] NULL,
	[Day] [float] NULL,
	[Ngang] [float] NULL,
	[Dung] [float] NULL,
	[KhoiLuong] [float] NULL,
	[DechetId] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_Tole] PRIMARY KEY CLUSTERED 
(
	[ToleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TrongLuongVatLieu]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrongLuongVatLieu](
	[TrongLuongVatLieuId] [int] IDENTITY(1,1) NOT NULL,
	[MaNguyenVatLieu] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KichThuoc] [nvarchar](50) NULL,
	[KhoiLuong] [float] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_TrongLuongVatLieu] PRIMARY KEY CLUSTERED 
(
	[TrongLuongVatLieuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Email] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[PasswordSalt] [nvarchar](max) NULL,
	[IsSupperAdmin] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[LastLoginDate] [datetime] NULL,
	[LastActivityDate] [datetime] NULL,
	[IsLockedOut] [bit] NOT NULL,
	[Avatar] [nvarchar](max) NULL,
	[Status] [bit] NOT NULL,
	[FullName] [nvarchar](150) NULL,
	[Tel] [nvarchar](50) NULL,
 CONSTRAINT [PK_dbo.User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserRoleId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VatTuCapRieng]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VatTuCapRieng](
	[VatTuCapRiengId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[VatTuId] [int] NOT NULL,
	[NhomSanPhamId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_VatTuCapRieng] PRIMARY KEY CLUSTERED 
(
	[VatTuCapRiengId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VatTuNhapRieng]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VatTuNhapRieng](
	[VatTuNhapRiengId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoId] [int] NOT NULL,
	[VatTuId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_VatTuNhapRieng] PRIMARY KEY CLUSTERED 
(
	[VatTuNhapRiengId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Xuong]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Xuong](
	[XuongId] [int] IDENTITY(1,1) NOT NULL,
	[MaXuong] [nvarchar](50) NULL,
	[Title] [nvarchar](250) NULL,
	[ThongTinXuong] [nvarchar](500) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[Type] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Xuong] PRIMARY KEY CLUSTERED 
(
	[XuongId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[YeuCauMuaHang]    Script Date: 8/2/2019 6:30:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[YeuCauMuaHang](
	[YeuCauMuaHangId] [bigint] IDENTITY(1,1) NOT NULL,
	[XuongId] [int] NULL,
	[KhoId] [int] NULL,
	[ChiTietVatTuId] [int] NULL,
	[PKHPhieuYeuCauMuaHangId] [bigint] NULL,
	[Title] [nvarchar](250) NULL,
	[SoPR] [nvarchar](50) NULL,
	[NgayYeuCau] [datetime] NULL,
	[NgayKetThucDuKien] [datetime] NULL,
	[TongSoLuong] [int] NULL,
	[NoiDung] [nvarchar](500) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[TrangThai] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_YeuCauMuaHang] PRIMARY KEY CLUSTERED 
(
	[YeuCauMuaHangId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[AccountToken] ON 

INSERT [dbo].[AccountToken] ([AccountTokenId], [AccountId], [IsAdminAccountSide], [TokenKey], [ExpiredDate], [TokenType]) VALUES (30, 3, 0, N'1a7d471e86c0432f9d37d47bce07e2b9', CAST(0x0000A70000BF7B86 AS DateTime), 3)
SET IDENTITY_INSERT [dbo].[AccountToken] OFF
SET IDENTITY_INSERT [dbo].[ChiTietVatTu] ON 

INSERT [dbo].[ChiTietVatTu] ([ChiTietVatTuId], [MaChiTietVatTu], [Title], [DonVi], [GhiChu], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status], [LoaiVatTuId]) VALUES (1, N'Vt001', N'Vật tư 001', 1, N'werwer', CAST(0x0000AA9D010FF22D AS DateTime), NULL, N'admin@nhonhoa.vn', NULL, 0, NULL)
INSERT [dbo].[ChiTietVatTu] ([ChiTietVatTuId], [MaChiTietVatTu], [Title], [DonVi], [GhiChu], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status], [LoaiVatTuId]) VALUES (2, N'Vt002', N'Vật tư 002', 1, NULL, CAST(0x0000AA9D01102D1E AS DateTime), NULL, N'admin@nhonhoa.vn', NULL, 0, NULL)
INSERT [dbo].[ChiTietVatTu] ([ChiTietVatTuId], [MaChiTietVatTu], [Title], [DonVi], [GhiChu], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status], [LoaiVatTuId]) VALUES (3, N'Vt003', N'Vật tư 003', 1, N'23ewrrwewe', CAST(0x0000AA9D0123E167 AS DateTime), CAST(0x0000AA9D012D1D36 AS DateTime), N'admin@nhonhoa.vn', N'admin@nhonhoa.vn', 0, NULL)
SET IDENTITY_INSERT [dbo].[ChiTietVatTu] OFF
SET IDENTITY_INSERT [dbo].[Configuration] ON 

INSERT [dbo].[Configuration] ([ConfigurationId], [ExchangeRate], [AdminAcceptanceBalancePercentage], [ClientAcceptanceBalancePercentage]) VALUES (1, 3366.0000, 70, 90)
SET IDENTITY_INSERT [dbo].[Configuration] OFF
SET IDENTITY_INSERT [dbo].[CoPhanCapXuong] ON 

INSERT [dbo].[CoPhanCapXuong] ([CoPhanCapXuongId], [Title], [KhoNhapId], [KhoXuatId], [VatTuId], [NhomSanPhamId], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status]) VALUES (6, N'Cơ phận 12', 1, 2, 1, 1, CAST(0x0000AA9D012878EF AS DateTime), CAST(0x0000AA9D012886C5 AS DateTime), N'admin@nhonhoa.vn', N'admin@nhonhoa.vn', 0)
SET IDENTITY_INSERT [dbo].[CoPhanCapXuong] OFF
SET IDENTITY_INSERT [dbo].[Dechet] ON 

INSERT [dbo].[Dechet] ([DechetId], [MaCoPhan], [Title], [GhiChu], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status]) VALUES (3, N'12cc', N'32234234 aa', N'dfsdfsdf', CAST(0x0000AA9901052F65 AS DateTime), NULL, N'admin@nhonhoa.vn', NULL, 0)
INSERT [dbo].[Dechet] ([DechetId], [MaCoPhan], [Title], [GhiChu], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status]) VALUES (4, N'12cc2', N'asdasdsf123asdasda asdasda ', N'dfsdfsdf', CAST(0x0000AA9901054340 AS DateTime), NULL, N'admin@nhonhoa.vn', NULL, 0)
SET IDENTITY_INSERT [dbo].[Dechet] OFF
SET IDENTITY_INSERT [dbo].[DieuChuyenQuyetToan] ON 

INSERT [dbo].[DieuChuyenQuyetToan] ([DieuChuyenQuyetToanId], [Title], [SoChungTu], [KhoNhapId], [KhoXuatId], [DaXem], [LoaiDieuChuyenQuyetToan], [LyDoXuat], [NguoiNhan], [CanCu], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status]) VALUES (2, N'32234234 aa', N'12aa', 1, 2, NULL, 1, N'12', NULL, NULL, CAST(0x0000AA99010B83A4 AS DateTime), NULL, N'admin@nhonhoa.vn', NULL, 1)
SET IDENTITY_INSERT [dbo].[DieuChuyenQuyetToan] OFF
SET IDENTITY_INSERT [dbo].[DinhMucSanXuat] ON 

INSERT [dbo].[DinhMucSanXuat] ([DinhMucSanXuatId], [Title], [MaDinhMucSanXuat], [ChiTietVatTuId], [SoChungTu], [MaCapPhat], [Loai], [DienGiai], [GhiChu], [KhoQuyetToanId], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status]) VALUES (1, N'32234234 aa', N'ab123', NULL, N'12aa', N'ab123', N'2', NULL, NULL, 1, CAST(0x0000AA9D00F7E911 AS DateTime), NULL, N'admin@nhonhoa.vn', NULL, 0)
SET IDENTITY_INSERT [dbo].[DinhMucSanXuat] OFF
SET IDENTITY_INSERT [dbo].[DoiMaVatTu] ON 

INSERT [dbo].[DoiMaVatTu] ([DoiMaVatTuId], [MaLoaiVatTu], [Title], [Cap], [GhiChu], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status]) VALUES (1, N'3424', N'32234234 aa', 2, N'dfsdfsdf', CAST(0x0000AA9400F39C8D AS DateTime), NULL, N'admin@nhonhoa.vn', NULL, 0)
SET IDENTITY_INSERT [dbo].[DoiMaVatTu] OFF
SET IDENTITY_INSERT [dbo].[Kho] ON 

INSERT [dbo].[Kho] ([KhoId], [MaKho], [Title], [NguoiGiao], [XuongId], [ThongTinKho], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status]) VALUES (1, N'Kho1', N'Kho vật tư', N'acc', NULL, N'ssss', CAST(0x0000AA99010B462C AS DateTime), NULL, N'admin@nhonhoa.vn', NULL, 1)
INSERT [dbo].[Kho] ([KhoId], [MaKho], [Title], [NguoiGiao], [XuongId], [ThongTinKho], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status]) VALUES (2, N'kho2', N'Kho số 2', N'acc', NULL, N'abc', CAST(0x0000AA99010B6186 AS DateTime), CAST(0x0000AA9D0110F0C9 AS DateTime), N'admin@nhonhoa.vn', N'admin@nhonhoa.vn', 1)
SET IDENTITY_INSERT [dbo].[Kho] OFF
SET IDENTITY_INSERT [dbo].[LoaiVatTu] ON 

INSERT [dbo].[LoaiVatTu] ([LoaiVatTuId], [MaLoaiVatTu], [Title], [Cap], [Type], [GhiChu], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status]) VALUES (1, N'LVT001', N'Nhóm vật tư 1', 1, 0, N'dfsdfsdf', CAST(0x0000AA9D01106377 AS DateTime), NULL, N'admin@nhonhoa.vn', NULL, 0)
SET IDENTITY_INSERT [dbo].[LoaiVatTu] OFF
SET IDENTITY_INSERT [dbo].[ModuleAction] ON 

INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (1, N'User', N'Listing', N'User listing', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (2, N'User', N'Create', N'Create new a user', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (3, N'User', N'Edit', N'Update user', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (4, N'User', N'Delete', N'Delete User', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (5, N'Role', N'Create', N'Create new a role', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (6, N'Role', N'Edit', N'Update role', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (7, N'Role', N'Listing', N'Role listing', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (8, N'Role', N'Delete', N'Delete role', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (10, N'Customer', N'Edit', N'Update Customer', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (11, N'Customer', N'Listing', N'Customer listing', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (12, N'Customer', N'Delete', N'Delete Customer', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (13, N'Customer', N'Create', N'Create customer', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (15, N'Role', N'Invisible', N'Disable ', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (65, N'Customer', N'Invisible', N'Disable customer', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (66, N'User', N'Invisible', N'Delete User', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (67, N'Order', N'OrderPendingListing', N'List of pending order ', 0)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (68, N'Order', N'DepositOrder', N'Deposit order', 5)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (69, N'Order', N'CancelOrderDeposited', N'Cancel order deposited', 6)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (70, N'Order', N'OrderPendingQuoteListing', N'Order Pending Quote Listing', 2)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (71, N'Order', N'Quote', N'Quote order', 4)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (72, N'Order', N'MergeOrder', N'Merge order', 3)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (73, N'Order', N'Latch', N'Latch Order', 7)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (74, N'Order', N'Detail', N'Detail of order', 4)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (75, N'Order', N'ShopOrderDetail', N'Detail of shop order detail', 8)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (76, N'Order', N'PurchaseOrder', NULL, 8)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (77, N'Order', N'CancelOrder', N'Cancel Order', 9)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (78, N'Order', N'OrderQuoteListing', N'Order Quote Listing', 4)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (79, N'Order', N'OrderPurchasingListing', N'Order Purchasing Listing', 8)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (80, N'Order', N'OrderPaidListing', N'Order Paid Listing', 10)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (81, N'Order', N'ShopperOrderListing', N'Shopper Order Listing', 12)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (82, N'Order', N'ImportChinaStoreListing', N'Order Chinese Inventory Receiving Listing', 13)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (83, N'Order', N'ExportChinaStoreListing', N'Order Chinese Inventory Delivering Listing', 14)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (84, N'Order', N'ImportVietNamStoreListing', N'Order Vietnamese Inventory Receiving Listing', 15)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (85, N'Order', N'ExportVietNamStoreListing', N'Order Vietnamese Inventory Delivering Listing', 16)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (86, N'Order', N'OrderDone', N'Order Done', 17)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (87, N'Order', N'SearchLanding', N'Search lading', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (88, N'OrderTrash', N'Listing', N'Listing', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (89, N'OrderTrash', N'Detail', N'Detail', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (90, N'OrderTrash', N'Restore', N'Restore', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (91, N'Notification', N'Create', N'Create', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (92, N'Notification', N'Edit', N'Edit', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (93, N'Notification', N'Listing', N'Listing', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (94, N'Notification', N'Invisible', N'Disable', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (95, N'Notification', N'Delete', N'Delete', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (96, N'Transaction', N'Listing', N'Listing', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (97, N'Transaction', N'Detail', N'Detail', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (98, N'Customer', N'Transaction', N'Transaction', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (99, N'Customer', N'RemoteLogin', N'RemoteLogin', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (100, N'Complaint', N'Listing', N'Listing', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (101, N'Complaint', N'Detail', N'Detail', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (102, N'Complaint', N'Cancel', N'Cancel', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (103, N'Complaint', N'Approve', N'Aprove', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (104, N'Order', N'ProcessOrder', N'Process order', 1)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (106, N'Order', N'AwaitPurchaseOrder', N'Await Purchase Order', 8)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (107, N'Order', N'RollBackToQuoteOrder', N'RollBack To Quote Order', 7)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (108, N'Order', N'EditBoughtOrder', N'Edit Bought Order', 9)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (109, N'Order', N'UpdateShippingCode', N'UpdateShippingCode', 10)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (110, N'Order', N'SearchShopOrder', N'SearchShopOrder', 12)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (111, N'Order', N'ProcessImportChinaStore', N'ProcessImportChinaStore', 13)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (112, N'Order', N'ReceivePackage', NULL, 14)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (113, N'Order', N'ProcessImportVietnamStore', NULL, 15)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (114, N'Order', N'ProcessExportVietnamStore', NULL, 16)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (115, N'Order', N'AddPackage', NULL, 14)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (116, N'Order', N'AddLandingCodeToPackage', NULL, 14)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (117, N'Order', N'DeletePackage', NULL, 14)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (118, N'Order', N'DeletePackageItem', NULL, 14)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (119, N'Order', N'UpdatePackage', NULL, 14)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (120, N'User', N'Invisibe', N'Invisibe', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (121, N'Configuration', N'Setting', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (122, N'Notification', N'Create', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (123, N'Notification', N'Edit', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (124, N'Notification', N'Listing', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (125, N'Notification', N'Delete', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (126, N'Notification', N'Invisible', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (127, N'Role', N'Invisible', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (128, N'Transaction', N'History', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (129, N'Transaction', N'Withdrawal', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (130, N'Transaction', N'Edit', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (131, N'Transaction', N'Delete', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (132, N'Transaction', N'ReCharge', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (133, N'Transaction', N'Refund', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (134, N'Credit', N'Listing', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (135, N'Credit', N'Info', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (136, N'Credit', N'ChargeToNV', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (137, N'Credit', N'ChargeToAdmin', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (138, N'Credit', N'WithdrawCreditAdmin', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (139, N'Credit', N'RefundUserCredit', NULL, NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (143, N'CarrierOrder', N'Create', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (144, N'CarrierOrder', N'Edit', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (145, N'CarrierOrder', N'Listing', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (146, N'CarrierOrder', N'Delete', N'', NULL)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (148, N'Order', N'PenddingDepositListing', NULL, 7)
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (149, N'Order', N'ReadyExportVNStoreListing', NULL, 17)
SET IDENTITY_INSERT [dbo].[ModuleAction] OFF
SET IDENTITY_INSERT [dbo].[NhomSanPham] ON 

INSERT [dbo].[NhomSanPham] ([NhomSanPhamId], [MaNhomSanPham], [Title], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status]) VALUES (1, N'Nhom1', N'Nhóm 1', CAST(0x0000AA9D01101096 AS DateTime), NULL, N'admin@nhonhoa.vn', NULL, 0)
SET IDENTITY_INSERT [dbo].[NhomSanPham] OFF
SET IDENTITY_INSERT [dbo].[Notification] ON 

INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (51, N'<p>Admin Logistics gửi bạn bảng báo giá cho đơn hàng <strong>DH4131711828</strong></p><p>Chi tiết vui lòng xem tại : <a target=''_blank'' href=''/Order''>Đơn hàng đã báo giá</a></p>', 2, 0, CAST(0x0000A6FF0179D7B6 AS DateTime), N'Báo giá đơn hàng DH4131711828', NULL, 1)
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (52, N'<p>Dear anh chị.</p>
<p>Admin đã mua đơn hàng DH4131711828. Anh chị kiểm tra lại đơn hàng những phần đã ghi chú lại (nếu có).</p>
<p>Khi hàng về Việt Nam bộ phận kho sẽ thông báo để anh chị làm thủ tục nhận hàng và chuyển xe đi tỉnh nếu cần.</p>
<p>Sau 7 ngày anh chị có thể gọi điện cho <strong>BP Kho : 04 39956222 </strong> để hỏi về tình trạng hàng hóa.</p>
<p><strong>Xin cảm ơn!</strong></p>

', 2, 0, CAST(0x0000A6FF017A4898 AS DateTime), N'Đã mua xong đơn hàng DH4131711828', NULL, 1)
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (53, N'<p>Admin Logistics gửi bạn bảng báo giá cho đơn hàng <strong>DH6131711826</strong></p><p>Chi tiết vui lòng xem tại : <a target=''_blank'' href=''/Order''>Đơn hàng đã báo giá</a></p>', 2, 0, CAST(0x0000A6FF017E517C AS DateTime), N'Báo giá đơn hàng DH6131711826', NULL, 1)
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (54, N'<p>Dear anh chị.</p>
<p>Admin đã mua đơn hàng DH6131711826. Anh chị kiểm tra lại đơn hàng những phần đã ghi chú lại (nếu có).</p>
<p>Khi hàng về Việt Nam bộ phận kho sẽ thông báo để anh chị làm thủ tục nhận hàng và chuyển xe đi tỉnh nếu cần.</p>
<p>Sau 7 ngày anh chị có thể gọi điện cho <strong>BP Kho : 04 39956222 </strong> để hỏi về tình trạng hàng hóa.</p>
<p><strong>Xin cảm ơn!</strong></p>

', 2, 0, CAST(0x0000A6FF017E945C AS DateTime), N'Đã mua xong đơn hàng DH6131711826', NULL, 1)
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (55, N'<p>Dear anh chị.</p>
<p>Admin đã mua đơn hàng DH3131711859. Anh chị kiểm tra lại đơn hàng những phần đã ghi chú lại (nếu có).</p>
<p>Khi hàng về Việt Nam bộ phận kho sẽ thông báo để anh chị làm thủ tục nhận hàng và chuyển xe đi tỉnh nếu cần.</p>
<p>Sau 7 ngày anh chị có thể gọi điện cho <strong>BP Kho : 04 39956222 </strong> để hỏi về tình trạng hàng hóa.</p>
<p><strong>Xin cảm ơn!</strong></p>

', 2, 0, CAST(0x0000A6FF017EAABB AS DateTime), N'Đã mua xong đơn hàng DH3131711859', NULL, 1)
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (56, N'<p>Admin Logistics gửi bạn bảng báo giá cho đơn hàng <strong>DH6131711954</strong></p><p>Chi tiết vui lòng xem tại : <a target=''_blank'' href=''/Order''>Đơn hàng đã báo giá</a></p>', 2, 0, CAST(0x0000A70000A68A4E AS DateTime), N'Báo giá đơn hàng DH6131711954', NULL, 1)
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (57, N'<p>Dear anh chị.</p>
<p>Admin đã mua đơn hàng DH6131711954. Anh chị kiểm tra lại đơn hàng những phần đã ghi chú lại (nếu có).</p>
<p>Khi hàng về Việt Nam bộ phận kho sẽ thông báo để anh chị làm thủ tục nhận hàng và chuyển xe đi tỉnh nếu cần.</p>
<p>Sau 7 ngày anh chị có thể gọi điện cho <strong>BP Kho : 04 39956222 </strong> để hỏi về tình trạng hàng hóa.</p>
<p><strong>Xin cảm ơn!</strong></p>

', 2, 0, CAST(0x0000A70000B9E554 AS DateTime), N'Đã mua xong đơn hàng DH6131711954', NULL, 1)
SET IDENTITY_INSERT [dbo].[Notification] OFF
SET IDENTITY_INSERT [dbo].[NotificationRecipient] ON 

INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (74, 51, 13, 1, CAST(0x0000A6FF0179D7D1 AS DateTime))
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (75, 52, 13, 0, CAST(0x0000A6FF017A48A2 AS DateTime))
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (76, 53, 13, 1, CAST(0x0000A6FF017E5185 AS DateTime))
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (77, 54, 13, 0, CAST(0x0000A6FF017E9468 AS DateTime))
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (78, 55, 13, 1, CAST(0x0000A6FF017EAAC6 AS DateTime))
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (79, 56, 13, 1, CAST(0x0000A70000A68A58 AS DateTime))
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (80, 57, 13, 1, CAST(0x0000A70000B9E55F AS DateTime))
SET IDENTITY_INSERT [dbo].[NotificationRecipient] OFF
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (16, N'R001', N'Check Đơn', N'Kiểm tra đơn hàng', 1, NULL, CAST(0x0000A6A1017B3593 AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (17, N'R002', N'Cộng tác viên', N'Cộng tác viên', 1, NULL, CAST(0x0000A6A1017B6C76 AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (18, N'R003', N'Kế toán', N'Kế toán', 1, NULL, CAST(0x0000A6A1017B9615 AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (19, N'R004', N'Kho Trung Quốc', N'Kho Trung Quốc', 1, NULL, CAST(0x0000A6A1017BB563 AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (20, N'R005', N'Kho Việt Nam', N'Kho Việt Nam', 1, NULL, CAST(0x0000A6A1017BDC4E AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (21, N'R006', N'Orders', N'Orders', 1, NULL, CAST(0x0000A6A1017BFEF8 AS DateTime), CAST(0x0000A6F3000866EA AS DateTime), N'admin@nhonhoa.vn', N'admin@nhonhoa.vn')
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (22, N'R007', N'Quản trị viên', N'Quản trị viên', 1, NULL, CAST(0x0000A6A1017C1B3A AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
SET IDENTITY_INSERT [dbo].[Roles] OFF
SET IDENTITY_INSERT [dbo].[ThanhPhamXuong] ON 

INSERT [dbo].[ThanhPhamXuong] ([ThanhPhamXuongId], [Title], [XuongId], [VatTuId], [NhomSanPhamId], [Loai], [NangSuatTuan], [LoaiVatLieu], [QuyCach], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status]) VALUES (7, N'Thành phẩm 01', 1, 2, 1, N'1', N'6', N'12', N'12', CAST(0x0000AA9D012DEE34 AS DateTime), NULL, N'admin@nhonhoa.vn', NULL, 0)
SET IDENTITY_INSERT [dbo].[ThanhPhamXuong] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (6, N'Admin', N'admin@nhonhoa.vn', N'1000:paqv42x0zf32cagcDdRNXcpLPTqVDovH:0+A7K0tvhPf6HV3HU2CZ2tUYYIAlTu9X', N'1000:4XwRFklHMIqnUCvhhZxx8Njut1rgzX3+:UiUrwx8Kuhyw3aRx0f2JZETaWv6PjXyw', 1, CAST(0x0000A69500000000 AS DateTime), CAST(0x0000A69500000000 AS DateTime), CAST(0x0000A69500000000 AS DateTime), CAST(0x0000A69500000000 AS DateTime), 0, NULL, 1, N'', NULL)
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (27, N'Ke Toan', N'ketoan@taobao.com.vn', N'1000:prLoKfnzcM2TLaqgUyL9hWhzwhGp260o:vyIEznpGv2JZ/W4quiEha6p8VZi1rJPv', N'1000:4XwRFklHMIqnUCvhhZxx8Njut1rgzX3+:UiUrwx8Kuhyw3aRx0f2JZETaWv6PjXyw', 0, CAST(0x0000A69500000000 AS DateTime), NULL, CAST(0x0000A69500000000 AS DateTime), CAST(0x0000A69500000000 AS DateTime), 0, NULL, 1, NULL, NULL)
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (44, N'Nguyen Van Ngu', N'ngunv@nhonhoa.vn', N'1000:Ey/r8F+D3FxlWNFkqOcnsWJgtPKl2Gtd:waDb3mz1VcVCXYwZWgETDWbmoiSpXoV9', NULL, 0, CAST(0x0000A6A2003053FB AS DateTime), CAST(0x0000A789010D9411 AS DateTime), NULL, NULL, 0, NULL, 1, NULL, NULL)
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (45, N'order', N'order@nhonhoa.vn', N'1000:Jpq5c+X4lEJ1PerLJHCPQYWDhbPjHysy:/Ki90iy696NhXzTQDVfnArvX+XvUMfSZ', NULL, 0, CAST(0x0000A6A20082C626 AS DateTime), CAST(0x0000A6A200883524 AS DateTime), NULL, NULL, 0, NULL, 1, NULL, NULL)
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (46, N'Cộng tác viên', N'congtacvien@nhonhoa.vn', N'1000:roLna4oGLzr7BL3W5aUd0AUcP6pnSils:WeFI3IC1CO3wsfcIJqOiYH6oq9nOqG3j', NULL, 0, CAST(0x0000A6A200897EA8 AS DateTime), NULL, NULL, NULL, 0, NULL, 1, NULL, NULL)
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (47, N'fdgdfg', N'sdsfsdf@gfg.cc', N'1000:2dqjQtKdF+fYLOTqys6zLwi99xRYmqDb:mKSgx9wuMx7FMSJzRWptxtqwG8Tfpsot', NULL, 0, CAST(0x0000A789011E3AE2 AS DateTime), NULL, NULL, NULL, 0, NULL, 0, NULL, NULL)
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (48, NULL, N'sâdsa', N'1000:slRi5zRLSuKv9BKnMT5zsDGXw/NL6l/+:RRL4Z6Sh9bYofEvvRtkK9JQ8a7y9bp4b', NULL, 0, CAST(0x0000AA19000C5666 AS DateTime), NULL, NULL, NULL, 0, NULL, 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[UserRole] ON 

INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (1, 27, 18, CAST(0x0000A6A100000000 AS DateTime))
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (6, 44, 19, CAST(0x0000A6A200305403 AS DateTime))
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (7, 45, 21, CAST(0x0000A6A20082C670 AS DateTime))
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (8, 46, 17, CAST(0x0000A6A20089803B AS DateTime))
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (9, 47, 16, CAST(0x0000A789011E64CE AS DateTime))
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (10, 48, 16, CAST(0x0000AA19000C567C AS DateTime))
SET IDENTITY_INSERT [dbo].[UserRole] OFF
SET IDENTITY_INSERT [dbo].[Xuong] ON 

INSERT [dbo].[Xuong] ([XuongId], [MaXuong], [Title], [ThongTinXuong], [GhiChu], [Type], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status]) VALUES (1, N'X001', N'Xưởng 01', N'Xưởng 01', N'xưởng', 1, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Xuong] OFF
ALTER TABLE [dbo].[CoPhanCapXuong]  WITH CHECK ADD  CONSTRAINT [FK_CoPhanCapXuong_Kho] FOREIGN KEY([KhoNhapId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[CoPhanCapXuong] CHECK CONSTRAINT [FK_CoPhanCapXuong_Kho]
GO
ALTER TABLE [dbo].[CoPhanCapXuong]  WITH CHECK ADD  CONSTRAINT [FK_CoPhanCapXuong_Kho1] FOREIGN KEY([KhoXuatId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[CoPhanCapXuong] CHECK CONSTRAINT [FK_CoPhanCapXuong_Kho1]
GO
ALTER TABLE [dbo].[CoPhanCapXuong]  WITH CHECK ADD  CONSTRAINT [FK_CoPhanCapXuong_LoaiVatTu] FOREIGN KEY([VatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[CoPhanCapXuong] CHECK CONSTRAINT [FK_CoPhanCapXuong_LoaiVatTu]
GO
ALTER TABLE [dbo].[CoPhanCapXuong]  WITH CHECK ADD  CONSTRAINT [FK_CoPhanCapXuong_NhomSanPham] FOREIGN KEY([NhomSanPhamId])
REFERENCES [dbo].[NhomSanPham] ([NhomSanPhamId])
GO
ALTER TABLE [dbo].[CoPhanCapXuong] CHECK CONSTRAINT [FK_CoPhanCapXuong_NhomSanPham]
GO
ALTER TABLE [dbo].[DanhSachChiTiet]  WITH CHECK ADD  CONSTRAINT [FK_DanhSachChiTiet_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[DanhSachChiTiet] CHECK CONSTRAINT [FK_DanhSachChiTiet_ChiTietVatTu]
GO
ALTER TABLE [dbo].[DanhSachChiTiet]  WITH CHECK ADD  CONSTRAINT [FK_DanhSachChiTiet_DanhSachChiTiet] FOREIGN KEY([ParentId])
REFERENCES [dbo].[DanhSachChiTiet] ([DanhSachChiTietId])
GO
ALTER TABLE [dbo].[DanhSachChiTiet] CHECK CONSTRAINT [FK_DanhSachChiTiet_DanhSachChiTiet]
GO
ALTER TABLE [dbo].[DanhSachChiTiet]  WITH CHECK ADD  CONSTRAINT [FK_DanhSachChiTiet_DinhMucCatTole] FOREIGN KEY([DinhMucCatToleId])
REFERENCES [dbo].[DinhMucCatTole] ([DinhMucCatToleId])
GO
ALTER TABLE [dbo].[DanhSachChiTiet] CHECK CONSTRAINT [FK_DanhSachChiTiet_DinhMucCatTole]
GO
ALTER TABLE [dbo].[DanhSachChiTiet]  WITH CHECK ADD  CONSTRAINT [FK_DanhSachChiTiet_DinhMucSanXuat] FOREIGN KEY([DinhMucSanXuatId])
REFERENCES [dbo].[DinhMucSanXuat] ([DinhMucSanXuatId])
GO
ALTER TABLE [dbo].[DanhSachChiTiet] CHECK CONSTRAINT [FK_DanhSachChiTiet_DinhMucSanXuat]
GO
ALTER TABLE [dbo].[DanhSachDieuChuyenQuyetToan]  WITH CHECK ADD  CONSTRAINT [FK_DanhSachDieuChuyenQuyetToan_LoaiVatTu] FOREIGN KEY([VatTuDiId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[DanhSachDieuChuyenQuyetToan] CHECK CONSTRAINT [FK_DanhSachDieuChuyenQuyetToan_LoaiVatTu]
GO
ALTER TABLE [dbo].[DanhSachDieuChuyenQuyetToan]  WITH CHECK ADD  CONSTRAINT [FK_DanhSachDieuChuyenQuyetToan_LoaiVatTu1] FOREIGN KEY([VatTuVeId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[DanhSachDieuChuyenQuyetToan] CHECK CONSTRAINT [FK_DanhSachDieuChuyenQuyetToan_LoaiVatTu1]
GO
ALTER TABLE [dbo].[DanhSachDieuChuyenQuyetToan]  WITH CHECK ADD  CONSTRAINT [FK_ListTranferSettlement_TranferSettlement] FOREIGN KEY([DieuChuyenQuyetToanId])
REFERENCES [dbo].[DieuChuyenQuyetToan] ([DieuChuyenQuyetToanId])
GO
ALTER TABLE [dbo].[DanhSachDieuChuyenQuyetToan] CHECK CONSTRAINT [FK_ListTranferSettlement_TranferSettlement]
GO
ALTER TABLE [dbo].[DieuChuyenQuyetToan]  WITH CHECK ADD  CONSTRAINT [FK_TranferSettlement_Kho] FOREIGN KEY([KhoNhapId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[DieuChuyenQuyetToan] CHECK CONSTRAINT [FK_TranferSettlement_Kho]
GO
ALTER TABLE [dbo].[DieuChuyenQuyetToan]  WITH CHECK ADD  CONSTRAINT [FK_TranferSettlement_Kho1] FOREIGN KEY([KhoXuatId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[DieuChuyenQuyetToan] CHECK CONSTRAINT [FK_TranferSettlement_Kho1]
GO
ALTER TABLE [dbo].[DinhMucCatTole]  WITH CHECK ADD  CONSTRAINT [FK_DinhMucCatTole_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[DinhMucCatTole] CHECK CONSTRAINT [FK_DinhMucCatTole_ChiTietVatTu]
GO
ALTER TABLE [dbo].[DinhMucCatTole]  WITH CHECK ADD  CONSTRAINT [FK_DinhMucCatTole_Tole] FOREIGN KEY([ToleId])
REFERENCES [dbo].[Tole] ([ToleId])
GO
ALTER TABLE [dbo].[DinhMucCatTole] CHECK CONSTRAINT [FK_DinhMucCatTole_Tole]
GO
ALTER TABLE [dbo].[DinhMucSanXuat]  WITH CHECK ADD  CONSTRAINT [FK_DinhMucSanXuat_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[DinhMucSanXuat] CHECK CONSTRAINT [FK_DinhMucSanXuat_ChiTietVatTu]
GO
ALTER TABLE [dbo].[DinhMucSanXuat]  WITH CHECK ADD  CONSTRAINT [FK_DinhMucSanXuat_Kho] FOREIGN KEY([KhoQuyetToanId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[DinhMucSanXuat] CHECK CONSTRAINT [FK_DinhMucSanXuat_Kho]
GO
ALTER TABLE [dbo].[DinhMucTuan]  WITH CHECK ADD  CONSTRAINT [FK_DinhMucTuan_LoaiVatTu] FOREIGN KEY([VatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[DinhMucTuan] CHECK CONSTRAINT [FK_DinhMucTuan_LoaiVatTu]
GO
ALTER TABLE [dbo].[GoiDau]  WITH CHECK ADD  CONSTRAINT [FK_GoiDau_LoaiVatTu] FOREIGN KEY([VatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[GoiDau] CHECK CONSTRAINT [FK_GoiDau_LoaiVatTu]
GO
ALTER TABLE [dbo].[GoiDau]  WITH CHECK ADD  CONSTRAINT [FK_GoiDau_PKHKeHoachSXNKXH] FOREIGN KEY([PKHKeHoachSXNKXHId])
REFERENCES [dbo].[PKHKeHoachSXNKXH] ([PKHKeHoachSXNKXHId])
GO
ALTER TABLE [dbo].[GoiDau] CHECK CONSTRAINT [FK_GoiDau_PKHKeHoachSXNKXH]
GO
ALTER TABLE [dbo].[Kho]  WITH CHECK ADD  CONSTRAINT [FK_Kho_Xuong] FOREIGN KEY([XuongId])
REFERENCES [dbo].[Xuong] ([XuongId])
GO
ALTER TABLE [dbo].[Kho] CHECK CONSTRAINT [FK_Kho_Xuong]
GO
ALTER TABLE [dbo].[KhoTinhTon]  WITH CHECK ADD  CONSTRAINT [FK_KhoTinhTon_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[KhoTinhTon] CHECK CONSTRAINT [FK_KhoTinhTon_Kho]
GO
ALTER TABLE [dbo].[KhoTinhTon]  WITH CHECK ADD  CONSTRAINT [FK_KhoTinhTon_NhomSanPham] FOREIGN KEY([NhomSanPhamId])
REFERENCES [dbo].[NhomSanPham] ([NhomSanPhamId])
GO
ALTER TABLE [dbo].[KhoTinhTon] CHECK CONSTRAINT [FK_KhoTinhTon_NhomSanPham]
GO
ALTER TABLE [dbo].[NguoiNhanKho]  WITH CHECK ADD  CONSTRAINT [FK_NguoiNhanKho_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[NguoiNhanKho] CHECK CONSTRAINT [FK_NguoiNhanKho_Kho]
GO
ALTER TABLE [dbo].[NhomVatTuNhapRieng]  WITH CHECK ADD  CONSTRAINT [FK_NhomVatTuNhapRieng_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[NhomVatTuNhapRieng] CHECK CONSTRAINT [FK_NhomVatTuNhapRieng_Kho]
GO
ALTER TABLE [dbo].[NotificationRecipient]  WITH CHECK ADD  CONSTRAINT [FK_NotificationRecipient_Notification1] FOREIGN KEY([NotificationId])
REFERENCES [dbo].[Notification] ([NotificationId])
GO
ALTER TABLE [dbo].[NotificationRecipient] CHECK CONSTRAINT [FK_NotificationRecipient_Notification1]
GO
ALTER TABLE [dbo].[PKHDuKienGiaoHang]  WITH CHECK ADD  CONSTRAINT [FK_PKHDuKienGiaoHang_PKHPhieuYeuCauMuaHang] FOREIGN KEY([PKHPhieuYeuCauMuaHangId])
REFERENCES [dbo].[PKHPhieuYeuCauMuaHang] ([PKHPhieuYeuCauMuaHangId])
GO
ALTER TABLE [dbo].[PKHDuKienGiaoHang] CHECK CONSTRAINT [FK_PKHDuKienGiaoHang_PKHPhieuYeuCauMuaHang]
GO
ALTER TABLE [dbo].[PKHKeHoachNhapKhoThanhPhamDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachNhapKhoThanhPhamDetail_LoaiVatTu] FOREIGN KEY([VatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[PKHKeHoachNhapKhoThanhPhamDetail] CHECK CONSTRAINT [FK_PKHKeHoachNhapKhoThanhPhamDetail_LoaiVatTu]
GO
ALTER TABLE [dbo].[PKHKeHoachNhapKhoThanhPhamDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachNhapKhoThanhPhamDetail_PKHKeHoachXuong] FOREIGN KEY([PKHKeHoachXuongId])
REFERENCES [dbo].[PKHKeHoachXuong] ([PKHKeHoachXuongId])
GO
ALTER TABLE [dbo].[PKHKeHoachNhapKhoThanhPhamDetail] CHECK CONSTRAINT [FK_PKHKeHoachNhapKhoThanhPhamDetail_PKHKeHoachXuong]
GO
ALTER TABLE [dbo].[PKHKeHoachSXNKXHDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachSXNKXHDetail_LoaiVatTu] FOREIGN KEY([VatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[PKHKeHoachSXNKXHDetail] CHECK CONSTRAINT [FK_PKHKeHoachSXNKXHDetail_LoaiVatTu]
GO
ALTER TABLE [dbo].[PKHKeHoachSXNKXHDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachSXNKXHDetail_PKHKeHoachSXNKXH] FOREIGN KEY([PKHKeHoachSXNKXHId])
REFERENCES [dbo].[PKHKeHoachSXNKXH] ([PKHKeHoachSXNKXHId])
GO
ALTER TABLE [dbo].[PKHKeHoachSXNKXHDetail] CHECK CONSTRAINT [FK_PKHKeHoachSXNKXHDetail_PKHKeHoachSXNKXH]
GO
ALTER TABLE [dbo].[PKHKeHoachSXTuanDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachSXTuanDetail_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[PKHKeHoachSXTuanDetail] CHECK CONSTRAINT [FK_PKHKeHoachSXTuanDetail_ChiTietVatTu]
GO
ALTER TABLE [dbo].[PKHKeHoachSXTuanDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachSXTuanDetail_PKHKeHoachXuong] FOREIGN KEY([PKHKeHoachXuongId])
REFERENCES [dbo].[PKHKeHoachXuong] ([PKHKeHoachXuongId])
GO
ALTER TABLE [dbo].[PKHKeHoachSXTuanDetail] CHECK CONSTRAINT [FK_PKHKeHoachSXTuanDetail_PKHKeHoachXuong]
GO
ALTER TABLE [dbo].[PKHKeHoachXuong]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXuong_PKHKeHoachSXNKXH] FOREIGN KEY([PKHKeHoachSXNKXHId])
REFERENCES [dbo].[PKHKeHoachSXNKXH] ([PKHKeHoachSXNKXHId])
GO
ALTER TABLE [dbo].[PKHKeHoachXuong] CHECK CONSTRAINT [FK_PKHKeHoachXuong_PKHKeHoachSXNKXH]
GO
ALTER TABLE [dbo].[PKHKeHoachXuong]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXuong_PKHKeHoachXuong] FOREIGN KEY([XuongId])
REFERENCES [dbo].[Xuong] ([XuongId])
GO
ALTER TABLE [dbo].[PKHKeHoachXuong] CHECK CONSTRAINT [FK_PKHKeHoachXuong_PKHKeHoachXuong]
GO
ALTER TABLE [dbo].[PKHKeHoachXuongDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXuongDetail_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[PKHKeHoachXuongDetail] CHECK CONSTRAINT [FK_PKHKeHoachXuongDetail_ChiTietVatTu]
GO
ALTER TABLE [dbo].[PKHKeHoachXuongDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXuongDetail_DinhMucSanXuat] FOREIGN KEY([DinhMucSanXuatId])
REFERENCES [dbo].[DinhMucSanXuat] ([DinhMucSanXuatId])
GO
ALTER TABLE [dbo].[PKHKeHoachXuongDetail] CHECK CONSTRAINT [FK_PKHKeHoachXuongDetail_DinhMucSanXuat]
GO
ALTER TABLE [dbo].[PKHKeHoachXuongDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXuongDetail_PKHKeHoachSXNKXHDetail] FOREIGN KEY([PKHKeHoachSXNKXHDetailId])
REFERENCES [dbo].[PKHKeHoachSXNKXHDetail] ([PKHKeHoachSXNKXHDetailId])
GO
ALTER TABLE [dbo].[PKHKeHoachXuongDetail] CHECK CONSTRAINT [FK_PKHKeHoachXuongDetail_PKHKeHoachSXNKXHDetail]
GO
ALTER TABLE [dbo].[PKHKeHoachXuongDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKeHoachXuongDetail_PKHKeHoachXuong] FOREIGN KEY([PKHKeHoachXuongId])
REFERENCES [dbo].[PKHKeHoachXuong] ([PKHKeHoachXuongId])
GO
ALTER TABLE [dbo].[PKHKeHoachXuongDetail] CHECK CONSTRAINT [FK_PKHKeHoachXuongDetail_PKHKeHoachXuong]
GO
ALTER TABLE [dbo].[PKHKiemHoa]  WITH CHECK ADD  CONSTRAINT [FK_PKHKiemHoa_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[PKHKiemHoa] CHECK CONSTRAINT [FK_PKHKiemHoa_Kho]
GO
ALTER TABLE [dbo].[PKHKiemHoaDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKiemHoaDetail_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[PKHKiemHoaDetail] CHECK CONSTRAINT [FK_PKHKiemHoaDetail_ChiTietVatTu]
GO
ALTER TABLE [dbo].[PKHKiemHoaDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHKiemHoaDetail_PKHKiemHoa] FOREIGN KEY([PKHKiemHoaId])
REFERENCES [dbo].[PKHKiemHoa] ([PKHKiemHoaId])
GO
ALTER TABLE [dbo].[PKHKiemHoaDetail] CHECK CONSTRAINT [FK_PKHKiemHoaDetail_PKHKiemHoa]
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTu]  WITH CHECK ADD  CONSTRAINT [FK_PKHPhieuNhapVatTu_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTu] CHECK CONSTRAINT [FK_PKHPhieuNhapVatTu_Kho]
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTu]  WITH CHECK ADD  CONSTRAINT [FK_PKHPhieuNhapVatTu_PKHKiemHoa] FOREIGN KEY([PKHKiemHoaId])
REFERENCES [dbo].[PKHKiemHoa] ([PKHKiemHoaId])
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTu] CHECK CONSTRAINT [FK_PKHPhieuNhapVatTu_PKHKiemHoa]
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTuDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHPhieuNhapVatTuDetail_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTuDetail] CHECK CONSTRAINT [FK_PKHPhieuNhapVatTuDetail_ChiTietVatTu]
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTuDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHPhieuNhapVatTuDetail_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTuDetail] CHECK CONSTRAINT [FK_PKHPhieuNhapVatTuDetail_Kho]
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTuDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHPhieuNhapVatTuDetail_PKHPhieuNhapVatTu] FOREIGN KEY([PKHPhieuNhapVatTuId])
REFERENCES [dbo].[PKHPhieuNhapVatTu] ([PKHPhieuNhapVatTuId])
GO
ALTER TABLE [dbo].[PKHPhieuNhapVatTuDetail] CHECK CONSTRAINT [FK_PKHPhieuNhapVatTuDetail_PKHPhieuNhapVatTu]
GO
ALTER TABLE [dbo].[PKHPhieuYeuCauMuaHangDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHPhieuYeuCauMuaHangDetail_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[PKHPhieuYeuCauMuaHangDetail] CHECK CONSTRAINT [FK_PKHPhieuYeuCauMuaHangDetail_ChiTietVatTu]
GO
ALTER TABLE [dbo].[PKHPhieuYeuCauMuaHangDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHPhieuYeuCauMuaHangDetail_NhomYeuCau] FOREIGN KEY([NhomYeuCauId])
REFERENCES [dbo].[NhomYeuCau] ([NhomYeuCauId])
GO
ALTER TABLE [dbo].[PKHPhieuYeuCauMuaHangDetail] CHECK CONSTRAINT [FK_PKHPhieuYeuCauMuaHangDetail_NhomYeuCau]
GO
ALTER TABLE [dbo].[PKHPhieuYeuCauMuaHangDetail]  WITH CHECK ADD  CONSTRAINT [FK_PKHPhieuYeuCauMuaHangDetail_PKHPhieuYeuCauMuaHang] FOREIGN KEY([PKHPhieuYeuCauMuaHangId])
REFERENCES [dbo].[PKHPhieuYeuCauMuaHang] ([PKHPhieuYeuCauMuaHangId])
GO
ALTER TABLE [dbo].[PKHPhieuYeuCauMuaHangDetail] CHECK CONSTRAINT [FK_PKHPhieuYeuCauMuaHangDetail_PKHPhieuYeuCauMuaHang]
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang]  WITH CHECK ADD  CONSTRAINT [FK_PKHTheoDoiGiaoHang_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang] CHECK CONSTRAINT [FK_PKHTheoDoiGiaoHang_ChiTietVatTu]
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang]  WITH CHECK ADD  CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHDuKienGiaoHang] FOREIGN KEY([PKHDuKienGiaoHangId])
REFERENCES [dbo].[PKHDuKienGiaoHang] ([PKHDuKienGiaoHangId])
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang] CHECK CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHDuKienGiaoHang]
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang]  WITH CHECK ADD  CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHKiemHoa] FOREIGN KEY([PKHKiemHoaId])
REFERENCES [dbo].[PKHKiemHoa] ([PKHKiemHoaId])
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang] CHECK CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHKiemHoa]
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang]  WITH CHECK ADD  CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHPhieuYeuCauMuaHang] FOREIGN KEY([PKHPhieuYeuCauMuaHangId])
REFERENCES [dbo].[PKHPhieuYeuCauMuaHang] ([PKHPhieuYeuCauMuaHangId])
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang] CHECK CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHPhieuYeuCauMuaHang]
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang]  WITH CHECK ADD  CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHPhieuYeuCauMuaHangDetail] FOREIGN KEY([PKHPhieuYeuCauMuaHangDetailId])
REFERENCES [dbo].[PKHPhieuYeuCauMuaHangDetail] ([PKHPhieuYeuCauMuaHangDetailId])
GO
ALTER TABLE [dbo].[PKHTheoDoiGiaoHang] CHECK CONSTRAINT [FK_PKHTheoDoiGiaoHang_PKHPhieuYeuCauMuaHangDetail]
GO
ALTER TABLE [dbo].[RoleModuleAction]  WITH CHECK ADD  CONSTRAINT [FK_RoleModuleAction_ModuleAction] FOREIGN KEY([ModuleActionID])
REFERENCES [dbo].[ModuleAction] ([ModuleActionID])
GO
ALTER TABLE [dbo].[RoleModuleAction] CHECK CONSTRAINT [FK_RoleModuleAction_ModuleAction]
GO
ALTER TABLE [dbo].[RoleModuleAction]  WITH CHECK ADD  CONSTRAINT [FK_RoleModuleAction_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[RoleModuleAction] CHECK CONSTRAINT [FK_RoleModuleAction_Roles]
GO
ALTER TABLE [dbo].[SoDuBanDau]  WITH CHECK ADD  CONSTRAINT [FK_SoDuBanDau_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[SoDuBanDau] CHECK CONSTRAINT [FK_SoDuBanDau_Kho]
GO
ALTER TABLE [dbo].[SoDuBanDau]  WITH CHECK ADD  CONSTRAINT [FK_SoDuBanDau_LoaiVatTu] FOREIGN KEY([VatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[SoDuBanDau] CHECK CONSTRAINT [FK_SoDuBanDau_LoaiVatTu]
GO
ALTER TABLE [dbo].[ThanhPhamXuong]  WITH CHECK ADD  CONSTRAINT [FK_ThanhPhamXuong_LoaiVatTu] FOREIGN KEY([VatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[ThanhPhamXuong] CHECK CONSTRAINT [FK_ThanhPhamXuong_LoaiVatTu]
GO
ALTER TABLE [dbo].[ThanhPhamXuong]  WITH CHECK ADD  CONSTRAINT [FK_ThanhPhamXuong_NhomSanPham] FOREIGN KEY([NhomSanPhamId])
REFERENCES [dbo].[NhomSanPham] ([NhomSanPhamId])
GO
ALTER TABLE [dbo].[ThanhPhamXuong] CHECK CONSTRAINT [FK_ThanhPhamXuong_NhomSanPham]
GO
ALTER TABLE [dbo].[ThanhPhamXuong]  WITH CHECK ADD  CONSTRAINT [FK_ThanhPhamXuong_Xuong] FOREIGN KEY([XuongId])
REFERENCES [dbo].[Xuong] ([XuongId])
GO
ALTER TABLE [dbo].[ThanhPhamXuong] CHECK CONSTRAINT [FK_ThanhPhamXuong_Xuong]
GO
ALTER TABLE [dbo].[Tole]  WITH CHECK ADD  CONSTRAINT [FK_Tole_Dechet] FOREIGN KEY([DechetId])
REFERENCES [dbo].[Dechet] ([DechetId])
GO
ALTER TABLE [dbo].[Tole] CHECK CONSTRAINT [FK_Tole_Dechet]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Role]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_User]
GO
ALTER TABLE [dbo].[VatTuCapRieng]  WITH CHECK ADD  CONSTRAINT [FK_VatTuCapRieng_LoaiVatTu] FOREIGN KEY([VatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[VatTuCapRieng] CHECK CONSTRAINT [FK_VatTuCapRieng_LoaiVatTu]
GO
ALTER TABLE [dbo].[VatTuCapRieng]  WITH CHECK ADD  CONSTRAINT [FK_VatTuCapRieng_NhomSanPham] FOREIGN KEY([NhomSanPhamId])
REFERENCES [dbo].[NhomSanPham] ([NhomSanPhamId])
GO
ALTER TABLE [dbo].[VatTuCapRieng] CHECK CONSTRAINT [FK_VatTuCapRieng_NhomSanPham]
GO
ALTER TABLE [dbo].[VatTuNhapRieng]  WITH CHECK ADD  CONSTRAINT [FK_VatTuNhapRieng_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[VatTuNhapRieng] CHECK CONSTRAINT [FK_VatTuNhapRieng_Kho]
GO
ALTER TABLE [dbo].[VatTuNhapRieng]  WITH CHECK ADD  CONSTRAINT [FK_VatTuNhapRieng_LoaiVatTu] FOREIGN KEY([VatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[VatTuNhapRieng] CHECK CONSTRAINT [FK_VatTuNhapRieng_LoaiVatTu]
GO
ALTER TABLE [dbo].[YeuCauMuaHang]  WITH CHECK ADD  CONSTRAINT [FK_YeuCauMuaHang_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[YeuCauMuaHang] CHECK CONSTRAINT [FK_YeuCauMuaHang_ChiTietVatTu]
GO
ALTER TABLE [dbo].[YeuCauMuaHang]  WITH CHECK ADD  CONSTRAINT [FK_YeuCauMuaHang_PKHPhieuYeuCauMuaHang] FOREIGN KEY([PKHPhieuYeuCauMuaHangId])
REFERENCES [dbo].[PKHPhieuYeuCauMuaHang] ([PKHPhieuYeuCauMuaHangId])
GO
ALTER TABLE [dbo].[YeuCauMuaHang] CHECK CONSTRAINT [FK_YeuCauMuaHang_PKHPhieuYeuCauMuaHang]
GO
ALTER TABLE [dbo].[YeuCauMuaHang]  WITH CHECK ADD  CONSTRAINT [FK_YeuCauMuaHang_Xuong] FOREIGN KEY([XuongId])
REFERENCES [dbo].[Xuong] ([XuongId])
GO
ALTER TABLE [dbo].[YeuCauMuaHang] CHECK CONSTRAINT [FK_YeuCauMuaHang_Xuong]
GO
ALTER TABLE [dbo].[YeuCauMuaHang]  WITH CHECK ADD  CONSTRAINT [FK_YeuCauMuaHang_YeuCauMuaHang] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[YeuCauMuaHang] CHECK CONSTRAINT [FK_YeuCauMuaHang_YeuCauMuaHang]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AccountType = 0(User  of client side), AccountType=1 User  of admin side' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountToken', @level2type=N'COLUMN',@level2name=N'IsAdminAccountSide'
GO
USE [master]
GO
ALTER DATABASE [EcommerceSystem_Dev] SET  READ_WRITE 
GO
