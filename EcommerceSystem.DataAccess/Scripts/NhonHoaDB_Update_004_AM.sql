USE [EcommerceSystem_Dev]
GO
/****** Object:  Table [dbo].[AccountToken]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountToken](
	[AccountTokenId] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [int] NOT NULL,
	[IsAdminAccountSide] [bit] NULL,
	[TokenKey] [varchar](500) NULL,
	[ExpiredDate] [datetime] NULL,
	[TokenType] [int] NOT NULL,
 CONSTRAINT [PK_AccountToken] PRIMARY KEY CLUSTERED 
(
	[AccountTokenId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChiTietVatTu]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietVatTu](
	[ChiTietVatTuId] [int] IDENTITY(1,1) NOT NULL,
	[MaChiTietVatTu] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[DonVi] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_ChiTietVatTu] PRIMARY KEY CLUSTERED 
(
	[ChiTietVatTuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Configuration]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Configuration](
	[ConfigurationId] [int] IDENTITY(1,1) NOT NULL,
	[ExchangeRate] [money] NULL,
	[AdminAcceptanceBalancePercentage] [int] NOT NULL,
	[ClientAcceptanceBalancePercentage] [int] NOT NULL,
 CONSTRAINT [PK_Configuration] PRIMARY KEY CLUSTERED 
(
	[ConfigurationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CoPhanCapXuong]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CoPhanCapXuong](
	[CoPhanCapXuongId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoNhapId] [int] NOT NULL,
	[KhoXuatId] [int] NOT NULL,
	[LoaiVatTuId] [int] NULL,
	[NhomSanPhamId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_CoPhanCapXuong] PRIMARY KEY CLUSTERED 
(
	[CoPhanCapXuongId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DanhSachChiTiet]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DanhSachChiTiet](
	[DanhSachChiTietId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[ChiTietVatTuId] [int] NULL,
	[SoLuongCoPhanId] [int] NULL,
	[SoLuong] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[HasChild] [bit] NULL,
	[ParentId] [int] NULL,
	[DinhMucCatToleId] [int] NULL,
	[DinhMucSanXuatId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DanhSachChiTiet_1] PRIMARY KEY CLUSTERED 
(
	[DanhSachChiTietId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DanhSachDieuChuyenQuyetToan]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DanhSachDieuChuyenQuyetToan](
	[DanhSachDieuChuyenQuyetToanId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[DieuChuyenQuyetToanId] [int] NOT NULL,
	[XuatKho] [bit] NOT NULL,
	[SoChungTuId] [int] NULL,
	[LoaiVatTuId] [int] NULL,
	[DonViTinh] [nvarchar](50) NULL,
	[SoLuong] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_ListChiTiet] PRIMARY KEY CLUSTERED 
(
	[DanhSachDieuChuyenQuyetToanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dechet]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dechet](
	[DechetId] [int] IDENTITY(1,1) NOT NULL,
	[MaCoPhan] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_Dechet] PRIMARY KEY CLUSTERED 
(
	[DechetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DieuChuyenQuyetToan]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DieuChuyenQuyetToan](
	[DieuChuyenQuyetToanId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[SoChungTuId] [int] NOT NULL,
	[KhoNhapId] [int] NULL,
	[KhoXuatId] [int] NULL,
	[DaXem] [bit] NULL,
	[LoaiDieuChuyenQuyetToanId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_TranferSettlement] PRIMARY KEY CLUSTERED 
(
	[DieuChuyenQuyetToanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DinhMucCatTole]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DinhMucCatTole](
	[DinhMucCatToleId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[SoChungTu] [nvarchar](50) NOT NULL,
	[Day] [float] NULL,
	[PhuongAn] [int] NULL,
	[ChiTietVatTuId] [int] NULL,
	[ToleId] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[Loai] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DinhMucCatTole] PRIMARY KEY CLUSTERED 
(
	[DinhMucCatToleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DinhMucSanXuat]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DinhMucSanXuat](
	[DinhMucSanXuatId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[MaDinhMucSanXuat] [nvarchar](50) NOT NULL,
	[SoChungTu] [nvarchar](50) NULL,
	[MaThanhPham] [nvarchar](50) NULL,
	[TenThanhPham] [nvarchar](250) NULL,
	[MaCapPhat] [nvarchar](50) NULL,
	[Loai] [nvarchar](50) NULL,
	[DienGiai] [nvarchar](500) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[KhoQuyetToanId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DinhMucSanXuat] PRIMARY KEY CLUSTERED 
(
	[DinhMucSanXuatId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DinhMucTuan]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DinhMucTuan](
	[DinhMucTuanId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[LoaiVatTuId] [int] NOT NULL,
	[DinhMuc] [float] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DinhMucTuan] PRIMARY KEY CLUSTERED 
(
	[DinhMucTuanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DoiMaChiTiet]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DoiMaChiTiet](
	[DoiMaChiTietId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[MaChiTietVatTu] [nvarchar](50) NOT NULL,
	[DonVi] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DoiMaChiTiet] PRIMARY KEY CLUSTERED 
(
	[DoiMaChiTietId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DoiMaVatTu]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DoiMaVatTu](
	[DoiMaVatTuId] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiVatTu] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[Cap] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_DoiMaVatTu] PRIMARY KEY CLUSTERED 
(
	[DoiMaVatTuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailTemplate]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplate](
	[EmailTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationID] [int] NOT NULL,
	[Placed] [varchar](256) NOT NULL,
	[Cancelled] [varchar](256) NOT NULL,
	[Updated] [varchar](256) NOT NULL,
	[PartialApproval] [varchar](256) NOT NULL,
	[NotApproved] [varchar](256) NOT NULL,
	[Approved] [varchar](256) NOT NULL,
	[IntoReviewOnPlaced] [varchar](256) NOT NULL,
	[Done] [varchar](256) NULL,
	[Paid] [varchar](256) NULL,
 CONSTRAINT [PK_EmailTemplate] PRIMARY KEY CLUSTERED 
(
	[EmailTemplateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GoiDau]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GoiDau](
	[GoiDauId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[LoaiVatTuId] [int] NOT NULL,
	[NgayGoiDau] [int] NULL,
	[TuanDuPhong] [int] NULL,
	[KeHoachId] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_GoiDau] PRIMARY KEY CLUSTERED 
(
	[GoiDauId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KeHoach]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KeHoach](
	[KeHoachId] [int] IDENTITY(1,1) NOT NULL,
	[MaKeHoach] [nvarchar](50) NOT NULL,
	[NgayGoiDau] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_KeHoach] PRIMARY KEY CLUSTERED 
(
	[KeHoachId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Kho]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kho](
	[KhoId] [int] IDENTITY(1,1) NOT NULL,
	[MaKho] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[NguoiGiao] [nvarchar](50) NULL,
	[ThongTinKho] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_Kho] PRIMARY KEY CLUSTERED 
(
	[KhoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KhoiLuongCoPhan]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhoiLuongCoPhan](
	[KhoiLuongCoPhanId] [int] IDENTITY(1,1) NOT NULL,
	[MaKhoiLuongCoPhan] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoiLuong] [float] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_KhoiLuongCoPhan] PRIMARY KEY CLUSTERED 
(
	[KhoiLuongCoPhanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KhoTinhTon]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhoTinhTon](
	[KhoTinhTonId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoId] [int] NOT NULL,
	[NhomSanPhamId] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_KhoTinhTon] PRIMARY KEY CLUSTERED 
(
	[KhoTinhTonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KyTen]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KyTen](
	[KyTenId] [int] IDENTITY(1,1) NOT NULL,
	[MaKyTen] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[NhanVienId] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[LoaiKyTen] [tinyint] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_KyTenKho] PRIMARY KEY CLUSTERED 
(
	[KyTenId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoaiDieuChuyenQuyetToan]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiDieuChuyenQuyetToan](
	[LoaiDieuChuyenQuyetToanId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_TypeChiTiet] PRIMARY KEY CLUSTERED 
(
	[LoaiDieuChuyenQuyetToanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoaiVatTu]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiVatTu](
	[LoaiVatTuId] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiVatTu] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[Cap] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_LoaiVatTu] PRIMARY KEY CLUSTERED 
(
	[LoaiVatTuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LyDoXuat]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LyDoXuat](
	[LyDoXuatId] [int] IDENTITY(1,1) NOT NULL,
	[MaLyDoXuat] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_LyDoXuat] PRIMARY KEY CLUSTERED 
(
	[LyDoXuatId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ModuleAction]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModuleAction](
	[ModuleActionID] [int] IDENTITY(1,1) NOT NULL,
	[Module] [varchar](50) NULL,
	[Action] [varchar](50) NULL,
	[Description] [varchar](500) NULL,
	[OrderIndex] [int] NULL,
 CONSTRAINT [PK_ModuleAction] PRIMARY KEY CLUSTERED 
(
	[ModuleActionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NguoiNhanKho]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NguoiNhanKho](
	[NguoiNhanKhoId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoId] [int] NOT NULL,
	[MaTen] [nvarchar](250) NULL,
	[NguoiGiaoNhanId] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[TruongDonViId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_NguoiNhanKho] PRIMARY KEY CLUSTERED 
(
	[NguoiNhanKhoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NhomSanPham]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhomSanPham](
	[NhomSanPhamId] [int] IDENTITY(1,1) NOT NULL,
	[MaNhomSanPham] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_NhomSanPham] PRIMARY KEY CLUSTERED 
(
	[NhomSanPhamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NhomVatTuNhapRieng]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhomVatTuNhapRieng](
	[NhomVatTuNhapRiengId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoId] [int] NOT NULL,
	[DangMa] [nvarchar](50) NOT NULL,
	[TenNhom] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_NhomVatTuNhapRieng] PRIMARY KEY CLUSTERED 
(
	[NhomVatTuNhapRiengId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notification]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[NotificationId] [int] IDENTITY(1,1) NOT NULL,
	[Note] [nvarchar](max) NOT NULL,
	[Type] [smallint] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Title] [nvarchar](250) NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[NotificationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationRecipient]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationRecipient](
	[NotificationRecipientId] [int] IDENTITY(1,1) NOT NULL,
	[NotificationId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[IsRead] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_NotificationRecipient] PRIMARY KEY CLUSTERED 
(
	[NotificationRecipientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhuLucISO]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhuLucIso](
	[PhuLucIsoId] [int] IDENTITY(1,1) NOT NULL,
	[MaPhuLucIso] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[SoPhuLuc] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PhuLucISO] PRIMARY KEY CLUSTERED 
(
	[PhuLucIsoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuyDoiDinhMuc]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuyDoiDinhMuc](
	[QuyDoiDinhMucId] [int] IDENTITY(1,1) NOT NULL,
	[MaQuyDoiDinhMuc] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[DinhMuc] [float] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_QuyDoiDinhMuc] PRIMARY KEY CLUSTERED 
(
	[QuyDoiDinhMucId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleModuleAction]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleModuleAction](
	[RoleModuleActionID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NOT NULL,
	[ModuleActionID] [int] NOT NULL,
 CONSTRAINT [PK_RoleModuleAction] PRIMARY KEY CLUSTERED 
(
	[RoleModuleActionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Note] [nvarchar](300) NULL,
	[Status] [bit] NOT NULL,
	[Type] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedByUserId] [nvarchar](50) NULL,
	[UpdatedUserId] [nvarchar](50) NULL,
 CONSTRAINT [PK_dbo.Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SoDuBanDau]    Script Date: 2019-07-16 2:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SoDuBanDau](
	[SoDuBanDauId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoId] [int] NOT NULL,
	[LoaiVatTuId] [int] NULL,
	[SoDuDauKy] [float] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_SoDuBanDau] PRIMARY KEY CLUSTERED 
(
	[SoDuBanDauId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ThanhPhamXuong]    Script Date: 2019-07-16 2:10:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThanhPhamXuong](
	[ThanhPhamXuongId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[XuongId] [int] NOT NULL,
	[LoaiVatTuId] [int] NOT NULL,
	[NhomSanPhamId] [int] NULL,
	[Loai] [nvarchar](50) NULL,
	[NangSuatTuan] [nvarchar](50) NULL,
	[LoaiVatLieu] [nvarchar](50) NULL,
	[QuyCach] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_ThanhPhamXuong] PRIMARY KEY CLUSTERED 
(
	[ThanhPhamXuongId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tole]    Script Date: 2019-07-16 2:10:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tole](
	[ToleId] [int] IDENTITY(1,1) NOT NULL,
	[MaTole] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[DonVi] [int] NULL,
	[Day] [float] NULL,
	[Ngang] [float] NULL,
	[Dung] [float] NULL,
	[KhoiLuong] [float] NULL,
	[DechetId] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_Tole] PRIMARY KEY CLUSTERED 
(
	[ToleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TrongLuongVatLieu]    Script Date: 2019-07-16 2:10:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrongLuongVatLieu](
	[TrongLuongVatLieuId] [int] IDENTITY(1,1) NOT NULL,
	[MaNguyenVatLieu] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KichThuoc] [nvarchar](50) NULL,
	[KhoiLuong] [float] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_TrongLuongVatLieu] PRIMARY KEY CLUSTERED 
(
	[TrongLuongVatLieuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 2019-07-16 2:10:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Email] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[PasswordSalt] [nvarchar](max) NULL,
	[IsSupperAdmin] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[LastLoginDate] [datetime] NULL,
	[LastActivityDate] [datetime] NULL,
	[IsLockedOut] [bit] NOT NULL,
	[Avatar] [nvarchar](max) NULL,
	[Status] [bit] NOT NULL,
	[FullName] [nvarchar](150) NULL,
	[Tel] [nvarchar](50) NULL,
 CONSTRAINT [PK_dbo.User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 2019-07-16 2:10:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserRoleId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VatTuCapRieng]    Script Date: 2019-07-16 2:10:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VatTuCapRieng](
	[VatTuCapRiengId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[LoaiVatTuId] [int] NOT NULL,
	[NhomSanPhamId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_VatTuCapRieng] PRIMARY KEY CLUSTERED 
(
	[VatTuCapRiengId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VatTuNhapRieng]    Script Date: 2019-07-16 2:10:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VatTuNhapRieng](
	[VatTuNhapRiengId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[KhoId] [int] NOT NULL,
	[LoaiVatTuId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_VatTuNhapRieng] PRIMARY KEY CLUSTERED 
(
	[VatTuNhapRiengId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AccountToken] ON 
GO
INSERT [dbo].[AccountToken] ([AccountTokenId], [AccountId], [IsAdminAccountSide], [TokenKey], [ExpiredDate], [TokenType]) VALUES (30, 3, 0, N'1a7d471e86c0432f9d37d47bce07e2b9', CAST(N'2017-01-19T11:37:09.993' AS DateTime), 3)
GO
SET IDENTITY_INSERT [dbo].[AccountToken] OFF
GO
SET IDENTITY_INSERT [dbo].[Configuration] ON 
GO
INSERT [dbo].[Configuration] ([ConfigurationId], [ExchangeRate], [AdminAcceptanceBalancePercentage], [ClientAcceptanceBalancePercentage]) VALUES (1, 3366.0000, 70, 90)
GO
SET IDENTITY_INSERT [dbo].[Configuration] OFF
GO
SET IDENTITY_INSERT [dbo].[LoaiDieuChuyenQuyetToan] ON 
GO
INSERT [dbo].[LoaiDieuChuyenQuyetToan] ([LoaiDieuChuyenQuyetToanId], [Title], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status]) VALUES (4, N'Điều chuyển Gia công', CAST(N'2019-07-12T03:18:33.267' AS DateTime), NULL, N'admin', NULL, 1)
GO
INSERT [dbo].[LoaiDieuChuyenQuyetToan] ([LoaiDieuChuyenQuyetToanId], [Title], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status]) VALUES (5, N'Quyết toán Gia công', CAST(N'2019-07-12T03:19:38.060' AS DateTime), NULL, N'admin', NULL, 1)
GO
INSERT [dbo].[LoaiDieuChuyenQuyetToan] ([LoaiDieuChuyenQuyetToanId], [Title], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Status]) VALUES (6, N'Điều chuyển Cấp hàng', CAST(N'2019-07-12T03:19:57.797' AS DateTime), NULL, N'admin', NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[LoaiDieuChuyenQuyetToan] OFF
GO
SET IDENTITY_INSERT [dbo].[ModuleAction] ON 
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (1, N'User', N'Listing', N'User listing', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (2, N'User', N'Create', N'Create new a user', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (3, N'User', N'Edit', N'Update user', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (4, N'User', N'Delete', N'Delete User', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (5, N'Role', N'Create', N'Create new a role', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (6, N'Role', N'Edit', N'Update role', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (7, N'Role', N'Listing', N'Role listing', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (8, N'Role', N'Delete', N'Delete role', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (10, N'Customer', N'Edit', N'Update Customer', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (11, N'Customer', N'Listing', N'Customer listing', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (12, N'Customer', N'Delete', N'Delete Customer', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (13, N'Customer', N'Create', N'Create customer', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (15, N'Role', N'Invisible', N'Disable ', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (65, N'Customer', N'Invisible', N'Disable customer', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (66, N'User', N'Invisible', N'Delete User', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (67, N'Order', N'OrderPendingListing', N'List of pending order ', 0)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (68, N'Order', N'DepositOrder', N'Deposit order', 5)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (69, N'Order', N'CancelOrderDeposited', N'Cancel order deposited', 6)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (70, N'Order', N'OrderPendingQuoteListing', N'Order Pending Quote Listing', 2)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (71, N'Order', N'Quote', N'Quote order', 4)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (72, N'Order', N'MergeOrder', N'Merge order', 3)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (73, N'Order', N'Latch', N'Latch Order', 7)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (74, N'Order', N'Detail', N'Detail of order', 4)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (75, N'Order', N'ShopOrderDetail', N'Detail of shop order detail', 8)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (76, N'Order', N'PurchaseOrder', NULL, 8)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (77, N'Order', N'CancelOrder', N'Cancel Order', 9)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (78, N'Order', N'OrderQuoteListing', N'Order Quote Listing', 4)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (79, N'Order', N'OrderPurchasingListing', N'Order Purchasing Listing', 8)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (80, N'Order', N'OrderPaidListing', N'Order Paid Listing', 10)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (81, N'Order', N'ShopperOrderListing', N'Shopper Order Listing', 12)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (82, N'Order', N'ImportChinaStoreListing', N'Order Chinese Inventory Receiving Listing', 13)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (83, N'Order', N'ExportChinaStoreListing', N'Order Chinese Inventory Delivering Listing', 14)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (84, N'Order', N'ImportVietNamStoreListing', N'Order Vietnamese Inventory Receiving Listing', 15)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (85, N'Order', N'ExportVietNamStoreListing', N'Order Vietnamese Inventory Delivering Listing', 16)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (86, N'Order', N'OrderDone', N'Order Done', 17)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (87, N'Order', N'SearchLanding', N'Search lading', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (88, N'OrderTrash', N'Listing', N'Listing', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (89, N'OrderTrash', N'Detail', N'Detail', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (90, N'OrderTrash', N'Restore', N'Restore', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (91, N'Notification', N'Create', N'Create', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (92, N'Notification', N'Edit', N'Edit', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (93, N'Notification', N'Listing', N'Listing', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (94, N'Notification', N'Invisible', N'Disable', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (95, N'Notification', N'Delete', N'Delete', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (96, N'Transaction', N'Listing', N'Listing', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (97, N'Transaction', N'Detail', N'Detail', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (98, N'Customer', N'Transaction', N'Transaction', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (99, N'Customer', N'RemoteLogin', N'RemoteLogin', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (100, N'Complaint', N'Listing', N'Listing', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (101, N'Complaint', N'Detail', N'Detail', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (102, N'Complaint', N'Cancel', N'Cancel', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (103, N'Complaint', N'Approve', N'Aprove', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (104, N'Order', N'ProcessOrder', N'Process order', 1)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (106, N'Order', N'AwaitPurchaseOrder', N'Await Purchase Order', 8)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (107, N'Order', N'RollBackToQuoteOrder', N'RollBack To Quote Order', 7)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (108, N'Order', N'EditBoughtOrder', N'Edit Bought Order', 9)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (109, N'Order', N'UpdateShippingCode', N'UpdateShippingCode', 10)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (110, N'Order', N'SearchShopOrder', N'SearchShopOrder', 12)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (111, N'Order', N'ProcessImportChinaStore', N'ProcessImportChinaStore', 13)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (112, N'Order', N'ReceivePackage', NULL, 14)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (113, N'Order', N'ProcessImportVietnamStore', NULL, 15)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (114, N'Order', N'ProcessExportVietnamStore', NULL, 16)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (115, N'Order', N'AddPackage', NULL, 14)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (116, N'Order', N'AddLandingCodeToPackage', NULL, 14)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (117, N'Order', N'DeletePackage', NULL, 14)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (118, N'Order', N'DeletePackageItem', NULL, 14)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (119, N'Order', N'UpdatePackage', NULL, 14)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (120, N'User', N'Invisibe', N'Invisibe', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (121, N'Configuration', N'Setting', NULL, NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (122, N'Notification', N'Create', N'', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (123, N'Notification', N'Edit', N'', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (124, N'Notification', N'Listing', N'', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (125, N'Notification', N'Delete', N'', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (126, N'Notification', N'Invisible', NULL, NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (127, N'Role', N'Invisible', NULL, NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (128, N'Transaction', N'History', N'', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (129, N'Transaction', N'Withdrawal', N'', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (130, N'Transaction', N'Edit', N'', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (131, N'Transaction', N'Delete', N'', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (132, N'Transaction', N'ReCharge', NULL, NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (133, N'Transaction', N'Refund', NULL, NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (134, N'Credit', N'Listing', NULL, NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (135, N'Credit', N'Info', NULL, NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (136, N'Credit', N'ChargeToNV', NULL, NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (137, N'Credit', N'ChargeToAdmin', NULL, NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (138, N'Credit', N'WithdrawCreditAdmin', NULL, NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (139, N'Credit', N'RefundUserCredit', NULL, NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (143, N'CarrierOrder', N'Create', N'', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (144, N'CarrierOrder', N'Edit', N'', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (145, N'CarrierOrder', N'Listing', N'', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (146, N'CarrierOrder', N'Delete', N'', NULL)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (148, N'Order', N'PenddingDepositListing', NULL, 7)
GO
INSERT [dbo].[ModuleAction] ([ModuleActionID], [Module], [Action], [Description], [OrderIndex]) VALUES (149, N'Order', N'ReadyExportVNStoreListing', NULL, 17)
GO
SET IDENTITY_INSERT [dbo].[ModuleAction] OFF
GO
SET IDENTITY_INSERT [dbo].[Notification] ON 
GO
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (51, N'<p>Admin Logistics gửi bạn bảng báo giá cho đơn hàng <strong>DH4131711828</strong></p><p>Chi tiết vui lòng xem tại : <a target=''_blank'' href=''/Order''>Đơn hàng đã báo giá</a></p>', 2, 0, CAST(N'2017-01-18T22:55:40.980' AS DateTime), N'Báo giá đơn hàng DH4131711828', NULL, 1)
GO
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (52, N'<p>Dear anh chị.</p>
<p>Admin đã mua đơn hàng DH4131711828. Anh chị kiểm tra lại đơn hàng những phần đã ghi chú lại (nếu có).</p>
<p>Khi hàng về Việt Nam bộ phận kho sẽ thông báo để anh chị làm thủ tục nhận hàng và chuyển xe đi tỉnh nếu cần.</p>
<p>Sau 7 ngày anh chị có thể gọi điện cho <strong>BP Kho : 04 39956222 </strong> để hỏi về tình trạng hàng hóa.</p>
<p><strong>Xin cảm ơn!</strong></p>

', 2, 0, CAST(N'2017-01-18T22:57:17.307' AS DateTime), N'Đã mua xong đơn hàng DH4131711828', NULL, 1)
GO
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (53, N'<p>Admin Logistics gửi bạn bảng báo giá cho đơn hàng <strong>DH6131711826</strong></p><p>Chi tiết vui lòng xem tại : <a target=''_blank'' href=''/Order''>Đơn hàng đã báo giá</a></p>', 2, 0, CAST(N'2017-01-18T23:11:58.707' AS DateTime), N'Báo giá đơn hàng DH6131711826', NULL, 1)
GO
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (54, N'<p>Dear anh chị.</p>
<p>Admin đã mua đơn hàng DH6131711826. Anh chị kiểm tra lại đơn hàng những phần đã ghi chú lại (nếu có).</p>
<p>Khi hàng về Việt Nam bộ phận kho sẽ thông báo để anh chị làm thủ tục nhận hàng và chuyển xe đi tỉnh nếu cần.</p>
<p>Sau 7 ngày anh chị có thể gọi điện cho <strong>BP Kho : 04 39956222 </strong> để hỏi về tình trạng hàng hóa.</p>
<p><strong>Xin cảm ơn!</strong></p>

', 2, 0, CAST(N'2017-01-18T23:12:55.773' AS DateTime), N'Đã mua xong đơn hàng DH6131711826', NULL, 1)
GO
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (55, N'<p>Dear anh chị.</p>
<p>Admin đã mua đơn hàng DH3131711859. Anh chị kiểm tra lại đơn hàng những phần đã ghi chú lại (nếu có).</p>
<p>Khi hàng về Việt Nam bộ phận kho sẽ thông báo để anh chị làm thủ tục nhận hàng và chuyển xe đi tỉnh nếu cần.</p>
<p>Sau 7 ngày anh chị có thể gọi điện cho <strong>BP Kho : 04 39956222 </strong> để hỏi về tình trạng hàng hóa.</p>
<p><strong>Xin cảm ơn!</strong></p>

', 2, 0, CAST(N'2017-01-18T23:13:14.863' AS DateTime), N'Đã mua xong đơn hàng DH3131711859', NULL, 1)
GO
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (56, N'<p>Admin Logistics gửi bạn bảng báo giá cho đơn hàng <strong>DH6131711954</strong></p><p>Chi tiết vui lòng xem tại : <a target=''_blank'' href=''/Order''>Đơn hàng đã báo giá</a></p>', 2, 0, CAST(N'2017-01-19T10:06:21.273' AS DateTime), N'Báo giá đơn hàng DH6131711954', NULL, 1)
GO
INSERT [dbo].[Notification] ([NotificationId], [Note], [Type], [IsDeleted], [CreatedDate], [Title], [ModifiedDate], [IsActive]) VALUES (57, N'<p>Dear anh chị.</p>
<p>Admin đã mua đơn hàng DH6131711954. Anh chị kiểm tra lại đơn hàng những phần đã ghi chú lại (nếu có).</p>
<p>Khi hàng về Việt Nam bộ phận kho sẽ thông báo để anh chị làm thủ tục nhận hàng và chuyển xe đi tỉnh nếu cần.</p>
<p>Sau 7 ngày anh chị có thể gọi điện cho <strong>BP Kho : 04 39956222 </strong> để hỏi về tình trạng hàng hóa.</p>
<p><strong>Xin cảm ơn!</strong></p>

', 2, 0, CAST(N'2017-01-19T11:16:49.560' AS DateTime), N'Đã mua xong đơn hàng DH6131711954', NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[Notification] OFF
GO
SET IDENTITY_INSERT [dbo].[NotificationRecipient] ON 
GO
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (74, 51, 13, 1, CAST(N'2017-01-18T22:55:41.070' AS DateTime))
GO
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (75, 52, 13, 0, CAST(N'2017-01-18T22:57:17.340' AS DateTime))
GO
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (76, 53, 13, 1, CAST(N'2017-01-18T23:11:58.737' AS DateTime))
GO
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (77, 54, 13, 0, CAST(N'2017-01-18T23:12:55.813' AS DateTime))
GO
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (78, 55, 13, 1, CAST(N'2017-01-18T23:13:14.900' AS DateTime))
GO
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (79, 56, 13, 1, CAST(N'2017-01-19T10:06:21.307' AS DateTime))
GO
INSERT [dbo].[NotificationRecipient] ([NotificationRecipientId], [NotificationId], [UserId], [IsRead], [CreatedDate]) VALUES (80, 57, 13, 1, CAST(N'2017-01-19T11:16:49.597' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[NotificationRecipient] OFF
GO
SET IDENTITY_INSERT [dbo].[Roles] ON 
GO
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (16, N'R001', N'Check Đơn', N'Kiểm tra đơn hàng', 1, NULL, CAST(N'2016-10-16T23:00:39.530' AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
GO
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (17, N'R002', N'Cộng tác viên', N'Cộng tác viên', 1, NULL, CAST(N'2016-10-16T23:01:26.367' AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
GO
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (18, N'R003', N'Kế toán', N'Kế toán', 1, NULL, CAST(N'2016-10-16T23:02:01.883' AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
GO
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (19, N'R004', N'Kho Trung Quốc', N'Kho Trung Quốc', 1, NULL, CAST(N'2016-10-16T23:02:28.597' AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
GO
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (20, N'R005', N'Kho Việt Nam', N'Kho Việt Nam', 1, NULL, CAST(N'2016-10-16T23:03:01.807' AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
GO
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (21, N'R006', N'Orders', N'Orders', 1, NULL, CAST(N'2016-10-16T23:03:31.387' AS DateTime), CAST(N'2017-01-06T00:30:35.447' AS DateTime), N'admin@nhonhoa.vn', N'admin@nhonhoa.vn')
GO
INSERT [dbo].[Roles] ([RoleId], [Code], [Name], [Note], [Status], [Type], [CreatedDate], [UpdatedDate], [CreatedByUserId], [UpdatedUserId]) VALUES (22, N'R007', N'Quản trị viên', N'Quản trị viên', 1, NULL, CAST(N'2016-10-16T23:03:55.500' AS DateTime), NULL, N'admin@nhonhoa.vn', NULL)
GO
SET IDENTITY_INSERT [dbo].[Roles] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 
GO
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (6, N'Admin', N'admin@nhonhoa.vn', N'1000:paqv42x0zf32cagcDdRNXcpLPTqVDovH:0+A7K0tvhPf6HV3HU2CZ2tUYYIAlTu9X', N'1000:4XwRFklHMIqnUCvhhZxx8Njut1rgzX3+:UiUrwx8Kuhyw3aRx0f2JZETaWv6PjXyw', 1, CAST(N'2016-10-04T00:00:00.000' AS DateTime), CAST(N'2016-10-04T00:00:00.000' AS DateTime), CAST(N'2016-10-04T00:00:00.000' AS DateTime), CAST(N'2016-10-04T00:00:00.000' AS DateTime), 0, NULL, 1, N'', NULL)
GO
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (27, N'Ke Toan', N'ketoan@taobao.com.vn', N'1000:prLoKfnzcM2TLaqgUyL9hWhzwhGp260o:vyIEznpGv2JZ/W4quiEha6p8VZi1rJPv', N'1000:4XwRFklHMIqnUCvhhZxx8Njut1rgzX3+:UiUrwx8Kuhyw3aRx0f2JZETaWv6PjXyw', 0, CAST(N'2016-10-04T00:00:00.000' AS DateTime), NULL, CAST(N'2016-10-04T00:00:00.000' AS DateTime), CAST(N'2016-10-04T00:00:00.000' AS DateTime), 0, NULL, 0, NULL, NULL)
GO
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (44, N'Nguyen Van Ngu', N'ngunv@nhonhoa.vn', N'1000:Ey/r8F+D3FxlWNFkqOcnsWJgtPKl2Gtd:waDb3mz1VcVCXYwZWgETDWbmoiSpXoV9', NULL, 0, CAST(N'2016-10-17T02:55:57.423' AS DateTime), CAST(N'2017-06-05T16:21:30.297' AS DateTime), NULL, NULL, 0, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (45, N'order', N'order@nhonhoa.vn', N'1000:Jpq5c+X4lEJ1PerLJHCPQYWDhbPjHysy:/Ki90iy696NhXzTQDVfnArvX+XvUMfSZ', NULL, 0, CAST(N'2016-10-17T07:56:08.020' AS DateTime), CAST(N'2016-10-17T08:15:55.000' AS DateTime), NULL, NULL, 0, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (46, N'Cộng tác viên', N'congtacvien@nhonhoa.vn', N'1000:roLna4oGLzr7BL3W5aUd0AUcP6pnSils:WeFI3IC1CO3wsfcIJqOiYH6oq9nOqG3j', NULL, 0, CAST(N'2016-10-17T08:20:36.187' AS DateTime), NULL, NULL, NULL, 0, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (47, N'fdgdfg', N'sdsfsdf@gfg.cc', N'1000:2dqjQtKdF+fYLOTqys6zLwi99xRYmqDb:mKSgx9wuMx7FMSJzRWptxtqwG8Tfpsot', NULL, 0, CAST(N'2017-06-05T17:22:07.900' AS DateTime), NULL, NULL, NULL, 0, NULL, 0, NULL, NULL)
GO
INSERT [dbo].[User] ([UserId], [UserName], [Email], [Password], [PasswordSalt], [IsSupperAdmin], [CreatedDate], [UpdatedDate], [LastLoginDate], [LastActivityDate], [IsLockedOut], [Avatar], [Status], [FullName], [Tel]) VALUES (48, NULL, N'sâdsa', N'1000:slRi5zRLSuKv9BKnMT5zsDGXw/NL6l/+:RRL4Z6Sh9bYofEvvRtkK9JQ8a7y9bp4b', NULL, 0, CAST(N'2019-03-23T00:44:55.167' AS DateTime), NULL, NULL, NULL, 0, NULL, 1, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
SET IDENTITY_INSERT [dbo].[UserRole] ON 
GO
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (1, 27, 18, CAST(N'2016-10-16T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (6, 44, 19, CAST(N'2016-10-17T02:55:57.450' AS DateTime))
GO
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (7, 45, 21, CAST(N'2016-10-17T07:56:08.267' AS DateTime))
GO
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (8, 46, 17, CAST(N'2016-10-17T08:20:37.530' AS DateTime))
GO
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (9, 47, 16, CAST(N'2017-06-05T17:22:43.673' AS DateTime))
GO
INSERT [dbo].[UserRole] ([UserRoleId], [UserId], [RoleId], [CreatedDate]) VALUES (10, 48, 16, CAST(N'2019-03-23T00:44:55.240' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[UserRole] OFF
GO
ALTER TABLE [dbo].[LoaiDieuChuyenQuyetToan] ADD  CONSTRAINT [DF_TypeTranferSettlement_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[CoPhanCapXuong]  WITH CHECK ADD  CONSTRAINT [FK_CoPhanCapXuong_Kho] FOREIGN KEY([KhoNhapId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[CoPhanCapXuong] CHECK CONSTRAINT [FK_CoPhanCapXuong_Kho]
GO
ALTER TABLE [dbo].[CoPhanCapXuong]  WITH CHECK ADD  CONSTRAINT [FK_CoPhanCapXuong_Kho1] FOREIGN KEY([KhoXuatId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[CoPhanCapXuong] CHECK CONSTRAINT [FK_CoPhanCapXuong_Kho1]
GO
ALTER TABLE [dbo].[CoPhanCapXuong]  WITH CHECK ADD  CONSTRAINT [FK_CoPhanCapXuong_LoaiVatTu] FOREIGN KEY([LoaiVatTuId])
REFERENCES [dbo].[LoaiVatTu] ([LoaiVatTuId])
GO
ALTER TABLE [dbo].[CoPhanCapXuong] CHECK CONSTRAINT [FK_CoPhanCapXuong_LoaiVatTu]
GO
ALTER TABLE [dbo].[CoPhanCapXuong]  WITH CHECK ADD  CONSTRAINT [FK_CoPhanCapXuong_NhomSanPham] FOREIGN KEY([NhomSanPhamId])
REFERENCES [dbo].[NhomSanPham] ([NhomSanPhamId])
GO
ALTER TABLE [dbo].[CoPhanCapXuong] CHECK CONSTRAINT [FK_CoPhanCapXuong_NhomSanPham]
GO
ALTER TABLE [dbo].[DanhSachChiTiet]  WITH CHECK ADD  CONSTRAINT [FK_DanhSachChiTiet_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[DanhSachChiTiet] CHECK CONSTRAINT [FK_DanhSachChiTiet_ChiTietVatTu]
GO
ALTER TABLE [dbo].[DanhSachChiTiet]  WITH CHECK ADD  CONSTRAINT [FK_DanhSachChiTiet_DinhMucCatTole] FOREIGN KEY([DinhMucCatToleId])
REFERENCES [dbo].[DinhMucCatTole] ([DinhMucCatToleId])
GO
ALTER TABLE [dbo].[DanhSachChiTiet] CHECK CONSTRAINT [FK_DanhSachChiTiet_DinhMucCatTole]
GO
ALTER TABLE [dbo].[DanhSachChiTiet]  WITH CHECK ADD  CONSTRAINT [FK_DanhSachChiTiet_DinhMucSanXuat] FOREIGN KEY([DinhMucSanXuatId])
REFERENCES [dbo].[DinhMucSanXuat] ([DinhMucSanXuatId])
GO
ALTER TABLE [dbo].[DanhSachChiTiet] CHECK CONSTRAINT [FK_DanhSachChiTiet_DinhMucSanXuat]
GO
ALTER TABLE [dbo].[DanhSachDieuChuyenQuyetToan]  WITH CHECK ADD  CONSTRAINT [FK_DanhSachDieuChuyenQuyetToan_LoaiVatTu] FOREIGN KEY([LoaiVatTuId])
REFERENCES [dbo].[LoaiVatTu] ([LoaiVatTuId])
GO
ALTER TABLE [dbo].[DanhSachDieuChuyenQuyetToan] CHECK CONSTRAINT [FK_DanhSachDieuChuyenQuyetToan_LoaiVatTu]
GO
ALTER TABLE [dbo].[DanhSachDieuChuyenQuyetToan]  WITH CHECK ADD  CONSTRAINT [FK_ListTranferSettlement_TranferSettlement] FOREIGN KEY([DieuChuyenQuyetToanId])
REFERENCES [dbo].[DieuChuyenQuyetToan] ([DieuChuyenQuyetToanId])
GO
ALTER TABLE [dbo].[DanhSachDieuChuyenQuyetToan] CHECK CONSTRAINT [FK_ListTranferSettlement_TranferSettlement]
GO
ALTER TABLE [dbo].[DieuChuyenQuyetToan]  WITH CHECK ADD  CONSTRAINT [FK_TranferSettlement_Kho] FOREIGN KEY([KhoNhapId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[DieuChuyenQuyetToan] CHECK CONSTRAINT [FK_TranferSettlement_Kho]
GO
ALTER TABLE [dbo].[DieuChuyenQuyetToan]  WITH CHECK ADD  CONSTRAINT [FK_TranferSettlement_Kho1] FOREIGN KEY([KhoXuatId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[DieuChuyenQuyetToan] CHECK CONSTRAINT [FK_TranferSettlement_Kho1]
GO
ALTER TABLE [dbo].[DieuChuyenQuyetToan]  WITH CHECK ADD  CONSTRAINT [FK_TranferSettlement_TypeTranferSettlement] FOREIGN KEY([LoaiDieuChuyenQuyetToanId])
REFERENCES [dbo].[LoaiDieuChuyenQuyetToan] ([LoaiDieuChuyenQuyetToanId])
GO
ALTER TABLE [dbo].[DieuChuyenQuyetToan] CHECK CONSTRAINT [FK_TranferSettlement_TypeTranferSettlement]
GO
ALTER TABLE [dbo].[DinhMucCatTole]  WITH CHECK ADD  CONSTRAINT [FK_DinhMucCatTole_ChiTietVatTu] FOREIGN KEY([ChiTietVatTuId])
REFERENCES [dbo].[ChiTietVatTu] ([ChiTietVatTuId])
GO
ALTER TABLE [dbo].[DinhMucCatTole] CHECK CONSTRAINT [FK_DinhMucCatTole_ChiTietVatTu]
GO
ALTER TABLE [dbo].[DinhMucCatTole]  WITH CHECK ADD  CONSTRAINT [FK_DinhMucCatTole_Tole] FOREIGN KEY([ToleId])
REFERENCES [dbo].[Tole] ([ToleId])
GO
ALTER TABLE [dbo].[DinhMucCatTole] CHECK CONSTRAINT [FK_DinhMucCatTole_Tole]
GO
ALTER TABLE [dbo].[DinhMucSanXuat]  WITH CHECK ADD  CONSTRAINT [FK_DinhMucSanXuat_Kho] FOREIGN KEY([KhoQuyetToanId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[DinhMucSanXuat] CHECK CONSTRAINT [FK_DinhMucSanXuat_Kho]
GO
ALTER TABLE [dbo].[DinhMucTuan]  WITH CHECK ADD  CONSTRAINT [FK_DinhMucTuan_LoaiVatTu] FOREIGN KEY([LoaiVatTuId])
REFERENCES [dbo].[LoaiVatTu] ([LoaiVatTuId])
GO
ALTER TABLE [dbo].[DinhMucTuan] CHECK CONSTRAINT [FK_DinhMucTuan_LoaiVatTu]
GO
ALTER TABLE [dbo].[GoiDau]  WITH CHECK ADD  CONSTRAINT [FK_GoiDau_KeHoach] FOREIGN KEY([KeHoachId])
REFERENCES [dbo].[KeHoach] ([KeHoachId])
GO
ALTER TABLE [dbo].[GoiDau] CHECK CONSTRAINT [FK_GoiDau_KeHoach]
GO
ALTER TABLE [dbo].[KhoTinhTon]  WITH CHECK ADD  CONSTRAINT [FK_KhoTinhTon_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[KhoTinhTon] CHECK CONSTRAINT [FK_KhoTinhTon_Kho]
GO
ALTER TABLE [dbo].[KhoTinhTon]  WITH CHECK ADD  CONSTRAINT [FK_KhoTinhTon_NhomSanPham] FOREIGN KEY([NhomSanPhamId])
REFERENCES [dbo].[NhomSanPham] ([NhomSanPhamId])
GO
ALTER TABLE [dbo].[KhoTinhTon] CHECK CONSTRAINT [FK_KhoTinhTon_NhomSanPham]
GO
ALTER TABLE [dbo].[NguoiNhanKho]  WITH CHECK ADD  CONSTRAINT [FK_NguoiNhanKho_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[NguoiNhanKho] CHECK CONSTRAINT [FK_NguoiNhanKho_Kho]
GO
ALTER TABLE [dbo].[NhomVatTuNhapRieng]  WITH CHECK ADD  CONSTRAINT [FK_NhomVatTuNhapRieng_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[NhomVatTuNhapRieng] CHECK CONSTRAINT [FK_NhomVatTuNhapRieng_Kho]
GO
ALTER TABLE [dbo].[NotificationRecipient]  WITH CHECK ADD  CONSTRAINT [FK_NotificationRecipient_Notification1] FOREIGN KEY([NotificationId])
REFERENCES [dbo].[Notification] ([NotificationId])
GO
ALTER TABLE [dbo].[NotificationRecipient] CHECK CONSTRAINT [FK_NotificationRecipient_Notification1]
GO
ALTER TABLE [dbo].[RoleModuleAction]  WITH CHECK ADD  CONSTRAINT [FK_RoleModuleAction_ModuleAction] FOREIGN KEY([ModuleActionID])
REFERENCES [dbo].[ModuleAction] ([ModuleActionID])
GO
ALTER TABLE [dbo].[RoleModuleAction] CHECK CONSTRAINT [FK_RoleModuleAction_ModuleAction]
GO
ALTER TABLE [dbo].[RoleModuleAction]  WITH CHECK ADD  CONSTRAINT [FK_RoleModuleAction_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[RoleModuleAction] CHECK CONSTRAINT [FK_RoleModuleAction_Roles]
GO
ALTER TABLE [dbo].[SoDuBanDau]  WITH CHECK ADD  CONSTRAINT [FK_SoDuBanDau_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[SoDuBanDau] CHECK CONSTRAINT [FK_SoDuBanDau_Kho]
GO
ALTER TABLE [dbo].[SoDuBanDau]  WITH CHECK ADD  CONSTRAINT [FK_SoDuBanDau_LoaiVatTu] FOREIGN KEY([LoaiVatTuId])
REFERENCES [dbo].[LoaiVatTu] ([LoaiVatTuId])
GO
ALTER TABLE [dbo].[SoDuBanDau] CHECK CONSTRAINT [FK_SoDuBanDau_LoaiVatTu]
GO
ALTER TABLE [dbo].[ThanhPhamXuong]  WITH CHECK ADD  CONSTRAINT [FK_ThanhPhamXuong_LoaiVatTu] FOREIGN KEY([LoaiVatTuId])
REFERENCES [dbo].[LoaiVatTu] ([LoaiVatTuId])
GO
ALTER TABLE [dbo].[ThanhPhamXuong] CHECK CONSTRAINT [FK_ThanhPhamXuong_LoaiVatTu]
GO
ALTER TABLE [dbo].[ThanhPhamXuong]  WITH CHECK ADD  CONSTRAINT [FK_ThanhPhamXuong_NhomSanPham] FOREIGN KEY([NhomSanPhamId])
REFERENCES [dbo].[NhomSanPham] ([NhomSanPhamId])
GO
ALTER TABLE [dbo].[ThanhPhamXuong] CHECK CONSTRAINT [FK_ThanhPhamXuong_NhomSanPham]
GO
ALTER TABLE [dbo].[Tole]  WITH CHECK ADD  CONSTRAINT [FK_Tole_Dechet] FOREIGN KEY([DechetId])
REFERENCES [dbo].[Dechet] ([DechetId])
GO
ALTER TABLE [dbo].[Tole] CHECK CONSTRAINT [FK_Tole_Dechet]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Role]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_User]
GO
ALTER TABLE [dbo].[VatTuCapRieng]  WITH CHECK ADD  CONSTRAINT [FK_VatTuCapRieng_LoaiVatTu] FOREIGN KEY([LoaiVatTuId])
REFERENCES [dbo].[LoaiVatTu] ([LoaiVatTuId])
GO
ALTER TABLE [dbo].[VatTuCapRieng] CHECK CONSTRAINT [FK_VatTuCapRieng_LoaiVatTu]
GO
ALTER TABLE [dbo].[VatTuCapRieng]  WITH CHECK ADD  CONSTRAINT [FK_VatTuCapRieng_NhomSanPham] FOREIGN KEY([NhomSanPhamId])
REFERENCES [dbo].[NhomSanPham] ([NhomSanPhamId])
GO
ALTER TABLE [dbo].[VatTuCapRieng] CHECK CONSTRAINT [FK_VatTuCapRieng_NhomSanPham]
GO
ALTER TABLE [dbo].[VatTuNhapRieng]  WITH CHECK ADD  CONSTRAINT [FK_VatTuNhapRieng_Kho] FOREIGN KEY([KhoId])
REFERENCES [dbo].[Kho] ([KhoId])
GO
ALTER TABLE [dbo].[VatTuNhapRieng] CHECK CONSTRAINT [FK_VatTuNhapRieng_Kho]
GO
ALTER TABLE [dbo].[VatTuNhapRieng]  WITH CHECK ADD  CONSTRAINT [FK_VatTuNhapRieng_LoaiVatTu] FOREIGN KEY([LoaiVatTuId])
REFERENCES [dbo].[LoaiVatTu] ([LoaiVatTuId])
GO
ALTER TABLE [dbo].[VatTuNhapRieng] CHECK CONSTRAINT [FK_VatTuNhapRieng_LoaiVatTu]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AccountType = 0(User  of client side), AccountType=1 User  of admin side' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountToken', @level2type=N'COLUMN',@level2name=N'IsAdminAccountSide'
GO
