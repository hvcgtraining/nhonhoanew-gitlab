﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.DechetModel
{
    public class DechetModel
    {
		public int DechetId { get; set; }
        [Required(ErrorMessage = "Mã cơ phận không được bỏ trống")]
        public string MaCoPhan { get; set; }
        [Required(ErrorMessage = "Tên cơ phận không được bỏ trống")]
		public string Title { get; set; }
		public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
