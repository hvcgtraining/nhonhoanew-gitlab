﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.DoiMaChiTietModel
{
    public class DoiMaChiTietSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<DoiMaChiTietModel> DoiMaChiTiets { get; set; }
    }
}