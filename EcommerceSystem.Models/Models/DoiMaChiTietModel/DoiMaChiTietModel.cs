﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.DoiMaChiTietModel
{
    public class DoiMaChiTietModel
    {
		public int DoiMaChiTietId { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Mã chi tiết vật tư không được bỏ trống")]
        public string MaChiTietVatTu { get; set; }
        [Required(ErrorMessage = "Đơn vị không được bỏ trống")]
        public int? DonVi { get; set; }
		public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
