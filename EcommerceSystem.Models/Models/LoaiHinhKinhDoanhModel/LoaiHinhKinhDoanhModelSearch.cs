﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.LoaiHinhKinhDoanhModel
{
    public class LoaiHinhKinhDoanhSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<LoaiHinhKinhDoanhModel> LoaiHinhKinhDoanhs { get; set; }
    }
}