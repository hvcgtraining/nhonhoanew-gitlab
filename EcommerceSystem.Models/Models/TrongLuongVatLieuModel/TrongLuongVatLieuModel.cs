﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.TrongLuongVatLieuModel
{
    public class TrongLuongVatLieuModel
    {
		public int TrongLuongVatLieuId { get; set; }
        [Required(ErrorMessage = "Mã nguyên vật liệu không được bỏ trống")]
        public string MaNguyenVatLieu { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Kích thước không được bỏ trống")]
        public string KichThuoc { get; set; }
        [Required(ErrorMessage = "Khối lượng không được bỏ trống")]
        public double? KhoiLuong { get; set; }
		public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
