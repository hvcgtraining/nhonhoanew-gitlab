﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.TrongLuongVatLieuModel
{
    public class TrongLuongVatLieuSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<TrongLuongVatLieuModel> TrongLuongVatLieus { get; set; }
    }
}