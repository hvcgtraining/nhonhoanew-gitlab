﻿
using System;
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHKeHoachNhapKhoThanhPhamDetailModel
{
    public class PKHKeHoachNhapKhoThanhPhamDetailSearchModel : Paging
    {
        public string TextSearch { get; set; }
        public long PKHKeHoachXuongId { get; set; }
        public string TenKeHoachXuong { get; set; }
        public string TenXuong { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? ThuHai { get; set; }
        public int? ThuBa { get; set; }
        public int? ThuTu { get; set; }
        public int? ThuNam { get; set; }
        public int? ThuSau { get; set; }
        public int? ThuBay { get; set; }
        public int? ChuNhat { get; set; }
        public int? Tong { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHKeHoachNhapKhoThanhPhamDetailModel> PKHKeHoachNhapKhoThanhPhamDetails { get; set; }
    }
}