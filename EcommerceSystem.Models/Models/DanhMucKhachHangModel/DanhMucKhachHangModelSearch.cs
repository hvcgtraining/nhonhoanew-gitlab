﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.DanhMucKhachHangModel
{
    public class DanhMucKhachHangSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<DanhMucKhachHangModel> DanhMucKhachHangs { get; set; }
    }
}