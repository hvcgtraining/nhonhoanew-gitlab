﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.DanhMucKhachHangModel
{
    public class DanhMucKhachHangModel
    {
		public int DanhMucKhachHangId { get; set; }
		public string Title { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
		public string MaNCC { get; set; }
		public string TenNCC { get; set; }
		public string MaSoThue { get; set; }
		public string NganhNghe { get; set; }
		public string SoDienThoai { get; set; }
		public string SoFax { get; set; }
		public string Email { get; set; }
		public int? LoaiHinhKinhDoanhId { get; set; }
		public int? NhomKhachHangId { get; set; }
		public string GhiChu { get; set; }

        public string TenLoaiHinhKinhDoanh { get; set; }
        public string TenNhomKhachHang { get; set; }
    }

}
