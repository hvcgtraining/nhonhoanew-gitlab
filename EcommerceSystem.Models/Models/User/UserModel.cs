﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace EcommerceSystem.Models.User
{
    public class UserModel
    {
        public Int32 UserId { get; set; }
        public string UserName { get; set; }
        [Required(ErrorMessage = "Email đăng nhập không được để trống")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Mật khẩu không được để trống")]
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public bool IsSupperAdmin { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime LastLoginDate { get; set; }
        public DateTime LastActivityDate { get; set; }
        public bool IsLockedOut { get; set; }
        public String Avatar { get; set; }
        public bool Status { get; set; }
        public bool Remember { get; set; }
        [Required(ErrorMessage = "Nhóm không được để trống")]
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        //public List<InventoryModel> Invnetories { get; set; }
        public IList<SelectListItem> AvailableInvnetories { get; set; }
        public int? InventorySelected { get; set; }

    }
    //public class InventoryModel
    //{
    //    public int InventoryId { get; set; }
    //    public string InventoryName { get; set; }
    //    public bool Checked { get; set; }
    //}
}
