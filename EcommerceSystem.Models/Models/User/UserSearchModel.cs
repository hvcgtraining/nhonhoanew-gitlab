﻿using EcommerceSystem.Models.Common;
using System.Collections.Generic;

namespace EcommerceSystem.Models.User
{
    public class UserSearchModel : Paging
    {
        public string UserName { get; set; }

        public List<UserModel> Users { get; set; }
    }
}
