﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.VatTuNhapRiengModel
{
    public class VatTuNhapRiengSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<VatTuNhapRiengModel> VatTuNhapRiengs { get; set; }
    }
}