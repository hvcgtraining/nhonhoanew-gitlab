﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.VatTuNhapRiengModel
{
    public class VatTuNhapRiengModel
    {
		public int VatTuNhapRiengId { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Kho ID không được bỏ trống")]
        public int KhoId { get; set; }
        [Required(ErrorMessage = "Loại vật tư ID không được bỏ trống")]
        public int VatTuId { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
