﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.DanhSachDieuChuyenQuyetToanModel
{
    public class DanhSachDieuChuyenQuyetToanModel
    {
		public long DanhSachDieuChuyenQuyetToanId { get; set; }
        //[Required(ErrorMessage = "Tên danh sách không được bỏ trống")]
        public string Title { get; set; }
        public long DieuChuyenQuyetToanId { get; set; }
        public string TenDieuChuyenQuyetToan { get; set; }
        //[Required(ErrorMessage = "Số chứng từ không được bỏ trống")]
        public string SoChungTu { get; set; }
        [Required(ErrorMessage = "Vật tư đi không được bỏ trống")]
        public int? VatTuDiId { get; set; }
        public string MaVatTuDi { get; set; }
        public string TenVatTuDi { get; set; }
        public string DonViChiTietVatTu { get; set; }
        [Required(ErrorMessage = "Số lượng đi không được bỏ trống")]
        public int? SoLuongDi { get; set; }
        //[Required(ErrorMessage = "Loại vật tư về không được bỏ trống")]
        public int? VatTuVeId { get; set; }
        public string MaVatTuVe { get; set; }
        public string TenVatTuVe { get; set; }
        //[Required(ErrorMessage = "Số lượng về không được bỏ trống")]
        public int? SoLuongVeId { get; set; }
        //[Required(ErrorMessage = "Đơn vị tính không được bỏ trống")]
        public string DonViTinh { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
        public DieuChuyenType LoaiDieuChuyenQuyetToan { get; set; }

        public DanhSachQuyetToanGiaCongModel MapToQuyetToanGiaCong()
        {
            return new DanhSachQuyetToanGiaCongModel
            {
                DanhSachDieuChuyenQuyetToanId = this.DanhSachDieuChuyenQuyetToanId,
                Title = this.Title,
                DieuChuyenQuyetToanId = this.DieuChuyenQuyetToanId,
                SoChungTu = this.SoChungTu,
                VatTuDiId = this.VatTuDiId,
                SoLuongDi = this.SoLuongDi,
                VatTuVeId = this.VatTuVeId,
                SoLuongVeId = this.SoLuongVeId,
                DonViTinh = this.DonViTinh,
                CreatedDate = this.CreatedDate,
                UpdatedDate = this.UpdatedDate,
                CreatedBy = this.CreatedBy,
                UpdatedBy = this.UpdatedBy,
                Status = this.Status,
                MaVatTuDi = this.MaVatTuDi,
                MaVatTuVe = this.MaVatTuVe,
                TenVatTuDi = this.TenVatTuDi,
                TenVatTuVe = this.TenVatTuVe
            };
        }
    }

}
