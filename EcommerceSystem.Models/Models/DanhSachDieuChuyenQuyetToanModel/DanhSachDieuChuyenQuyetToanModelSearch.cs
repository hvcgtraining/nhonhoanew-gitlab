﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;
using EcommerceSystem.Models;

namespace EcommerceSystem.Models.DanhSachDieuChuyenQuyetToanModel
{
    public class DanhSachDieuChuyenQuyetToanSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public DieuChuyenType LoaiDieuChuyenQuyetToan { get; set; }
        public int DieuChuyenQuyetToanId { get; set; }
        public string TenDieuChuyenQuyetToan { get; set; }
        public List<DanhSachDieuChuyenQuyetToanModel> DanhSachDieuChuyenQuyetToans { get; set; }
    }
}