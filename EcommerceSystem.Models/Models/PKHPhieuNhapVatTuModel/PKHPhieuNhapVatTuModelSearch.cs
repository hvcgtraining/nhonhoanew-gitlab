﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHPhieuNhapVatTuModel
{
    public class PKHPhieuNhapVatTuSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHPhieuNhapVatTuModel> PKHPhieuNhapVatTus { get; set; }
    }
}