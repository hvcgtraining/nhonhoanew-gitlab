﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.PKHPhieuNhapVatTuModel
{
    public class PKHPhieuNhapVatTuModel
    {
		public long PKHPhieuNhapVatTuId { get; set; }
		public string SoChungTu { get; set; }
		public System.DateTime NgayLapPhieu { get; set; }
		public int KhoId { get; set; }
		public long PKHKiemHoaId { get; set; }
		public string Title { get; set; }
		public int DanhMucKhachHangId { get; set; }
		public string SoKiemHoa { get; set; }
		public string SoDC { get; set; }
		public byte? TrangThai { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

        public string NgayLapPhieuDangChuoi { get; set; }

        public string KhoName { get; set; }
        public string KiemHoaName { get; set; }
        public string DanhMucKhachHangName { get; set; }
        public PKHPhieuNhapVatTuModel()
        {
            //NgayLapPhieu = DateTime.Now;

            NgayLapPhieuDangChuoi = DateTime.Now.ToString("dd/MM/yyyy");
            TrangThai = (byte)TrangThaiPhieuNhapVatTu.DangCho;
        }

    }

}
