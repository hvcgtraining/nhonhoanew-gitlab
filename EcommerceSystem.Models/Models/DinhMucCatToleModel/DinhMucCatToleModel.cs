﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.DinhMucCatToleModel
{
    public class DinhMucCatToleModel
    {
		public int DinhMucCatToleId { get; set; }
		public string Title { get; set; }
		public string SoChungTu { get; set; }
		public double? Day { get; set; }
		public int? PhuongAn { get; set; }
		public string MaCT { get; set; }
		public int? SLCT { get; set; }
		public int? ChiTietVatTuId { get; set; }
		public int? ToleId { get; set; }
		public string GhiChu { get; set; }
		public int? Loai { get; set; }
		public string HinhAnh { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
        public string MaTole { get; set; }
        public string TenTole { get; set; }
        public double? DoDay { get; set; }
        public string MaChiTiet { get; set; }
        public string CreateDateValue { get; set; }
        public DinhMucCatToleModel()
        {
            CreateDateValue = DateTime.Now.ToString("dd/MM/yyyy");
        }
    }

}
