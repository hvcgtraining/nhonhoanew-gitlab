﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.DinhMucCatToleModel
{
    public class DinhMucCatToleSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<DinhMucCatToleModel> DinhMucCatToles { get; set; }
    }
}