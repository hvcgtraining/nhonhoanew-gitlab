﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.PhuLucIsoModel
{
    public class PhuLucIsoModel
    {
		public int PhuLucIsoId { get; set; }
        [Required(ErrorMessage = "Mã phụ lục không được bỏ trống")]
        public string MaPhuLucIso { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Số phụ lục không được bỏ trống")]
        public string SoPhuLuc { get; set; }
		public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
