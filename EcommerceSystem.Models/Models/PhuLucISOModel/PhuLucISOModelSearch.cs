﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PhuLucIsoModel
{
    public class PhuLucIsoSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PhuLucIsoModel> PhuLucIsos { get; set; }
    }
}