﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.PKHKeHoachSXNKXHDetailModel
{
    public class PKHKeHoachSXNKXHDetailModel
    {
		public long PKHKeHoachSXNKXHDetailId { get; set; }
		public long PKHKeHoachSXNKXHId { get; set; }
        [Required(ErrorMessage = "Mã vật tư không được bỏ trống")]
        public int VatTuId { get; set; }
		public string Title { get; set; }
		public int? TonKho { get; set; }
        [Required(ErrorMessage = "Nhập kho không được bỏ trống")]
        public int? NhapKhoTuanTruoc { get; set; }
		public int? XuatHaNoi { get; set; }
		public int? XuatKho { get; set; }
		public int? TongTonKho { get; set; }
		public int? TongNhapKho { get; set; }
		public int? XuatDaiLy { get; set; }
		public int? DuKienXuatKho { get; set; }
		public int? DuKienTonKho { get; set; }
		public int? SoLuongCan { get; set; }
		public int? SoLuongBaoBi { get; set; }
		public int? DieuChinhCan { get; set; }
		public int? DieuChinhBaoBi { get; set; }
		public int? TongSoCan { get; set; }
		public int? Xuong4 { get; set; }
		public int? Xuong5 { get; set; }
		public int? Xuong6 { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
		public int? XuongEx { get; set; }
        public string MaVatTu { get; set; }
        public string TenVatTu { get; set; }

    }

}
