﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;
using System;

namespace EcommerceSystem.Models.PKHKeHoachSXNKXHDetailModel
{
    public class PKHKeHoachSXNKXHDetailSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHKeHoachSXNKXHDetailModel> PKHKeHoachSXNKXHDetails { get; set; }
        public List<PKHKeHoachSXNKXHDetailModel> PKHKeHoachSXDetails { get; set; }
        public string MaKeHoach { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}