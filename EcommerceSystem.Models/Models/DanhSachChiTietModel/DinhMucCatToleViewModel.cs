﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceSystem.Models.DanhSachChiTietModel
{
    public class DinhMucCatToleViewModel
    {
        public int DanhSachChiTietId { get; set; }
        public int? ChiTietVatTuId { get; set; }
        public int? SoLuongCoPhanId { get; set; }
        public int? SoLuong { get; set; }
        public string GhiChu { get; set; }
        public bool HasChild { get; set; }
        public int? DinhMucCatToleId { get; set; }
        public int? DinhMucSanXuatId { get; set; }

        public int? ChiTietVatTuChildId { get; set; }
        public int? SoLuongChild { get; set; }
        public int? ParentId { get; set; }

    }
}
