﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace EcommerceSystem.Models.DanhSachChiTietModel
{
    public class DanhSachChiTietModel
    {
		public int DanhSachChiTietId { get; set; }
		public string Title { get; set; }
        [Required(ErrorMessage = "Chi tiết vật tư không được bỏ trống")]
        public int? ChiTietVatTuId { get; set; }
		public int? SoLuongCoPhanId { get; set; }
        [Required(ErrorMessage = "Số lượng không được bỏ trống")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Số lượng phải là số tự nhiên")]
        public int? SoLuong { get; set; }
		public string GhiChu { get; set; }
		public bool HasChild { get; set; }
		public int? ParentId { get; set; }
		public int? DinhMucCatToleId { get; set; }
		public int? DinhMucSanXuatId { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

        public string MaChiTietVatTu { get; set; }
        public string TenChiTietVatTu { get; set; }
        public string DonVi { get; set; }
        public List<SelectListItem> ListChiTietVatTu { get; set; }
    }

}
