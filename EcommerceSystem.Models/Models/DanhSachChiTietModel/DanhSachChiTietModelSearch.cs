﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.DanhSachChiTietModel
{
    public class DanhSachChiTietSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<DanhSachChiTietModel> DanhSachChiTiets { get; set; }
    }
}