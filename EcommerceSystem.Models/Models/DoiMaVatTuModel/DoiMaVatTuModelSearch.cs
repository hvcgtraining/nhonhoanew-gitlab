﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.DoiMaVatTuModel
{
    public class DoiMaVatTuSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<DoiMaVatTuModel> DoiMaVatTus { get; set; }
    }
}