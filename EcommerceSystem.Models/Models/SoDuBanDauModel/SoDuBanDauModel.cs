﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.SoDuBanDauModel
{
    public class SoDuBanDauModel
    {
		public int SoDuBanDauId { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        [MaxLength(250, ErrorMessage ="Không vượt quá 250 ký tự")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Kho ID không được bỏ trống")]
        public int KhoId { get; set; }
        public string MaKho { get; set; }
        [Required(ErrorMessage = "Loại vật tư ID không được bỏ trống")]
        public int? VatTuId { get; set; }
        public string MaLoaiVatTu { get; set; }
        [Required(ErrorMessage = "Vật tư không được bỏ trống")]
        public string TenVatTu { get; set; }
        [Required(ErrorMessage = "Số dư đầu kì không được bỏ trống")]
        public double? SoDuDauKy { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
