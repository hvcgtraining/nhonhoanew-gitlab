﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.SoDuBanDauModel
{
    public class SoDuBanDauSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<SoDuBanDauModel> SoDuBanDaus { get; set; }
    }
}