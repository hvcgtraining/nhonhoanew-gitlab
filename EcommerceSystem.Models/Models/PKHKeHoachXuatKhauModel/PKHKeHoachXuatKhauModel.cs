﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.PKHKeHoachXuatKhauModel
{
    public class PKHKeHoachXuatKhauModel
    {
		public int PKHKeHoachXuatKhauId { get; set; }
		public string Title { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
		public string MaHopDong { get; set; }
		public string SoHopDong { get; set; }
		public int? DanhMucKhachHangId { get; set; }
		public int? SoLuong { get; set; }
		public System.DateTime? NgayXep { get; set; }
		public System.DateTime? NgayNhap { get; set; }
		public System.DateTime? NgayCP { get; set; }
		public System.DateTime? NgaySon { get; set; }
		public System.DateTime? NgayMS { get; set; }
		public System.DateTime? NgayBB { get; set; }
		public System.DateTime? NgayTuChinh { get; set; }
		public int? Lan { get; set; }
		public System.DateTime? NgayHopDong { get; set; }
		public System.DateTime? NgayXuat { get; set; }
		public System.DateTime? NgayLapPhieu { get; set; }
		public int? Tong { get; set; }

    }

}
