﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHKeHoachXuatKhauModel
{
    public class PKHKeHoachXuatKhauSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHKeHoachXuatKhauModel> PKHKeHoachXuatKhaus { get; set; }
    }
}