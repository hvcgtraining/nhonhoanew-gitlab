﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.LoaiVatTuModel
{
    public class LoaiVatTuSearchModel : Paging
    {
        public string TextSearch { get; set; }
        public int searchType { get; set; }
        public int? cap { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<LoaiVatTuModel> LoaiVatTus { get; set; }
    }
}