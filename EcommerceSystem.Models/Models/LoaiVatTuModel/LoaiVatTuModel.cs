﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.LoaiVatTuModel
{
    public class LoaiVatTuModel
    {
		public int LoaiVatTuId { get; set; }
        [Required(ErrorMessage = "Mã loại vật tư không được bỏ trống")]
        public string MaLoaiVatTu { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Cấp không được bỏ trống")]
        public int? Cap { get; set; }
        //[Required(ErrorMessage = "Phân loại vật tư không được bỏ trống")]
        public int? Type { get; set; }
        public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
