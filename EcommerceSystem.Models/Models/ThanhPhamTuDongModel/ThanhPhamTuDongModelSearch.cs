﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.ThanhPhamTuDongModel
{
    public class ThanhPhamTuDongSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<ThanhPhamTuDongModel> ThanhPhamTuDongs { get; set; }
    }
}