﻿
using System;
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHPhieuYeuCauCapHangDetailModel
{
    public class PKHPhieuYeuCauCapHangDetailSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHPhieuYeuCauCapHangDetailModel> PKHPhieuYeuCauCapHangDetails { get; set; }
        public string NgayTao { get; set; }
        public string SoChungTu { get; set; }
        public string MaThanhPham { get; set; }
        public string SoLuong { get; set; }
        public string Kho { get; set; }
        public string NguoiGiao { get; set; }
        public string NguoiNhan { get; set; }
        public string LyDo { get; set; }
    }
}