﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.PKHPhieuYeuCauCapHangDetailModel
{
    public class PKHPhieuYeuCauCapHangDetailModel
    {
		public long PKHPhieuYeuCauCapHangDetailId { get; set; }
		public long PKHPhieuYeuCauCapHangId { get; set; }
		public int? ChiTietVatTuId { get; set; }
		public double? SoLuongXuat { get; set; }
		public double? SoTam { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
		public string Title { get; set; }
        public string MaChiTiet { get; set; }
        public string TenChiTiet { get; set; }

    }

}
