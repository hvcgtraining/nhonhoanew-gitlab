﻿namespace EcommerceSystem.Models.Common
{
    public class Filter
    {
        public string FieldName { get; set; }
        public bool IsAscending { get; set; }
    }
}
