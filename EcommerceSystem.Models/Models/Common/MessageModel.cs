﻿namespace EcommerceSystem.Models.Common
{
    public class MessagesModel
    {
        public MessageType MessageType { get; set; }
        public string MessageContent { get; set; }
        public string LinkText { get; set; }
        public string LinkUrl { get; set; }
    }
}
