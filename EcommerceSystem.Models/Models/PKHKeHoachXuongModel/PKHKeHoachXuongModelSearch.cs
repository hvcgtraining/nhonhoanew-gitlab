﻿
using System;
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHKeHoachXuongModel
{
    public class PKHKeHoachXuongSearchModel : Paging
    {
        public string TextSearch { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? XuongId { get; set; }
        public List<PKHKeHoachXuongModel> PKHKeHoachXuongs { get; set; }
    }
}