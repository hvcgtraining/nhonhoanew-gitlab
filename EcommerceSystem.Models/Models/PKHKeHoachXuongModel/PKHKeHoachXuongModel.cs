﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.PKHKeHoachXuongModel
{
    public class PKHKeHoachXuongModel
    {
		public long PKHKeHoachXuongId { get; set; }
		public string Title { get; set; }
		public long PKHKeHoachSXNKXHId { get; set; }
		public int? XuongId { get; set; }
		public int? TongSoLuong { get; set; }
		public bool? ThemGio { get; set; }
		public byte? TinhTrang { get; set; }
		public string GhiChu { get; set; }
        public string TenXuong { get; set; }
        public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

}
