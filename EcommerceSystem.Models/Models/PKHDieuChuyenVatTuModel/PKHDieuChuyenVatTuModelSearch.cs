﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHDieuChuyenVatTuModel
{
    public class PKHDieuChuyenVatTuSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHDieuChuyenVatTuModel> PKHDieuChuyenVatTus { get; set; }
    }
}