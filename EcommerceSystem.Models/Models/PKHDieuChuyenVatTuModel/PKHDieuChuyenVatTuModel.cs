﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.PKHDieuChuyenVatTuModel
{
    public class PKHDieuChuyenVatTuModel
    {
		public long PKHDieuChuyenVatTuId { get; set; }
		public long? PKHKeHoachXuongId { get; set; }
		public long? PKHKeHoachSXNKXHId { get; set; }
		public System.DateTime? NgayLap { get; set; }
		public string SoChungTu { get; set; }
		public int? LyDoXuatId { get; set; }
		public string LyDoXuat { get; set; }
		public byte? LoaiDieuChuyen { get; set; }
		public int? KhoXuatId { get; set; }
		public int? KhoNhapId { get; set; }
		public int? XuongXuatId { get; set; }
		public int? XuongNhapId { get; set; }
		public int? NguoiNhanId { get; set; }
		public string NguoiNhan { get; set; }
		public int? NguoiGiaoId { get; set; }
		public string NguoiGiao { get; set; }
		public string GhiChu { get; set; }
		public byte? TinhTrang { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
		public string Title { get; set; }
        [Required(ErrorMessage = "Ngày lập không được bỏ trống")]
        public string NgayLapDangChuoi { get; set; }
        public string PKHKeHoachXuongName { get; set; }
        public string PKHKeHoachSXNKXHName { get; set; }
        public string LyDoXuatName { get; set; }
        public string KhoXuatName { get; set; }
        public string KhoNhapName { get; set; }
        public string XuongXuatName { get; set; }
        public string XuongNhapName { get; set; }
        public string NguoiNhapName { get; set; }
        public string NguoiGiaoName { get; set; }
        public PKHDieuChuyenVatTuModel()
        {
            NgayLapDangChuoi = DateTime.Now.ToString("dd/MM/yyyy");
            TinhTrang = (byte)TrangThaiDieuChuyenVatTu.DangCho;
        }
    }

}
