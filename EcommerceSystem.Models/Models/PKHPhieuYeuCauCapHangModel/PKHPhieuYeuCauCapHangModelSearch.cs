﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHPhieuYeuCauCapHangModel
{
    public class PKHPhieuYeuCauCapHangSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHPhieuYeuCauCapHangModel> PKHPhieuYeuCauCapHangs { get; set; }
    }
}