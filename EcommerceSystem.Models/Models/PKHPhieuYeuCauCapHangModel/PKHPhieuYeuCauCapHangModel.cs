﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.PKHPhieuYeuCauCapHangModel
{
    public class PKHPhieuYeuCauCapHangModel
    {
		public long PKHPhieuYeuCauCapHangId { get; set; }
		public long KeHoachXuongId { get; set; }
		public long? PKHKeHoachSXNKXHId { get; set; }
        [Required(ErrorMessage = "Số CT không được bỏ trống")]
        public string SoCT { get; set; }
        [Required(ErrorMessage = "Ngày lập không được bỏ trống")]
        public System.DateTime NgayLap { get; set; }
		public int? LoaiVatTuId { get; set; }
		public int? SoLuong { get; set; }
		public int? NguoiNhanId { get; set; }
		public string NguoiNhan { get; set; }
		public int? NguoiGiaoId { get; set; }
		public string NguoiGiao { get; set; }
		public int? LyDoXuatId { get; set; }
		public string LyDoXuat { get; set; }
		public string SoDieuChuyen { get; set; }
		public byte? TrangThai { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
		public string Title { get; set; }
		public int? KhoId { get; set; }
        public string MaTP { get; set; }
        public string TenTP { get; set; }
        public string Kho { get; set; }
        public string CreateDateValue { get; set; }
        public PKHPhieuYeuCauCapHangModel()
        {
            CreateDateValue = DateTime.Now.ToString("dd/MM/yyyy");
        }

    }

}
