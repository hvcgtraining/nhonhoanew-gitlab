﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.NhomYeuCauModel
{
    public class NhomYeuCauSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<NhomYeuCauModel> NhomYeuCaus { get; set; }
    }
}