﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.NhomYeuCauModel
{
    public class NhomYeuCauModel
    {
		public long NhomYeuCauId { get; set; }
        [Required(ErrorMessage = "Tên nhóm yêu cầu không được bỏ trống")]
        public string Title { get; set; }
        public string MaVatTu { get; set; }
        public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
        public long PKHPhieuYeuCauMuaHangId { get; set; }

    }

}
