﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;
using System;

namespace EcommerceSystem.Models.PKHKeHoachSXNKXHModel
{
    public class PKHKeHoachSXNKXHSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHKeHoachSXNKXHModel> PKHKeHoachSXNKXHs { get; set; }
        public List<PKHKeHoachSXNKXHModel> PKHKeHoachSXs { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public PKHKeHoachSXNKXHSearchModel()
        {
            //FromDate = DateTime.Now.AddDays(-6).ToString("dd/MM/yyyy");
            //ToDate = DateTime.Now.ToString("dd/MM/yyyy");
        }
    }
}