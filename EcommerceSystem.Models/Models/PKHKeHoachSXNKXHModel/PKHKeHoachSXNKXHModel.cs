﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace EcommerceSystem.Models.PKHKeHoachSXNKXHModel
{
    public class PKHKeHoachSXNKXHModel
    {
		public long PKHKeHoachSXNKXHId { get; set; }
		public string Title { get; set; }
        [Required(ErrorMessage = "Mã tuần không được bỏ trống")]
        public string MaTuan { get; set; }
        [Required(ErrorMessage = "Từ ngày không được bỏ trống")]
        public System.DateTime StartDate { get; set; }
        [Required(ErrorMessage = "Đến ngày không được bỏ trống")]
        public System.DateTime EndDate { get; set; }
        [Required(ErrorMessage = "Mã tuần không được bỏ trống")]
        public System.DateTime NgayTonKho { get; set; }
        [Required(ErrorMessage = "Ngày tồn không được bỏ trống")]
        public short? SoNgayXuatKho { get; set; }
		public int? SoLuongCan { get; set; }
		public int? SoLuongTon { get; set; }
		public byte? TinhTrang { get; set; }
		public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
        [Required(ErrorMessage = "Từ ngày không được bỏ trống")]
        public string StartDateValue { get; set; }
        [Required(ErrorMessage = "Đến ngày không được bỏ trống")]
        public string EndDateValue { get; set; }
        [Required(ErrorMessage = "Ngày tồn không được bỏ trống")]
        public string NgayTonKhoValue { get; set; }
        public PKHKeHoachSXNKXHModel()
        {
            StartDateValue = DateTime.Now.ToString("dd/MM/yyyy");
            EndDateValue = DateTime.Now.AddDays(6).ToString("dd/MM/yyyy");
            NgayTonKhoValue = DateTime.Now.ToString("dd/MM/yyyy");
            SoNgayXuatKho = 6;
            TinhTrang = (byte)TrangThaiKeHoach.DangCho;
            MaTuan = GetWeekOfYear(DateTime.Now).ToString();
        }

        public int GetWeekOfYear(DateTime date)
        {
            if (date == null)
                return 0;

            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            Calendar cal = dfi.Calendar;

            return cal.GetWeekOfYear(date, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
        }

    }

}
