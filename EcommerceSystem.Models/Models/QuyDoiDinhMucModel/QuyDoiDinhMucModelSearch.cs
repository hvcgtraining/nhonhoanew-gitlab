﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.QuyDoiDinhMucModel
{
    public class QuyDoiDinhMucSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<QuyDoiDinhMucModel> QuyDoiDinhMucs { get; set; }
    }
}