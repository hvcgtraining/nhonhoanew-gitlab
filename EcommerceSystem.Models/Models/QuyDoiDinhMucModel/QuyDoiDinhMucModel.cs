﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.QuyDoiDinhMucModel
{
    public class QuyDoiDinhMucModel
    {
		public int QuyDoiDinhMucId { get; set; }
		public string MaQuyDoiDinhMuc { get; set; }
		public string Title { get; set; }
		public double? DinhMuc { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
