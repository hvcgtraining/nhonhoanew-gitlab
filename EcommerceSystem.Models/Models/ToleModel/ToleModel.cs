﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace EcommerceSystem.Models.ToleModel
{
    public class ToleModel
    {
		public int ToleId { get; set; }
        [Required(ErrorMessage = "Mã Tole không được bỏ trống")]
        public string MaTole { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Đơn vị tính không được bỏ trống")]
        public string DonVi { get; set; }
        [Required(ErrorMessage = "Dầy không được bỏ trống")]
        public double? Day { get; set; }
        [Required(ErrorMessage = "Ngang không được bỏ trống")]
        public double? Ngang { get; set; }
        [Required(ErrorMessage = "Đứng không được bỏ trống")]
        public double? Dung { get; set; }
        [Required(ErrorMessage = "Khối lượng không được bỏ trống")]
        public double? KhoiLuong { get; set; }
        [Required(ErrorMessage = "Dechet ID không được bỏ trống")]
        public int? DechetId { get; set; }
        public string DechetName { get; set; }
        public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

        public List<SelectListItem> ListDechet { get; set; }
    }

}
