﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.NhomVatTuNhapRiengModel
{
    public class NhomVatTuNhapRiengSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<NhomVatTuNhapRiengModel> NhomVatTuNhapRiengs { get; set; }
    }
}