﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.NhomVatTuNhapRiengModel
{
    public class NhomVatTuNhapRiengModel
    {
		public int NhomVatTuNhapRiengId { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Kho ID không được bỏ trống")]
        public int KhoId { get; set; }
        [Required(ErrorMessage = "Dạng mã không được bỏ trống")]
        public string DangMa { get; set; }
        [Required(ErrorMessage = "Tên nhóm không được bỏ trống")]
        public string TenNhom { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
