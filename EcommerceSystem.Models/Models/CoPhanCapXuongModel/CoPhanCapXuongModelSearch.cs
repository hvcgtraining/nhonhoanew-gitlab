﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.CoPhanCapXuongModel
{
    public class CoPhanCapXuongSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<CoPhanCapXuongModel> CoPhanCapXuongs { get; set; }
    }
}