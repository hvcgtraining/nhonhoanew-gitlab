﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.CoPhanCapXuongModel
{
    public class CoPhanCapXuongModel
    {
		public int CoPhanCapXuongId { get; set; }
        [Required(ErrorMessage = "Tên cơ phận không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Kho nhập ID không được bỏ trống")]
        public int KhoNhapId { get; set; }
        public string MaKhoNhap { get; set; }
        [Required(ErrorMessage = "Kho xuất ID không được bỏ trống")]
        public int KhoXuatId { get; set; }
        public string MaKhoXuat { get; set; }
        [Required(ErrorMessage = "Loại vật tư ID không được bỏ trống")]
        public int? VatTuId { get; set; }
        public string MaVatTu { get; set; }
        public string TenVatTu { get; set; }
        [Required(ErrorMessage = "Nhóm sản phẩm ID không được bỏ trống")]
        public int? NhomSanPhamId { get; set; }
        public string MaNhom { get; set; }
        public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
