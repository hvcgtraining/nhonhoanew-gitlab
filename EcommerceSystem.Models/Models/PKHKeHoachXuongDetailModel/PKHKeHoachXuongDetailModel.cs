﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.PKHKeHoachXuongDetailModel
{
    public class PKHKeHoachXuongDetailModel
    {
		public long PKHKeHoachXuongDetailId { get; set; }
		public long? PKHKeHoachSXNKXHDetailId { get; set; }
        [Required(ErrorMessage = "Vật tư không được bỏ trống")]
        public int? ChiTietVatTuId { get; set; }
        public string MaVatTu { get; set; }
        public string TenVatTu { get; set; }
        public int? KhoId { get; set; }
        public string TenKho { get; set; }
        public string Title { get; set; }
		public int DinhMucSanXuatId { get; set; }
		public long PKHKeHoachXuongId { get; set; }
		public int? DinhMuc { get; set; }
		public double? ThoiLuong { get; set; }
        [Required(ErrorMessage = "Số lượng yêu cầu không được bỏ trống")]
        public int? YeuCau { get; set; }
		public int? ChenhLech { get; set; }
		public int? UuTien { get; set; }
		public int? TonKho { get; set; }
		public string ChiTietTon { get; set; }
		public int? TonDuKien { get; set; }
		public System.DateTime? DauKy { get; set; }
		public System.DateTime? NgayCapHang { get; set; }
        [Required(ErrorMessage = "Ngày cấp hàng không được bỏ trống")]
        public string NgayCapHangValue { get; set; }
        public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
        public PKHKeHoachXuongDetailModel()
        {
            NgayCapHangValue = DateTime.Now.ToString("dd/MM/yyyy");
        }
    }

}
