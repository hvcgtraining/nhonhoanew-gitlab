﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHKeHoachXuongDetailModel
{
    public class PKHKeHoachXuongDetailSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHKeHoachXuongDetailModel> PKHKeHoachXuongDetails { get; set; }
    }
}