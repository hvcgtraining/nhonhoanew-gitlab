﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.KyTenModel
{
    public class KyTenModel
    {
		public int KyTenId { get; set; }
		public string MaKyTen { get; set; }
		public string Title { get; set; }
		public int? NhanVienId { get; set; }
		public string TenThuKho { get; set; }
		public string GhiChu { get; set; }
		public int? XuongId { get; set; }
		public byte? LoaiKyTen { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
