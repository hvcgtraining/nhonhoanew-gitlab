﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.YeuCauMuaHangModel
{
    public class YeuCauMuaHangSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<YeuCauMuaHangModel> YeuCauMuaHangs { get; set; }
    }
}