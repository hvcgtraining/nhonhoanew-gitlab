﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.YeuCauMuaHangModel
{
    public class YeuCauMuaHangModel
    {
        public long YeuCauMuaHangId { get; set; }
        [Required(ErrorMessage = "Xưởng không được bỏ trống")]
        public int? XuongId { get; set; }
        public string Xuong { get; set; }
        [Required(ErrorMessage = "Kho không được bỏ trống")]
        public int? KhoId { get; set; }
        public string Kho { get; set; }
        [Required(ErrorMessage = "Chi tiết vật tư không được bỏ trống")]
        public int? ChiTietVatTuId { get; set; }
        public string TenChiTietVatTu { get; set; }
        public long? PKHPhieuYeuCauMuaHangId { get; set; }
        public string TenPhieuYeuCauMuaHang { get; set; }
        [Required(ErrorMessage = "tên yêu cầu không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Số PR không được bỏ trống")]
        public string SoPR { get; set; }
        [Required(ErrorMessage = "Ngày yêu cầu không được bỏ trống")]
        public System.DateTime? NgayYeuCau { get; set; }
        [Required(ErrorMessage = "Ngày kết thúc không được bỏ trống")]
        public System.DateTime? NgayKetThucDuKien { get; set; }
        public int? TongSoLuong { get; set; }
        public string NoiDung { get; set; }
        public string GhiChu { get; set; }
        [Required(ErrorMessage = "Trạng thái không được bỏ trống")]
        public int? TrangThai { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool Status { get; set; }
        public YeuCauMuaHangModel()
        {
            NgayYeuCau = DateTime.Now;
            NgayKetThucDuKien = DateTime.Now;
        }

    }

}
