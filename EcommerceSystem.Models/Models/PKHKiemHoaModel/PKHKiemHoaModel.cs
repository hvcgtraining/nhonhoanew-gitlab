﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.PKHKiemHoaModel
{
    public class PKHKiemHoaModel
    {
		public long PKHKiemHoaId { get; set; }
		public byte LoaiKiemHoa { get; set; }
		public string SoKiemHoa { get; set; }
		public System.DateTime? NgayLapPhieu { get; set; }
		public int? KhoId { get; set; }
		public string Title { get; set; }
		public string MaKiemHoa { get; set; }
		public string PhieuNhap { get; set; }
		public int? SoHD { get; set; }
		public byte? TrangThai { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
