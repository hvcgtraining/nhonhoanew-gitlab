﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.VatTuCapRiengModel
{
    public class VatTuCapRiengSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<VatTuCapRiengModel> VatTuCapRiengs { get; set; }
    }
}