﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHKeHoachXuatKhauDetailModel
{
    public class PKHKeHoachXuatKhauDetailSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHKeHoachXuatKhauDetailModel> PKHKeHoachXuatKhauDetails { get; set; }
    }
}