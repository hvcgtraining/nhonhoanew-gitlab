﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.PKHKeHoachXuatKhauDetailModel
{
    public class PKHKeHoachXuatKhauDetailModel
    {
		public int PKHKeHoachXuatKhauDetailId { get; set; }
		public string Title { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
		public int? PKHKeHoachXuatKhauId { get; set; }
		public int? VatTuId { get; set; }
		public int? SoLuong { get; set; }
		public int? TongSo { get; set; }
		public int? XuongId { get; set; }
		public string LoaiCan { get; set; }
		public string GhiChu { get; set; }

    }

}
