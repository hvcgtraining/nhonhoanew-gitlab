﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.ThanhPhamXuongModel
{
    public class ThanhPhamXuongModel
    {
		public int ThanhPhamXuongId { get; set; }
        [Required(ErrorMessage = "Tên thành phẩm xưởng không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Tên xưởng không được bỏ trống")]
        public int XuongId { get; set; }
        public string MaXuong { get; set; }
        [Required(ErrorMessage = "Loại vật tư không được bỏ trống")]
        public int VatTuId { get; set; }
        public string MaVatTu { get; set; }
        public string TenVatTu { get; set; }
        [Required(ErrorMessage = "Nhóm sản phẩm không được bỏ trống")]
        public int? NhomSanPhamId { get; set; }
        public string TenNhomSanPham { get; set; }
        //[Required(ErrorMessage = "Loại thành phẩm không được bỏ trống")]
        public string Loai { get; set; }
        [Required(ErrorMessage = "Năng suất tuần không được bỏ trống")]
        public string NangSuatTuan { get; set; }
        [Required(ErrorMessage = "Loại vật liệu không được bỏ trống")]
        public string LoaiVatLieu { get; set; }
        public string QuyCach { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
