﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.ThanhPhamXuongModel
{
    public class ThanhPhamXuongSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<ThanhPhamXuongModel> ThanhPhamXuongs { get; set; }
    }
}