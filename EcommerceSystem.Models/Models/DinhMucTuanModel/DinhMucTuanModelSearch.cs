﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.DinhMucTuanModel
{
    public class DinhMucTuanSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<DinhMucTuanModel> DinhMucTuans { get; set; }
    }
}