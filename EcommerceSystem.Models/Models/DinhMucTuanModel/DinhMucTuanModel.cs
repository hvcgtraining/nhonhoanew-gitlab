﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace EcommerceSystem.Models.DinhMucTuanModel
{
    public class DinhMucTuanModel
    {
        public int DinhMucTuanId { get; set; }
        [Required(ErrorMessage = "Tên định mức tuần không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Loại vật tư ID không được bỏ trống")]
        public int VatTuId { get; set; }
        [Required(ErrorMessage = "Định mức tuần không được bỏ trống")]
        public double? DinhMuc { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

        public string MaLoaiVatTu { get; set; }
        public string TenLoaiVatTu { get; set; }
        public List<SelectListItem> ListLoaiVatTu { get; set; }

    }

}
