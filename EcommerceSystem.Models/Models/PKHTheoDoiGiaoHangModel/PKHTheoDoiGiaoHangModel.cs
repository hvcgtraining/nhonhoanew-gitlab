﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.PKHTheoDoiGiaoHangModel
{
    public class PKHTheoDoiGiaoHangModel
    {
		public long PKHTheoDoiGiaoHangId { get; set; }
		public long PKHPhieuYeuCauMuaHangId { get; set; }
		public long PKHPhieuYeuCauMuaHangDetailId { get; set; }
		public long PKHDuKienGiaoHangId { get; set; }
		public long PKHKiemHoaId { get; set; }
		public System.DateTime? NgayGiaoHang { get; set; }
		public int? ChiTietVatTuId { get; set; }
		public int? CustomerId { get; set; }
		public string Title { get; set; }
		public int? SoLuongGiaoDuKien { get; set; }
		public int? SoLuongGiaoThucTe { get; set; }
		public int? SoLuongConThieu { get; set; }
		public bool KiemHoaFlag { get; set; }
		public int? SoLuongDat { get; set; }
		public int? SoLuongKhongDat { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
