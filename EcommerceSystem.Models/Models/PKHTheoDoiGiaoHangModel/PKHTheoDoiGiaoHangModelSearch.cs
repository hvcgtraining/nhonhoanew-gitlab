﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHTheoDoiGiaoHangModel
{
    public class PKHTheoDoiGiaoHangSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHTheoDoiGiaoHangModel> PKHTheoDoiGiaoHangs { get; set; }
    }
}