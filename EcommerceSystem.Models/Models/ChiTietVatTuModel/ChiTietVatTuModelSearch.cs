﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.ChiTietVatTuModel
{
    public class ChiTietVatTuSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<ChiTietVatTuModel> ChiTietVatTus { get; set; }
    }
}