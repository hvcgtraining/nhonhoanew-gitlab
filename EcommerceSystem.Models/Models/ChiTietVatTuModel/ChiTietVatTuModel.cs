﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.ChiTietVatTuModel
{
    public class ChiTietVatTuModel
    {
		public int ChiTietVatTuId { get; set; }
        [Required(ErrorMessage = "Mã chi tiết vật tư không được bỏ trống")]
        public string MaChiTietVatTu { get; set; }
        [Required(ErrorMessage = "Tên vật tư không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Đơn vị tính không được bỏ trống")]
        public string DonVi { get; set; }
		public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
        [Required(ErrorMessage = "Loại vật tư không được bỏ trống")]
        public int? LoaiVatTuId { get; set; }
        public string MaLoaiVatTu { get; set; }
        public string TenLoaiVatTu { get; set; }
    }

}
