﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.NhomSanPhamModel
{
    public class NhomSanPhamSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<NhomSanPhamModel> NhomSanPhams { get; set; }
    }
}