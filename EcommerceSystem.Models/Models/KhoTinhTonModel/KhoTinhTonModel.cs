﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.KhoTinhTonModel
{
    public class KhoTinhTonModel
    {
		public int KhoTinhTonId { get; set; }
        [Required(ErrorMessage="Tên kho tính tồn không được bỏ trống")]
		public string Title { get; set; }
        [Required(ErrorMessage = "Kho ID không được bỏ trống")]
        public int KhoId { get; set; }
        [Required(ErrorMessage = "Nhóm sản phẩm ID không được bỏ trống")]
        public int? NhomSanPhamId { get; set; }
		public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
        public string KhoName { get; set; }
        public string NhomSanPhamName { get; set; }
    }

}
