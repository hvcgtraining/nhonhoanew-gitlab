﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.KhoTinhTonModel
{
    public class KhoTinhTonSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<KhoTinhTonModel> KhoTinhTons { get; set; }
    }
}