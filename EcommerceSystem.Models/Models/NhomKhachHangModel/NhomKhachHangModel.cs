﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.NhomKhachHangModel
{
    public class NhomKhachHangModel
    {
		public int NhomKhachHangId { get; set; }
		public string Title { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
