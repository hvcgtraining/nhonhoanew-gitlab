﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.NhomKhachHangModel
{
    public class NhomKhachHangSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<NhomKhachHangModel> NhomKhachHangs { get; set; }
    }
}