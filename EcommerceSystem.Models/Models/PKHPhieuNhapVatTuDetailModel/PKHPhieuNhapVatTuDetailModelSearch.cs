﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHPhieuNhapVatTuDetailModel
{
    public class PKHPhieuNhapVatTuDetailSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHPhieuNhapVatTuDetailModel> PKHPhieuNhapVatTuDetails { get; set; }
    }
}