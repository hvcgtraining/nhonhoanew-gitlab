﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.PKHPhieuNhapVatTuDetailModel
{
    public class PKHPhieuNhapVatTuDetailModel
    {
		public long PKHPhieuNhapVatTuDetailId { get; set; }
		public long PKHPhieuNhapVatTuId { get; set; }
		public int? KhoId { get; set; }
		public int? CustomerId { get; set; }
		public string Title { get; set; }
		public int? ChiTietVatTuId { get; set; }
		public int? SoLuong { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
