﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.QuanLySoNModel
{
    public class QuanLySoNModel
    {
		public int QuanLySoNId { get; set; }
		public string Title { get; set; }
		public int? SoTong { get; set; }
		public int? SoN { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
		public int? VatTuId { get; set; }

    }

}
