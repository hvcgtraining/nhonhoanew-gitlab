﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.QuanLySoNModel
{
    public class QuanLySoNSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<QuanLySoNModel> QuanLySoNs { get; set; }
    }
}