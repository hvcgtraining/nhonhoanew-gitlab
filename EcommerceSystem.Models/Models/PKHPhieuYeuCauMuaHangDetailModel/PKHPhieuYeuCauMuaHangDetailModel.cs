﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.PKHPhieuYeuCauMuaHangDetailModel
{
    public class PKHPhieuYeuCauMuaHangDetailModel
    {
        public long PKHPhieuYeuCauMuaHangDetailId { get; set; }
        public long PKHPhieuYeuCauMuaHangId { get; set; }
        public string TenPhieuYeuCauMuaHang { get; set; }
        [Required(ErrorMessage = "Chi tiết vật tư không được bỏ trống")]
        public int ChiTietVatTuId { get; set; }
        public string MaChiTietVatTu { get; set; }
        public string TenChiTietVatTu { get; set; }
        public string DonViTinh { get; set; }
        [Required(ErrorMessage = "Tên chi tiết yêu cầu mua hàng không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Số lượng không được bỏ trống")]
        public int? SoLuong { get; set; }
        [Required(ErrorMessage = "Đơn giá tham khảo không được bỏ trống")]
        public int DonGiaThamKhao { get; set; }
        [Required(ErrorMessage = "Quy cách không được bỏ trống")]
        public string QuyCach { get; set; }
        [Required(ErrorMessage = "Đơn giá không được bỏ trống")]
        public int? DonGia { get; set; }
        [Required(ErrorMessage = "Nhóm yêu cầu không được bỏ trống")]
        public long? NhomYeuCauId { get; set; }
        public string TenNhomYeuCau { get; set; }
        public string GhiChu { get; set; }
        public int? TongSoLuong { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool Status { get; set; }

    }

}
