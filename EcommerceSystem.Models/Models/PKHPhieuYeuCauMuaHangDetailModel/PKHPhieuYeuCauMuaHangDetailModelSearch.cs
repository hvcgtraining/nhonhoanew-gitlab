﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHPhieuYeuCauMuaHangDetailModel
{
    public class PKHPhieuYeuCauMuaHangDetailSearchModel : Paging
    {
        public string TextSearch { get; set; }
        public long PKHPhieuYeuCauMuaHangId { get; set; }
        public string TenPhieuYeuCauMuaHang { get; set; }
        public long PKHPhieuYeuCauMuaHangDetailId { get; set; }
        public int? MaNhom { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public bool IsCreateRequest { get; set; }
        public List<PKHPhieuYeuCauMuaHangDetailModel> PKHPhieuYeuCauMuaHangDetails { get; set; }
        public PKHPhieuYeuCauMuaHangDetailSearchModel()
        {
            IsCreateRequest = true;
        }
    }
}