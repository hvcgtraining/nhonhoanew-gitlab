﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.PKHPhieuYeuCauMuaHangModel
{
    public class PKHPhieuYeuCauMuaHangModel
    {
		public long PKHPhieuYeuCauMuaHangId { get; set; }
        [Required(ErrorMessage = "Tên phiếu yêu cầu mua hàng không được bỏ trống")]
		public string Title { get; set; }
        [Required(ErrorMessage = "Ngày yêu cầu không được bỏ trống")]
        public System.DateTime NgayYeuCau { get; set; }
        [Required(ErrorMessage = "Ngày dự kiến không được bỏ trống")]
        public System.DateTime NgayDuKien { get; set; }
        [Required(ErrorMessage = "Ngày hoàn tất không được bỏ trống")]
        public System.DateTime? NgayHoanTat { get; set; }
        [Required(ErrorMessage = "Trạng thái không được bỏ trống")]
        public byte? TrangThai { get; set; }
        [Required(ErrorMessage = "Yêu cầu kết thúc không được bỏ trống")]
        public string YeuCauKT { get; set; }
		public string NoiDung { get; set; }
        [Required(ErrorMessage = "Địa chỉ công ty không được bỏ trống")]
        public string DiaDiem { get; set; }
		public string MucDich { get; set; }
		public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
        public PKHPhieuYeuCauMuaHangModel()
        {
            NgayYeuCau = DateTime.Now.Date;
            NgayDuKien = DateTime.Now.Date;
            NgayHoanTat = DateTime.Now.Date;
        }
    }

}
