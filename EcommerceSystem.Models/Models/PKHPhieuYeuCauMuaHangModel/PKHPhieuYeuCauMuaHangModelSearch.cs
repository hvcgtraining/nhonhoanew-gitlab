﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHPhieuYeuCauMuaHangModel
{
    public class PKHPhieuYeuCauMuaHangSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public bool IsCreateRequest { get; set; }
        public List<PKHPhieuYeuCauMuaHangModel> PKHPhieuYeuCauMuaHangs { get; set; }
        public PKHPhieuYeuCauMuaHangSearchModel()
        {
            IsCreateRequest = false;
        }
    }
}