﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.XuongModel
{
    public class XuongModel
    {
		public int XuongId { get; set; }
		public string MaXuong { get; set; }
		public string Title { get; set; }
		public string ThongTinXuong { get; set; }
		public string GhiChu { get; set; }
		public int? Type { get; set; }
		public System.DateTime? CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool? Status { get; set; }

    }

}
