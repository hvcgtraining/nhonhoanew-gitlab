﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.GoiDauModel
{
    public class GoiDauModel
    {
        public int GoiDauId { get; set; }
        [Required(ErrorMessage ="Tên thời gian gối đầu không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Loại vật tư không được bỏ trống")]
        public int VatTuId { get; set; }
        public string MaLoaiVatTu { get; set; }
        public string TenLoaiVatTu { get; set; }
        [Required(ErrorMessage = "Số ngày gối đầu không được bỏ trống")]
        public int? NgayGoiDau { get; set; }
        [Required(ErrorMessage = "Số tuần dự phòng không được bỏ trống")]
        public int? TuanDuPhong { get; set; }
        [Required(ErrorMessage = "Kế hoạch SX-NK-XH không được bỏ trống")]
        public long? PKHKeHoachSXNKXHId { get; set; }
        public string TenPKHKeHoachSXNKXH { get; set; }
        [Required(ErrorMessage = "Mã kế hoạch không được bỏ trống")]
        public string MaKeHoach { get; set; }
        public string GhiChu { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool Status { get; set; }

    }

}
