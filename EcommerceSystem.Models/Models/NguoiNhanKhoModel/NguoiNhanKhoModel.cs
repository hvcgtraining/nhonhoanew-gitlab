﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.NguoiNhanKhoModel
{
    public class NguoiNhanKhoModel
    {
		public int NguoiNhanKhoId { get; set; }
		public string Title { get; set; }
		public int KhoId { get; set; }
        public string KhoName { get; set; }
        public string MaTen { get; set; }
		public string TenGiaoNhan { get; set; }
		public string GhiChu { get; set; }
		public string MaTruongDonVi { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
