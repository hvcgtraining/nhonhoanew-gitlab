﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.NguoiNhanKhoModel
{
    public class NguoiNhanKhoSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<NguoiNhanKhoModel> NguoiNhanKhos { get; set; }
    }
}