﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.KhoiLuongCoPhanModel
{
    public class KhoiLuongCoPhanSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<KhoiLuongCoPhanModel> KhoiLuongCoPhans { get; set; }
    }
}