﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.KhoiLuongCoPhanModel
{
    public class KhoiLuongCoPhanModel
    {
		public int KhoiLuongCoPhanId { get; set; }
        [Required(ErrorMessage = "Mã khối lượng cơ phận không được bỏ trống")]
        public string MaKhoiLuongCoPhan { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Khối lượng không được bỏ trống")]
        public double? KhoiLuong { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
