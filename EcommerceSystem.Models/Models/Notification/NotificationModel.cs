﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceSystem.Models
{
    public class NotificationModel
    {
        public int NotificationId { get; set; }

        public int? NotificationRecipientId { get; set; }

        [Required(ErrorMessage = "Tiêu đề thông báo không được để trống")]
        public string Title { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required(ErrorMessage = "Nội dung thông báo không được để trống")]
        public string Message { get; set; }
        public int Type { get; set; }
        public bool IsActive { get; set; }
        public bool IsRead { get; set; }
    }
}
