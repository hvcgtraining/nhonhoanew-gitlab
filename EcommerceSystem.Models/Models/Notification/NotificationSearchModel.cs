﻿using EcommerceSystem.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceSystem.Models
{
    public class NotificationSearchModel : Paging
    {
        public string TextSearch { get; set; }

        public List<NotificationModel> Notifications { get; set; }
    }
}
