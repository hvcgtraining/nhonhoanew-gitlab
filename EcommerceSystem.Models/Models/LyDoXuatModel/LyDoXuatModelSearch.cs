﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.LyDoXuatModel
{
    public class LyDoXuatSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<LyDoXuatModel> LyDoXuats { get; set; }
    }
}