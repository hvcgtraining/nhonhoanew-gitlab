﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.LyDoXuatModel
{
    public class LyDoXuatModel
    {
		public int LyDoXuatId { get; set; }
        [Required(ErrorMessage = "Mã lý do xuất không được bỏ trống")]
        public string MaLyDoXuat { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Title { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
