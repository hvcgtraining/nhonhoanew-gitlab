﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.PKHKeHoachGiaoHangDetailModel
{
    public class PKHKeHoachGiaoHangDetailModel
    {
		public long PKHKeHoachGiaoHangDetailId { get; set; }
		public long PKHKeHoachGiaoHangId { get; set; }
		public int? LoaiVatTuId { get; set; }
		public int? ChiTietVatTuId { get; set; }
		public int? XuongId { get; set; }
		public int? Thu2 { get; set; }
		public int? Thu3 { get; set; }
		public int? Thu4 { get; set; }
		public int? Thu5 { get; set; }
		public int? Thu6 { get; set; }
		public int? Thu7 { get; set; }
		public int? ChuNhat { get; set; }
		public int? TongCong { get; set; }
		public int? TongSoCanChuyen { get; set; }
		public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
		public string Title { get; set; }

    }

}
