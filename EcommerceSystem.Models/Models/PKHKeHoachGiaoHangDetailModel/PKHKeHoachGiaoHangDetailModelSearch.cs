﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHKeHoachGiaoHangDetailModel
{
    public class PKHKeHoachGiaoHangDetailSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHKeHoachGiaoHangDetailModel> PKHKeHoachGiaoHangDetails { get; set; }
    }
}