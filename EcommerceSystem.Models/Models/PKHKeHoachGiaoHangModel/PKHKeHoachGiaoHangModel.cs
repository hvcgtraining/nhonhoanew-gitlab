﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace EcommerceSystem.Models.PKHKeHoachGiaoHangModel
{
    public class PKHKeHoachGiaoHangModel 
    {
		public long PKHKeHoachGiaoHangId { get; set; }
        [Required(ErrorMessage = "Tên kế hoạch xưởng không được bỏ trống")]
        public long? PKHKeHoachXuongId { get; set; }
        [Required(ErrorMessage = "Tên kế hoạch SXNKXH không được bỏ trống")]
        public long? PKHKeHoachSXNKXHId { get; set; }
        [Required(ErrorMessage = "Tuần không được bỏ trống")]
        public string MaTuan { get; set; }
        
        public System.DateTime? StartDate { get; set; }
        
        public System.DateTime? EndDate { get; set; }
        [Required(ErrorMessage = "Tên xưởng không được bỏ trống")]
        public int? XuongId { get; set; }
		public byte? TinhTrang { get; set; }
		public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
		public string Title { get; set; }

        public string XuongName { get; set; }
        public string KeHoachXuongName { get; set; }
        public string KeHoachSXNKXHName { get; set; }
        [Required(ErrorMessage = "Ngày bắt đầu không được bỏ trống")]
       
        public string NgayBatDauDangChuoi { get; set; }
        [Required(ErrorMessage = "Ngày kết thúc không được bỏ trống")]
        
        public string NgayKetThucDangChuoi { get; set; }
        public PKHKeHoachGiaoHangModel()
        {
            //NgayLapPhieu = DateTime.Now;

            NgayBatDauDangChuoi = DateTime.Now.ToString("dd/MM/yyyy");
            NgayKetThucDangChuoi = DateTime.Now.ToString("dd/MM/yyyy");
            TinhTrang = (byte)TrangThaiGiaoHang.DangCho;
        }
       
    }

}
