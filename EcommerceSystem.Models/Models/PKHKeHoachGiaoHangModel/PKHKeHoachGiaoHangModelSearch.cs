﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHKeHoachGiaoHangModel
{
    public class PKHKeHoachGiaoHangSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHKeHoachGiaoHangModel> PKHKeHoachGiaoHangs { get; set; }
    }
}