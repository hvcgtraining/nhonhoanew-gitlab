﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.DieuChuyenQuyetToanModel
{
    public class DieuChuyenQuyetToanSearchModel : Paging
    {
        public DieuChuyenType LoaiDieuChuyen { get; set; }
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<DieuChuyenQuyetToanModel> DieuChuyenQuyetToans { get; set; }
    }
}