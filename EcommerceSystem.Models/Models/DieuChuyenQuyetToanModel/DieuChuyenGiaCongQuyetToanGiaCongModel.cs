﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.DieuChuyenQuyetToanModel
{
    public class DieuChuyenGiaCongQuyetToanGiaCongModel
    {
        public long DieuChuyenQuyetToanId { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Số chứng từ không được bỏ trống")]
        public string SoChungTu { get; set; }
        [Required(ErrorMessage = "Kho nhập không được bỏ trống")]
        public int? KhoNhapId { get; set; }
        public string MaKhoNhap { get; set; }
        [Required(ErrorMessage = "Kho xuất không được bỏ trống")]
        public int? KhoXuatId { get; set; }
        public string MaKhoXuat { get; set; }
        [Required(ErrorMessage = "Mục trạng thái không được bỏ trống")]
        public bool? DaXem { get; set; }
        public int? LoaiDieuChuyenQuyetToan { get; set; }
        public string LyDoXuat { get; set; }
        public string NguoiNhan { get; set; }
        public string CanCu { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool Status { get; set; }
        public DieuChuyenType LoaiDieuChuyen { get; set; }

        public DieuChuyenGiaCongQuyetToanGiaCongModel()
        {
            if (LoaiDieuChuyenQuyetToan.HasValue)
            {
                LoaiDieuChuyen = (DieuChuyenType)LoaiDieuChuyenQuyetToan.Value;
            }
            CreatedDate = DateTime.Now;
        }

        public DieuChuyenQuyetToanModel MapToDieuChuyenQuyetToanModel()
        {
            return new DieuChuyenQuyetToanModel
            {
                DieuChuyenQuyetToanId = this.DieuChuyenQuyetToanId,
                Title = this.Title,
                SoChungTu = this.SoChungTu,
                KhoNhapId = this.KhoNhapId,
                KhoXuatId = this.KhoXuatId,
                DaXem = this.DaXem,
                LoaiDieuChuyenQuyetToan = this.LoaiDieuChuyenQuyetToan,
                LyDoXuat = this.LyDoXuat,
                NguoiNhan = this.NguoiNhan,
                CanCu = this.CanCu,
                CreatedDate = this.CreatedDate,
                UpdatedDate = this.UpdatedDate,
                CreatedBy = this.CreatedBy,
                UpdatedBy = this.UpdatedBy,
                Status = this.Status,
                MaKhoNhap = this.MaKhoNhap,
                MaKhoXuat = this.MaKhoXuat,
            };
        }
    }

}
