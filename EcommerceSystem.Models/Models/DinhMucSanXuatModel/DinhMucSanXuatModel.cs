﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace EcommerceSystem.Models.DinhMucSanXuatModel
{
    public class DinhMucSanXuatModel
    {
		public int DinhMucSanXuatId { get; set; }
        [Required(ErrorMessage="Tên định mức sản xuất không được bỏ trống")]
		public string Title { get; set; }
        [Required(ErrorMessage = "Mã định mức sản xuất không được bỏ trống")]
        public string MaDinhMucSanXuat { get; set; }
        [Required(ErrorMessage = "Số chứng từ không được bỏ trống")]
        public string SoChungTu { get; set; }
        [Required(ErrorMessage = "Mã thành phẩm không được bỏ trống")]
        public int? ChiTietVatTuId { get; set; }
        
        public string MaThanhPham { get; set; }
        
        public string TenThanhPham { get; set; }
        [Required(ErrorMessage = "Mã cấp phát không được bỏ trống")]
        public string MaCapPhat { get; set; }
        [Required(ErrorMessage = "Loại không được bỏ trống")]
        public string Loai { get; set; }
		public string DienGiai { get; set; }
		public string GhiChu { get; set; }
        [Required(ErrorMessage = "Kho quyết toán ID không được bỏ trống")]
        public int? KhoQuyetToanId { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

        public List<SelectListItem> ListKho { get; set; }
        public List<SelectListItem> ListChiTietVatTu { get; set; }


    }

}
