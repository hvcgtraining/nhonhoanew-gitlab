﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.DinhMucSanXuatModel
{
    public class DinhMucSanXuatSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<DinhMucSanXuatModel> DinhMucSanXuats { get; set; }
    }
}