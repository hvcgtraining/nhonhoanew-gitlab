﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHDieuChuyenVatTuDetailModel
{
    public class PKHDieuChuyenVatTuDetailSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHDieuChuyenVatTuDetailModel> PKHDieuChuyenVatTuDetails { get; set; }
    }
}