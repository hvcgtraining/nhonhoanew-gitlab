﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.PKHDieuChuyenVatTuDetailModel
{
    public class PKHDieuChuyenVatTuDetailModel
    {
		public long PKHDieuChuyenVatTuDetailId { get; set; }
		public long PKHDieuChuyenVatTuId { get; set; }
		public int? ChiTietVatTuId { get; set; }
		public int? LoaiVatTuId { get; set; }
		public int? SoLuong { get; set; }
		public int? GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
		public string Title { get; set; }

    }

}
