﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.KhoModel
{
    public class KhoModel
    {
		public int KhoId { get; set; }
        [Required(ErrorMessage = "Mã kho không được bỏ trống")]
        public string MaKho { get; set; }
        [Required(ErrorMessage = "Tên kho không được bỏ trống")]
        public string Title { get; set; }
        public string NguoiGiao { get; set; }
        public string ThongTinKho { get; set; }
        public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
