﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models.PKHDuKienGiaoHangModel
{
    public class PKHDuKienGiaoHangModel
    {
		public long PKHDuKienGiaoHangId { get; set; }
		public long PKHPhieuYeuCauMuaHangId { get; set; }
		public long PKHPhieuYeuCauMuaHangDetailId { get; set; }
        public string MaChiTietVatTu { get; set; }
        [Required(ErrorMessage = "Tên dự kiến không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Ngày dự kiến không được bỏ trống")]
        public System.DateTime? NgayDuKien { get; set; }
        [Required(ErrorMessage = " dự kiến không được bỏ trống")]
        public int? SoLuongGiaoDuKien { get; set; }
        public int? TongSoLuong { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }
        public PKHDuKienGiaoHangModel()
        {
            NgayDuKien = DateTime.Now;
        }
    }

}
