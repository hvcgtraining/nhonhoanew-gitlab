﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHDuKienGiaoHangModel
{
    public class PKHDuKienGiaoHangSearchModel : Paging
    {
        public string TextSearch { get; set; }
        public int PhieuYeuCauMuaHangId { get; set; }
        public string TenPhieuYeuCauMuaHang { get; set; }
        public int PhieuYeuCauMuaHangDetailId { get; set; }
        public string TenPhieuYeuCauMuaHangDetail { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHDuKienGiaoHangModel> PKHDuKienGiaoHangs { get; set; }
    }
}