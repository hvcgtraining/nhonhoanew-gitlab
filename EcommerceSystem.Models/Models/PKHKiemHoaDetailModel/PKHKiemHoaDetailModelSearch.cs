﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHKiemHoaDetailModel
{
    public class PKHKiemHoaDetailSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHKiemHoaDetailModel> PKHKiemHoaDetails { get; set; }
    }
}