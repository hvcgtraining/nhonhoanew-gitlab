﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.PKHKiemHoaDetailModel
{
    public class PKHKiemHoaDetailModel
    {
		public long PKHKiemHoaDetailId { get; set; }
		public long PKHKiemHoaId { get; set; }
		public int? MaPYC { get; set; }
		public int? CustomerId { get; set; }
		public int? ChiTietVatTuId { get; set; }
		public string Title { get; set; }
		public int? SoLuong { get; set; }
		public int? SoLuongDat { get; set; }
		public int? SoLuongKhongDat { get; set; }
		public bool? CheckFlag { get; set; }
		public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
