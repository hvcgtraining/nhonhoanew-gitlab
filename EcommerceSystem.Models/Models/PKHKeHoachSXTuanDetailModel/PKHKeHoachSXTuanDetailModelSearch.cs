﻿
using System.Collections.Generic;
using EcommerceSystem.Models.Common;

namespace EcommerceSystem.Models.PKHKeHoachSXTuanDetailModel
{
    public class PKHKeHoachSXTuanDetailSearchModel : Paging
    {
        public string TextSearch { get; set; }
		public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<PKHKeHoachSXTuanDetailModel> PKHKeHoachSXTuanDetails { get; set; }
    }
}