﻿using System;
using System.Collections.Generic;

namespace EcommerceSystem.Models.PKHKeHoachSXTuanDetailModel
{
    public class PKHKeHoachSXTuanDetailModel
    {
		public long PKHKeHoachSXTuanDetailId { get; set; }
		public long PKHKeHoachXuongDetailId { get; set; }
		public long PKHKeHoachXuongId { get; set; }
		public int? SoLuongSanXuat { get; set; }
		public string Title { get; set; }
		public int? ThuHai { get; set; }
		public int? ThuBa { get; set; }
		public int? ThuTu { get; set; }
		public int? ThuNam { get; set; }
		public int? ThuSau { get; set; }
		public int? ThuBay { get; set; }
		public int? ChuNhat { get; set; }
		public string GhiChu { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public System.DateTime? UpdatedDate { get; set; }
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }
		public bool Status { get; set; }

    }

}
