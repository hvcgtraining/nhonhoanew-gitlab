﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EcommerceSystem.Models
{
    public enum DieuChuyenType
    {
        DieuChuyenGiaCong = 1,
        QuyetToanGiaCong = 2,
        DieuChuyenCapHang = 3,
        DieuChuyenQuyetToan = 4,
    }
    public enum TrangThaiNhapkhoThanhPham : byte
    {
        [Display(Name = "Đã hủy")]
        DaHuy = 0,
        [Display(Name = "Đang chờ")]
        DangCho = 1,
        [Display(Name = "Đã xử lý")]
        DaXuLy = 2,
        [Display(Name = "Đã Hoàn thành")]
        DaHoanThanh = 3,
    }
    public enum LoaiDieuChuyen:byte
    {
        [Display(Name = "Xưởng")]
        DaHuy = 1,
        [Display(Name = "Kho")]
        DangCho = 2,
    }
    public enum TrangThaiKeHoach: byte
    {
        [Display(Name = "Đang chờ")]
        DangCho = 1,
        [Display(Name = "Đang thực hiện")]
        DangThucHien = 2,
        [Display(Name = "Đã xử lý")]
        DaXuLy = 3,
        [Display(Name = "Hoàn thành")]
        HoanThanh = 4,
        [Display(Name = "Hủy bỏ")]
        HuyBo = 8,
    }

    public enum TrangThaiYeuCauMuaHang : byte
    {
        [Display(Name = "Đã hủy")]
        DaHuy = 0,
        [Display(Name = "Đang chờ")]
        DangCho = 1,
        [Display(Name = "Đã xử lý")]
        DaXuLy = 2,
        [Display(Name = "Đã Hoàn thành")]
        DaHoanThanh = 3,
    }

    public enum TrangThaiKeHoachXuong : byte
    {
        [Display(Name = "Đang chờ")]
        DangCho = 1,
        [Display(Name = "Đang thực hiện")]
        DangThucHien = 2,
        [Display(Name = "Đã xử lý")]
        DaXuLy = 3,
        [Display(Name = "Hoàn thành")]
        HoanThanh = 4,
        [Display(Name = "Hủy bỏ")]
        HuyBo = 8,
    }
    public enum TrangThaiPhieuNhapVatTu:byte
    {
        [Display(Name = "Đang chờ")]
        DangCho = 1,
        [Display(Name = "Đã xử lý")]
        DaXuLy = 2,
        [Display(Name = "Đã hoàn thành")]
        HoanThanh = 3,
    }
    public enum TrangThaiGiaoHang:byte
    {
        [Display(Name = "Đang chờ")]
        DangCho = 1,
        [Display(Name = "Đã xử lý")]
        DaXuLy = 2,
        [Display(Name = "Đã hoàn thành")]
        HoanThanh = 3,
    }
    public enum TrangThaiDieuChuyenVatTu: byte
    {
        [Display(Name = "Đang chờ")]
        DangCho = 1,
        [Display(Name = "Đã xử lý")]
        DaXuLy = 2,
        [Display(Name = "Đã hoàn thành")]
        HoanThanh = 3,
    }
    public enum LoaiXuong
    {
        XuongLapRap = 1,
        XuongChitiet = 2
    }
    public enum LoginResult
    {
        Unknown = 0,
        Success = 1,
        IsLockedOut = 2,
        InvalidEmail = 3,
        InvalidPassword = 4,
        UnActive = 5,
    }
    public enum MessageType
    {
        Success,
        Error,
        Notice,
        Warning
    }
    public enum ModuleType
    {
        [Display(Name = "Người dùng")]
        User = 0,
        [Display(Name = "Phân quyền")]
        Role = 1,
        [Display(Name = "Khách hàng")]
        Customer = 2,
        [Display(Name = "Đơn hàng")]
        Order = 3,
        [Display(Name = "Giao dịch")]
        Transaction = 4,
        [Display(Name = "Thông báo")]
        Notification = 5,
        [Display(Name = "Khiếu nại")]
        Complaint = 6,
        [Display(Name = "Thùng rác")]
        OrderTrash = 7,
        [Display(Name = "Thiết lập chung")]
        Configuration = 8,
        [Display(Name = "Tài khoản tiền tệ")]
        Credit = 9,
        [Display(Name = "Đơn kí gửi")]
        CarrierOrder = 10,
    }
    public enum OrderStatusType
    {
        [Display(Name = "Chưa xử lý")]
        PenddingProcess = 1,
        [Display(Name = "Chờ báo giá")]
        PenddingQuote = 2,
        [Display(Name = "Đã báo giá")]
        Quoted = 3,
        [Display(Name = "Chờ đặt cọc")]
        PenddingDeposit = 4,
        [Display(Name = "Chờ mua")]
        PenddingBuy = 5,
        [Display(Name = "Đã mua")]
        Bought = 6,
        [Display(Name = "Tất cả đã mua")]
        AllBought = 7,
        [Display(Name = "Nhập kho TQ")]
        ImportChinaStore = 8,
        [Display(Name = "Xuất kho TQ")]
        ExportChinaStore = 9,
        [Display(Name = "Nhập kho VN")]
        ImportVNStore = 10,
        [Display(Name = "Xuất kho VN")]
        ExportVNStore = 11,
        [Display(Name = "Chuyển cho khách")]
        ReadyExportVNStore = 12,
        [Display(Name = "Đã hoàn thành")]
        Finished = 18,
        [Display(Name = "Đã hủy")]
        Cancelled = 14,
        [Display(Name = "Đã Kiểm hàng")]
        ChinaStoreChecked = 15
    }

    public enum ProccesShippingCodeStatus : short
    {
        PendingImportCNStore = 1,
        ImportCNStore = 2,
        ExportCNStore = 3,
        ImportVNStore = 4,
        ExportVNStore = 5,
        Compeleted = 6
    }

    /// <summary>
    ///  Sử dụng chung orderstatus a nhé ,sẽ remove sau
    /// </summary>
    public enum OrderLineItemStatus
    {
        [Display(Name = "Chờ xử lý")]
        PenddingProcess = 1,
        [Display(Name = "Chờ mua")]
        PenddingBuy = 2,
        [Display(Name = "Đã mua")]
        Bought = 3,
        [Display(Name = "Nhập kho TQ")]
        ImportChinaStore = 4,
        [Display(Name = "Kiểm hàng")]
        HasChecked = 5,
        [Display(Name = "Xuất kho TQ")]
        ExportChinaStore = 6,
        [Display(Name = "Nhập kho VN")]
        ImportVNStore = 7,
        [Display(Name = "Xuất kho VN")]
        ExportVNStore = 8,
        [Display(Name = "Đã hoàn thành")]
        Finished = 10,
        [Display(Name = "Khiếu nại")]
        Complain = 15,
        [Display(Name = "Xử lý khiếu nại")]
        ComplainProcess = 16,
        [Display(Name = "Hoàn thành khiếu nại")]
        ComplainCompleted = 17
    }

    public enum CustomerStatus : short
    {
        Unactive = 0,
        Active = 1
    }

    public enum AccountTokenType : short
    {
        ActiveAccount = 1,
        PasswordRecover = 2,
        RemoteLogin = 3
    }

    public enum NotificationType
    {
        System = 1,
        Message = 2
    }
    public enum CarrierOrderStatus
    {
        //New = 0 ; Về KhoTQ = 1; Về KhoVN =2; Xuất KhoVN =3; Khách nhận hàng = 4; Hủy = 7
        [Display(Name = "Đơn mới")]
        News = 0,
        [Display(Name = "Nhập kho TQ")]
        ImportChinaStore = 1,
        [Display(Name = "Xuất kho TQ")]
        ExportChinaStore = 2,
        [Display(Name = "Nhập kho VN")]
        ImportVNStore = 3,
        [Display(Name = "Xuất kho VN")]
        ExportVNStore = 4,
        [Display(Name = "Đã hoàn thành")]
        Finished = 5,
        [Display(Name = "Hủy")]
        Cancel = 6
    }

    public enum PaymentType
    {
        [Display(Name = "Nạp tiền vào tài khoản")]
        Recharge = 1,
        [Display(Name = "Rút tiền")]
        Withdraw = 2,
        [Display(Name = "Hoàn tiền")]
        Refund = 3,
        [Display(Name = "Thanh toán phí")]
        PayOrder = 4,
        [Display(Name = "Chuyển khoản")]
        Transfer = 5,
    }

    public enum TransactionStatus
    {
        Done = 1,
        Error = 2
    }
    public enum CreditType
    {
        VNAccount = 1,
        CNAccount = 2,
        AlipayAccount = 3
    }
    public enum TransferType
    {
        FromCNYCardToAlipay = 1,
        FromAlipayToCNYCard = 2
    }
}
