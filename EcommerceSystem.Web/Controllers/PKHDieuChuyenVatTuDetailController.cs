﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.PKHDieuChuyenVatTuDetailModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHDieuChuyenVatTuDetailController : BaseController
    {
        private readonly IPKHDieuChuyenVatTuDetailService _pKHDieuChuyenVatTuDetailService = (IPKHDieuChuyenVatTuDetailService)DependencyResolver.Current.GetService(typeof(IPKHDieuChuyenVatTuDetailService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new PKHDieuChuyenVatTuDetailSearchModel
            {
                PKHDieuChuyenVatTuDetails = _pKHDieuChuyenVatTuDetailService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHDieuChuyenVatTuDetailSearchModel
            {
                PKHDieuChuyenVatTuDetails = _pKHDieuChuyenVatTuDetailService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHDieuChuyenVatTuDetail/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHDieuChuyenVatTuDetailModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHDieuChuyenVatTuDetailModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_pKHDieuChuyenVatTuDetailService.CreatePKHDieuChuyenVatTuDetail(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHDieuChuyenVatTuDetail");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var pKHDieuChuyenVatTuDetailEntity = _pKHDieuChuyenVatTuDetailService.GetById(id);
            if(pKHDieuChuyenVatTuDetailEntity != null)
            {
                var model = pKHDieuChuyenVatTuDetailEntity.MapToModel();
                return View(model);
            }
            return View(new PKHDieuChuyenVatTuDetailModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHDieuChuyenVatTuDetailModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _pKHDieuChuyenVatTuDetailService.UpdatePKHDieuChuyenVatTuDetail(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHDieuChuyenVatTuDetail");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHDieuChuyenVatTuDetailId)
        {
            string message;
            var result = _pKHDieuChuyenVatTuDetailService.Delete(pKHDieuChuyenVatTuDetailId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int pKHDieuChuyenVatTuDetailId)
        {
            string message;
            var result = _pKHDieuChuyenVatTuDetailService.ChangeStatus(pKHDieuChuyenVatTuDetailId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}