﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.PKHKeHoachXuongModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHKeHoachXuongController : BaseController
    {
        private readonly IPKHKeHoachXuongService _pKHKeHoachXuongService = (IPKHKeHoachXuongService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachXuongService));
        private readonly IXuongService _xuongService = (IXuongService)DependencyResolver.Current.GetService(typeof(IXuongService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new PKHKeHoachXuongSearchModel
            {
                PKHKeHoachXuongs = _pKHKeHoachXuongService.Search(1, SystemConfiguration.PageSizeDefault, null,null, null, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult KeHoachNhapKhoThanhPham()
        {
            ViewBag.XuongGroups = _xuongService.GetAllXuongs();
            int totalRecords;

            var model = new PKHKeHoachXuongSearchModel
            {
                PKHKeHoachXuongs = _pKHKeHoachXuongService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHKeHoachXuongSearchModel
            {
                PKHKeHoachXuongs = _pKHKeHoachXuongService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, null, null, null, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHKeHoachXuong/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHKeHoachXuongModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHKeHoachXuongModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_pKHKeHoachXuongService.CreatePKHKeHoachXuong(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHKeHoachXuong");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var pKHKeHoachXuongEntity = _pKHKeHoachXuongService.GetById(id);
            if(pKHKeHoachXuongEntity != null)
            {
                var model = pKHKeHoachXuongEntity.MapToModel();
                return View(model);
            }
            return View(new PKHKeHoachXuongModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHKeHoachXuongModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _pKHKeHoachXuongService.UpdatePKHKeHoachXuong(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHKeHoachXuong");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHKeHoachXuongId)
        {
            string message;
            var result = _pKHKeHoachXuongService.Delete(pKHKeHoachXuongId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int pKHKeHoachXuongId)
        {
            string message;
            var result = _pKHKeHoachXuongService.ChangeStatus(pKHKeHoachXuongId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}