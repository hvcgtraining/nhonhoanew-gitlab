﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.LoaiHinhKinhDoanhModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class LoaiHinhKinhDoanhController : BaseController
    {
        private readonly ILoaiHinhKinhDoanhService _loaiHinhKinhDoanhService = (ILoaiHinhKinhDoanhService)DependencyResolver.Current.GetService(typeof(ILoaiHinhKinhDoanhService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new LoaiHinhKinhDoanhSearchModel
            {
                LoaiHinhKinhDoanhs = _loaiHinhKinhDoanhService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new LoaiHinhKinhDoanhSearchModel
            {
                LoaiHinhKinhDoanhs = _loaiHinhKinhDoanhService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/LoaiHinhKinhDoanh/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new LoaiHinhKinhDoanhModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LoaiHinhKinhDoanhModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_loaiHinhKinhDoanhService.CreateLoaiHinhKinhDoanh(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "LoaiHinhKinhDoanh");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var loaiHinhKinhDoanhEntity = _loaiHinhKinhDoanhService.GetById(id);
            if(loaiHinhKinhDoanhEntity != null)
            {
                var model = loaiHinhKinhDoanhEntity.MapToModel();
                return View(model);
            }
            return View(new LoaiHinhKinhDoanhModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LoaiHinhKinhDoanhModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _loaiHinhKinhDoanhService.UpdateLoaiHinhKinhDoanh(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "LoaiHinhKinhDoanh");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int loaiHinhKinhDoanhId)
        {
            string message;
            var result = _loaiHinhKinhDoanhService.Delete(loaiHinhKinhDoanhId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int loaiHinhKinhDoanhId)
        {
            string message;
            var result = _loaiHinhKinhDoanhService.ChangeStatus(loaiHinhKinhDoanhId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}