﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.CoPhanCapXuongModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class CoPhanCapXuongController : BaseController
    {
        private readonly ICoPhanCapXuongService _coPhanCapXuongService = (ICoPhanCapXuongService)DependencyResolver.Current.GetService(typeof(ICoPhanCapXuongService));
        private readonly IKhoService _khoService = (IKhoService)DependencyResolver.Current.GetService(typeof(IKhoService));
        private readonly IChiTietVatTuService _loaiVatTuService = (IChiTietVatTuService)DependencyResolver.Current.GetService(typeof(IChiTietVatTuService));
        private readonly INhomSanPhamService _nhomSanPhamService = (INhomSanPhamService)DependencyResolver.Current.GetService(typeof(INhomSanPhamService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new CoPhanCapXuongSearchModel
            {
                CoPhanCapXuongs = _coPhanCapXuongService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new CoPhanCapXuongSearchModel
            {
                CoPhanCapXuongs = _coPhanCapXuongService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/CoPhanCapXuong/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            ViewBag.KhoGroups = _khoService.GetAllKhos();
            ViewBag.LoaiVatTuGroups = _loaiVatTuService.GetAllChiTietVatTus();
            ViewBag.NhomSanPhamGroups = _nhomSanPhamService.GetAllNhomSanPhams();
            var model = new CoPhanCapXuongModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CoPhanCapXuongModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_coPhanCapXuongService.CreateCoPhanCapXuong(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "CoPhanCapXuong");
                }

                ViewBag.KhoGroups = _khoService.GetAllKhos();
                ViewBag.LoaiVatTuGroups = _loaiVatTuService.GetAllChiTietVatTus();
                ViewBag.NhomSanPhamGroups = _nhomSanPhamService.GetAllNhomSanPhams();
                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            ViewBag.KhoGroups = _khoService.GetAllKhos();
            ViewBag.LoaiVatTuGroups = _loaiVatTuService.GetAllChiTietVatTus();
            ViewBag.NhomSanPhamGroups = _nhomSanPhamService.GetAllNhomSanPhams();
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.KhoGroups = _khoService.GetAllKhos();
            ViewBag.LoaiVatTuGroups = _loaiVatTuService.GetAllChiTietVatTus();
            ViewBag.NhomSanPhamGroups = _nhomSanPhamService.GetAllNhomSanPhams();
            var coPhanCapXuongEntity = _coPhanCapXuongService.GetById(id);
            if(coPhanCapXuongEntity != null)
            {
                var model = coPhanCapXuongEntity.MapToModel();
                return View(model);
            }
            return View(new CoPhanCapXuongModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CoPhanCapXuongModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _coPhanCapXuongService.UpdateCoPhanCapXuong(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "CoPhanCapXuong");
                }

                ViewBag.KhoGroups = _khoService.GetAllKhos();
                ViewBag.LoaiVatTuGroups = _loaiVatTuService.GetAllChiTietVatTus();
                ViewBag.NhomSanPhamGroups = _nhomSanPhamService.GetAllNhomSanPhams();
                TempData["error"] = message;
                return View(model);
            }

            ViewBag.KhoGroups = _khoService.GetAllKhos();
            ViewBag.LoaiVatTuGroups = _loaiVatTuService.GetAllChiTietVatTus();
            ViewBag.NhomSanPhamGroups = _nhomSanPhamService.GetAllNhomSanPhams();
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int coPhanCapXuongId)
        {
            string message;
            var result = _coPhanCapXuongService.Delete(coPhanCapXuongId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int coPhanCapXuongId)
        {
            string message;
            var result = _coPhanCapXuongService.ChangeStatus(coPhanCapXuongId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}