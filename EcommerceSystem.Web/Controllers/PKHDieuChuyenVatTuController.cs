﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.PKHDieuChuyenVatTuModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHDieuChuyenVatTuController : BaseController
    {
        private readonly IPKHDieuChuyenVatTuService _pKHDieuChuyenVatTuService = (IPKHDieuChuyenVatTuService)DependencyResolver.Current.GetService(typeof(IPKHDieuChuyenVatTuService));
        private readonly IPKHKeHoachXuongService _pKHXuongService = (IPKHKeHoachXuongService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachXuongService));
        private readonly IPKHKeHoachSXNKXHService _pKHKeHoachSXNKXH = (IPKHKeHoachSXNKXHService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachSXNKXHService));
        private readonly ILyDoXuatService _pLyDoXuat = (ILyDoXuatService)DependencyResolver.Current.GetService(typeof(ILyDoXuatService));
        private readonly IKhoService _pKho = (IKhoService)DependencyResolver.Current.GetService(typeof(IKhoService));
        private readonly IXuongService _pXuong = (IXuongService)DependencyResolver.Current.GetService(typeof(IXuongService));
        private readonly INguoiNhanKhoService _pNguoiNhanKho = (INguoiNhanKhoService)DependencyResolver.Current.GetService(typeof(INguoiNhanKhoService));
        public ActionResult Index()
        {
            int totalRecords;
            var model = new PKHDieuChuyenVatTuSearchModel
            {
                PKHDieuChuyenVatTus = _pKHDieuChuyenVatTuService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHDieuChuyenVatTuSearchModel
            {
                PKHDieuChuyenVatTus = _pKHDieuChuyenVatTuService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHDieuChuyenVatTu/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }
        public dynamic LoadDanhMucDieuChuyen()
        {
            List<SelectListItem> DanhMucDieChuyen = new List<SelectListItem>();
            DanhMucDieChuyen.Add(new SelectListItem() { Text = "Xưởng", Value = "1" });
            DanhMucDieChuyen.Add(new SelectListItem() { Text = "Kho", Value = "2" });
            return DanhMucDieChuyen;
        }
        public ActionResult Create()
        {
            var model = new PKHDieuChuyenVatTuModel();
            ViewBag.DanhMucDieuChuyenGroups = LoadDanhMucDieuChuyen();
            ViewBag.KeHoachXuongs = _pKHXuongService.GetAllPKHKeHoachXuongs();
            ViewBag.KeHoachSXNKXHs = _pKHKeHoachSXNKXH.GetAllPKHKeHoachSXNKXHs() ;
            ViewBag.LyDoXuats = _pLyDoXuat.GetAllLyDoXuats();
            ViewBag.Khos = _pKho.GetAllKhos();
            ViewBag.Xuongs = _pXuong.GetAllXuongs();
            ViewBag.NguoiNhanKhos = _pNguoiNhanKho.GetAllNguoiNhanKhos() ;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHDieuChuyenVatTuModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;
                model.NgayLap = DateTime.Parse(model.NgayLapDangChuoi, CultureInfo.GetCultureInfo("vi-VN"));
                if (_pKHDieuChuyenVatTuService.CreatePKHDieuChuyenVatTu(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHDieuChuyenVatTu");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var pKHDieuChuyenVatTuEntity = _pKHDieuChuyenVatTuService.GetById(id);
            ViewBag.DanhMucDieuChuyenGroups = LoadDanhMucDieuChuyen();
            ViewBag.KeHoachXuongs = _pKHXuongService.GetAllPKHKeHoachXuongs();
            ViewBag.KeHoachSXNKXHs = _pKHKeHoachSXNKXH.GetAllPKHKeHoachSXNKXHs();
            ViewBag.LyDoXuats = _pLyDoXuat.GetAllLyDoXuats();
            ViewBag.Khos = _pKho.GetAllKhos();
            ViewBag.Xuongs = _pXuong.GetAllXuongs();
            ViewBag.NguoiNhanKhos = _pNguoiNhanKho.GetAllNguoiNhanKhos();
            if (pKHDieuChuyenVatTuEntity != null)
            {
                var model = pKHDieuChuyenVatTuEntity.MapToModel();
                return View(model);
            }
            return View(new PKHDieuChuyenVatTuModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHDieuChuyenVatTuModel model)
        {
            ViewBag.DanhMucDieuChuyenGroups = LoadDanhMucDieuChuyen();
            ViewBag.KeHoachXuongs = _pKHXuongService.GetAllPKHKeHoachXuongs();
            ViewBag.KeHoachSXNKXHs = _pKHKeHoachSXNKXH.GetAllPKHKeHoachSXNKXHs();
            ViewBag.LyDoXuats = _pLyDoXuat.GetAllLyDoXuats();
            ViewBag.Khos = _pKho.GetAllKhos();
            ViewBag.Xuongs = _pXuong.GetAllXuongs();
            ViewBag.NguoiNhanKhos = _pNguoiNhanKho.GetAllNguoiNhanKhos();
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                model.NgayLap = DateTime.Parse(model.NgayLapDangChuoi, CultureInfo.GetCultureInfo("vi-VN"));
                var isSuccess = _pKHDieuChuyenVatTuService.UpdatePKHDieuChuyenVatTu(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHDieuChuyenVatTu");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHDieuChuyenVatTuId)
        {
            
            string message;
            var result = _pKHDieuChuyenVatTuService.Delete(pKHDieuChuyenVatTuId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int pKHDieuChuyenVatTuId)
        {
            string message;
            var result = _pKHDieuChuyenVatTuService.ChangeStatus(pKHDieuChuyenVatTuId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}