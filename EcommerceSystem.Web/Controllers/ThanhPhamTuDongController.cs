﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.ThanhPhamTuDongModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class ThanhPhamTuDongController : BaseController
    {
        private readonly IThanhPhamTuDongService _thanhPhamTuDongService = (IThanhPhamTuDongService)DependencyResolver.Current.GetService(typeof(IThanhPhamTuDongService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new ThanhPhamTuDongSearchModel
            {
                ThanhPhamTuDongs = _thanhPhamTuDongService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new ThanhPhamTuDongSearchModel
            {
                ThanhPhamTuDongs = _thanhPhamTuDongService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/ThanhPhamTuDong/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new ThanhPhamTuDongModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ThanhPhamTuDongModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_thanhPhamTuDongService.CreateThanhPhamTuDong(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "ThanhPhamTuDong");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var thanhPhamTuDongEntity = _thanhPhamTuDongService.GetById(id);
            if(thanhPhamTuDongEntity != null)
            {
                var model = thanhPhamTuDongEntity.MapToModel();
                return View(model);
            }
            return View(new ThanhPhamTuDongModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ThanhPhamTuDongModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _thanhPhamTuDongService.UpdateThanhPhamTuDong(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "ThanhPhamTuDong");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int thanhPhamTuDongId)
        {
            string message;
            var result = _thanhPhamTuDongService.Delete(thanhPhamTuDongId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int thanhPhamTuDongId)
        {
            string message;
            var result = _thanhPhamTuDongService.ChangeStatus(thanhPhamTuDongId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}