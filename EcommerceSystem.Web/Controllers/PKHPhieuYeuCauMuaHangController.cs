﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models;
using EcommerceSystem.Models.PKHPhieuYeuCauMuaHangModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHPhieuYeuCauMuaHangController : BaseController
    {
        private readonly IPKHPhieuYeuCauMuaHangService _pKHPhieuYeuCauMuaHangService = (IPKHPhieuYeuCauMuaHangService)DependencyResolver.Current.GetService(typeof(IPKHPhieuYeuCauMuaHangService));
        private readonly IYeuCauMuaHangService _yeuCauMuaHangService = (IYeuCauMuaHangService)DependencyResolver.Current.GetService(typeof(IYeuCauMuaHangService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new PKHPhieuYeuCauMuaHangSearchModel
            {
                PKHPhieuYeuCauMuaHangs = _pKHPhieuYeuCauMuaHangService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            return View(model);
        }
        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHPhieuYeuCauMuaHangSearchModel
            {
                PKHPhieuYeuCauMuaHangs = _pKHPhieuYeuCauMuaHangService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
                SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHPhieuYeuCauMuaHang/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHPhieuYeuCauMuaHangModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHPhieuYeuCauMuaHangModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;
                var phieuYeuCauMuaHangId = _pKHPhieuYeuCauMuaHangService.CreatePKHPhieuYeuCauMuaHang(model);

                if (phieuYeuCauMuaHangId != 0)
                {
                    TempData["success"] = "Tạo mới thành công. Vui lòng nhập phiếu yêu cầu mua hàng chi tiết.";

                    var yeuCauMuaHangList = _yeuCauMuaHangService.GetYeuCauMuaHangsByParams((int)TrangThaiYeuCauMuaHang.DangCho, null);
                    if (yeuCauMuaHangList != null)
                    {
                        foreach (var s in yeuCauMuaHangList)
                        {
                            if (s.PKHPhieuYeuCauMuaHangId == null)
                            {
                                s.PKHPhieuYeuCauMuaHangId = (int?)phieuYeuCauMuaHangId;
                            }
                            _yeuCauMuaHangService.UpdateYeuCauMuaHangByPhieuMuaHangID(s);
                        }
                    }
                    return RedirectToAction("Index", "PKHPhieuYeuCauMuaHangDetail", new { PKHPhieuYeuCauMuaHangId = phieuYeuCauMuaHangId });
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var pKHPhieuYeuCauMuaHangEntity = _pKHPhieuYeuCauMuaHangService.GetById(id);
            if (pKHPhieuYeuCauMuaHangEntity != null)
            {
                var model = pKHPhieuYeuCauMuaHangEntity.MapToModel();
                return View(model);
            }
            return View(new PKHPhieuYeuCauMuaHangModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHPhieuYeuCauMuaHangModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
                model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _pKHPhieuYeuCauMuaHangService.UpdatePKHPhieuYeuCauMuaHang(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHPhieuYeuCauMuaHang");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        public ActionResult TheoDoiPhieuYeuCauMuaHang(int id)
        {
            var pKHPhieuYeuCauMuaHangEntity = _pKHPhieuYeuCauMuaHangService.GetById(id);
            if (pKHPhieuYeuCauMuaHangEntity != null)
            {
                var model = pKHPhieuYeuCauMuaHangEntity.MapToModel();
                return View(model);
            }
            return View(new PKHPhieuYeuCauMuaHangModel());
        }

        [HttpPost]
        public ActionResult Delete(int pKHPhieuYeuCauMuaHangId)
        {
            string message;
            var result = _pKHPhieuYeuCauMuaHangService.Delete(pKHPhieuYeuCauMuaHangId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

        [HttpPost]
        public ActionResult Invisibe(int pKHPhieuYeuCauMuaHangId)
        {
            string message;
            var result = _pKHPhieuYeuCauMuaHangService.ChangeStatus(pKHPhieuYeuCauMuaHangId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}