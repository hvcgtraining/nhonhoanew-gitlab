﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.QuyDoiDinhMucModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class QuyDoiDinhMucController : BaseController
    {
        private readonly IQuyDoiDinhMucService _quyDoiDinhMucService = (IQuyDoiDinhMucService)DependencyResolver.Current.GetService(typeof(IQuyDoiDinhMucService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new QuyDoiDinhMucSearchModel
            {
                QuyDoiDinhMucs = _quyDoiDinhMucService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new QuyDoiDinhMucSearchModel
            {
                QuyDoiDinhMucs = _quyDoiDinhMucService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/QuyDoiDinhMuc/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new QuyDoiDinhMucModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(QuyDoiDinhMucModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_quyDoiDinhMucService.CreateQuyDoiDinhMuc(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "QuyDoiDinhMuc");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var quyDoiDinhMucEntity = _quyDoiDinhMucService.GetById(id);
            if(quyDoiDinhMucEntity != null)
            {
                var model = quyDoiDinhMucEntity.MapToModel();
                return View(model);
            }
            return View(new QuyDoiDinhMucModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(QuyDoiDinhMucModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _quyDoiDinhMucService.UpdateQuyDoiDinhMuc(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "QuyDoiDinhMuc");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int quyDoiDinhMucId)
        {
            string message;
            var result = _quyDoiDinhMucService.Delete(quyDoiDinhMucId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int quyDoiDinhMucId)
        {
            string message;
            var result = _quyDoiDinhMucService.ChangeStatus(quyDoiDinhMucId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}