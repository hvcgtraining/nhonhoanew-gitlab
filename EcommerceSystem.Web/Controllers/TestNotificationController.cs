﻿using EcommerceSystem.Core;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class TestNotificationController : Controller
    {
        // GET: TestNotification
        public ActionResult Index()
        {
            SendMessage("Hello SignalR, I'm Nguyễn Văn Ngự ahihi");
            return View();
            
        }
        public ActionResult Client()
        {
            return View();
        }
        private void SendMessage(string message)
        {
            GlobalHost.ConnectionManager.GetHubContext<NotificationHub>().Clients.All.addMessage("server", message);
        }
    }
}