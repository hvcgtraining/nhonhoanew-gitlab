﻿using EcommerceSystem.Core.Configurations;
using ELFinder.Connector.ASPNet.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    /// </summary>
    public class ELFinderConnectorController : ELFinderBaseConnectorController
    {

        #region Constructors

        /// <summary>
        /// Create a new instance
        /// </summary>
        public ELFinderConnectorController() : base(ELFinderSharedConfig.ELFinder)
        {
        }

        #endregion

    }
}