﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.DanhMucKhachHangModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class DanhMucKhachHangController : BaseController
    {
        private readonly IDanhMucKhachHangService _danhMucKhachHangService = (IDanhMucKhachHangService)DependencyResolver.Current.GetService(typeof(IDanhMucKhachHangService));
        private readonly INhomKhachHangService _nhomKhachHangService = (INhomKhachHangService)DependencyResolver.Current.GetService(typeof(INhomKhachHangService));
        private readonly ILoaiHinhKinhDoanhService _loaiHinhKinhDoanhService = (ILoaiHinhKinhDoanhService)DependencyResolver.Current.GetService(typeof(ILoaiHinhKinhDoanhService));
        public ActionResult Index()
        {
            int totalRecords;
            var model = new DanhMucKhachHangSearchModel
            {
                DanhMucKhachHangs = _danhMucKhachHangService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new DanhMucKhachHangSearchModel
            {
                DanhMucKhachHangs = _danhMucKhachHangService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/DanhMucKhachHang/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new DanhMucKhachHangModel();
            ViewBag.NhomKhachHangGroups = _nhomKhachHangService.GetAllNhomKhachHangs();
            ViewBag.LoaiHinhKinhDoanhGroups = _loaiHinhKinhDoanhService.GetAllLoaiHinhKinhDoanhs();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DanhMucKhachHangModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_danhMucKhachHangService.CreateDanhMucKhachHang(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "DanhMucKhachHang");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.NhomKhachHangGroups = _nhomKhachHangService.GetAllNhomKhachHangs();
            ViewBag.LoaiHinhKinhDoanhGroups = _loaiHinhKinhDoanhService.GetAllLoaiHinhKinhDoanhs();
            var danhMucKhachHangEntity = _danhMucKhachHangService.GetById(id);
            if(danhMucKhachHangEntity != null)
            {
                var model = danhMucKhachHangEntity.MapToModel();
                return View(model);
            }
            return View(new DanhMucKhachHangModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DanhMucKhachHangModel model)
        {
            ViewBag.NhomKhachHangGroups = _nhomKhachHangService.GetAllNhomKhachHangs();
            ViewBag.LoaiHinhKinhDoanhGroups = _loaiHinhKinhDoanhService.GetAllLoaiHinhKinhDoanhs();
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _danhMucKhachHangService.UpdateDanhMucKhachHang(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "DanhMucKhachHang");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int danhMucKhachHangId)
        {
            string message;
            var result = _danhMucKhachHangService.Delete(danhMucKhachHangId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int danhMucKhachHangId)
        {
            string message;
            var result = _danhMucKhachHangService.ChangeStatus(danhMucKhachHangId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}