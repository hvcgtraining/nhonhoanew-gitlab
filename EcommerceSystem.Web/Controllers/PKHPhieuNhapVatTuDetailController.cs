﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.PKHPhieuNhapVatTuDetailModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHPhieuNhapVatTuDetailController : BaseController
    {
        private readonly IPKHPhieuNhapVatTuDetailService _pKHPhieuNhapVatTuDetailService = (IPKHPhieuNhapVatTuDetailService)DependencyResolver.Current.GetService(typeof(IPKHPhieuNhapVatTuDetailService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new PKHPhieuNhapVatTuDetailSearchModel
            {
                PKHPhieuNhapVatTuDetails = _pKHPhieuNhapVatTuDetailService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHPhieuNhapVatTuDetailSearchModel
            {
                PKHPhieuNhapVatTuDetails = _pKHPhieuNhapVatTuDetailService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHPhieuNhapVatTuDetail/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHPhieuNhapVatTuDetailModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHPhieuNhapVatTuDetailModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_pKHPhieuNhapVatTuDetailService.CreatePKHPhieuNhapVatTuDetail(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHPhieuNhapVatTuDetail");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var pKHPhieuNhapVatTuDetailEntity = _pKHPhieuNhapVatTuDetailService.GetById(id);
            if(pKHPhieuNhapVatTuDetailEntity != null)
            {
                var model = pKHPhieuNhapVatTuDetailEntity.MapToModel();
                return View(model);
            }
            return View(new PKHPhieuNhapVatTuDetailModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHPhieuNhapVatTuDetailModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _pKHPhieuNhapVatTuDetailService.UpdatePKHPhieuNhapVatTuDetail(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHPhieuNhapVatTuDetail");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHPhieuNhapVatTuDetailId)
        {
            string message;
            var result = _pKHPhieuNhapVatTuDetailService.Delete(pKHPhieuNhapVatTuDetailId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int pKHPhieuNhapVatTuDetailId)
        {
            string message;
            var result = _pKHPhieuNhapVatTuDetailService.ChangeStatus(pKHPhieuNhapVatTuDetailId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}