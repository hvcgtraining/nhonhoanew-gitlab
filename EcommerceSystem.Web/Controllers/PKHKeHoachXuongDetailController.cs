﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models;
using EcommerceSystem.Models.PKHKeHoachXuongDetailModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Threading;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHKeHoachXuongDetailController : BaseController
    {
        private readonly IPKHKeHoachXuongDetailService _pKHKeHoachXuongDetailService = (IPKHKeHoachXuongDetailService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachXuongDetailService));
        private readonly IPKHKeHoachXuongService _khXuongService = (IPKHKeHoachXuongService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachXuongService));
        private readonly IPKHKeHoachSXNKXHService _pKHKeHoachSXNKXHService = (IPKHKeHoachSXNKXHService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachSXNKXHService));
        private readonly IChiTietVatTuService _chiTietVatTuService = (IChiTietVatTuService)DependencyResolver.Current.GetService(typeof(IChiTietVatTuService));
        public ActionResult Index(long keHoachId)
        {
            int totalRecords;
            var model = new PKHKeHoachXuongDetailSearchModel
            {
                PKHKeHoachXuongDetails = _pKHKeHoachXuongDetailService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };
            ViewBag.KHXuongs = _khXuongService.GetAllXuongKeHoachSX(keHoachId, (int)LoaiXuong.XuongLapRap);
            ViewBag.VatTus = _chiTietVatTuService.GetAllChiTietVatTus();
            //ViewBag.VatTus = _chiTietVatTuService.GetAllChiTietVatTus();
            return View(model);
        }

        public ActionResult XuongChiTiet(long keHoachId)
        {
            int totalRecords;
            var model = new PKHKeHoachXuongDetailSearchModel
            {
                PKHKeHoachXuongDetails = _pKHKeHoachXuongDetailService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };
            ViewBag.KHXuongs = _khXuongService.GetAllXuongKeHoachSX(keHoachId, (int)LoaiXuong.XuongLapRap);
            ViewBag.VatTus = _chiTietVatTuService.GetAllChiTietVatTus();
            //ViewBag.VatTus = _chiTietVatTuService.GetAllChiTietVatTus();
            return View(model);
        }

        public ActionResult GetKHXDetailByKhXuong(long keHoachXuongId)
        {
            int totalRecords = 0;
            var model = new PKHKeHoachXuongDetailSearchModel
            {
                PKHKeHoachXuongDetails = _pKHKeHoachXuongDetailService.GetAllByParent(keHoachXuongId),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };
            
            var html = RenderPartialViewToString("~/Views/PKHKeHoachXuongDetail/Partial/_XuongListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHKeHoachXuongDetailSearchModel
            {
                PKHKeHoachXuongDetails = _pKHKeHoachXuongDetailService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHKeHoachXuongDetail/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHKeHoachXuongDetailModel();
            ViewBag.VatTus = _chiTietVatTuService.GetAllChiTietVatTus();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHKeHoachXuongDetailModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                //Thread myNewThread = new Thread(() => _pKHKeHoachSXNKXHDetailService.UpdateKHSXXuong(model.VatTuId, model.PKHKeHoachSXNKXHId, keHoachSXNKXHDetailId, LoaiXuong.XuongLapRap, CurrentUser.UserName));
                //myNewThread.Start();

                //Thread myNewThread2 = new Thread(() => _pKHKeHoachSXNKXHDetailService.UpdateKHSXXuong(model.VatTuId, model.PKHKeHoachSXNKXHId, keHoachSXNKXHDetailId, LoaiXuong.XuongChitiet, CurrentUser.UserName));
                //myNewThread2.Start();


                if (_pKHKeHoachXuongDetailService.CreatePKHKeHoachXuongDetail(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHKeHoachXuongDetail");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult UpdateKhsxDetail(long maKHXuongChiTiet, string maVatTu, int soLuong, string ngayCapHang, string ghiChu)
        {
            string message;
            DateTime ngayCap = DateTime.Now;
            if (!string.IsNullOrEmpty(ngayCapHang))
            {
                ngayCap = DateTime.Parse(ngayCapHang, System.Globalization.CultureInfo.GetCultureInfo("vi-VN"));
            }
            var result = _pKHKeHoachXuongDetailService.UpdatePKHKeHoachXuongDetail(maKHXuongChiTiet, maVatTu, soLuong, ngayCap, ghiChu, out message);
            if (result)
            {
                TempData["success"] = "Cập nhật thành công";
                return Json(new { IsError = false, Message = message });
                
            }
            TempData["error"] = "Caaph nhật thất bại, xin vui lòng thử lại";
            return Json(new { IsError = true, Message = message });
        }

        public ActionResult Edit(int id)
        {
            var pKHKeHoachXuongDetailEntity = _pKHKeHoachXuongDetailService.GetById(id);
            if(pKHKeHoachXuongDetailEntity != null)
            {
                var model = pKHKeHoachXuongDetailEntity.MapToModel();
                return View(model);
            }
            ViewBag.VatTus = _chiTietVatTuService.GetAllChiTietVatTus();
            return View(new PKHKeHoachXuongDetailModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHKeHoachXuongDetailModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _pKHKeHoachXuongDetailService.UpdatePKHKeHoachXuongDetail(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHKeHoachXuongDetail");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHKeHoachXuongDetailId)
        {
            string message;
            var result = _pKHKeHoachXuongDetailService.Delete(pKHKeHoachXuongDetailId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int pKHKeHoachXuongDetailId)
        {
            string message;
            var result = _pKHKeHoachXuongDetailService.ChangeStatus(pKHKeHoachXuongDetailId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}