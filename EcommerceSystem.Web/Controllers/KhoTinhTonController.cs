﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.KhoTinhTonModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class KhoTinhTonController : BaseController
    {
        private readonly IKhoTinhTonService _khoTinhTonService = (IKhoTinhTonService)DependencyResolver.Current.GetService(typeof(IKhoTinhTonService));
        private readonly IKhoService _khoService = (IKhoService)DependencyResolver.Current.GetService(typeof(IKhoService));
        private readonly INhomSanPhamService _nhomSanPhamService = (INhomSanPhamService)DependencyResolver.Current.GetService(typeof(INhomSanPhamService));
        public ActionResult Index()
        {
            int totalRecords;
            var model = new KhoTinhTonSearchModel
            {
                KhoTinhTons = _khoTinhTonService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new KhoTinhTonSearchModel
            {
                KhoTinhTons = _khoTinhTonService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/KhoTinhTon/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new KhoTinhTonModel();
            ViewBag.KhoGroups = _khoService.GetAllKhos();
            ViewBag.NhomSanPhamGroups = _nhomSanPhamService.GetAllNhomSanPhams();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(KhoTinhTonModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_khoTinhTonService.CreateKhoTinhTon(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "KhoTinhTon");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.KhoGroups = _khoService.GetAllKhos();
            ViewBag.NhomSanPhamGroups = _nhomSanPhamService.GetAllNhomSanPhams();
            var khoTinhTonEntity = _khoTinhTonService.GetById(id);
            if(khoTinhTonEntity != null)
            {
                var model = khoTinhTonEntity.MapToModel();
                return View(model);
            }
            return View(new KhoTinhTonModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(KhoTinhTonModel model)
        {
            ViewBag.KhoGroups = _khoService.GetAllKhos();
            ViewBag.NhomSanPhamGroups = _nhomSanPhamService.GetAllNhomSanPhams();
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _khoTinhTonService.UpdateKhoTinhTon(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "KhoTinhTon");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int khoTinhTonId)
        {
            string message;
            var result = _khoTinhTonService.Delete(khoTinhTonId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int khoTinhTonId)
        {
            string message;
            var result = _khoTinhTonService.ChangeStatus(khoTinhTonId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}