﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.YeuCauMuaHangModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class YeuCauMuaHangController : BaseController
    {
        private readonly IYeuCauMuaHangService _yeuCauMuaHangService = (IYeuCauMuaHangService)DependencyResolver.Current.GetService(typeof(IYeuCauMuaHangService));
        private readonly IXuongService _xuongService = (IXuongService)DependencyResolver.Current.GetService(typeof(IXuongService));
        private readonly IKhoService _khoService = (IKhoService)DependencyResolver.Current.GetService(typeof(IKhoService));
        private readonly IChiTietVatTuService _chiTietVatTuService = (IChiTietVatTuService)DependencyResolver.Current.GetService(typeof(IChiTietVatTuService));
        private readonly IPKHPhieuYeuCauMuaHangService _pkhPhieuYeuCauMuaHangService = (IPKHPhieuYeuCauMuaHangService)DependencyResolver.Current.GetService(typeof(IPKHPhieuYeuCauMuaHangService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new YeuCauMuaHangSearchModel
            {
                YeuCauMuaHangs = _yeuCauMuaHangService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new YeuCauMuaHangSearchModel
            {
                YeuCauMuaHangs = _yeuCauMuaHangService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/YeuCauMuaHang/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            ViewBag.XuongGroups = _xuongService.GetAllXuongs();
            ViewBag.KhoGroups = _khoService.GetAllKhos();
            ViewBag.ChiTietVatTuGroups = _chiTietVatTuService.GetAllChiTietVatTus();
            ViewBag.PhieuYeuCauMuaHangGroups = _pkhPhieuYeuCauMuaHangService.GetAllPKHPhieuYeuCauMuaHangs();
            var model = new YeuCauMuaHangModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(YeuCauMuaHangModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_yeuCauMuaHangService.CreateYeuCauMuaHang(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "YeuCauMuaHang");
                }


                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.XuongGroups = _xuongService.GetAllXuongs();
            ViewBag.KhoGroups = _khoService.GetAllKhos();
            ViewBag.ChiTietVatTuGroups = _chiTietVatTuService.GetAllChiTietVatTus();
            ViewBag.PhieuYeuCauMuaHangGroups = _pkhPhieuYeuCauMuaHangService.GetAllPKHPhieuYeuCauMuaHangs();
            var yeuCauMuaHangEntity = _yeuCauMuaHangService.GetById(id);
            if(yeuCauMuaHangEntity != null)
            {
                var model = yeuCauMuaHangEntity.MapToModel();
                return View(model);
            }
            return View(new YeuCauMuaHangModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(YeuCauMuaHangModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
                model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _yeuCauMuaHangService.UpdateYeuCauMuaHang(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "YeuCauMuaHang");
                }


                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int yeuCauMuaHangId)
        {
            string message;
            var result = _yeuCauMuaHangService.Delete(yeuCauMuaHangId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int yeuCauMuaHangId)
        {
            string message;
            var result = _yeuCauMuaHangService.ChangeStatus(yeuCauMuaHangId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}