﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.KhoiLuongCoPhanModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class KhoiLuongCoPhanController : BaseController
    {
        private readonly IKhoiLuongCoPhanService _khoiLuongCoPhanService = (IKhoiLuongCoPhanService)DependencyResolver.Current.GetService(typeof(IKhoiLuongCoPhanService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new KhoiLuongCoPhanSearchModel
            {
                KhoiLuongCoPhans = _khoiLuongCoPhanService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new KhoiLuongCoPhanSearchModel
            {
                KhoiLuongCoPhans = _khoiLuongCoPhanService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/KhoiLuongCoPhan/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new KhoiLuongCoPhanModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(KhoiLuongCoPhanModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_khoiLuongCoPhanService.CreateKhoiLuongCoPhan(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "KhoiLuongCoPhan");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var khoiLuongCoPhanEntity = _khoiLuongCoPhanService.GetById(id);
            if(khoiLuongCoPhanEntity != null)
            {
                var model = khoiLuongCoPhanEntity.MapToModel();
                return View(model);
            }
            return View(new KhoiLuongCoPhanModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(KhoiLuongCoPhanModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _khoiLuongCoPhanService.UpdateKhoiLuongCoPhan(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "KhoiLuongCoPhan");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int khoiLuongCoPhanId)
        {
            string message;
            var result = _khoiLuongCoPhanService.Delete(khoiLuongCoPhanId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int khoiLuongCoPhanId)
        {
            string message;
            var result = _khoiLuongCoPhanService.ChangeStatus(khoiLuongCoPhanId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}