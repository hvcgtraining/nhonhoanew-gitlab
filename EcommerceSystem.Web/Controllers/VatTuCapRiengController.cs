﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.VatTuCapRiengModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class VatTuCapRiengController : BaseController
    {
        private readonly IVatTuCapRiengService _vatTuCapRiengService = (IVatTuCapRiengService)DependencyResolver.Current.GetService(typeof(IVatTuCapRiengService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new VatTuCapRiengSearchModel
            {
                VatTuCapRiengs = _vatTuCapRiengService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new VatTuCapRiengSearchModel
            {
                VatTuCapRiengs = _vatTuCapRiengService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/VatTuCapRieng/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new VatTuCapRiengModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VatTuCapRiengModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_vatTuCapRiengService.CreateVatTuCapRieng(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "VatTuCapRieng");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var vatTuCapRiengEntity = _vatTuCapRiengService.GetById(id);
            if(vatTuCapRiengEntity != null)
            {
                var model = vatTuCapRiengEntity.MapToModel();
                return View(model);
            }
            return View(new VatTuCapRiengModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VatTuCapRiengModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _vatTuCapRiengService.UpdateVatTuCapRieng(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "VatTuCapRieng");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int vatTuCapRiengId)
        {
            string message;
            var result = _vatTuCapRiengService.Delete(vatTuCapRiengId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int vatTuCapRiengId)
        {
            string message;
            var result = _vatTuCapRiengService.ChangeStatus(vatTuCapRiengId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}