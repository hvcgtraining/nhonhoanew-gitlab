﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.DoiMaChiTietModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class DoiMaChiTietController : BaseController
    {
        private readonly IDoiMaChiTietService _doiMaChiTietService = (IDoiMaChiTietService)DependencyResolver.Current.GetService(typeof(IDoiMaChiTietService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new DoiMaChiTietSearchModel
            {
                DoiMaChiTiets = _doiMaChiTietService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new DoiMaChiTietSearchModel
            {
                DoiMaChiTiets = _doiMaChiTietService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/DoiMaChiTiet/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new DoiMaChiTietModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DoiMaChiTietModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_doiMaChiTietService.CreateDoiMaChiTiet(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "DoiMaChiTiet");
                }
                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var doiMaChiTietEntity = _doiMaChiTietService.GetById(id);
            if(doiMaChiTietEntity != null)
            {
                var model = doiMaChiTietEntity.MapToModel();
                return View(model);
            }
            return View(new DoiMaChiTietModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DoiMaChiTietModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _doiMaChiTietService.UpdateDoiMaChiTiet(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "DoiMaChiTiet");
                }
                TempData["error"] = message;
                return View(model);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int doiMaChiTietId)
        {
            string message;
            var result = _doiMaChiTietService.Delete(doiMaChiTietId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int doiMaChiTietId)
        {
            string message;
            var result = _doiMaChiTietService.ChangeStatus(doiMaChiTietId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}