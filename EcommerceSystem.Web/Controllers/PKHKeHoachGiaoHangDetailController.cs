﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.PKHKeHoachGiaoHangDetailModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHKeHoachGiaoHangDetailController : BaseController
    {
        private readonly IPKHKeHoachGiaoHangDetailService _pKHKeHoachGiaoHangDetailService = (IPKHKeHoachGiaoHangDetailService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachGiaoHangDetailService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new PKHKeHoachGiaoHangDetailSearchModel
            {
                PKHKeHoachGiaoHangDetails = _pKHKeHoachGiaoHangDetailService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHKeHoachGiaoHangDetailSearchModel
            {
                PKHKeHoachGiaoHangDetails = _pKHKeHoachGiaoHangDetailService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHKeHoachGiaoHangDetail/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHKeHoachGiaoHangDetailModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHKeHoachGiaoHangDetailModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_pKHKeHoachGiaoHangDetailService.CreatePKHKeHoachGiaoHangDetail(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHKeHoachGiaoHangDetail");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var pKHKeHoachGiaoHangDetailEntity = _pKHKeHoachGiaoHangDetailService.GetById(id);
            if(pKHKeHoachGiaoHangDetailEntity != null)
            {
                var model = pKHKeHoachGiaoHangDetailEntity.MapToModel();
                return View(model);
            }
            return View(new PKHKeHoachGiaoHangDetailModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHKeHoachGiaoHangDetailModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _pKHKeHoachGiaoHangDetailService.UpdatePKHKeHoachGiaoHangDetail(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHKeHoachGiaoHangDetail");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHKeHoachGiaoHangDetailId)
        {
            string message;
            var result = _pKHKeHoachGiaoHangDetailService.Delete(pKHKeHoachGiaoHangDetailId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int pKHKeHoachGiaoHangDetailId)
        {
            string message;
            var result = _pKHKeHoachGiaoHangDetailService.ChangeStatus(pKHKeHoachGiaoHangDetailId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}