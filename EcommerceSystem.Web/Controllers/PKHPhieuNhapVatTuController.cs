﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.PKHPhieuNhapVatTuModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Globalization;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHPhieuNhapVatTuController : BaseController
    {
        private readonly IPKHPhieuNhapVatTuService _pKHPhieuNhapVatTuService = (IPKHPhieuNhapVatTuService)DependencyResolver.Current.GetService(typeof(IPKHPhieuNhapVatTuService));
        private readonly IKhoService _pKho = (IKhoService)DependencyResolver.Current.GetService(typeof(IKhoService));
        private readonly IPKHKiemHoaService _pIPKHKiemHoa = (IPKHKiemHoaService)DependencyResolver.Current.GetService(typeof(IPKHKiemHoaService));
        private readonly IDanhMucKhachHangService _pDanhMucKhachhang = (IDanhMucKhachHangService)DependencyResolver.Current.GetService(typeof(IDanhMucKhachHangService));
        public ActionResult Index()
        {
            int totalRecords;
            var model = new PKHPhieuNhapVatTuSearchModel
            {
                PKHPhieuNhapVatTus = _pKHPhieuNhapVatTuService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHPhieuNhapVatTuSearchModel
            {
                PKHPhieuNhapVatTus = _pKHPhieuNhapVatTuService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHPhieuNhapVatTu/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHPhieuNhapVatTuModel();
            ViewBag.KhoGroups = _pKho.GetAllKhos();
            ViewBag.KiemHoaGroups = _pIPKHKiemHoa.GetAllPKHKiemHoas();
            ViewBag.DanhMucKhachhangGroups = _pDanhMucKhachhang.GetAllDanhMucKhachHangs();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHPhieuNhapVatTuModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;
                model.NgayLapPhieu = DateTime.Parse(model.NgayLapPhieuDangChuoi, CultureInfo.GetCultureInfo("vi-VN"));
                if (_pKHPhieuNhapVatTuService.CreatePKHPhieuNhapVatTu(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHPhieuNhapVatTu");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.KhoGroups = _pKho.GetAllKhos();
            ViewBag.KiemhoaGroups = _pIPKHKiemHoa.GetAllPKHKiemHoas();
            ViewBag.DanhMucKhachhangGroups = _pDanhMucKhachhang.GetAllDanhMucKhachHangs();
            var pKHPhieuNhapVatTuEntity = _pKHPhieuNhapVatTuService.GetById(id);
            if(pKHPhieuNhapVatTuEntity != null)
            {
                var model = pKHPhieuNhapVatTuEntity.MapToModel();
                return View(model);
            }
            return View(new PKHPhieuNhapVatTuModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHPhieuNhapVatTuModel model)
        {
            ViewBag.KhoGroups = _pKho.GetAllKhos();
            ViewBag.KiemhoaGroups = _pIPKHKiemHoa.GetAllPKHKiemHoas();
            ViewBag.DanhMucKhachhangGroups = _pDanhMucKhachhang.GetAllDanhMucKhachHangs();
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                model.NgayLapPhieu = DateTime.Parse(model.NgayLapPhieuDangChuoi, CultureInfo.GetCultureInfo("vi-VN"));
                var isSuccess = _pKHPhieuNhapVatTuService.UpdatePKHPhieuNhapVatTu(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHPhieuNhapVatTu");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHPhieuNhapVatTuId)
        {
            string message;
            var result = _pKHPhieuNhapVatTuService.Delete(pKHPhieuNhapVatTuId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int pKHPhieuNhapVatTuId)
        {
            string message;
            var result = _pKHPhieuNhapVatTuService.ChangeStatus(pKHPhieuNhapVatTuId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}