﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.LoaiVatTuModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class LoaiVatTuController : BaseController
    {
        private readonly ILoaiVatTuService _loaiVatTuService = (ILoaiVatTuService)DependencyResolver.Current.GetService(typeof(ILoaiVatTuService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new LoaiVatTuSearchModel
            {
                LoaiVatTus = _loaiVatTuService.Search(1, SystemConfiguration.PageSizeDefault, null, 1, 0, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch,int searchType, int cap, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new LoaiVatTuSearchModel
            {
                LoaiVatTus = _loaiVatTuService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, searchType, cap, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/LoaiVatTu/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new LoaiVatTuModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LoaiVatTuModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_loaiVatTuService.CreateLoaiVatTu(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "LoaiVatTu");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var loaiVatTuEntity = _loaiVatTuService.GetById(id);
            if(loaiVatTuEntity != null)
            {
                var model = loaiVatTuEntity.MapToModel();
                return View(model);
            }
            return View(new LoaiVatTuModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LoaiVatTuModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _loaiVatTuService.UpdateLoaiVatTu(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "LoaiVatTu");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int loaiVatTuId)
        {
            string message;
            var result = _loaiVatTuService.Delete(loaiVatTuId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int loaiVatTuId)
        {
            string message;
            var result = _loaiVatTuService.ChangeStatus(loaiVatTuId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}