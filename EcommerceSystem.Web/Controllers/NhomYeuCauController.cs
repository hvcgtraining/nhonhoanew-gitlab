﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.NhomYeuCauModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class NhomYeuCauController : BaseController
    {
        private readonly INhomYeuCauService _nhomYeuCauService = (INhomYeuCauService)DependencyResolver.Current.GetService(typeof(INhomYeuCauService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new NhomYeuCauSearchModel
            {
                NhomYeuCaus = _nhomYeuCauService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new NhomYeuCauSearchModel
            {
                NhomYeuCaus = _nhomYeuCauService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/NhomYeuCau/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var pycId = Request.Params["PKHPhieuYeuCauMuaHangId"].ToString().Trim();

            var model = new NhomYeuCauModel();
            if (!string.IsNullOrEmpty(pycId))
            {
                model.PKHPhieuYeuCauMuaHangId = long.Parse(pycId);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NhomYeuCauModel model)
        {
            var pycId = Request.Params["PKHPhieuYeuCauMuaHangId"].ToString().Trim();
            if(!string.IsNullOrEmpty(pycId))
            {
                model.PKHPhieuYeuCauMuaHangId = long.Parse(pycId);
            }
            
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;
                string message = "";
                if (_nhomYeuCauService.CreateNhomYeuCau(model, out message))
                {
                    TempData["success"] = "Tạo mới thành công";

                    return RedirectToAction("Index", "PKHPhieuYeuCauMuaHangDetail", new { PKHPhieuYeuCauMuaHangId = model.PKHPhieuYeuCauMuaHangId });
                }

                TempData["error"] = "Tạo mới thất bại. " + message;
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var nhomYeuCauEntity = _nhomYeuCauService.GetById(id);
            if(nhomYeuCauEntity != null)
            {
                var model = nhomYeuCauEntity.MapToModel();
                return View(model);
            }
            return View(new NhomYeuCauModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NhomYeuCauModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _nhomYeuCauService.UpdateNhomYeuCau(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "NhomYeuCau");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int nhomYeuCauId)
        {
            string message;
            var result = _nhomYeuCauService.Delete(nhomYeuCauId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int nhomYeuCauId)
        {
            string message;
            var result = _nhomYeuCauService.ChangeStatus(nhomYeuCauId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}