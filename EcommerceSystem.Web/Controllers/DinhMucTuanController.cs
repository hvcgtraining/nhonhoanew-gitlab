﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.DinhMucTuanModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class DinhMucTuanController : BaseController
    {
        private readonly IDinhMucTuanService _dinhMucTuanService = (IDinhMucTuanService)DependencyResolver.Current.GetService(typeof(IDinhMucTuanService));
        private readonly IChiTietVatTuService _chiTietVatTuService = (IChiTietVatTuService)DependencyResolver.Current.GetService(typeof(IChiTietVatTuService));
        public ActionResult Index()
        {
            int totalRecords;
            var model = new DinhMucTuanSearchModel
            {
                DinhMucTuans = _dinhMucTuanService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new DinhMucTuanSearchModel
            {
                DinhMucTuans = _dinhMucTuanService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/DinhMucTuan/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new DinhMucTuanModel();
            //model.ListLoaiVatTu = _dinhMucTuanService.GetListLoaiVatTu();
            ViewBag.ChiTietVatTuGroups = _chiTietVatTuService.GetAllChiTietVatTus();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DinhMucTuanModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_dinhMucTuanService.CreateDinhMucTuan(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "DinhMucTuan");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.ChiTietVatTuGroups = _chiTietVatTuService.GetAllChiTietVatTus();
            var dinhMucTuanEntity = _dinhMucTuanService.GetById(id);
            if(dinhMucTuanEntity != null)
            {
                var model = dinhMucTuanEntity.MapToModel();
                model.ListLoaiVatTu = _dinhMucTuanService.GetListLoaiVatTu();

                return View(model);
            }
            return View(new DinhMucTuanModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DinhMucTuanModel model)
        {
            ViewBag.ChiTietVatTuGroups = _chiTietVatTuService.GetAllChiTietVatTus();
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _dinhMucTuanService.UpdateDinhMucTuan(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "DinhMucTuan");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int dinhMucTuanId)
        {
            string message;
            var result = _dinhMucTuanService.Delete(dinhMucTuanId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int dinhMucTuanId)
        {
            string message;
            var result = _dinhMucTuanService.ChangeStatus(dinhMucTuanId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}