﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.LyDoXuatModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class LyDoXuatController : BaseController
    {
        private readonly ILyDoXuatService _lyDoXuatService = (ILyDoXuatService)DependencyResolver.Current.GetService(typeof(ILyDoXuatService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new LyDoXuatSearchModel
            {
                LyDoXuats = _lyDoXuatService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new LyDoXuatSearchModel
            {
                LyDoXuats = _lyDoXuatService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/LyDoXuat/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new LyDoXuatModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LyDoXuatModel model)
        {

            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_lyDoXuatService.CreateLyDoXuat(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "LyDoXuat");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var lyDoXuatEntity = _lyDoXuatService.GetById(id);
            if(lyDoXuatEntity != null)
            {
                var model = lyDoXuatEntity.MapToModel();
                return View(model);
            }
            return View(new LyDoXuatModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LyDoXuatModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _lyDoXuatService.UpdateLyDoXuat(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "LyDoXuat");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int lyDoXuatId)
        {
            string message;
            var result = _lyDoXuatService.Delete(lyDoXuatId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int lyDoXuatId)
        {
            string message;
            var result = _lyDoXuatService.ChangeStatus(lyDoXuatId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}