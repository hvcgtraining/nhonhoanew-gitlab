﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.DinhMucCatToleModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Globalization;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class DinhMucCatToleController : BaseController
    {
        private readonly IDinhMucCatToleService _dinhMucCatToleService = (IDinhMucCatToleService)DependencyResolver.Current.GetService(typeof(IDinhMucCatToleService));
        private readonly IChiTietVatTuService _chiTietVatTuService = (IChiTietVatTuService)DependencyResolver.Current.GetService(typeof(IChiTietVatTuService));
        private readonly IToleService _toleService = (IToleService)DependencyResolver.Current.GetService(typeof(IToleService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new DinhMucCatToleSearchModel
            {
                DinhMucCatToles = _dinhMucCatToleService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }        

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new DinhMucCatToleSearchModel
            {
                DinhMucCatToles = _dinhMucCatToleService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/DinhMucCatTole/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new DinhMucCatToleModel();
            ViewBag.LoaiChiTiet = _chiTietVatTuService.GetAllChiTietVatTus();
            ViewBag.DanhSachTole = _toleService.GetAllToles();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DinhMucCatToleModel model)
        {
            if (ModelState.IsValid)
            {
                //model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;
                model.CreatedDate = DateTime.Parse(model.CreateDateValue, CultureInfo.GetCultureInfo("vi-VN"));

                if (_dinhMucCatToleService.CreateDinhMucCatTole(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    //return RedirectToAction("Index", "DinhMucCatTole");
                    return RedirectToAction("DinhMucCatTole", "DanhSachChiTiet", new { dinhMucCatToleId = model.DinhMucCatToleId });
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var dinhMucCatToleEntity = _dinhMucCatToleService.GetById(id);
            if(dinhMucCatToleEntity != null)
            {
                var model = dinhMucCatToleEntity.MapToModel();
                ViewBag.LoaiChiTiet = _chiTietVatTuService.GetAllChiTietVatTus();
                ViewBag.DanhSachTole = _toleService.GetAllToles();
                return View(model);
            }
            return View(new DinhMucCatToleModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DinhMucCatToleModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _dinhMucCatToleService.UpdateDinhMucCatTole(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "DinhMucCatTole");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int dinhMucCatToleId)
        {
            string message;
            var result = _dinhMucCatToleService.Delete(dinhMucCatToleId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int dinhMucCatToleId)
        {
            string message;
            var result = _dinhMucCatToleService.ChangeStatus(dinhMucCatToleId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}