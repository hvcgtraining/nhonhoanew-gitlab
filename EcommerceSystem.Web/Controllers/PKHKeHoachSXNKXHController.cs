﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.PKHKeHoachSXNKXHModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Globalization;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHKeHoachSXNKXHController : BaseController
    {
        private readonly IPKHKeHoachSXNKXHService _pKHKeHoachSXNKXHService = (IPKHKeHoachSXNKXHService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachSXNKXHService));
        private readonly IPKHKeHoachSXNKXHDetailService _pKHKeHoachSXNKXHDetailService = (IPKHKeHoachSXNKXHDetailService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachSXNKXHDetailService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new PKHKeHoachSXNKXHSearchModel
            {
                PKHKeHoachSXNKXHs = _pKHKeHoachSXNKXHService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult KeHoachSX()
        {
            int totalRecords;
            var model = new PKHKeHoachSXNKXHSearchModel
            {
                PKHKeHoachSXs = _pKHKeHoachSXNKXHService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }
        public ActionResult KeHoachSXLapRap()
        {
            int totalRecords;
            var model = new PKHKeHoachSXNKXHSearchModel
            {
                PKHKeHoachSXs = _pKHKeHoachSXNKXHService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, null, null, out totalRecords, true),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult KeHoachSXLapChiTiet()
        {
            int totalRecords;
            var model = new PKHKeHoachSXNKXHSearchModel
            {
                PKHKeHoachSXs = _pKHKeHoachSXNKXHService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, null, null, out totalRecords, true),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string fromDateValue, String toDateValue, string sortColumn, string sortDirection)
        {
            int totalRecords;
            DateTime? fromDate = null;
            DateTime? toDate = null;
            if(!string.IsNullOrEmpty(fromDateValue))
            {
                fromDate = DateTime.Parse(fromDateValue, CultureInfo.GetCultureInfo("vi-VN"));
            }
            if (!string.IsNullOrEmpty(toDateValue))
            {
                toDate = DateTime.Parse(toDateValue, CultureInfo.GetCultureInfo("vi-VN"));
            }
            var model = new PKHKeHoachSXNKXHSearchModel
            {
                PKHKeHoachSXNKXHs = _pKHKeHoachSXNKXHService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, fromDate, toDate, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHKeHoachSXNKXH/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHKeHoachSXNKXHModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHKeHoachSXNKXHModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;
                model.StartDate = DateTime.Parse(model.StartDateValue, CultureInfo.GetCultureInfo("vi-VN"));
                model.EndDate = DateTime.Parse(model.EndDateValue, CultureInfo.GetCultureInfo("vi-VN"));
                model.NgayTonKho = DateTime.Parse(model.NgayTonKhoValue, CultureInfo.GetCultureInfo("vi-VN"));
                if (_pKHKeHoachSXNKXHService.CreatePKHKeHoachSXNKXH(model))
                {
                    TempData["success"] = "Tạo mới thành công. Vui lòng nhập kế khoạch chi tiết.";
                    return RedirectToAction("Index", "PKHKeHoachSXNKXHDetail", new { keHoachId = model.PKHKeHoachSXNKXHId, maTuan = model.MaTuan });
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var pKHKeHoachSXNKXHEntity = _pKHKeHoachSXNKXHService.GetById(id);
            if(pKHKeHoachSXNKXHEntity != null)
            {
                var model = pKHKeHoachSXNKXHEntity.MapToModel();
                return View(model);
            }
            return View(new PKHKeHoachSXNKXHModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHKeHoachSXNKXHModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                model.StartDate = DateTime.Parse(model.StartDateValue, CultureInfo.GetCultureInfo("vi-VN"));
                model.EndDate = DateTime.Parse(model.EndDateValue, CultureInfo.GetCultureInfo("vi-VN"));
                model.NgayTonKho = DateTime.Parse(model.NgayTonKhoValue, CultureInfo.GetCultureInfo("vi-VN"));
                var isSuccess = _pKHKeHoachSXNKXHService.UpdatePKHKeHoachSXNKXH(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHKeHoachSXNKXH");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(long pKHKeHoachSXNKXHId)
        {
            string message;
            var result = _pKHKeHoachSXNKXHService.Delete(pKHKeHoachSXNKXHId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int pKHKeHoachSXNKXHId)
        {
            string message;
            var result = _pKHKeHoachSXNKXHService.ChangeStatus(pKHKeHoachSXNKXHId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}