﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models;
using EcommerceSystem.Models.PKHPhieuYeuCauMuaHangDetailModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHPhieuYeuCauMuaHangDetailController : BaseController
    {
        private readonly IPKHPhieuYeuCauMuaHangDetailService _pKHPhieuYeuCauMuaHangDetailService = (IPKHPhieuYeuCauMuaHangDetailService)DependencyResolver.Current.GetService(typeof(IPKHPhieuYeuCauMuaHangDetailService));
        private readonly IPKHPhieuYeuCauMuaHangService _pKHPhieuYeuCauMuaHangService = (IPKHPhieuYeuCauMuaHangService)DependencyResolver.Current.GetService(typeof(IPKHPhieuYeuCauMuaHangService));
        private readonly IChiTietVatTuService _chiTietVatTuService = (IChiTietVatTuService)DependencyResolver.Current.GetService(typeof(IChiTietVatTuService));
        private readonly INhomYeuCauService _nhomYeuCauService = (INhomYeuCauService)DependencyResolver.Current.GetService(typeof(INhomYeuCauService));
        private readonly IYeuCauMuaHangService _yeuCauMuaHangService = (IYeuCauMuaHangService)DependencyResolver.Current.GetService(typeof(IYeuCauMuaHangService));

        public ActionResult Index()
        {
            ViewBag.NhomYeuCauGroups = _nhomYeuCauService.GetAllNhomYeuCaus();
            int totalRecords;
            int phieuYeuCauMuaHangId = 0;
            int.TryParse(Request.QueryString["PKHPhieuYeuCauMuaHangId"], out phieuYeuCauMuaHangId);
            var model = new PKHPhieuYeuCauMuaHangDetailSearchModel
            {
                PKHPhieuYeuCauMuaHangDetails = _pKHPhieuYeuCauMuaHangDetailService.Search(1, SystemConfiguration.PageSizeDefault, null, phieuYeuCauMuaHangId, 0, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
                PKHPhieuYeuCauMuaHangId = phieuYeuCauMuaHangId,
                TenPhieuYeuCauMuaHang = _pKHPhieuYeuCauMuaHangService.GetById(phieuYeuCauMuaHangId).Title.ToString(),
            };

            return View(model);
        }

        public ActionResult TheoDoiPhieuYeuCauMuaHangDetail()
        {
            ViewBag.NhomYeuCauGroups = _nhomYeuCauService.GetAllNhomYeuCaus();
            int totalRecords;
            int phieuYeuCauMuaHangId = 0;
            int.TryParse(Request.QueryString["PKHPhieuYeuCauMuaHangId"], out phieuYeuCauMuaHangId);
            var model = new PKHPhieuYeuCauMuaHangDetailSearchModel
            {
                PKHPhieuYeuCauMuaHangDetails = _pKHPhieuYeuCauMuaHangDetailService.Search(1, SystemConfiguration.PageSizeDefault, null, phieuYeuCauMuaHangId, 0, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
                PKHPhieuYeuCauMuaHangId = phieuYeuCauMuaHangId,
            };

            return View(model);
        }
        public ActionResult TheoDoiYeuCauMuaHang()
        {
            int totalRecords;
            var model = new PKHPhieuYeuCauMuaHangDetailSearchModel
            {
                PKHPhieuYeuCauMuaHangDetails = _pKHPhieuYeuCauMuaHangDetailService.Search(1, SystemConfiguration.PageSizeDefault, null, 0, 0, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }
        public ActionResult Search(int currentPage, string textSearch, int PKHPhieuYeuCauMuaHangId, int? maNhom, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHPhieuYeuCauMuaHangDetailSearchModel
            {
                PKHPhieuYeuCauMuaHangDetails = _pKHPhieuYeuCauMuaHangDetailService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, PKHPhieuYeuCauMuaHangId, maNhom, sortColumn, sortDirection, out totalRecords),
                SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHPhieuYeuCauMuaHangDetail/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create(int PKHPhieuYeuCauMuaHangId)
        {
            ViewBag.PhieuYeuCauMuaHangGroups = _pKHPhieuYeuCauMuaHangService.GetAllPKHPhieuYeuCauMuaHangs();
            ViewBag.ChiTietVatTuGroups = _chiTietVatTuService.GetAllChiTietVatTus();
            ViewBag.NhomYeuCauGroups = _nhomYeuCauService.GetAllNhomYeuCaus();
            var model = new PKHPhieuYeuCauMuaHangDetailModel();
            model.PKHPhieuYeuCauMuaHangId = PKHPhieuYeuCauMuaHangId;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHPhieuYeuCauMuaHangDetailModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_pKHPhieuYeuCauMuaHangDetailService.CreatePKHPhieuYeuCauMuaHangDetail(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHPhieuYeuCauMuaHangDetail", new { PKHPhieuYeuCauMuaHangId = model.PKHPhieuYeuCauMuaHangId });
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.PhieuYeuCauMuaHangGroups = _pKHPhieuYeuCauMuaHangService.GetAllPKHPhieuYeuCauMuaHangs();
            ViewBag.ChiTietVatTuGroups = _chiTietVatTuService.GetAllChiTietVatTus();
            ViewBag.NhomYeuCauGroups = _nhomYeuCauService.GetAllNhomYeuCaus();
            var pKHPhieuYeuCauMuaHangDetailEntity = _pKHPhieuYeuCauMuaHangDetailService.GetById(id);
            if (pKHPhieuYeuCauMuaHangDetailEntity != null)
            {
                var model = pKHPhieuYeuCauMuaHangDetailEntity.MapToModel();
                return View(model);
            }
            return View(new PKHPhieuYeuCauMuaHangDetailModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHPhieuYeuCauMuaHangDetailModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
                model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _pKHPhieuYeuCauMuaHangDetailService.UpdatePKHPhieuYeuCauMuaHangDetail(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHPhieuYeuCauMuaHangDetail", new { PKHPhieuYeuCauMuaHangId = model.PKHPhieuYeuCauMuaHangId });
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHPhieuYeuCauMuaHangDetailId)
        {
            string message;
            var result = _pKHPhieuYeuCauMuaHangDetailService.Delete(pKHPhieuYeuCauMuaHangDetailId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

        [HttpPost]
        public ActionResult Invisibe(int pKHPhieuYeuCauMuaHangDetailId)
        {
            string message;
            var result = _pKHPhieuYeuCauMuaHangDetailService.ChangeStatus(pKHPhieuYeuCauMuaHangDetailId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }

        [HttpPost]
        public ActionResult HoanThanhYeuCau(int pKHPhieuYeuCauMuaHangId)
        {
            try
            {
                var message = "";
                var pKHPhieuYeuCauMuaHangEntity = _pKHPhieuYeuCauMuaHangService.GetById(pKHPhieuYeuCauMuaHangId).MapToModel();
                var yeuCauMuaHangEntity = _yeuCauMuaHangService.GetYeuCauMuaHangsByParams((int)TrangThaiYeuCauMuaHang.DangCho, pKHPhieuYeuCauMuaHangId);
                pKHPhieuYeuCauMuaHangEntity.TrangThai = (int)TrangThaiYeuCauMuaHang.DaHoanThanh;
                _pKHPhieuYeuCauMuaHangService.UpdatePKHPhieuYeuCauMuaHang(pKHPhieuYeuCauMuaHangEntity, out message);
                foreach (var item in yeuCauMuaHangEntity)
                {
                    item.TrangThai = (int)TrangThaiYeuCauMuaHang.DaHoanThanh;
                    _yeuCauMuaHangService.UpdateYeuCauMuaHang(item, out message);
                }
                TempData["success"] = "Thành công! Hoàn thành yêu cầu.";
                return Json(new { IsError = false, Message = "Hoàn thành yêu cầu." });
            }
            catch (Exception ex)
            {
                return Json(new { IsError = true, Message = "Thao tác thất bại!" });
            }

        }

        [HttpPost]
        public ActionResult ChuyenYeuCau(int pKHPhieuYeuCauMuaHangId)
        {
            try
            {
                var message = "";
                var pKHPhieuYeuCauMuaHangEntity = _pKHPhieuYeuCauMuaHangService.GetById(pKHPhieuYeuCauMuaHangId).MapToModel();
                var yeuCauMuaHangEntity = _yeuCauMuaHangService.GetYeuCauMuaHangsByParams((int)TrangThaiYeuCauMuaHang.DangCho, pKHPhieuYeuCauMuaHangId);
                pKHPhieuYeuCauMuaHangEntity.TrangThai = (int)TrangThaiYeuCauMuaHang.DaXuLy;
                _pKHPhieuYeuCauMuaHangService.UpdatePKHPhieuYeuCauMuaHang(pKHPhieuYeuCauMuaHangEntity, out message);
                foreach (var item in yeuCauMuaHangEntity)
                {
                    item.TrangThai = (int)TrangThaiYeuCauMuaHang.DaXuLy;
                    _yeuCauMuaHangService.UpdateYeuCauMuaHang(item, out message);
                }
                TempData["success"] = "Thành công! Chuyển yêu cầu thành công.";
                return Json(new { IsError = false, Message = "Chuyển yêu cầu thành công." });
            }
            catch (Exception ex)
            {
                return Json(new { IsError = true, Message = "Thao tác thất bại!" });
            }

        }

        [HttpPost]
        public ActionResult HuyYeuCau(int pKHPhieuYeuCauMuaHangId)
        {
            try
            {
                var message = "";
                var pKHPhieuYeuCauMuaHangEntity = _pKHPhieuYeuCauMuaHangService.GetById(pKHPhieuYeuCauMuaHangId).MapToModel();
                var yeuCauMuaHangEntity = _yeuCauMuaHangService.GetYeuCauMuaHangsByParams((int)TrangThaiYeuCauMuaHang.DangCho, pKHPhieuYeuCauMuaHangId);
                pKHPhieuYeuCauMuaHangEntity.TrangThai = (int)TrangThaiYeuCauMuaHang.DaHuy;
                _pKHPhieuYeuCauMuaHangService.UpdatePKHPhieuYeuCauMuaHang(pKHPhieuYeuCauMuaHangEntity, out message);
                foreach (var item in yeuCauMuaHangEntity)
                {
                    item.TrangThai = (int)TrangThaiYeuCauMuaHang.DaHuy;
                    _yeuCauMuaHangService.UpdateYeuCauMuaHang(item, out message);
                }
                TempData["success"] = "Thành công! Hủy thành công.";
                return Json(new { IsError = false, Message = "Hủy thành công." });
            }
            catch (Exception ex)
            {
                return Json(new { IsError = true, Message = "Thao tác thất bại!" });
            }

        }
    }
}