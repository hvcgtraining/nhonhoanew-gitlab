﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.PKHKeHoachXuatKhauModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHKeHoachXuatKhauController : BaseController
    {
        private readonly IPKHKeHoachXuatKhauService _pKHKeHoachXuatKhauService = (IPKHKeHoachXuatKhauService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachXuatKhauService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new PKHKeHoachXuatKhauSearchModel
            {
                PKHKeHoachXuatKhaus = _pKHKeHoachXuatKhauService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHKeHoachXuatKhauSearchModel
            {
                PKHKeHoachXuatKhaus = _pKHKeHoachXuatKhauService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHKeHoachXuatKhau/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHKeHoachXuatKhauModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHKeHoachXuatKhauModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_pKHKeHoachXuatKhauService.CreatePKHKeHoachXuatKhau(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHKeHoachXuatKhau");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var pKHKeHoachXuatKhauEntity = _pKHKeHoachXuatKhauService.GetById(id);
            if(pKHKeHoachXuatKhauEntity != null)
            {
                var model = pKHKeHoachXuatKhauEntity.MapToModel();
                return View(model);
            }
            return View(new PKHKeHoachXuatKhauModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHKeHoachXuatKhauModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _pKHKeHoachXuatKhauService.UpdatePKHKeHoachXuatKhau(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHKeHoachXuatKhau");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHKeHoachXuatKhauId)
        {
            string message;
            var result = _pKHKeHoachXuatKhauService.Delete(pKHKeHoachXuatKhauId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int pKHKeHoachXuatKhauId)
        {
            string message;
            var result = _pKHKeHoachXuatKhauService.ChangeStatus(pKHKeHoachXuatKhauId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}