﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.QuanLySoNModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class QuanLySoNController : BaseController
    {
        private readonly IQuanLySoNService _quanLySoNService = (IQuanLySoNService)DependencyResolver.Current.GetService(typeof(IQuanLySoNService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new QuanLySoNSearchModel
            {
                QuanLySoNs = _quanLySoNService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new QuanLySoNSearchModel
            {
                QuanLySoNs = _quanLySoNService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/QuanLySoN/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new QuanLySoNModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(QuanLySoNModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_quanLySoNService.CreateQuanLySoN(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "QuanLySoN");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var quanLySoNEntity = _quanLySoNService.GetById(id);
            if(quanLySoNEntity != null)
            {
                var model = quanLySoNEntity.MapToModel();
                return View(model);
            }
            return View(new QuanLySoNModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(QuanLySoNModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _quanLySoNService.UpdateQuanLySoN(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "QuanLySoN");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int quanLySoNId)
        {
            string message;
            var result = _quanLySoNService.Delete(quanLySoNId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int quanLySoNId)
        {
            string message;
            var result = _quanLySoNService.ChangeStatus(quanLySoNId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}