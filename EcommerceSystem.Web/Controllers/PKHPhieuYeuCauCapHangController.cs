﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.PKHPhieuYeuCauCapHangModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Globalization;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHPhieuYeuCauCapHangController : BaseController
    {
        private readonly IPKHPhieuYeuCauCapHangService _pKHPhieuYeuCauCapHangService = (IPKHPhieuYeuCauCapHangService)DependencyResolver.Current.GetService(typeof(IPKHPhieuYeuCauCapHangService));
        private readonly IChiTietVatTuService _loaiVatTuService = (IChiTietVatTuService)DependencyResolver.Current.GetService(typeof(IChiTietVatTuService));
        private readonly IKhoService _khoService = (IKhoService)DependencyResolver.Current.GetService(typeof(IKhoService));
        private readonly ILyDoXuatService _lyDoXuatService = (ILyDoXuatService)DependencyResolver.Current.GetService(typeof(ILyDoXuatService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new PKHPhieuYeuCauCapHangSearchModel
            {
                PKHPhieuYeuCauCapHangs = _pKHPhieuYeuCauCapHangService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHPhieuYeuCauCapHangSearchModel
            {
                PKHPhieuYeuCauCapHangs = _pKHPhieuYeuCauCapHangService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHPhieuYeuCauCapHang/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHPhieuYeuCauCapHangModel();
            ViewBag.DanhSachThanhPham = _loaiVatTuService.GetAllChiTietVatTus();
            ViewBag.DanhSachKho = _khoService.GetAllKhos();
            ViewBag.DanhMucLyDo = _lyDoXuatService.GetAllLyDoXuats();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHPhieuYeuCauCapHangModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;
                model.NgayLap = DateTime.Parse(model.CreateDateValue, CultureInfo.GetCultureInfo("vi-VN"));

                if (_pKHPhieuYeuCauCapHangService.CreatePKHPhieuYeuCauCapHang(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHPhieuYeuCauCapHangDetail", new { phieuYeuCauId = model.PKHPhieuYeuCauCapHangId });
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var pKHPhieuYeuCauCapHangEntity = _pKHPhieuYeuCauCapHangService.GetById(id);
            if(pKHPhieuYeuCauCapHangEntity != null)
            {
                var model = pKHPhieuYeuCauCapHangEntity.MapToModel();
                ViewBag.DanhSachThanhPham = _loaiVatTuService.GetAllChiTietVatTus();
                ViewBag.DanhSachKho = _khoService.GetAllKhos();
                ViewBag.DanhMucLyDo = _lyDoXuatService.GetAllLyDoXuats();
                return View(model);
            }
            return View(new PKHPhieuYeuCauCapHangModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHPhieuYeuCauCapHangModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                model.NgayLap = DateTime.Parse(model.CreateDateValue, CultureInfo.GetCultureInfo("vi-VN"));
                var isSuccess = _pKHPhieuYeuCauCapHangService.UpdatePKHPhieuYeuCauCapHang(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHPhieuYeuCauCapHang");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHPhieuYeuCauCapHangId)
        {
            string message;
            var result = _pKHPhieuYeuCauCapHangService.Delete(pKHPhieuYeuCauCapHangId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int pKHPhieuYeuCauCapHangId)
        {
            string message;
            var result = _pKHPhieuYeuCauCapHangService.ChangeStatus(pKHPhieuYeuCauCapHangId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}