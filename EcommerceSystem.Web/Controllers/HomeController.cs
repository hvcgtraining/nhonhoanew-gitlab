﻿using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
     
        public ActionResult Header()
        {
            return PartialView("~/Views/Shared/Partial/_HeaderPartial.cshtml", CurrentUser);
        }
     
        public ActionResult SwichLanguage(string cultureCode)
        {
            //LanguageUtils.SetLanguage(cultureCode);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }    
    }
}