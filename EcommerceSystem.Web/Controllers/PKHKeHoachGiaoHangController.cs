﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.PKHKeHoachGiaoHangModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Globalization;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHKeHoachGiaoHangController : BaseController
    {
        private readonly IPKHKeHoachGiaoHangService _pKHKeHoachGiaoHangService = (IPKHKeHoachGiaoHangService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachGiaoHangService));
        private readonly IPKHKeHoachXuongService _pKHKeHoachXuongService = (IPKHKeHoachXuongService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachXuongService));
        private readonly IPKHKeHoachSXNKXHService _pKHKeHoachSXNKXHService = (IPKHKeHoachSXNKXHService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachSXNKXHService));
        private readonly IXuongService _pXuongService = (IXuongService)DependencyResolver.Current.GetService(typeof(IXuongService));
        public ActionResult Index()
        {
            int totalRecords;
            var model = new PKHKeHoachGiaoHangSearchModel
            {
                PKHKeHoachGiaoHangs = _pKHKeHoachGiaoHangService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHKeHoachGiaoHangSearchModel
            {
                PKHKeHoachGiaoHangs = _pKHKeHoachGiaoHangService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHKeHoachGiaoHang/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHKeHoachGiaoHangModel();
            ViewBag.XuongGroups = _pXuongService.GetAllXuongs();
            ViewBag.KeHoachXuongGroups = _pKHKeHoachXuongService.GetAllPKHKeHoachXuongs();
            ViewBag.KeHoachSXNKXHs = _pKHKeHoachSXNKXHService.GetAllPKHKeHoachSXNKXHs();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHKeHoachGiaoHangModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;
                model.StartDate = DateTime.Parse(model.NgayBatDauDangChuoi, CultureInfo.GetCultureInfo("vi-VN"));
                model.EndDate = DateTime.Parse(model.NgayKetThucDangChuoi, CultureInfo.GetCultureInfo("vi-VN"));
                if (_pKHKeHoachGiaoHangService.CreatePKHKeHoachGiaoHang(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHKeHoachGiaoHang");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.XuongGroups = _pXuongService.GetAllXuongs();
            ViewBag.KeHoachXuongGroups = _pKHKeHoachXuongService.GetAllPKHKeHoachXuongs();
            ViewBag.KeHoachSXNKXHs = _pKHKeHoachSXNKXHService.GetAllPKHKeHoachSXNKXHs();
            var pKHKeHoachGiaoHangEntity = _pKHKeHoachGiaoHangService.GetById(id);
            if(pKHKeHoachGiaoHangEntity != null)
            {
                var model = pKHKeHoachGiaoHangEntity.MapToModel();
                return View(model);
            }
            return View(new PKHKeHoachGiaoHangModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHKeHoachGiaoHangModel model)
        {
            ViewBag.XuongGroups = _pXuongService.GetAllXuongs();
            ViewBag.KeHoachXuongGroups = _pKHKeHoachXuongService.GetAllPKHKeHoachXuongs();
            ViewBag.KeHoachSXNKXHs = _pKHKeHoachSXNKXHService.GetAllPKHKeHoachSXNKXHs();
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                model.StartDate = DateTime.Parse(model.NgayBatDauDangChuoi, CultureInfo.GetCultureInfo("vi-VN"));
                model.EndDate = DateTime.Parse(model.NgayKetThucDangChuoi, CultureInfo.GetCultureInfo("vi-VN"));
                var isSuccess = _pKHKeHoachGiaoHangService.UpdatePKHKeHoachGiaoHang(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHKeHoachGiaoHang");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHKeHoachGiaoHangId)
        {
            string message;
            var result = _pKHKeHoachGiaoHangService.Delete(pKHKeHoachGiaoHangId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int pKHKeHoachGiaoHangId)
        {
            string message;
            var result = _pKHKeHoachGiaoHangService.ChangeStatus(pKHKeHoachGiaoHangId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}