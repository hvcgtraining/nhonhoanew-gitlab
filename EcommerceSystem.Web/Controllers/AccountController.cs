﻿using EcommerceSystem.Core;
using EcommerceSystem.Models;
using EcommerceSystem.Models.User;
using EcommerceSystem.Services;
using System;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Security;
using Newtonsoft.Json;
using EcommerceSystem.Core.Configurations;
using System.Linq;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;

namespace EcommerceSystem.Web.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IRoleService _roleService;
        private readonly IUserService _userService;
        private readonly IContextService _contextService;
        public AccountController(IUserService userService, IContextService contextService, IRoleService roleService)
        {
            _roleService = roleService;
            _userService = userService;
            _contextService = contextService;
        }
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(UserModel model, string returnUrl)
        {
            var msgError = "";
            if (ModelState.IsValid)
            {
                var user = _userService.ValidateLogon(model.Email, model.Password, out msgError);
                if (user.Status == LoginResult.Success)
                {
                    var userData = JsonConvert.SerializeObject(user);
                    var authTicket = new FormsAuthenticationTicket(1,
                        user.Email,
                        DateTime.Now,
                        DateTime.Now.AddMinutes(60),
                        model.Remember,
                        userData);

                    var encTicket = FormsAuthentication.Encrypt(authTicket);
                    var cookieKey = FormsAuthentication.FormsCookieName;

                    _contextService.SaveInCookie(cookieKey, encTicket);
                    FormsAuthentication.RedirectFromLoginPage(model.Email, false);

                    if (encTicket != null)
                        HttpRuntime.Cache.Insert(cookieKey, userData, null, DateTime.Now.AddHours(30),
                            Cache.NoSlidingExpiration);

                    if (!string.IsNullOrEmpty(returnUrl) && returnUrl.Length > 1)
                        Response.Redirect(returnUrl);

                    return RedirectToAction("Index", "Home");
                }
            }

            if (!string.IsNullOrEmpty(msgError))
            {
                ViewBag.MessageError = msgError;
            }

            return View();
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        [CustomAuthorize(Roles = RoleHelper.UserListing)]
        public ActionResult Index()
        {
            int totalRecords;
            var searchModel = new UserSearchModel()
            {
                Users = _userService.SearchUser(1, SystemConfiguration.PageSizeDefault, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };
            return View(searchModel);
        }

        [CustomAuthorize(Roles = RoleHelper.UserListing)]
        public ActionResult Search(int currentPage, string textSearch)
        {
            int totalRecords;
            var model = new UserSearchModel
            {
                Users = _userService.SearchUser(currentPage, SystemConfiguration.PageSizeDefault, textSearch, out totalRecords),
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/Account/Partial/_TableUser.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Groups = _roleService.GetRoles();

            return PartialView("Create", new UserModel());
        }

        [HttpPost]
        [CustomAuthorize(Roles = RoleHelper.UserCreate)]
        public ActionResult Create(UserModel model)
        {
            string message;
            if(string.IsNullOrEmpty(model.Password))
            {
                ModelState.AddModelError("Password","Mật khẩu không được để trống.");
            }
            if (ModelState.IsValid)
            {
                var isSuccess = _userService.CreateUser(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "Account");
                }

                ViewBag.Groups = _roleService.GetRoles();
                TempData["error"] = message;
                return View(model);
            }
            
            ViewBag.Groups = _roleService.GetRoles();
            return View(model);
        }

        [HttpGet]
        [CustomAuthorize(Roles = RoleHelper.UserEdit)]
        public ActionResult Edit(int id)
        {
            ViewBag.Groups = _roleService.GetRoles();
            var user = _userService.RetrieveUser(id);
            return PartialView("Edit", user);
        }

        [HttpPost]
        [CustomAuthorize(Roles = RoleHelper.UserEdit)]
        public ActionResult Edit(UserModel model)
        {
            string message;
           
            if (ModelState.IsValid)
            {
                var isSuccess = _userService.UpdateUser(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "Account");
                }

                ViewBag.Groups = _roleService.GetRoles();
                TempData["error"] = message;
                return View(model);
            }

            ViewBag.Groups = _roleService.GetRoles();
            return View(model);
        }

        [HttpPost]
        [CustomAuthorize(Roles = RoleHelper.UserDelete)]
        public ActionResult Delete(int userId)
        {
            string message;
            if (CurrentUser.UserId == userId)
            {
                return Json(new { IsError = true, Message = "Bạn không thể xóa tài khoản của bạn" });
            }

            var result = _userService.DeleteUser(userId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

        [HttpPost]
        [CustomAuthorize(Roles =RoleHelper.UserInvisibe)]
        public ActionResult Invisibe(int userId)
        {
            string message;
            var result = _userService.ChangeStatus(userId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult ChangePassword(Models.ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                string message;
                var isSuccess = _userService.ChangePassword(CurrentUser.UserId, model.OldPassword, model.NewPassword, out message);
                if (isSuccess)
                {
                    ModelState.Clear();
                    return Json(new { IsError = false, Message = message });
                }

                return Json(new { IsError = true, Message = message });
            }

            var html = RenderPartialViewToString("~/Views/Account/Partial/_ChangePassword.cshtml", model);
            return Json(new { IsError = true, HTML = html, Message = "" });
        }

        #region Helpers

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
        #endregion
    }
}