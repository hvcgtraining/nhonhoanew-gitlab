﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.ThanhPhamXuongModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class ThanhPhamXuongController : BaseController
    {
        private readonly IThanhPhamXuongService _thanhPhamXuongService = (IThanhPhamXuongService)DependencyResolver.Current.GetService(typeof(IThanhPhamXuongService));
        private readonly INhomSanPhamService _nhomSanPhamService = (INhomSanPhamService)DependencyResolver.Current.GetService(typeof(INhomSanPhamService));
        private readonly IChiTietVatTuService _chiTietVatTuService = (IChiTietVatTuService)DependencyResolver.Current.GetService(typeof(IChiTietVatTuService));
        private readonly IXuongService _xuongService = (IXuongService)DependencyResolver.Current.GetService(typeof(IXuongService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new ThanhPhamXuongSearchModel
            {
                ThanhPhamXuongs = _thanhPhamXuongService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new ThanhPhamXuongSearchModel
            {
                ThanhPhamXuongs = _thanhPhamXuongService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/ThanhPhamXuong/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            ViewBag.XuongGroups = _xuongService.GetAllXuongs();
            ViewBag.NhomSanPhamGroups = _nhomSanPhamService.GetAllNhomSanPhams();
            ViewBag.ChiTietVatTuGroups = _chiTietVatTuService.GetAllChiTietVatTus();
            var model = new ThanhPhamXuongModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ThanhPhamXuongModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_thanhPhamXuongService.CreateThanhPhamXuong(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "ThanhPhamXuong");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var thanhPhamXuongEntity = _thanhPhamXuongService.GetById(id);
            ViewBag.XuongGroups = _xuongService.GetAllXuongs();
            ViewBag.NhomSanPhamGroups = _nhomSanPhamService.GetAllNhomSanPhams();
            ViewBag.ChiTietVatTuGroups = _chiTietVatTuService.GetAllChiTietVatTus();

            if (thanhPhamXuongEntity != null)
            {
                var model = thanhPhamXuongEntity.MapToModel();
                return View(model);
            }
            return View(new ThanhPhamXuongModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ThanhPhamXuongModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _thanhPhamXuongService.UpdateThanhPhamXuong(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "ThanhPhamXuong");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int thanhPhamXuongId)
        {
            string message;
            var result = _thanhPhamXuongService.Delete(thanhPhamXuongId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int thanhPhamXuongId)
        {
            string message;
            var result = _thanhPhamXuongService.ChangeStatus(thanhPhamXuongId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}