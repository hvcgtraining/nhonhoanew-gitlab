﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.KyTenModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class KyTenController : BaseController
    {
        private readonly IKyTenService _kyTenService = (IKyTenService)DependencyResolver.Current.GetService(typeof(IKyTenService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new KyTenSearchModel
            {
                KyTens = _kyTenService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new KyTenSearchModel
            {
                KyTens = _kyTenService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
                SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/KyTen/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }
      
        public dynamic LoadDanhMucKyTen()
        {
            List<SelectListItem> DanhMucKyTen = new List<SelectListItem>();
            DanhMucKyTen.Add(new SelectListItem() { Text = "Danh mục ký tên", Value = "1" });
            DanhMucKyTen.Add(new SelectListItem() { Text = "Danh mục ký tên kho", Value = "2" });
            return DanhMucKyTen;
        }
        public ActionResult Create()
        {
            
           
            var model = new KyTenModel();
            ViewBag.KyTenGroups = LoadDanhMucKyTen();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(KyTenModel model)
        {

            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_kyTenService.CreateKyTen(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "KyTen");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }



            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.KyTenGroups = LoadDanhMucKyTen();
            var kyTenEntity = _kyTenService.GetById(id);
            if (kyTenEntity != null)
            {
                var model = kyTenEntity.MapToModel();
                return View(model);
            }
            return View(new KyTenModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(KyTenModel model)
        {
            ViewBag.KyTenGroups = LoadDanhMucKyTen();
            string message;
            if (ModelState.IsValid)
            {
                model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _kyTenService.UpdateKyTen(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "KyTen");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int kyTenId)
        {
            string message;
            var result = _kyTenService.Delete(kyTenId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

        [HttpPost]
        public ActionResult Invisibe(int kyTenId)
        {
            string message;
            var result = _kyTenService.ChangeStatus(kyTenId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}