﻿using EcommerceSystem.Models;
using EcommerceSystem.Services;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class ConfigurationController : BaseController
    {
        #region Fields       
        private readonly IConfigurationService _configurationService = (IConfigurationService)DependencyResolver.Current.GetService(typeof(IConfigurationService));
        #endregion

        [CustomAuthorize(Roles = RoleHelper.ConfigurationSetting)]
        [HttpGet]
        public ActionResult Index()
        {
            var configModel = _configurationService.GetConfiguration();
            return View(configModel);
        }

        [CustomAuthorize(Roles =RoleHelper.ConfigurationSetting)]
        [HttpPost]
        public ActionResult Index(ConfigurationModel model)
        {
            var isSucces = _configurationService.UpdateConfiguration(model);
            if (isSucces)
            {
                TempData["success"] = "Cập nhật cấu hình hệ thống thành công.";
            }
            else
            {
                TempData["error"] = "Có lỗi xảy ra.";
            }
            return View(model);

        }
    }
}