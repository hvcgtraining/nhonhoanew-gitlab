﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.DinhMucSanXuatModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class DinhMucSanXuatController : BaseController
    {
        private readonly IDinhMucSanXuatService _dinhMucSanXuatService = (IDinhMucSanXuatService)DependencyResolver.Current.GetService(typeof(IDinhMucSanXuatService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new DinhMucSanXuatSearchModel
            {
                DinhMucSanXuats = _dinhMucSanXuatService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new DinhMucSanXuatSearchModel
            {
                DinhMucSanXuats = _dinhMucSanXuatService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/DinhMucSanXuat/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new DinhMucSanXuatModel();
            model.ListKho = _dinhMucSanXuatService.GetListKho();
            model.ListChiTietVatTu = _dinhMucSanXuatService.GetListChiTietVatTu();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DinhMucSanXuatModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_dinhMucSanXuatService.CreateDinhMucSanXuat(model))
                {
                    TempData["success"] = "Tạo mới thành công. Vui lòng nhập định mức chi tiết!";
                    return RedirectToAction("Index", "DanhSachChiTiet", new { dinhMucSanXuatId = model.DinhMucSanXuatId });
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var dinhMucSanXuatEntity = _dinhMucSanXuatService.GetById(id);
            if(dinhMucSanXuatEntity != null)
            {
                var model = dinhMucSanXuatEntity.MapToModel();
                model.ListKho = _dinhMucSanXuatService.GetListKho();
                model.ListChiTietVatTu = _dinhMucSanXuatService.GetListChiTietVatTu();

                return View(model);
            }
            return View(new DinhMucSanXuatModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DinhMucSanXuatModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _dinhMucSanXuatService.UpdateDinhMucSanXuat(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "DinhMucSanXuat");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int dinhMucSanXuatId)
        {
            string message;
            var result = _dinhMucSanXuatService.Delete(dinhMucSanXuatId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int dinhMucSanXuatId)
        {
            string message;
            var result = _dinhMucSanXuatService.ChangeStatus(dinhMucSanXuatId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}