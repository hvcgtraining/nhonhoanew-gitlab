﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.DieuChuyenQuyetToanModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;
using EcommerceSystem.Models;
namespace EcommerceSystem.Web.Controllers
{
    public class DieuChuyenQuyetToanController : BaseController
    {
        private readonly IDieuChuyenQuyetToanService _dieuChuyenQuyetToanService = (IDieuChuyenQuyetToanService)DependencyResolver.Current.GetService(typeof(IDieuChuyenQuyetToanService));
        private readonly IKhoService _khoService = (IKhoService)DependencyResolver.Current.GetService(typeof(IKhoService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new DieuChuyenQuyetToanSearchModel
            {
                DieuChuyenQuyetToans = _dieuChuyenQuyetToanService.Search(1, SystemConfiguration.PageSizeDefault, null, 0, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
                LoaiDieuChuyen = DieuChuyenType.DieuChuyenQuyetToan,
            };

            return View(model);
        }

        public ActionResult DieuChuyenCapHang()
        {
            int totalRecords;
            var model = new DieuChuyenQuyetToanSearchModel
            {
                DieuChuyenQuyetToans = _dieuChuyenQuyetToanService.Search(1, SystemConfiguration.PageSizeDefault, null, (int)DieuChuyenType.DieuChuyenCapHang, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
                LoaiDieuChuyen = DieuChuyenType.DieuChuyenCapHang,
            };

            return View(model);
        }

        public ActionResult DieuChuyenGiaCong()
        {
            int totalRecords;
            var model = new DieuChuyenQuyetToanSearchModel
            {
                DieuChuyenQuyetToans = _dieuChuyenQuyetToanService.Search(1, SystemConfiguration.PageSizeDefault, null, (int)DieuChuyenType.DieuChuyenGiaCong, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
                LoaiDieuChuyen = DieuChuyenType.DieuChuyenGiaCong,
            };

            return View(model);
        }

        public ActionResult QuyetToanGiaCong()
        {
            int totalRecords;
            var model = new DieuChuyenQuyetToanSearchModel
            {
                DieuChuyenQuyetToans = _dieuChuyenQuyetToanService.Search(1, SystemConfiguration.PageSizeDefault, null, (int)DieuChuyenType.QuyetToanGiaCong, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
                LoaiDieuChuyen = DieuChuyenType.QuyetToanGiaCong,
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, int loaiDieuChuyen, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new DieuChuyenQuyetToanSearchModel
            {
                DieuChuyenQuyetToans = _dieuChuyenQuyetToanService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, loaiDieuChuyen, sortColumn, sortDirection, out totalRecords),
                SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
                LoaiDieuChuyen = (DieuChuyenType)loaiDieuChuyen,
            };

            var html = RenderPartialViewToString("~/Views/DieuChuyenQuyetToan/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create(int dieuChuyenType)
        {
            ViewBag.KhoGroups = _khoService.GetAllKhos();
            var model = new DieuChuyenQuyetToanModel
            {
                LoaiDieuChuyen = (DieuChuyenType)dieuChuyenType,
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DieuChuyenQuyetToanModel model)
        {
            if (ModelState.IsValid)
            {
                //model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;
                model.LoaiDieuChuyenQuyetToan = (int)model.LoaiDieuChuyen;
                if (_dieuChuyenQuyetToanService.CreateDieuChuyenQuyetToan(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    var urlRedirect = (model.LoaiDieuChuyenQuyetToan == (int)DieuChuyenType.DieuChuyenCapHang) ? "DieuChuyenQuyetToan/DieuChuyenCapHang" : ((model.LoaiDieuChuyenQuyetToan == (int)DieuChuyenType.DieuChuyenGiaCong) ? "DieuChuyenQuyetToan/DieuChuyenGiaCong" : "DieuChuyenQuyetToan/QuyetToanGiaCong");
                    return RedirectToAction("Index", urlRedirect);
                }

                ViewBag.KhoGroups = _khoService.GetAllKhos();
                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            ViewBag.KhoGroups = _khoService.GetAllKhos();
            return View(model);
        }

        public ActionResult CreateDieuChuyenGCQuyetToanGC(DieuChuyenGiaCongQuyetToanGiaCongModel model)
        {
            if (ModelState.IsValid)
            {
                //model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;
                model.LoaiDieuChuyenQuyetToan = (int)model.LoaiDieuChuyen;
                if (_dieuChuyenQuyetToanService.CreateDieuChuyenQuyetToan(model.MapToDieuChuyenQuyetToanModel()))
                {
                    TempData["success"] = "Tạo mới thành công";
                    var urlRedirect = (model.LoaiDieuChuyenQuyetToan == (int)DieuChuyenType.DieuChuyenCapHang) ? "DieuChuyenQuyetToan/DieuChuyenCapHang" : ((model.LoaiDieuChuyenQuyetToan == (int)DieuChuyenType.DieuChuyenGiaCong) ? "DieuChuyenQuyetToan/DieuChuyenGiaCong" : "DieuChuyenQuyetToan/QuyetToanGiaCong");
                    return RedirectToAction("Index", urlRedirect);
                }

                ViewBag.KhoGroups = _khoService.GetAllKhos();
                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            ViewBag.KhoGroups = _khoService.GetAllKhos();
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.KhoGroups = _khoService.GetAllKhos();
            var dieuChuyenQuyetToanEntity = _dieuChuyenQuyetToanService.GetById(id);
            if (dieuChuyenQuyetToanEntity != null)
            {
                var model = dieuChuyenQuyetToanEntity.MapToModel();
                model.LoaiDieuChuyen = (DieuChuyenType)model.LoaiDieuChuyenQuyetToan;
                return View(model);
            }
            return View(new DieuChuyenQuyetToanModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DieuChuyenQuyetToanModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
                //model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                model.LoaiDieuChuyenQuyetToan = (int)model.LoaiDieuChuyen;
                var isSuccess = _dieuChuyenQuyetToanService.UpdateDieuChuyenQuyetToan(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    var urlRedirect = (model.LoaiDieuChuyen == DieuChuyenType.DieuChuyenCapHang) ? "DieuChuyenQuyetToan/DieuChuyenCapHang" : ((model.LoaiDieuChuyen == DieuChuyenType.DieuChuyenGiaCong) ? "DieuChuyenQuyetToan/DieuChuyenGiaCong" : "DieuChuyenQuyetToan/QuyetToanGiaCong");
                    return RedirectToAction("Index", urlRedirect);
                }

                ViewBag.KhoGroups = _khoService.GetAllKhos();
                TempData["error"] = message;
                return View(model);
            }

            ViewBag.KhoGroups = _khoService.GetAllKhos();
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int dieuChuyenQuyetToanId)
        {
            string message;
            var result = _dieuChuyenQuyetToanService.Delete(dieuChuyenQuyetToanId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

        [HttpPost]
        public ActionResult Invisibe(int dieuChuyenQuyetToanId)
        {
            string message;
            var result = _dieuChuyenQuyetToanService.ChangeStatus(dieuChuyenQuyetToanId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}