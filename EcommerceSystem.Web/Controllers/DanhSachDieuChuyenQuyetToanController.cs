﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.DanhSachDieuChuyenQuyetToanModel;
using EcommerceSystem.Models.DieuChuyenQuyetToanModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;
using EcommerceSystem.Models;

namespace EcommerceSystem.Web.Controllers
{
    public class DanhSachDieuChuyenQuyetToanController : BaseController
    {
        private readonly IDanhSachDieuChuyenQuyetToanService _danhSachDieuChuyenQuyetToanService = (IDanhSachDieuChuyenQuyetToanService)DependencyResolver.Current.GetService(typeof(IDanhSachDieuChuyenQuyetToanService));
        private readonly IDieuChuyenQuyetToanService _dieuChuyenQuyetToanService = (IDieuChuyenQuyetToanService)DependencyResolver.Current.GetService(typeof(IDieuChuyenQuyetToanService));
        private readonly IChiTietVatTuService _chiTietVatTuService = (IChiTietVatTuService)DependencyResolver.Current.GetService(typeof(IChiTietVatTuService));


        public ActionResult Index()
        {
            int totalRecords;
            var model = new DanhSachDieuChuyenQuyetToanSearchModel
            {
                DanhSachDieuChuyenQuyetToans = _danhSachDieuChuyenQuyetToanService.Search(1, SystemConfiguration.PageSizeDefault, null, 0, 0, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
                LoaiDieuChuyenQuyetToan = DieuChuyenType.DieuChuyenQuyetToan,
            };

            return View(model);
        }

        public ActionResult DanhSachDieuChuyenCapHang()
        {
            var quyetToanId = GetDieuChuyenQuyetToanIdByUrl("qtid");
            var model = GetDanhSachDieuChuyenQuyetToanDetail((int)DieuChuyenType.DieuChuyenCapHang, quyetToanId);
            if (quyetToanId != 0)
            {
                model.TenDieuChuyenQuyetToan = _dieuChuyenQuyetToanService.GetById(quyetToanId).Title.ToString();
            }
            return View(model);
        }

        public ActionResult DanhSachDieuChuyenGiaCong()
        {
            var quyetToanId = GetDieuChuyenQuyetToanIdByUrl("qtid");
            var model = GetDanhSachDieuChuyenQuyetToanDetail((int)DieuChuyenType.DieuChuyenGiaCong, quyetToanId);
            if (quyetToanId != 0)
            {
                model.TenDieuChuyenQuyetToan = _dieuChuyenQuyetToanService.GetById(quyetToanId).Title.ToString();
            }
            return View(model);
        }
        public ActionResult DanhSachQuyetToanGiaCong()
        {
            var quyetToanId = GetDieuChuyenQuyetToanIdByUrl("qtid");
            var model = GetDanhSachDieuChuyenQuyetToanDetail((int)DieuChuyenType.QuyetToanGiaCong, quyetToanId);
            if (quyetToanId != 0)
            {
                model.TenDieuChuyenQuyetToan = _dieuChuyenQuyetToanService.GetById(quyetToanId).Title.ToString();
            }
            return View(model);
        }
        public ActionResult Search(int currentPage, string textSearch, int loaiDieuChuyen, int quyettoanId, string sortColumn, string sortDirection)
        {

            int totalRecords;
            var model = new DanhSachDieuChuyenQuyetToanSearchModel
            {
                DanhSachDieuChuyenQuyetToans = _danhSachDieuChuyenQuyetToanService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, loaiDieuChuyen, quyettoanId, sortColumn, sortDirection, out totalRecords),
                SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
                LoaiDieuChuyenQuyetToan = (DieuChuyenType)loaiDieuChuyen
            };

            var html = RenderPartialViewToString("~/Views/DanhSachDieuChuyenQuyetToan/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create(int loaiDieuChuyen, int id)
        {
            ViewBag.DieuChuyenQuyetToanGroups = _dieuChuyenQuyetToanService.GetDieuChuyenQuyetToansByLoaiDieuChuyen(loaiDieuChuyen);
            ViewBag.ChiTietVatTuGroups = _chiTietVatTuService.GetAllChiTietVatTus();
            if(loaiDieuChuyen == (int)DieuChuyenType.QuyetToanGiaCong)
            {
                var qTGCmodel = new DanhSachDieuChuyenQuyetToanModel
                {
                    LoaiDieuChuyenQuyetToan = (DieuChuyenType)loaiDieuChuyen,
                    DieuChuyenQuyetToanId = id
                };
                return View(qTGCmodel);
            }
            var model = new DanhSachDieuChuyenQuyetToanModel {
                LoaiDieuChuyenQuyetToan = (DieuChuyenType)loaiDieuChuyen,
                DieuChuyenQuyetToanId = id
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DanhSachDieuChuyenQuyetToanModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_danhSachDieuChuyenQuyetToanService.CreateDanhSachDieuChuyenQuyetToan(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    model.LoaiDieuChuyenQuyetToan = (DieuChuyenType)GetLoaiDieuChuyen((int)model.DieuChuyenQuyetToanId);
                    var urlRedirect = (model.LoaiDieuChuyenQuyetToan == DieuChuyenType.DieuChuyenCapHang) ? "DanhSachDieuChuyenCapHang" : ((model.LoaiDieuChuyenQuyetToan == DieuChuyenType.DieuChuyenGiaCong) ? "DanhSachDieuChuyenGiaCong" : (model.LoaiDieuChuyenQuyetToan == DieuChuyenType.QuyetToanGiaCong) ? "DanhSachQuyetToanGiaCong" : "DanhSachDieuChuyenQuyetToan");
                    return RedirectToAction(urlRedirect, "DanhSachDieuChuyenQuyetToan", new { @qtid = model.DieuChuyenQuyetToanId });
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateQuyetToanGC(DanhSachQuyetToanGiaCongModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_danhSachDieuChuyenQuyetToanService.CreateDanhSachDieuChuyenQuyetToan(model.MapToDieuChuyenQuyetToan()))
                {
                    TempData["success"] = "Tạo mới thành công";
                    model.LoaiDieuChuyenQuyetToan = (DieuChuyenType)GetLoaiDieuChuyen((int)model.DieuChuyenQuyetToanId);
                    var urlRedirect = (model.LoaiDieuChuyenQuyetToan == DieuChuyenType.DieuChuyenCapHang) ? "DanhSachDieuChuyenCapHang" : ((model.LoaiDieuChuyenQuyetToan == DieuChuyenType.DieuChuyenGiaCong) ? "DanhSachDieuChuyenGiaCong" : (model.LoaiDieuChuyenQuyetToan == DieuChuyenType.QuyetToanGiaCong) ? "DanhSachQuyetToanGiaCong" : "DanhSachDieuChuyenQuyetToan");
                    return RedirectToAction(urlRedirect, "DanhSachDieuChuyenQuyetToan", new { @qtid = model.DieuChuyenQuyetToanId });
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var dieuChuyenQuyetToan = _danhSachDieuChuyenQuyetToanService.GetById(id);
            var loaiDieuChuyen = _dieuChuyenQuyetToanService.GetById(dieuChuyenQuyetToan.DieuChuyenQuyetToanId).LoaiDieuChuyenQuyetToan;
            ViewBag.DieuChuyenQuyetToanGroups = _dieuChuyenQuyetToanService.GetDieuChuyenQuyetToansByLoaiDieuChuyen(loaiDieuChuyen);
            ViewBag.ChiTietVatTuGroups = _chiTietVatTuService.GetAllChiTietVatTus();
            var danhSachDieuChuyenQuyetToanEntity = _danhSachDieuChuyenQuyetToanService.GetById(id);
            if (danhSachDieuChuyenQuyetToanEntity != null)
            {
                var model = danhSachDieuChuyenQuyetToanEntity.MapToModel();
                model.LoaiDieuChuyenQuyetToan = (DieuChuyenType)loaiDieuChuyen;
                return View(model);
            }
            return View(new DanhSachDieuChuyenQuyetToanModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DanhSachDieuChuyenQuyetToanModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
                model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;

                var isSuccess = _danhSachDieuChuyenQuyetToanService.UpdateDanhSachDieuChuyenQuyetToan(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    model.LoaiDieuChuyenQuyetToan = (DieuChuyenType)GetLoaiDieuChuyen((int)model.DieuChuyenQuyetToanId);
                    var urlRedirect = (model.LoaiDieuChuyenQuyetToan == DieuChuyenType.DieuChuyenCapHang) ? "DanhSachDieuChuyenCapHang" : ((model.LoaiDieuChuyenQuyetToan == DieuChuyenType.DieuChuyenGiaCong) ? "DanhSachDieuChuyenGiaCong" : (model.LoaiDieuChuyenQuyetToan == DieuChuyenType.QuyetToanGiaCong) ? "DanhSachQuyetToanGiaCong" : "DanhSachDieuChuyenQuyetToan");
                    return RedirectToAction(urlRedirect, "DanhSachDieuChuyenQuyetToan", new { @qtid = model.DieuChuyenQuyetToanId });
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int danhSachDieuChuyenQuyetToanId)
        {
            string message;
            var result = _danhSachDieuChuyenQuyetToanService.Delete(danhSachDieuChuyenQuyetToanId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

        [HttpPost]
        public ActionResult Invisibe(int danhSachDieuChuyenQuyetToanId)
        {
            string message;
            var result = _danhSachDieuChuyenQuyetToanService.ChangeStatus(danhSachDieuChuyenQuyetToanId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }

        #region Private GetDanhSachDieuChuyenQuyetToanDetail
        private DanhSachDieuChuyenQuyetToanSearchModel GetDanhSachDieuChuyenQuyetToanDetail(int loaiDieuChuyen, int quyetToanId)
        {
            int totalRecords;

            var model = new DanhSachDieuChuyenQuyetToanSearchModel
            {
                DanhSachDieuChuyenQuyetToans = _danhSachDieuChuyenQuyetToanService.Search(1, SystemConfiguration.PageSizeDefault, null, loaiDieuChuyen, quyetToanId, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
                LoaiDieuChuyenQuyetToan = (DieuChuyenType)loaiDieuChuyen,
                DieuChuyenQuyetToanId = quyetToanId,
            };

            return model;
        }
        #endregion

        #region GetLoaiDieuChuyen
        private int GetLoaiDieuChuyen(int id)
        {
            var dieuChuyenQuyetToanById = _dieuChuyenQuyetToanService.GetById(id);
            if (dieuChuyenQuyetToanById.LoaiDieuChuyenQuyetToan != 0)
            {
                return (int)dieuChuyenQuyetToanById.LoaiDieuChuyenQuyetToan;
            }
            else
            {
                return 0;
            }
        }
        #endregion

        #region
        private int GetDieuChuyenQuyetToanIdByUrl(string urlStrim)
        {
            var quyettoanId = 0;
            int.TryParse(Request.QueryString[urlStrim], out quyettoanId);
            return quyettoanId;
        }
        #endregion
    }
}