﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.PKHKeHoachXuatKhauDetailModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHKeHoachXuatKhauDetailController : BaseController
    {
        private readonly IPKHKeHoachXuatKhauDetailService _pKHKeHoachXuatKhauDetailService = (IPKHKeHoachXuatKhauDetailService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachXuatKhauDetailService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new PKHKeHoachXuatKhauDetailSearchModel
            {
                PKHKeHoachXuatKhauDetails = _pKHKeHoachXuatKhauDetailService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHKeHoachXuatKhauDetailSearchModel
            {
                PKHKeHoachXuatKhauDetails = _pKHKeHoachXuatKhauDetailService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHKeHoachXuatKhauDetail/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHKeHoachXuatKhauDetailModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHKeHoachXuatKhauDetailModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_pKHKeHoachXuatKhauDetailService.CreatePKHKeHoachXuatKhauDetail(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHKeHoachXuatKhauDetail");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var pKHKeHoachXuatKhauDetailEntity = _pKHKeHoachXuatKhauDetailService.GetById(id);
            if(pKHKeHoachXuatKhauDetailEntity != null)
            {
                var model = pKHKeHoachXuatKhauDetailEntity.MapToModel();
                return View(model);
            }
            return View(new PKHKeHoachXuatKhauDetailModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHKeHoachXuatKhauDetailModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _pKHKeHoachXuatKhauDetailService.UpdatePKHKeHoachXuatKhauDetail(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHKeHoachXuatKhauDetail");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHKeHoachXuatKhauDetailId)
        {
            string message;
            var result = _pKHKeHoachXuatKhauDetailService.Delete(pKHKeHoachXuatKhauDetailId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int pKHKeHoachXuatKhauDetailId)
        {
            string message;
            var result = _pKHKeHoachXuatKhauDetailService.ChangeStatus(pKHKeHoachXuatKhauDetailId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}