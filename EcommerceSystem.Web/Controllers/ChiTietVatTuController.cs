﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.ChiTietVatTuModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Linq;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class ChiTietVatTuController : BaseController
    {
        private readonly IChiTietVatTuService _chiTietVatTuService = (IChiTietVatTuService)DependencyResolver.Current.GetService(typeof(IChiTietVatTuService));
        private readonly ILoaiVatTuService _loaiVatTuServicee = (ILoaiVatTuService)DependencyResolver.Current.GetService(typeof(ILoaiVatTuService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new ChiTietVatTuSearchModel
            {
                ChiTietVatTus = _chiTietVatTuService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new ChiTietVatTuSearchModel
            {
                ChiTietVatTus = _chiTietVatTuService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/ChiTietVatTu/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new ChiTietVatTuModel();
            ViewBag.LoaiVatTus = _loaiVatTuServicee.GetAllLoaiVatTus();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ChiTietVatTuModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_chiTietVatTuService.CreateChiTietVatTu(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "ChiTietVatTu");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var chiTietVatTuEntity = _chiTietVatTuService.GetById(id);
            ViewBag.LoaiVatTus = _loaiVatTuServicee.GetAllLoaiVatTus();
            if (chiTietVatTuEntity != null)
            {
                var model = chiTietVatTuEntity.MapToModel();
                return View(model);
            }
            return View(new ChiTietVatTuModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ChiTietVatTuModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _chiTietVatTuService.UpdateChiTietVatTu(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "ChiTietVatTu");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int chiTietVatTuId)
        {
            string message;
            var result = _chiTietVatTuService.Delete(chiTietVatTuId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int chiTietVatTuId)
        {
            string message;
            var result = _chiTietVatTuService.ChangeStatus(chiTietVatTuId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}