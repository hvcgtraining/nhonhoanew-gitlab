﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.DanhSachChiTietModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class DanhSachChiTietController : BaseController
    {
        private readonly IDanhSachChiTietService _danhSachChiTietService = (IDanhSachChiTietService)DependencyResolver.Current.GetService(typeof(IDanhSachChiTietService));
        private readonly IChiTietVatTuService _loaiVatTuService = (IChiTietVatTuService)DependencyResolver.Current.GetService(typeof(IChiTietVatTuService));

        public ActionResult Index(int dinhMucSanXuatId)
        {
            int totalRecords = 0;
            var dinhMucSanXuat = _danhSachChiTietService.GetById(dinhMucSanXuatId);
            var model = new DanhSachChiTietSearchModel
            {
                //DanhSachChiTiets = _danhSachChiTietService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                DanhSachChiTiets = _danhSachChiTietService.GetAllByParentId(dinhMucSanXuatId),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult DinhMucCatTole(int dinhMucCatToleId)
        {
            int totalRecords = 0;
            var dinhMucCatTole = _danhSachChiTietService.GetById(dinhMucCatToleId);
            var model = new DanhSachChiTietSearchModel
            {
                //DanhSachChiTiets = _danhSachChiTietService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                DanhSachChiTiets = _danhSachChiTietService.GetAllByDinhMucCatToleId(dinhMucCatToleId),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int dinhMucSanXuatId, int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new DanhSachChiTietSearchModel
            {
                DanhSachChiTiets = _danhSachChiTietService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/DanhSachChiTiet/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new DanhSachChiTietModel();
            model.ListChiTietVatTu = _danhSachChiTietService.GetListChiTietVatTu();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DanhSachChiTietModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_danhSachChiTietService.CreateDanhSachChiTiet(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "DanhSachChiTiet", new { dinhMucSanXuatId = model.DinhMucSanXuatId });
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult CreateDinhMucCatTole()
        {
            var model = new DanhSachChiTietModel();
            model.ListChiTietVatTu = _danhSachChiTietService.GetListChiTietVatTu();
            ViewBag.LoaiChiTiet = _loaiVatTuService.GetAllChiTietVatTus();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateDinhMucCatTole(DanhSachChiTietModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;
                if (_danhSachChiTietService.CreateDanhSachChiTiet(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    if (model.HasChild)
                    {
                        //return RedirectToAction("DinhMucCatTole", "DanhSachChiTiet", new { dinhMucCatToleId = model.DinhMucCatToleId });
                        return RedirectToAction("CreateDinhMucCatToleDetail", "DanhSachChiTiet", new { dinhMucCatToleId = model.DinhMucCatToleId , parentId = model.DanhSachChiTietId });
                    }
                    else
                    {
                        return RedirectToAction("DinhMucCatTole", "DanhSachChiTiet", new { dinhMucCatToleId = model.DinhMucCatToleId });
                    }
                    
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult CreateDinhMucCatToleDetail()
        {
            var model = new DanhSachChiTietModel();
            model.ListChiTietVatTu = _danhSachChiTietService.GetListChiTietVatTu();
            ViewBag.LoaiChiTiet = _loaiVatTuService.GetAllChiTietVatTus();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateDinhMucCatToleDetail(DanhSachChiTietModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;
                if (_danhSachChiTietService.CreateDanhSachChiTiet(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("DinhMucCatTole", "DanhSachChiTiet", new { dinhMucCatToleId = model.DinhMucCatToleId });
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var danhSachChiTietEntity = _danhSachChiTietService.GetById(id);
            if(danhSachChiTietEntity != null)
            {
                var model = danhSachChiTietEntity.MapToModel();
                model.ListChiTietVatTu = _danhSachChiTietService.GetListChiTietVatTu();

                return View(model);
            }
            return View(new DanhSachChiTietModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DanhSachChiTietModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _danhSachChiTietService.UpdateDanhSachChiTiet(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "DanhSachChiTiet", new { dinhMucSanXuatId = model.DinhMucSanXuatId });
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        public ActionResult EditDinhMucCatTole(int id)
        {
            var danhSachChiTietEntity = _danhSachChiTietService.GetById(id);
            if (danhSachChiTietEntity != null)
            {
                var model = danhSachChiTietEntity.MapToModel();
                model.ListChiTietVatTu = _danhSachChiTietService.GetListChiTietVatTu();
                ViewBag.LoaiChiTiet = _loaiVatTuService.GetAllChiTietVatTus();

                return View(model);
            }
            return View(new DanhSachChiTietModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDinhMucCatTole(DanhSachChiTietModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
                model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _danhSachChiTietService.UpdateDanhSachChiTiet(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("DinhMucCatTole", "DanhSachChiTiet", new { dinhMucCatToleId = model.DinhMucCatToleId });
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int danhSachChiTietId)
        {
            string message;
            var result = _danhSachChiTietService.Delete(danhSachChiTietId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int danhSachChiTietId)
        {
            string message;
            var result = _danhSachChiTietService.ChangeStatus(danhSachChiTietId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}