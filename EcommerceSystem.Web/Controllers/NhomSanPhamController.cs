﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.NhomSanPhamModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class NhomSanPhamController : BaseController
    {
        private readonly INhomSanPhamService _nhomSanPhamService = (INhomSanPhamService)DependencyResolver.Current.GetService(typeof(INhomSanPhamService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new NhomSanPhamSearchModel
            {
                NhomSanPhams = _nhomSanPhamService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new NhomSanPhamSearchModel
            {
                NhomSanPhams = _nhomSanPhamService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/NhomSanPham/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new NhomSanPhamModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NhomSanPhamModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_nhomSanPhamService.CreateNhomSanPham(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "NhomSanPham");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var nhomSanPhamEntity = _nhomSanPhamService.GetById(id);
            if(nhomSanPhamEntity != null)
            {
                var model = nhomSanPhamEntity.MapToModel();
                return View(model);
            }
            return View(new NhomSanPhamModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NhomSanPhamModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _nhomSanPhamService.UpdateNhomSanPham(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "NhomSanPham");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int nhomSanPhamId)
        {
            string message;
            var result = _nhomSanPhamService.Delete(nhomSanPhamId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int nhomSanPhamId)
        {
            string message;
            var result = _nhomSanPhamService.ChangeStatus(nhomSanPhamId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}