﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.PKHTheoDoiGiaoHangModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHTheoDoiGiaoHangController : BaseController
    {
        private readonly IPKHTheoDoiGiaoHangService _pKHTheoDoiGiaoHangService = (IPKHTheoDoiGiaoHangService)DependencyResolver.Current.GetService(typeof(IPKHTheoDoiGiaoHangService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new PKHTheoDoiGiaoHangSearchModel
            {
                PKHTheoDoiGiaoHangs = _pKHTheoDoiGiaoHangService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHTheoDoiGiaoHangSearchModel
            {
                PKHTheoDoiGiaoHangs = _pKHTheoDoiGiaoHangService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHTheoDoiGiaoHang/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHTheoDoiGiaoHangModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHTheoDoiGiaoHangModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_pKHTheoDoiGiaoHangService.CreatePKHTheoDoiGiaoHang(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHTheoDoiGiaoHang");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var pKHTheoDoiGiaoHangEntity = _pKHTheoDoiGiaoHangService.GetById(id);
            if(pKHTheoDoiGiaoHangEntity != null)
            {
                var model = pKHTheoDoiGiaoHangEntity.MapToModel();
                return View(model);
            }
            return View(new PKHTheoDoiGiaoHangModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHTheoDoiGiaoHangModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _pKHTheoDoiGiaoHangService.UpdatePKHTheoDoiGiaoHang(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHTheoDoiGiaoHang");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHTheoDoiGiaoHangId)
        {
            string message;
            var result = _pKHTheoDoiGiaoHangService.Delete(pKHTheoDoiGiaoHangId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int pKHTheoDoiGiaoHangId)
        {
            string message;
            var result = _pKHTheoDoiGiaoHangService.ChangeStatus(pKHTheoDoiGiaoHangId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}