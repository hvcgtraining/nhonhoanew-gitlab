﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.VatTuNhapRiengModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class VatTuNhapRiengController : BaseController
    {
        private readonly IVatTuNhapRiengService _vatTuNhapRiengService = (IVatTuNhapRiengService)DependencyResolver.Current.GetService(typeof(IVatTuNhapRiengService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new VatTuNhapRiengSearchModel
            {
                VatTuNhapRiengs = _vatTuNhapRiengService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new VatTuNhapRiengSearchModel
            {
                VatTuNhapRiengs = _vatTuNhapRiengService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/VatTuNhapRieng/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new VatTuNhapRiengModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VatTuNhapRiengModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_vatTuNhapRiengService.CreateVatTuNhapRieng(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "VatTuNhapRieng");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var vatTuNhapRiengEntity = _vatTuNhapRiengService.GetById(id);
            if(vatTuNhapRiengEntity != null)
            {
                var model = vatTuNhapRiengEntity.MapToModel();
                return View(model);
            }
            return View(new VatTuNhapRiengModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VatTuNhapRiengModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _vatTuNhapRiengService.UpdateVatTuNhapRieng(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "VatTuNhapRieng");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int vatTuNhapRiengId)
        {
            string message;
            var result = _vatTuNhapRiengService.Delete(vatTuNhapRiengId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int vatTuNhapRiengId)
        {
            string message;
            var result = _vatTuNhapRiengService.ChangeStatus(vatTuNhapRiengId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}