﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.ToleModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class ToleController : BaseController
    {
        private readonly IToleService _toleService = (IToleService)DependencyResolver.Current.GetService(typeof(IToleService));
        private readonly IDechetService _dechetService = (IDechetService)DependencyResolver.Current.GetService(typeof(IDechetService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new ToleSearchModel
            {
                Toles = _toleService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new ToleSearchModel
            {
                Toles = _toleService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/Tole/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            ViewBag.DechetGroups = _dechetService.GetAllDechets();
            var model = new ToleModel();
            model.ListDechet = _toleService.GetListDechet();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ToleModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_toleService.CreateTole(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "Tole");
                }
                ViewBag.DechetGroups = _dechetService.GetAllDechets();

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }
            ViewBag.DechetGroups = _dechetService.GetAllDechets();

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.DechetGroups = _dechetService.GetAllDechets();
            var toleEntity = _toleService.GetById(id);
            if(toleEntity != null)
            {
                var model = toleEntity.MapToModel();
                model.ListDechet = _toleService.GetListDechet();
                return View(model);
            }
            return View(new ToleModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ToleModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _toleService.UpdateTole(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "Tole");
                }

                ViewBag.DechetGroups = _dechetService.GetAllDechets();
                TempData["error"] = message;
                return View(model);
            }

            ViewBag.DechetGroups = _dechetService.GetAllDechets();
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int toleId)
        {
            string message;
            var result = _toleService.Delete(toleId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int toleId)
        {
            string message;
            var result = _toleService.ChangeStatus(toleId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}