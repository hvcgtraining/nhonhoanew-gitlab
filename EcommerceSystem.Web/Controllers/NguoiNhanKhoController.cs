﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.NguoiNhanKhoModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class NguoiNhanKhoController : BaseController
    {
        private readonly INguoiNhanKhoService _nguoiNhanKhoService = (INguoiNhanKhoService)DependencyResolver.Current.GetService(typeof(INguoiNhanKhoService));
        private readonly IKhoService _khoService = (IKhoService)DependencyResolver.Current.GetService(typeof(IKhoService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new NguoiNhanKhoSearchModel
            {
                NguoiNhanKhos = _nguoiNhanKhoService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new NguoiNhanKhoSearchModel
            {
                NguoiNhanKhos = _nguoiNhanKhoService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/NguoiNhanKho/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new NguoiNhanKhoModel();
            ViewBag.KhoGroups = _khoService.GetAllKhos();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NguoiNhanKhoModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_nguoiNhanKhoService.CreateNguoiNhanKho(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "NguoiNhanKho");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.KhoGroups = _khoService.GetAllKhos();
            var nguoiNhanKhoEntity = _nguoiNhanKhoService.GetById(id);            
            if (nguoiNhanKhoEntity != null)
            {
                var model = nguoiNhanKhoEntity.MapToModel();
               
                return View(model);
            }
            return View(new NguoiNhanKhoModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NguoiNhanKhoModel model)
        {
            ViewBag.KhoGroups = _khoService.GetAllKhos();
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _nguoiNhanKhoService.UpdateNguoiNhanKho(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "NguoiNhanKho");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int nguoiNhanKhoId)
        {
            string message;
            var result = _nguoiNhanKhoService.Delete(nguoiNhanKhoId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int nguoiNhanKhoId)
        {
            string message;
            var result = _nguoiNhanKhoService.ChangeStatus(nguoiNhanKhoId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}