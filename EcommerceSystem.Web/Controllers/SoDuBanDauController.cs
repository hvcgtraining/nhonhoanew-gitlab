﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.SoDuBanDauModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class SoDuBanDauController : BaseController
    {
        private readonly ISoDuBanDauService _soDuBanDauService = (ISoDuBanDauService)DependencyResolver.Current.GetService(typeof(ISoDuBanDauService));
        private readonly IKhoService _khoService  = (IKhoService) DependencyResolver.Current.GetService(typeof(IKhoService));
        private readonly IChiTietVatTuService  _loaiVatTu = (IChiTietVatTuService)DependencyResolver.Current.GetService(typeof(IChiTietVatTuService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new SoDuBanDauSearchModel
            {
                SoDuBanDaus = _soDuBanDauService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new SoDuBanDauSearchModel
            {
                SoDuBanDaus = _soDuBanDauService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/SoDuBanDau/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            ViewBag.KhoGroups = _khoService.GetAllKhos();
            ViewBag.LoaiVatTuGroups = _loaiVatTu.GetAllChiTietVatTus();
            var model = new SoDuBanDauModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SoDuBanDauModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_soDuBanDauService.CreateSoDuBanDau(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "SoDuBanDau");
                }
                //ViewBag.KhoGroups = _khoService.GetAllKhos();
                //ViewBag.LoaiVatTuGroups = _loaiVatTu.GetAllChiTietVatTus();
                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            //ViewBag.KhoGroups = _khoService.GetAllKhos();
            //ViewBag.LoaiVatTuGroups = _loaiVatTu.GetAllChiTietVatTus();
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.KhoGroups = _khoService.GetAllKhos();
            ViewBag.LoaiVatTuGroups = _loaiVatTu.GetAllChiTietVatTus();
            var soDuBanDauEntity = _soDuBanDauService.GetById(id);
            if(soDuBanDauEntity != null)
            {
                var model = soDuBanDauEntity.MapToModel();
                return View(model);
            }

            return View(new SoDuBanDauModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SoDuBanDauModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
                model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _soDuBanDauService.UpdateSoDuBanDau(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "SoDuBanDau");
                }

                //ViewBag.KhoGroups = _khoService.GetAllKhos();
                //ViewBag.LoaiVatTuGroups = _loaiVatTu.GetAllChiTietVatTus();
                TempData["error"] = message;
                return View(model);
            }

            //ViewBag.KhoGroups = _khoService.GetAllKhos();
            //ViewBag.LoaiVatTuGroups = _loaiVatTu.GetAllChiTietVatTus();
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int soDuBanDauId)
        {
            string message;
            var result = _soDuBanDauService.Delete(soDuBanDauId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int soDuBanDauId)
        {
            string message;
            var result = _soDuBanDauService.ChangeStatus(soDuBanDauId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}