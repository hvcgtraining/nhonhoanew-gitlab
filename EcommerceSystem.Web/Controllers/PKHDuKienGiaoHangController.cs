﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.PKHDuKienGiaoHangModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHDuKienGiaoHangController : BaseController
    {
        private readonly IPKHDuKienGiaoHangService _pKHDuKienGiaoHangService = (IPKHDuKienGiaoHangService)DependencyResolver.Current.GetService(typeof(IPKHDuKienGiaoHangService));
        private readonly IPKHPhieuYeuCauMuaHangService _pKHPhieuYeuCauMuaHangService = (IPKHPhieuYeuCauMuaHangService)DependencyResolver.Current.GetService(typeof(IPKHPhieuYeuCauMuaHangService));
        private readonly IPKHPhieuYeuCauMuaHangDetailService _pKHPhieuYeuCauMuaHangDetailService = (IPKHPhieuYeuCauMuaHangDetailService)DependencyResolver.Current.GetService(typeof(IPKHPhieuYeuCauMuaHangDetailService));


        public ActionResult Index()
        {
            int totalRecords;
            var phieuYeuCauMuaHangId = 0;
            var phieuYeuCauMuaHangDetailId = 0;
            int.TryParse(Request.QueryString["PKHPhieuYeuCauMuaHangId"], out phieuYeuCauMuaHangId);
            int.TryParse(Request.QueryString["PKHPhieuYeuCauMuaHangDetailId"], out phieuYeuCauMuaHangDetailId);

            var model = new PKHDuKienGiaoHangSearchModel
            {
                PKHDuKienGiaoHangs = _pKHDuKienGiaoHangService.Search(1, SystemConfiguration.PageSizeDefault, null, phieuYeuCauMuaHangId, phieuYeuCauMuaHangDetailId, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
                PhieuYeuCauMuaHangId = phieuYeuCauMuaHangId,
                PhieuYeuCauMuaHangDetailId = phieuYeuCauMuaHangDetailId,
                TenPhieuYeuCauMuaHang = _pKHPhieuYeuCauMuaHangService.GetById(phieuYeuCauMuaHangId).Title.ToString(),
                TenPhieuYeuCauMuaHangDetail = _pKHPhieuYeuCauMuaHangDetailService.GetById(phieuYeuCauMuaHangDetailId).Title.ToString(),
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHDuKienGiaoHangSearchModel
            {
                PKHDuKienGiaoHangs = _pKHDuKienGiaoHangService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, 0, 0, sortColumn, sortDirection, out totalRecords),
                SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHDuKienGiaoHang/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create(int PKHPhieuYeuCauMuaHangDetailId, int PKHPhieuYeuCauMuaHangId)
        {
            ViewBag.PhieuYeuCauMuaHangGroups = _pKHPhieuYeuCauMuaHangService.GetAllPKHPhieuYeuCauMuaHangs();
            var model = new PKHDuKienGiaoHangModel();
            model.PKHPhieuYeuCauMuaHangDetailId = PKHPhieuYeuCauMuaHangDetailId;
            model.PKHPhieuYeuCauMuaHangId = PKHPhieuYeuCauMuaHangId;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHDuKienGiaoHangModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_pKHDuKienGiaoHangService.CreatePKHDuKienGiaoHang(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHDuKienGiaoHang", new { PKHPhieuYeuCauMuaHangDetailId = model.PKHPhieuYeuCauMuaHangDetailId, PKHPhieuYeuCauMuaHangId = model.PKHPhieuYeuCauMuaHangId });
                }
                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var pKHDuKienGiaoHangEntity = _pKHDuKienGiaoHangService.GetById(id);
            ViewBag.PhieuYeuCauMuaHangGroups = _pKHPhieuYeuCauMuaHangService.GetAllPKHPhieuYeuCauMuaHangs();
            if (pKHDuKienGiaoHangEntity != null)
            {
                var model = pKHDuKienGiaoHangEntity.MapToModel();
                return View(model);
            }
            return View(new PKHDuKienGiaoHangModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHDuKienGiaoHangModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
                model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _pKHDuKienGiaoHangService.UpdatePKHDuKienGiaoHang(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHDuKienGiaoHang", new { PKHPhieuYeuCauMuaHangDetailId = model.PKHPhieuYeuCauMuaHangDetailId, PKHPhieuYeuCauMuaHangId = model.PKHPhieuYeuCauMuaHangId });
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHDuKienGiaoHangId)
        {
            string message;
            var result = _pKHDuKienGiaoHangService.Delete(pKHDuKienGiaoHangId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

        [HttpPost]
        public ActionResult Invisibe(int pKHDuKienGiaoHangId)
        {
            string message;
            var result = _pKHDuKienGiaoHangService.ChangeStatus(pKHDuKienGiaoHangId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}