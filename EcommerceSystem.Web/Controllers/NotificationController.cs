﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.DataAccess;
using EcommerceSystem.Models;
using EcommerceSystem.Services;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class NotificationController : BaseController
    {
        private readonly INotificationService _notificationService;
        public NotificationController(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }
        [CustomAuthorize(Roles = RoleHelper.NotificationListing)]
        public ActionResult Index()
        {
            int totalRecords;
            var systemType = (int)NotificationType.System;
            var model = new NotificationSearchModel
            {
                Notifications = _notificationService.SearchNotification(null, systemType, 1, SystemConfiguration.PageSizeDefault, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        [CustomAuthorize(Roles = RoleHelper.NotificationListing)]
        public ActionResult Search(int currentPage, string textSearch, int? type = 1)
        {
            int totalRecords;
            var model = new NotificationSearchModel
            {
                Notifications = _notificationService.SearchNotification(textSearch, type, 1, SystemConfiguration.PageSizeDefault, out totalRecords),
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };
            TempData["CurrentPage"] = currentPage;
            TempData["TextSearch"] = textSearch;
            var html = RenderPartialViewToString("~/Views/Customer/Partial/_TableCustomer.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize(Roles = RoleHelper.NotificationCreate)]
        public ActionResult Create()
        {
            //ViewBag.Groups = _roleService.GetRoles();
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost, ValidateInput(false)]
        [CustomAuthorize(Roles = RoleHelper.NotificationCreate)]
        public ActionResult Create(NotificationModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var notification = new Notification()
                    {
                        CreatedDate = DateTime.Now,
                        Note = model.Message,
                        Title = model.Title,
                        Type = (int)NotificationType.System,
                        IsActive = true
                    };

                    var insertedNotification = _notificationService.Insert(notification);

                    _notificationService.SendNotification(null, notification.NotificationId, (int)NotificationType.System, notification.CreatedDate.ToString("dd/MM/yyyy HH:mm"), notification.Title, notification.Note);

                    if (insertedNotification != null)
                    {
                        TempData["success"] = "Thêm mới tin nhắn thành công.";
                        return View(model);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
                TempData["error"] = "Có lỗi xảy ra";
                return View(model);
            }

            return View(model);
        }

        [HttpGet]
        [CustomAuthorize(Roles = RoleHelper.NotificationEdit)]
        public ActionResult Edit(int id)
        {
            var notification = _notificationService.GetById(id);
            if (notification != null)
            {
                return View(notification.MaptoModel());
            }
            return View(new NotificationModel());
        }

        [HttpPost, ValidateInput(false)]
        [CustomAuthorize(Roles = RoleHelper.NotificationEdit)]
        public ActionResult Edit(NotificationModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var notification = _notificationService.GetById(model.NotificationId);
                    if (notification == null)
                    {
                        TempData["error"] = "Có lỗi xảy ra.";
                        return View(model);
                    }
                    notification.Title = model.Title;
                    notification.Note = model.Message;
                    notification.ModifiedDate = DateTime.Now;
                    var updatedNotification = _notificationService.Update(notification);
                    if (updatedNotification != null)
                    {
                        TempData["success"] = "Cập nhật tin nhắn thành công."; ;
                        return View(model);
                    }

                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
                TempData["error"] = "Có lỗi xảy ra";
                return View(model);
            }
            return View(model);
        }

        [HttpPost]
        [CustomAuthorize(Roles = RoleHelper.NotificationDelete)]
        public ActionResult Delete(int id)
        {
            var notification = _notificationService.GetById(id);
            if (notification != null)
            {
                notification.IsDeleted = true;
                if (_notificationService.Update(notification) != null) ;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = "Có lỗi xảy ra" });
        }

        [HttpPost]
        [CustomAuthorize(Roles = RoleHelper.NotificationInvisible)]
        public ActionResult ChangeStatus(int id, bool isActive = false)
        {
            var notification = _notificationService.GetById(id);
            if (notification != null)
            {
                notification.IsActive = isActive;
                if (_notificationService.Update(notification) != null)
                {
                    var message = isActive ? "Ngừng hoạt động thành công thông báo này" : "Kích hoạt thành công thông báo này";
                    return Json(new { IsError = false, Message = message });
                }
            }
            return Json(new { IsError = true, Message = "Có lỗi xảy ra" });
        }
    }
}