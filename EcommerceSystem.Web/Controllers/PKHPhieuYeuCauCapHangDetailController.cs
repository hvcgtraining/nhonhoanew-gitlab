﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.PKHPhieuYeuCauCapHangDetailModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHPhieuYeuCauCapHangDetailController : BaseController
    {
        private readonly IPKHPhieuYeuCauCapHangDetailService _pKHPhieuYeuCauCapHangDetailService = (IPKHPhieuYeuCauCapHangDetailService)DependencyResolver.Current.GetService(typeof(IPKHPhieuYeuCauCapHangDetailService));
        private readonly IPKHPhieuYeuCauCapHangService _pKHPhieuYeuCauCapHangService = (IPKHPhieuYeuCauCapHangService)DependencyResolver.Current.GetService(typeof(IPKHPhieuYeuCauCapHangService));
        private readonly IChiTietVatTuService _loaiVatTuService = (IChiTietVatTuService)DependencyResolver.Current.GetService(typeof(IChiTietVatTuService));
        private readonly IKhoService _khoService = (IKhoService)DependencyResolver.Current.GetService(typeof(IKhoService));

        public ActionResult Index(long phieuYeuCauId)
        {
            int totalRecords = 0;
            var phieuYeuCau = _pKHPhieuYeuCauCapHangService.GetById(phieuYeuCauId);
            var maThanhPham = string.Empty;
            var maKho = string.Empty;
            var ngayTao = string.Empty;
            var soLuong = string.Empty;
            var nguoiGiao = string.Empty;
            var nguoiNhan = string.Empty;
            var lyDo = string.Empty;
            var soCT = string.Empty;

            if (phieuYeuCau != null)
            {
                var khoId = phieuYeuCau.KhoId.HasValue ? phieuYeuCau.KhoId.Value : 0;
                if (khoId != 0)
                {
                    var kho = _khoService.GetById(khoId);
                    maKho = kho.MaKho + " - " + kho.Title;
                }
                var vatTuId = phieuYeuCau.LoaiVatTuId.HasValue ? phieuYeuCau.LoaiVatTuId.Value : 0;                
                if (vatTuId != 0)
                {
                    var vatTu = _loaiVatTuService.GetById(vatTuId);
                    maThanhPham = vatTu.MaChiTietVatTu + " - " + vatTu.Title;
                }
                ngayTao = phieuYeuCau.NgayLap.ToString("dd/MM/yyyy");
                soLuong = phieuYeuCau.SoLuong.ToString();
                nguoiGiao = phieuYeuCau.NguoiGiao;
                nguoiNhan = phieuYeuCau.NguoiNhan;
                lyDo = phieuYeuCau.LyDoXuat;
                soCT = phieuYeuCau.SoCT;
            }            

            var model = new PKHPhieuYeuCauCapHangDetailSearchModel
            {
                PKHPhieuYeuCauCapHangDetails = _pKHPhieuYeuCauCapHangDetailService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
                NgayTao = ngayTao,
                SoChungTu = soCT,
                MaThanhPham = maThanhPham,
                SoLuong = soLuong,
                Kho = maKho,
                NguoiGiao = nguoiGiao,
                NguoiNhan = nguoiNhan,
                LyDo = lyDo
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHPhieuYeuCauCapHangDetailSearchModel
            {
                PKHPhieuYeuCauCapHangDetails = _pKHPhieuYeuCauCapHangDetailService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHPhieuYeuCauCapHangDetail/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHPhieuYeuCauCapHangDetailModel();
            ViewBag.DanhSachChiTiet = _loaiVatTuService.GetAllChiTietVatTus();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHPhieuYeuCauCapHangDetailModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_pKHPhieuYeuCauCapHangDetailService.CreatePKHPhieuYeuCauCapHangDetail(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHPhieuYeuCauCapHangDetail", new { phieuYeuCauId = model.PKHPhieuYeuCauCapHangId });
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var pKHPhieuYeuCauCapHangDetailEntity = _pKHPhieuYeuCauCapHangDetailService.GetById(id);
            if(pKHPhieuYeuCauCapHangDetailEntity != null)
            {
                var model = pKHPhieuYeuCauCapHangDetailEntity.MapToModel();
                ViewBag.DanhSachChiTiet = _loaiVatTuService.GetAllChiTietVatTus();
                return View(model);
            }
            return View(new PKHPhieuYeuCauCapHangDetailModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHPhieuYeuCauCapHangDetailModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _pKHPhieuYeuCauCapHangDetailService.UpdatePKHPhieuYeuCauCapHangDetail(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHPhieuYeuCauCapHangDetail", new { phieuYeuCauId = model.PKHPhieuYeuCauCapHangId });
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHPhieuYeuCauCapHangDetailId)
        {
            string message;
            var result = _pKHPhieuYeuCauCapHangDetailService.Delete(pKHPhieuYeuCauCapHangDetailId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int pKHPhieuYeuCauCapHangDetailId)
        {
            string message;
            var result = _pKHPhieuYeuCauCapHangDetailService.ChangeStatus(pKHPhieuYeuCauCapHangDetailId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}