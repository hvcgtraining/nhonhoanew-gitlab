﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.GoiDauModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class GoiDauController : BaseController
    {
        private readonly IGoiDauService _goiDauService = (IGoiDauService)DependencyResolver.Current.GetService(typeof(IGoiDauService));
        private readonly IChiTietVatTuService _chiTietVatTuService = (IChiTietVatTuService)DependencyResolver.Current.GetService(typeof(IChiTietVatTuService));
        private readonly IPKHKeHoachSXNKXHService _pkhKeHoachSXNKXHService = (IPKHKeHoachSXNKXHService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachSXNKXHService));

        public ActionResult Index()
        {
            int totalRecords;
            var model = new GoiDauSearchModel
            {
                GoiDaus = _goiDauService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new GoiDauSearchModel
            {
                GoiDaus = _goiDauService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/GoiDau/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            ViewBag.LoaiVatTuGroups = _chiTietVatTuService.GetAllChiTietVatTus();
            ViewBag.KeHoachSXNKXHGroups = _pkhKeHoachSXNKXHService.GetAllPKHKeHoachSXNKXHs();
            var model = new GoiDauModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GoiDauModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_goiDauService.CreateGoiDau(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "GoiDau");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.LoaiVatTuGroups = _chiTietVatTuService.GetAllChiTietVatTus();
            ViewBag.KeHoachSXNKXHGroups = _pkhKeHoachSXNKXHService.GetAllPKHKeHoachSXNKXHs();
            var goiDauEntity = _goiDauService.GetById(id);
            if(goiDauEntity != null)
            {
                var model = goiDauEntity.MapToModel();
                return View(model);
            }
            return View(new GoiDauModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GoiDauModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _goiDauService.UpdateGoiDau(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "GoiDau");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int goiDauId)
        {
            string message;
            var result = _goiDauService.Delete(goiDauId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int goiDauId)
        {
            string message;
            var result = _goiDauService.ChangeStatus(goiDauId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}