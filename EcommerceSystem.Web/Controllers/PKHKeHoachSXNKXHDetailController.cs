﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models;
using EcommerceSystem.Models.PKHKeHoachSXNKXHDetailModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHKeHoachSXNKXHDetailController : BaseController
    {
        private readonly IPKHKeHoachSXNKXHDetailService _pKHKeHoachSXNKXHDetailService = (IPKHKeHoachSXNKXHDetailService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachSXNKXHDetailService));
        private readonly IChiTietVatTuService _loaiVatTuService = (IChiTietVatTuService)DependencyResolver.Current.GetService(typeof(IChiTietVatTuService));
        private readonly IPKHKeHoachSXNKXHService _pKHKeHoachSXNKXHService = (IPKHKeHoachSXNKXHService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachSXNKXHService));
        private readonly IXuongService _xuongService = (IXuongService)DependencyResolver.Current.GetService(typeof(IXuongService));
        public ActionResult Index(long keHoachId)
        {
            int totalRecords = 0;
            var keHoach = _pKHKeHoachSXNKXHService.GetById(keHoachId);
            var model = new PKHKeHoachSXNKXHDetailSearchModel
            {
                PKHKeHoachSXNKXHDetails = _pKHKeHoachSXNKXHDetailService.GetAllByParentId(keHoachId),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
                FromDate = keHoach.StartDate,
                ToDate = keHoach.EndDate,
                MaKeHoach = keHoach.MaTuan
            };
            
            ViewBag.DanhSachCan = _loaiVatTuService.GetAllChiTietVatTus();
            //ViewBag.DanhSachXuongs = _xuongService.GetAXuongsByType((int)LoaiXuong.XuongLapRap);
            return View(model);
        }
        
        public ActionResult KeHoachSX(long keHoachId)
        {
            int totalRecords = 0;
            var keHoach = _pKHKeHoachSXNKXHService.GetById(keHoachId);
            var model = new PKHKeHoachSXNKXHDetailSearchModel
            {
                PKHKeHoachSXDetails = _pKHKeHoachSXNKXHDetailService.GetAllByParentId(keHoachId),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
                FromDate = keHoach.StartDate,
                ToDate = keHoach.EndDate,
                MaKeHoach = keHoach.MaTuan,
            };
            ViewBag.KHSX = keHoach;
            return View(model);
        }

        public ActionResult Search(long keHoachId, int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHKeHoachSXNKXHDetailSearchModel
            {
                PKHKeHoachSXNKXHDetails = _pKHKeHoachSXNKXHDetailService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHKeHoachSXNKXHDetail/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHKeHoachSXNKXHDetailModel();
            ViewBag.DanhSachCan = _loaiVatTuService.GetAllChiTietVatTus();
            //ViewBag.DanhSachXuongs = _xuongService.GetAXuongsByType((int)LoaiXuong.XuongLapRap);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHKeHoachSXNKXHDetailModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;
                var luongTonkho = model.TonKho.HasValue ? model.TonKho.Value : 0;
                var luongNhapKho = model.NhapKhoTuanTruoc.HasValue ? model.NhapKhoTuanTruoc.Value : 0;
                var luongXuatHaNoi = model.XuatHaNoi.HasValue ? model.XuatHaNoi.Value : 0;
                var luongXuatKho = model.XuatKho.HasValue ? model.XuatKho.Value : 0;
                var xuong4 = model.Xuong4.HasValue ? model.Xuong4.Value : 0;
                var xuong5 = model.Xuong5.HasValue ? model.Xuong5.Value : 0;
                var xuong6 = model.Xuong6.HasValue ? model.Xuong6.Value : 0;
                var xuatDaiLy = model.XuatDaiLy.HasValue ? model.XuatDaiLy.Value : 0;
                var duKienXuatKho = model.DuKienXuatKho.HasValue ? model.DuKienXuatKho.Value : 0;

                model.TongTonKho = luongTonkho + luongNhapKho - luongXuatHaNoi - luongXuatKho;
                model.TongNhapKho = xuong4 + xuong5 + xuong6;
                model.SoLuongBaoBi = model.SoLuongCan = model.TongNhapKho;
                model.DuKienTonKho = model.TongTonKho + model.TongNhapKho - xuatDaiLy - luongXuatHaNoi - duKienXuatKho;

                long keHoachSXNKXHDetailId = 0;
                string message = "";
                if (_pKHKeHoachSXNKXHDetailService.CreatePKHKeHoachSXNKXHDetail(model, out keHoachSXNKXHDetailId, out message))
                {
                    Thread myNewThread = new Thread(() => _pKHKeHoachSXNKXHDetailService.UpdateKHSXXuong(model.VatTuId, model.PKHKeHoachSXNKXHId, keHoachSXNKXHDetailId, model.SoLuongCan.Value, LoaiXuong.XuongLapRap, CurrentUser.UserName));
                    myNewThread.Start();

                    //Thread myNewThread2 = new Thread(() => _pKHKeHoachSXNKXHDetailService.UpdateKHSXXuong(model.VatTuId, model.PKHKeHoachSXNKXHId, keHoachSXNKXHDetailId, LoaiXuong.XuongChitiet, CurrentUser.UserName));
                    //myNewThread2.Start();
                    TempData["success"] = "Tạo mới thành công";
                    _pKHKeHoachSXNKXHService.UpdateSoLuongCan(model.PKHKeHoachSXNKXHId, model.TongNhapKho.Value);
                    return RedirectToAction("Index", "PKHKeHoachSXNKXHDetail", new { keHoachId = model.PKHKeHoachSXNKXHId });
                }

                TempData["error"] = "Tạo mới thất bại. " + message;
                return RedirectToAction("Index", "PKHKeHoachSXNKXHDetail", new { keHoachId = model.PKHKeHoachSXNKXHId });
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var pKHKeHoachSXNKXHDetailEntity = _pKHKeHoachSXNKXHDetailService.GetById(id);
            if(pKHKeHoachSXNKXHDetailEntity != null)
            {
                var model = pKHKeHoachSXNKXHDetailEntity.MapToModel();
                ViewBag.DanhSachCan = _loaiVatTuService.GetAllChiTietVatTus();
                return View(model);
            }
            return View(new PKHKeHoachSXNKXHDetailModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHKeHoachSXNKXHDetailModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;
                var pKHKeHoachSXNKXHDetailEntity = _pKHKeHoachSXNKXHDetailService.GetById(model.PKHKeHoachSXNKXHDetailId);
                var currentTongNhapKho = pKHKeHoachSXNKXHDetailEntity.TongNhapKho.HasValue ? pKHKeHoachSXNKXHDetailEntity.TongNhapKho.Value : 0;
                var luongTonkho = model.TonKho.HasValue ? model.TonKho.Value : 0;
                var luongNhapKho = model.NhapKhoTuanTruoc.HasValue ? model.NhapKhoTuanTruoc.Value : 0;
                var luongXuatHaNoi = model.XuatHaNoi.HasValue ? model.XuatHaNoi.Value : 0;
                var luongXuatKho = model.XuatKho.HasValue ? model.XuatKho.Value : 0;
                var xuong4 = model.Xuong4.HasValue ? model.Xuong4.Value : 0;
                var xuong5 = model.Xuong5.HasValue ? model.Xuong5.Value : 0;
                var xuong6 = model.Xuong6.HasValue ? model.Xuong6.Value : 0;
                var xuatDaiLy = model.XuatDaiLy.HasValue ? model.XuatDaiLy.Value : 0;
                var duKienXuatKho = model.DuKienXuatKho.HasValue ? model.DuKienXuatKho.Value : 0;

                model.TongTonKho = luongTonkho + luongNhapKho - luongXuatHaNoi - luongXuatKho;
                model.SoLuongCan = model.TongNhapKho = xuong4 + xuong5 + xuong6;
                var diffValue = model.TongNhapKho - currentTongNhapKho;
                model.SoLuongBaoBi = pKHKeHoachSXNKXHDetailEntity.SoLuongBaoBi.HasValue ? pKHKeHoachSXNKXHDetailEntity.SoLuongBaoBi.Value : 0;
                model.DuKienTonKho = model.TongTonKho + model.TongNhapKho - xuatDaiLy - luongXuatHaNoi - duKienXuatKho;
                var isSuccess = _pKHKeHoachSXNKXHDetailService.UpdatePKHKeHoachSXNKXHDetail(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    _pKHKeHoachSXNKXHService.UpdateSoLuongCan(model.PKHKeHoachSXNKXHId, diffValue.Value);
                    return RedirectToAction("Index", "PKHKeHoachSXNKXHDetail", new { keHoachId = model.PKHKeHoachSXNKXHId });
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        public ActionResult EditKeHoachSX(int id)
        {
            var pKHKeHoachSXNKXHDetailEntity = _pKHKeHoachSXNKXHDetailService.GetById(id);
            if (pKHKeHoachSXNKXHDetailEntity != null)
            {
                var model = pKHKeHoachSXNKXHDetailEntity.MapToModel();
                return View(model);
            }
            return View(new PKHKeHoachSXNKXHDetailModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditKeHoachSX(PKHKeHoachSXNKXHDetailModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
                model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _pKHKeHoachSXNKXHDetailService.UpdateSoLuongPKHKeHoachSX(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("KeHoachSX", "PKHKeHoachSXNKXHDetail", new { keHoachId = model.PKHKeHoachSXNKXHId });
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(long pKHKeHoachSXNKXHDetailId)
        {
            string message;
            var result = _pKHKeHoachSXNKXHDetailService.Delete(pKHKeHoachSXNKXHDetailId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int pKHKeHoachSXNKXHDetailId)
        {
            string message;
            var result = _pKHKeHoachSXNKXHDetailService.ChangeStatus(pKHKeHoachSXNKXHDetailId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}