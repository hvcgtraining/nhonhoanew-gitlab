﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHKeHoachNhapKhoThanhPhamDetailModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class PKHKeHoachNhapKhoThanhPhamDetailController : BaseController
    {
        private readonly IPKHKeHoachNhapKhoThanhPhamDetailService _pKHKeHoachNhapKhoThanhPhamDetailService = (IPKHKeHoachNhapKhoThanhPhamDetailService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachNhapKhoThanhPhamDetailService));
        private readonly IPKHKeHoachXuongService _pKHKeHoachXuongService = (IPKHKeHoachXuongService)DependencyResolver.Current.GetService(typeof(IPKHKeHoachXuongService));
        private readonly IXuongService _xuongService = (IXuongService)DependencyResolver.Current.GetService(typeof(IXuongService));

        public ActionResult Index()
        {
            int totalRecords;
            var keHoachXuongId = 0;
            int? xuongId = 0;

            int.TryParse(Request.QueryString["PKHKeHoachXuongId"], out keHoachXuongId);
            xuongId = (keHoachXuongId != 0) ? _pKHKeHoachXuongService.GetById(keHoachXuongId).XuongId : 0;
            var tenXuong = (xuongId != 0) ? _xuongService.GetById((int)xuongId).Title : "";
            var model = new PKHKeHoachNhapKhoThanhPhamDetailSearchModel
            {
                PKHKeHoachNhapKhoThanhPhamDetails = _pKHKeHoachNhapKhoThanhPhamDetailService.Search(1, SystemConfiguration.PageSizeDefault, null, keHoachXuongId, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
                PKHKeHoachXuongId = keHoachXuongId,
                TenXuong = tenXuong,
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new PKHKeHoachNhapKhoThanhPhamDetailSearchModel
            {
                PKHKeHoachNhapKhoThanhPhamDetails = _pKHKeHoachNhapKhoThanhPhamDetailService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, 0, sortColumn, sortDirection, out totalRecords),
                SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/PKHKeHoachNhapKhoThanhPhamDetail/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new PKHKeHoachNhapKhoThanhPhamDetailModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PKHKeHoachNhapKhoThanhPhamDetailModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_pKHKeHoachNhapKhoThanhPhamDetailService.CreatePKHKeHoachNhapKhoThanhPhamDetail(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "PKHKeHoachNhapKhoThanhPhamDetail");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var pKHKeHoachNhapKhoThanhPhamDetailEntity = _pKHKeHoachNhapKhoThanhPhamDetailService.GetById(id);
            if (pKHKeHoachNhapKhoThanhPhamDetailEntity != null)
            {
                var model = pKHKeHoachNhapKhoThanhPhamDetailEntity.MapToModel();
                return View(model);
            }
            return View(new PKHKeHoachNhapKhoThanhPhamDetailModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PKHKeHoachNhapKhoThanhPhamDetailModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
                model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _pKHKeHoachNhapKhoThanhPhamDetailService.UpdatePKHKeHoachNhapKhoThanhPhamDetail(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "PKHKeHoachNhapKhoThanhPhamDetail");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int pKHKeHoachNhapKhoThanhPhamDetailId)
        {
            string message;
            var result = _pKHKeHoachNhapKhoThanhPhamDetailService.Delete(pKHKeHoachNhapKhoThanhPhamDetailId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

        [HttpPost]
        public ActionResult Invisibe(int pKHKeHoachNhapKhoThanhPhamDetailId)
        {
            string message;
            var result = _pKHKeHoachNhapKhoThanhPhamDetailService.ChangeStatus(pKHKeHoachNhapKhoThanhPhamDetailId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}