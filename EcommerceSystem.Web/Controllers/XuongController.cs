﻿using EcommerceSystem.Core.Configurations;
using EcommerceSystem.Models.XuongModel;
using EcommerceSystem.Services;
using EcommerceSystem.Services.AutoMap;
using EcommerceSystem.Web.Framework.Authentication;
using EcommerceSystem.Web.Framework.Helpers;
using System;
using System.Web.Mvc;

namespace EcommerceSystem.Web.Controllers
{
    public class XuongController : BaseController
    {
        private readonly IXuongService _xuongService = (IXuongService)DependencyResolver.Current.GetService(typeof(IXuongService));
        
        public ActionResult Index()
        {
            int totalRecords;
            var model = new XuongSearchModel
            {
                Xuongs = _xuongService.Search(1, SystemConfiguration.PageSizeDefault, null, null, null, out totalRecords),
                PageIndex = 1,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords
            };

            return View(model);
        }

        public ActionResult Search(int currentPage, string textSearch, string sortColumn, string sortDirection)
        {
            int totalRecords;
            var model = new XuongSearchModel
            {
                Xuongs = _xuongService.Search(currentPage, SystemConfiguration.PageSizeDefault, textSearch, sortColumn, sortDirection, out totalRecords),
				SortColumn = sortColumn,
                SortDirection = sortDirection,
                PageIndex = currentPage,
                PageSize = SystemConfiguration.PageSizeDefault,
                TotalRecords = totalRecords,
            };

            var html = RenderPartialViewToString("~/Views/Xuong/Partial/_ListItems.cshtml", model);
            return Json(new
            {
                IsError = false,
                HTML = html
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new XuongModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(XuongModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = CurrentUser.Email;

                if (_xuongService.CreateXuong(model))
                {
                    TempData["success"] = "Tạo mới thành công";
                    return RedirectToAction("Index", "Xuong");
                }

                TempData["error"] = "Tạo mới thất bại";
                return View(model);
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var xuongEntity = _xuongService.GetById(id);
            if(xuongEntity != null)
            {
                var model = xuongEntity.MapToModel();
                return View(model);
            }
            return View(new XuongModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(XuongModel model)
        {
            string message;
            if (ModelState.IsValid)
            {
				model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = CurrentUser.Email;
                var isSuccess = _xuongService.UpdateXuong(model, out message);
                if (isSuccess)
                {
                    TempData["success"] = message;
                    return RedirectToAction("Index", "Xuong");
                }

                TempData["error"] = message;
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int xuongId)
        {
            string message;
            var result = _xuongService.Delete(xuongId, out message);
            if (result)
            {
                TempData["success"] = message;
                return Json(new { IsError = false });
            }
            return Json(new { IsError = true, Message = message });
        }

		[HttpPost]
        public ActionResult Invisibe(int xuongId)
        {
            string message;
            var result = _xuongService.ChangeStatus(xuongId, out message);
            if (result)
            {
                return Json(new { IsError = false, Message = message });
            }
            return Json(new { IsError = true, Message = message });
        }
    }
}