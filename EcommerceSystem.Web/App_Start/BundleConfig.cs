﻿using System.Web;
using System.Web.Optimization;

namespace EcommerceSystem.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Assets/js/bootstrap.js",
                      "~/Assets/js/bootstrap-datepicker.js",
                      "~/Assets/js/notify.min.js",
                      "~/Assets/plugins/sweetalert/dist/sweetalert2.min.js",
                      "~/Scripts/respond.js",
                      "~/Assets/scripts/ecommerceSystem.lib.js",
                      //"~/Assets/scripts/ecommerceSystem.js",
                      "~/Assets/scripts/ecommerceSystem.control.js",
                      "~/Assets/scripts/ecommerceSystem.client.js",
                      "~/Assets/scripts/ecommerceSystem.role.js",
                      "~/Assets/scripts/ecommerceSystem.account.js",

                       "~/Assets/scripts/ecommerceSystem.notification.js",
                       "~/Assets/scripts/ecommerceSystem.chat.js",
                       "~/Assets/js/custom.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/jscommon").Include(
                     "~/Assets/scripts/ecommerceSystem.common.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      //"~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/admin-bundles/jshome-all").Include(
                    "~/Assets/js/plugins/charts/sparkline.min.js",
                    "~/Assets/js/plugins/forms/uniform.min.js",
                    "~/Assets/js/plugins/forms/select2.min.js",
                    "~/Assets/js/plugins/forms/inputmask.js",
                    "~/Assets/js/plugins/forms/autosize.js",
                    "~/Assets/js/plugins/forms/inputlimit.min.js",
                    "~/Assets/js/plugins/forms/listbox.js",
                    "~/Assets/js/plugins/forms/multiselect.js",
                    //"~/Assets/js/plugins/forms/validate.min.js",
                    "~/Assets/js/plugins/forms/jquery.tagsinput.js",
                    "~/Assets/js/plugins/forms/switch.min.js",
                    "~/Assets/js/plugins/forms/uploader/plupload.full.min.js",
                    "~/Assets/js/plugins/forms/uploader/plupload.queue.min.js",
                    "~/Assets/js/plugins/forms/wysihtml5/wysihtml5.min.js",
                    "~/Assets/js/plugins/forms/wysihtml5/toolbar.js",
                    //"~/Assets/js/plugins/interface/daterangepicker.js",
                    "~/Assets/js/plugins/interface/fancybox.min.js",
                    "~/Assets/js/plugins/interface/moment.js",
                    "~/Assets/js/plugins/interface/jgrowl.min.js",
                    //"~/Assets/js/plugins/interface/datatables.min.js",
                    "~/Assets/js/plugins/interface/colorpicker.js",
                    //"~/Assets/js/plugins/interface/fullcalendar.min.js",
                    //"~/Assets/js/plugins/interface/timepicker.min.js",
                    //"~/Assets/js/bootstrap.min.js",
                    //"~/Assets/js/common.js",
                    //"~/Assets/js/application_blank.js",
                    //"~/Assets/js/table_js.js",
                    //"~/Scripts/jquery.unobtrusive-ajax.js",
                    //"~/Scripts/jquery.validate.min.js",
                    //"~/Scripts/jquery.validate.unobtrusive.min.js",
                    "~/Scripts/tinymce/tinymce.min.js",
                   "~/Assets/lib/dist/jstree.min.js"
              ));

            bundles.Add(new StyleBundle("~/admin-bundles/csshome").Include(
                        "~/Assets/css/icons.css", new CssRewriteUrlTransform()).Include(
                        "~/Assets/css/bootstrap.min.css",
                        "~/Assets/css/datepicker.css",
                        "~/Assets/css/londinium-theme.css",
                        //"~/Assets/css/jquery.datetimepicker.css",
                        "~/Assets/css/styles.css",
                        "~/Assets/css/select2.css",
                        "~/Assets/css/jquery.tagsinput.css",
                        "~/Assets/css/ecommerceSystem.common.css",
                        "~/Assets/css/custom.css",
                        "~/Assets/lib/dist/themes/default/style.min.css",
                        "~/Content/fonts/font-awesome-4.5.0/css/font-awesome.css",
                        "~/Assets/css/Adminlte.custom.css",
                        "~/Assets/css/customfe.css",
                        "~/Assets/plugins/sweetalert/dist/sweetalert2.min.css",
                        "~/Content/Common.css"
                      ));
        }
    }
}
