﻿

var inActive = 0;
var active = 1;

var pKHPhieuYeuCauMuaHangDetailModule = {
    init: function () {
        pKHPhieuYeuCauMuaHangDetailModule.openConfirmDelete();
        pKHPhieuYeuCauMuaHangDetailModule.onClickActive();
        pKHPhieuYeuCauMuaHangDetailModule.onClickSearch();
        pKHPhieuYeuCauMuaHangDetailModule.onClickSorting();
        pKHPhieuYeuCauMuaHangDetailModule.initCheckpKHPhieuYeuCauMuaHangDetail();
        pKHPhieuYeuCauMuaHangDetailModule.openPopup();
        pKHPhieuYeuCauMuaHangDetailModule.executeCompleteRequest();
        pKHPhieuYeuCauMuaHangDetailModule.executeTransferRequest();
        pKHPhieuYeuCauMuaHangDetailModule.executeCancelRequest();
    },
    initCheckpKHPhieuYeuCauMuaHangDetail: function () {
        var checkAll = $(".checkedAll");
        checkAll.each(function (index, element) {
            var rootParent = element.id;

            var isChecked = $(".pKHPhieuYeuCauMuaHangDetail-item").find("input[data-root='" + rootParent + "']").val();
            if ("" != isChecked && (isChecked === "1" || isChecked === 1)) {
                $(element).prop('checked', true);
            } else {
                $(element).prop('checked', false);
            }
        });
    },
	openPopup: function () {
        $(document).on("click", "a.dialog", function () {
            var url = $(this).attr('href');
            var title = $(this).attr('title');
            var dialog = $('<div style="display:none"></div>').appendTo('body');

            dialog.load(url,
                function (responseText, textStatus, XMLHttpRequest) {
                    $.validator.unobtrusive.parse(this);
                    dialog.dialog({
                        modal: true,
                        title: title,
                        width: ($(window).width() * 0.8),
                        resizable: false,
                        close: function (event, ui) {
                            dialog.remove();
                        }
                    });
                });
            return false;
        });
    },
    openConfirmDelete: function () {
        $(document).on("click", ".icon-remove4", function () {
            var dataId = $(this).attr("data-id");
            var root = $("#pKHPhieuYeuCauMuaHangDetail_modal_info").find(".confirm-yes");
            $(root).attr("onclick", pKHPhieuYeuCauMuaHangDetailModule.executeDelete);
            $(root).attr("data-id", dataId);

            common.modal.show("pKHPhieuYeuCauMuaHangDetail_modal_info");
            pKHPhieuYeuCauMuaHangDetailModule.executeDelete();
        });
    },
    
    executeDelete: function () {
        $(document).on("click", "#btn_pKHPhieuYeuCauMuaHangDetail", function () {
            var dataId = $(this).attr("data-id");
            $.ajax({
                url: "/PKHPhieuYeuCauMuaHangDetail/Delete",
                type: "POST",
                data: { PKHPhieuYeuCauMuaHangDetailId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        location.href = window.location;
                    } else {
                        common.notify.showError(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },
    executeCompleteRequest: function () {
        $(document).on("click", "#hoanthanhyeucau", function () {
            var dataId = $(this).attr("data-id");
            $.ajax({
                url: "/PKHPhieuYeuCauMuaHangDetail/HoanThanhYeuCau",
                type: "POST",
                data: { pKHPhieuYeuCauMuaHangId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        location.href = "/YeuCauMuaHang/Index";
                    } else {
                        common.notify.showError(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },

    executeTransferRequest: function () {
        $(document).on("click", "#chuyenyeucau", function () {
            var dataId = $(this).attr("data-id");
            $.ajax({
                url: "/PKHPhieuYeuCauMuaHangDetail/ChuyenYeuCau",
                type: "POST",
                data: { pKHPhieuYeuCauMuaHangId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        location.href = "/YeuCauMuaHang/Index";
                    } else {
                        common.notify.showError(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },

    executeCancelRequest: function () {
        $(document).on("click", "#huyyeucau", function () {
            var dataId = $(this).attr("data-id");
            $.ajax({
                url: "/PKHPhieuYeuCauMuaHangDetail/HuyYeuCau",
                type: "POST",
                data: { pKHPhieuYeuCauMuaHangId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        location.href = "/YeuCauMuaHang/Index";
                    } else {
                        common.notify.showError(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },

    onClickPaging: function (currentPage, sortColumn, sortDirection) {
        var textSearch = $("#TextSearch").val();
        var maNhom = ($("#MaNhom").val() != 0) ? $("#MaNhom").val() : 0;
        var PKHPhieuYeuCauMuaHangId = $("#PKHPhieuYeuCauMuaHangId").val();
        $.ajax({
            url: "/PKHPhieuYeuCauMuaHangDetail/Search",
            data: {
                currentPage: currentPage, textSearch: textSearch, PKHPhieuYeuCauMuaHangId: PKHPhieuYeuCauMuaHangId, maNhom: maNhom,
                sortColumn: sortColumn, sortDirection: sortDirection
            },
            type: "GET",
            success: function (response) {
                if (response.IsError === false && (undefined != response.HTML && "" !== response.HTML)) {
                    $("#main-pKHPhieuYeuCauMuaHangDetail").html("");
                    $("#main-pKHPhieuYeuCauMuaHangDetail").html(response.HTML);
                }
            }
        });
    },
    onClickSorting: function () {
        $(document).on("click", ".sorting,.sorting_asc,.sorting_desc", function () {
            var sortColumn = $(this).attr("data-column");
            var sortDirection;
            var sortDirectionValue;
            if ($(this).hasClass("sorting_asc")) {
                $(this).removeClass("sorting_asc");
                sortDirection = "sorting_desc";
                sortDirectionValue = 1;
            } else {
                $(this).removeClass("sorting_desc");
                sortDirection = "sorting_asc";
                sortDirectionValue = 0;
            }

            $(this).addClass(sortDirection);
            pKHPhieuYeuCauMuaHangDetailModule.onClickPaging(1, sortColumn, sortDirectionValue);
        });
    },
    onClickActive: function () {
        $(document).on("click", ".active", function () {
            var root = $(this).attr("data-parent");
            var item = $(this);
            var dataId = $(item).attr("data-id");
            var status = $(item).attr("data-status");
            $.ajax({
                url: "/PKHPhieuYeuCauMuaHangDetail/Invisibe",
                type: "POST",
                data: { PKHPhieuYeuCauMuaHangDetailId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        if (status === "True" || parseInt(status) === active) {
                            $(item).attr("data-status", inActive);
                            $(item).find("i").removeClass("icon-eye5").addClass("icon-eye4");
                            $("#" + root).find("span[data-span='inactive_" + dataId + "']").removeClass("hide").addClass("show");
                            $("#" + root).find("span[data-span='active_" + dataId + "']").removeClass("show").addClass("hide");
                        } else {
                            $(item).attr("data-status", active);
                            $(item).find("i").removeClass("icon-eye4").addClass("icon-eye5");
                            $("#" + root).find("span[data-span='active_" + dataId + "']").removeClass("hide").addClass("show");
                            $("#" + root).find("span[data-span='inactive_" + dataId + "']").removeClass("show").addClass("hide");
                        }
                        common.notify.showSuccess(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },
    onClickSearch: function () {
        $(document).on("click", "#btnSearch", function () {
            pKHPhieuYeuCauMuaHangDetailModule.onClickPaging(1);
        });
    }
}

function ModuleAction(moduleActionId) {
    this.ModuleActionId = moduleActionId;
}