﻿var reportModule = {
    init: function () {
        reportModule.onClickSearch();
        //$(document).ready(function () {
        //    ecommerceSystem.SetUpDateTimePicker("#FromDate", "#ToDate");
        //});
        ecommerceSystem.Client.Common.Init();
    },
    onClickPaging: function (currentPage) {
        var orderEmail = $("#Email").val();
        var fromDate = $("#FromDate").val();
        var toDate = $("#ToDate").val();
        $.ajax({
            url: "/Report/SearchReportByOrder",
            data: { currentPage: currentPage, orderEmail: orderEmail, fromDate: fromDate, toDate: toDate },
            type: "GET",
            success: function (response) {
                if (response.IsError === false && (undefined != response.HTML && "" !== response.HTML)) {
                    $("#main-report").html("");
                    $("#main-report").html(response.HTML);
                }
            }
        });
    },
    onClickSearch: function () {
        $(document).on("click", "#btnSearch", function () {
            reportModule.onClickPaging(1);
        });
    }
}