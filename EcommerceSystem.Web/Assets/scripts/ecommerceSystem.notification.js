﻿var notificationModule = {
    init: function () {
        $("#txtSearch").click(function () {
            if ($("#TextSearch").val() != '') {
                notificationModule.search();
            }
        });
        this.deleteNotification();
        this.changeStatusNotification();
        $(document).ready(function () {
            $(".note-editable").html($('#Message').val());
            $(".btnSaveNotification").click(function () {
                $('#Message').val($(".note-editable").html());
                $("#frmUpdate").submit();
            });
        });
    },
    search: function () {
        notificationModule.onClickPaging(1);
    },
    deleteNotification: function () {
        $('.btnDelete').click(function () {
            var dataId = $(this).attr("data-id");
            common.modal.showConfirm("Xóa thông báo", "Bạn muốn xóa thông báo này?", function () {
                $.ajax({
                    url: "/Notification/Delete",
                    type: "POST",
                    data: { id: dataId },
                    success: function (data) {
                        if (data.IsError === false) {
                            common.notify.showSuccess("Xóa thông báo thành công!");
                            reloadPage();
                        }
                        else if (typeof data.Message != 'undefined' && data.Message !== '') {
                            common.notify.showError(data.Message);
                        }
                    },
                    error: function () {
                        console.log("Err");
                    }
                });
            })
        });
    },
    changeStatusNotification: function () {
        $('.btnChangeStatus').click(function () {
            var dataId = $(this).attr("data-id");
            var dataStatus = $(this).attr("data-status");
            var isActive = dataStatus == "True";
            var message = dataStatus == "True" ? "Bạn có muốn kích hoạt thông báo này?" : "Bạn có muốn ngừng hoạt động thông báo này?";
            var title = dataStatus == "True" ? "Kích hoạt thông báo" : "Ngừng hoạt động thông báo";
            common.modal.showConfirm(title, message, function () {
                $.ajax({
                    url: "/Notification/ChangeStatus",
                    type: "POST",
                    data: { id: dataId, isActive: isActive },
                    success: function (data) {
                        if (data.IsError === false) {
                            common.notify.showSuccess(data.Message);
                            reloadPage();
                        }
                        else if (typeof data.Message != 'undefined' && data.Message !== '') {
                            common.notify.showError(data.Message);
                        }
                    },
                    error: function () {
                        console.log("Err");
                    }
                });
            })
        });
    },
    onClickPaging: function (currentPage) {
        var keyword = $("#TextSearch").val();
        if (typeof currentPage != 'undefined' && currentPage != '' && currentPage != '1') {
            location.href = "/Notification/Search?keyword=" + keyword + "&pageIndex=" + currentPage;
        }
        else {
            location.href = "/Notification/Search?keyword=" + keyword;
        }
    }
}