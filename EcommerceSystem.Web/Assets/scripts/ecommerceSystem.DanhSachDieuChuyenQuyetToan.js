﻿

var inActive = 0;
var active = 1;

var danhSachDieuChuyenQuyetToanModule = {
    init: function () {
        danhSachDieuChuyenQuyetToanModule.openConfirmDelete();
        danhSachDieuChuyenQuyetToanModule.onClickActive();
        danhSachDieuChuyenQuyetToanModule.onClickSearch();
        danhSachDieuChuyenQuyetToanModule.onClickSorting();
        danhSachDieuChuyenQuyetToanModule.initCheckdanhSachDieuChuyenQuyetToan();
		danhSachDieuChuyenQuyetToanModule.openPopup();
    },
    initCheckdanhSachDieuChuyenQuyetToan: function () {
        var checkAll = $(".checkedAll");
        checkAll.each(function (index, element) {
            var rootParent = element.id;

            var isChecked = $(".danhSachDieuChuyenQuyetToan-item").find("input[data-root='" + rootParent + "']").val();
            if ("" != isChecked && (isChecked === "1" || isChecked === 1)) {
                $(element).prop('checked', true);
            } else {
                $(element).prop('checked', false);
            }
        });
    },
	openPopup: function () {
        $(document).on("click", "a.dialog", function () {
            var url = $(this).attr('href');
            var title = $(this).attr('title');
            var dialog = $('<div style="display:none"></div>').appendTo('body');

            dialog.load(url,
                function (responseText, textStatus, XMLHttpRequest) {
                    $.validator.unobtrusive.parse(this);
                    dialog.dialog({
                        modal: true,
                        title: title,
                        width: ($(window).width() * 0.8),
                        resizable: false,
                        close: function (event, ui) {
                            dialog.remove();
                        }
                    });
                });
            return false;
        });
    },
    openConfirmDelete: function () {
        $(document).on("click", ".icon-remove4", function () {
            var dataId = $(this).attr("data-id");
            var root = $("#danhSachDieuChuyenQuyetToan_modal_info").find(".confirm-yes");
            $(root).attr("onclick", danhSachDieuChuyenQuyetToanModule.executeDelete);
            $(root).attr("data-id", dataId);

            common.modal.show("danhSachDieuChuyenQuyetToan_modal_info");
            danhSachDieuChuyenQuyetToanModule.executeDelete();
        });
    },
    
    executeDelete: function () {
        $(document).on("click", "#btn_danhSachDieuChuyenQuyetToan", function () {
            var dataId = $(this).attr("data-id");
            $.ajax({
                url: "/DanhSachDieuChuyenQuyetToan/Delete",
                type: "POST",
                data: { DanhSachDieuChuyenQuyetToanId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        location.href = window.location;
                    } else {
                        common.notify.showError(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },
    onClickPaging: function (currentPage, sortColumn, sortDirection) {
        var textSearch = $("#TextSearch").val();
        var loaiDieuChuyen = $("#loaiDieuChuyenhd").val();
        //var quyetToanId = $("#quyetToanIdhd").val();
        var quyetToanId = danhSachDieuChuyenQuyetToanModule.GetURLParameter("qtid");
        $.ajax({
            url: "/DanhSachDieuChuyenQuyetToan/Search",
            data: {
                currentPage: currentPage, textSearch: textSearch, loaiDieuChuyen: loaiDieuChuyen, quyetToanId: quyetToanId,
                sortColumn: sortColumn, sortDirection: sortDirection
            },
            type: "GET",
            success: function (response) {
                if (response.IsError === false && (undefined != response.HTML && "" !== response.HTML)) {
                    $("#main-danhSachDieuChuyenQuyetToan").html("");
                    $("#main-danhSachDieuChuyenQuyetToan").html(response.HTML);
                }
            }
        });
    },
    GetURLParameter: function (sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    },
    onClickSorting: function () {
        $(document).on("click", ".sorting,.sorting_asc,.sorting_desc", function () {
            var sortColumn = $(this).attr("data-column");
            var sortDirection;
            var sortDirectionValue;
            if ($(this).hasClass("sorting_asc")) {
                $(this).removeClass("sorting_asc");
                sortDirection = "sorting_desc";
                sortDirectionValue = 1;
            } else {
                $(this).removeClass("sorting_desc");
                sortDirection = "sorting_asc";
                sortDirectionValue = 0;
            }

            $(this).addClass(sortDirection);
            danhSachDieuChuyenQuyetToanModule.onClickPaging(1, sortColumn, sortDirectionValue);
        });
    },
    onClickActive: function () {
        $(document).on("click", ".active", function () {
            var root = $(this).attr("data-parent");
            var item = $(this);
            var dataId = $(item).attr("data-id");
            var status = $(item).attr("data-status");
            $.ajax({
                url: "/DanhSachDieuChuyenQuyetToan/Invisibe",
                type: "POST",
                data: { DanhSachDieuChuyenQuyetToanId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        if (status === "True" || parseInt(status) === active) {
                            $(item).attr("data-status", inActive);
                            $(item).find("i").removeClass("icon-eye5").addClass("icon-eye4");
                            $("#" + root).find("span[data-span='inactive_" + dataId + "']").removeClass("hide").addClass("show");
                            $("#" + root).find("span[data-span='active_" + dataId + "']").removeClass("show").addClass("hide");
                        } else {
                            $(item).attr("data-status", active);
                            $(item).find("i").removeClass("icon-eye4").addClass("icon-eye5");
                            $("#" + root).find("span[data-span='active_" + dataId + "']").removeClass("hide").addClass("show");
                            $("#" + root).find("span[data-span='inactive_" + dataId + "']").removeClass("show").addClass("hide");
                        }
                        common.notify.showSuccess(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },
    onClickSearch: function () {
        $(document).on("click", "#btnSearch", function () {
            danhSachDieuChuyenQuyetToanModule.onClickPaging(1);
        });
    }
}

function ModuleAction(moduleActionId) {
    this.ModuleActionId = moduleActionId;
}