﻿

var inActive = 0;
var active = 1;

var pKHKeHoachNhapKhoThanhPhamDetailModule = {
    init: function () {
        pKHKeHoachNhapKhoThanhPhamDetailModule.openConfirmDelete();
        pKHKeHoachNhapKhoThanhPhamDetailModule.onClickActive();
        pKHKeHoachNhapKhoThanhPhamDetailModule.onClickSearch();
        pKHKeHoachNhapKhoThanhPhamDetailModule.onClickSorting();
        pKHKeHoachNhapKhoThanhPhamDetailModule.initCheckpKHKeHoachNhapKhoThanhPhamDetail();
		pKHKeHoachNhapKhoThanhPhamDetailModule.openPopup();
    },
    initCheckpKHKeHoachNhapKhoThanhPhamDetail: function () {
        var checkAll = $(".checkedAll");
        checkAll.each(function (index, element) {
            var rootParent = element.id;

            var isChecked = $(".pKHKeHoachNhapKhoThanhPhamDetail-item").find("input[data-root='" + rootParent + "']").val();
            if ("" != isChecked && (isChecked === "1" || isChecked === 1)) {
                $(element).prop('checked', true);
            } else {
                $(element).prop('checked', false);
            }
        });
    },
	openPopup: function () {
        $(document).on("click", "a.dialog", function () {
            var url = $(this).attr('href');
            var title = $(this).attr('title');
            var dialog = $('<div style="display:none"></div>').appendTo('body');

            dialog.load(url,
                function (responseText, textStatus, XMLHttpRequest) {
                    $.validator.unobtrusive.parse(this);
                    dialog.dialog({
                        modal: true,
                        title: title,
                        width: ($(window).width() * 0.8),
                        resizable: false,
                        close: function (event, ui) {
                            dialog.remove();
                        }
                    });
                });
            return false;
        });
    },
    openConfirmDelete: function () {
        $(document).on("click", ".icon-remove4", function () {
            var dataId = $(this).attr("data-id");
            var root = $("#pKHKeHoachNhapKhoThanhPhamDetail_modal_info").find(".confirm-yes");
            $(root).attr("onclick", pKHKeHoachNhapKhoThanhPhamDetailModule.executeDelete);
            $(root).attr("data-id", dataId);

            common.modal.show("pKHKeHoachNhapKhoThanhPhamDetail_modal_info");
            pKHKeHoachNhapKhoThanhPhamDetailModule.executeDelete();
        });
    },
    
    executeDelete: function () {
        $(document).on("click", "#btn_pKHKeHoachNhapKhoThanhPhamDetail", function () {
            var dataId = $(this).attr("data-id");
            $.ajax({
                url: "/PKHKeHoachNhapKhoThanhPhamDetail/Delete",
                type: "POST",
                data: { PKHKeHoachNhapKhoThanhPhamDetailId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        location.href = "/PKHKeHoachNhapKhoThanhPhamDetail/Index";
                    } else {
                        common.notify.showError(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },
    onClickPaging: function (currentPage, sortColumn, sortDirection) {
        var textSearch = $("#TextSearch").val();
        $.ajax({
            url: "/PKHKeHoachNhapKhoThanhPhamDetail/Search",
            data: {
                currentPage: currentPage, textSearch: textSearch,
                sortColumn: sortColumn, sortDirection: sortDirection
            },
            type: "GET",
            success: function (response) {
                if (response.IsError === false && (undefined != response.HTML && "" !== response.HTML)) {
                    $("#main-pKHKeHoachNhapKhoThanhPhamDetail").html("");
                    $("#main-pKHKeHoachNhapKhoThanhPhamDetail").html(response.HTML);
                }
            }
        });
    },
    onClickSorting: function () {
        $(document).on("click", ".sorting,.sorting_asc,.sorting_desc", function () {
            var sortColumn = $(this).attr("data-column");
            var sortDirection;
            var sortDirectionValue;
            if ($(this).hasClass("sorting_asc")) {
                $(this).removeClass("sorting_asc");
                sortDirection = "sorting_desc";
                sortDirectionValue = 1;
            } else {
                $(this).removeClass("sorting_desc");
                sortDirection = "sorting_asc";
                sortDirectionValue = 0;
            }

            $(this).addClass(sortDirection);
            pKHKeHoachNhapKhoThanhPhamDetailModule.onClickPaging(1, sortColumn, sortDirectionValue);
        });
    },
    onClickActive: function () {
        $(document).on("click", ".active", function () {
            var root = $(this).attr("data-parent");
            var item = $(this);
            var dataId = $(item).attr("data-id");
            var status = $(item).attr("data-status");
            $.ajax({
                url: "/PKHKeHoachNhapKhoThanhPhamDetail/Invisibe",
                type: "POST",
                data: { PKHKeHoachNhapKhoThanhPhamDetailId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        if (status === "True" || parseInt(status) === active) {
                            $(item).attr("data-status", inActive);
                            $(item).find("i").removeClass("icon-eye5").addClass("icon-eye4");
                            $("#" + root).find("span[data-span='inactive_" + dataId + "']").removeClass("hide").addClass("show");
                            $("#" + root).find("span[data-span='active_" + dataId + "']").removeClass("show").addClass("hide");
                        } else {
                            $(item).attr("data-status", active);
                            $(item).find("i").removeClass("icon-eye4").addClass("icon-eye5");
                            $("#" + root).find("span[data-span='active_" + dataId + "']").removeClass("hide").addClass("show");
                            $("#" + root).find("span[data-span='inactive_" + dataId + "']").removeClass("show").addClass("hide");
                        }
                        common.notify.showSuccess(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },
    onClickSearch: function () {
        $(document).on("click", "#btnSearch", function () {
            pKHKeHoachNhapKhoThanhPhamDetailModule.onClickPaging(1);
        });
    }
}

function ModuleAction(moduleActionId) {
    this.ModuleActionId = moduleActionId;
}