﻿

var inActive = 0;
var active = 1;

var pKHKeHoachXuongDetailModule = {
    init: function () {
        pKHKeHoachXuongDetailModule.openConfirmDelete();
        //pKHKeHoachXuongDetailModule.onClickActive();
        pKHKeHoachXuongDetailModule.onClickSearch();
        pKHKeHoachXuongDetailModule.onClickSorting();
        pKHKeHoachXuongDetailModule.initCheckpKHKeHoachXuongDetail();
        pKHKeHoachXuongDetailModule.openPopup();
        pKHKeHoachXuongDetailModule.createKhsxDetail();
        //pKHKeHoachXuongDetailModule.loadKHXuongDetail();
        pKHKeHoachXuongDetailModule.loadKHXuongDetailAction();
        pKHKeHoachXuongDetailModule.onClickEditKeHoachXuongChiTiet();
        pKHKeHoachXuongDetailModule.UpdateKhsxDetail();
    },
    initCheckpKHKeHoachXuongDetail: function () {
        var checkAll = $(".checkedAll");
        checkAll.each(function (index, element) {
            var rootParent = element.id;

            var isChecked = $(".pKHKeHoachXuongDetail-item").find("input[data-root='" + rootParent + "']").val();
            if ("" != isChecked && (isChecked === "1" || isChecked === 1)) {
                $(element).prop('checked', true);
            } else {
                $(element).prop('checked', false);
            }
        });
    },
	openPopup: function () {
        $(document).on("click", "a.dialog", function () {
            var url = $(this).attr('href');
            var title = $(this).attr('title');
            var dialog = $('<div style="display:none"></div>').appendTo('body');

            dialog.load(url,
                function (responseText, textStatus, XMLHttpRequest) {
                    $.validator.unobtrusive.parse(this);
                    dialog.dialog({
                        modal: true,
                        title: title,
                        width: ($(window).width() * 0.8),
                        resizable: false,
                        close: function (event, ui) {
                            dialog.remove();
                        }
                    });
                });
            return false;
        });
    },
    openConfirmDelete: function () {
        $(document).on("click", ".icon-remove4", function () {
            var dataId = $(this).attr("data-id");
            var root = $("#pKHKeHoachXuongDetail_modal_info").find(".confirm-yes");
            $(root).attr("onclick", pKHKeHoachXuongDetailModule.executeDelete);
            $(root).attr("data-id", dataId);

            common.modal.show("pKHKeHoachXuongDetail_modal_info");
            pKHKeHoachXuongDetailModule.executeDelete();
        });
    },
    createKhsxDetail: function () {
        $(document).on("click", "#createKhsxDetail", function () {
            var dataId = $(this).attr("data-id");
            $.ajax({
                url: "/PKHKeHoachXuongDetail/createKhsxDetail",
                type: "POST",
                data: { PKHKeHoachXuongDetailId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        location.href = "/PKHKeHoachXuongDetail/Index";
                    } else {
                        common.notify.showError(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },

    UpdateKhsxDetail: function () {
        $(document).on("click", "#updatekhsxDetail", function () {
            var maKHXuongChiTiet = $("#PKHKeHoachSXNKXHDetailId").val();
            var maVatTu = $("#MaVatTu").val();
            var soLuong = $("#YeuCau").val();
            var ngayCapHang = $("#NgayCapHangValue").val();
            var ghiChu = $("#GhiChu").val();
            //Validator
            $(".callout-danger").hide();
            $("#YeuCau").parent().find(".field-validation-error").hide();
            $("#NgayCapHangValue").parent().find(".field-validation-error").hide();
            if (maKHXuongChiTiet == null || maKHXuongChiTiet == "" || maVatTu == null || maVatTu == "")
            {
                $(".callout-danger").show();
                return;
            }
            if (soLuong == null || soLuong == "")
            {
                $("#YeuCau").parent().find(".field-validation-error").show();
                return;
            }
            if (ngayCapHang == null || ngayCapHang == "") {
                $("#NgayCapHangValue").parent().find(".field-validation-error").show();
                return;
            }

            $.ajax({
                url: "/PKHKeHoachXuongDetail/UpdateKhsxDetail",
                type: "POST",
                data: { maKHXuongChiTiet: maKHXuongChiTiet, maVatTu: maVatTu, soLuong: soLuong, ngayCapHang: ngayCapHang, ghiChu: ghiChu },
                success: function (data) {
                    if (data.IsError === false) {
                        window.location.reload(false);
                    } else {
                        common.notify.showError(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },
    
    executeDelete: function () {
        $(document).on("click", "#btn_pKHKeHoachXuongDetail", function () {
            var dataId = $(this).attr("data-id");
            $.ajax({
                url: "/PKHKeHoachXuongDetail/Delete",
                type: "POST",
                data: { PKHKeHoachXuongDetailId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        location.href = window.location.href;
                    } else {
                        common.notify.showError(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },
    loadKHXuongDetailAction: function () {
        $(document).on("click", ".khxuongdetail", function () {
            var dataId = $(this).attr("data-id");
            var dataClass = $(this).attr("data-content");
            pKHKeHoachXuongDetailModule.loadKHXuongDetail(dataId, dataClass);
        });
    },
    loadKHXuongDetail: function (dataId, dataClass) {
        var dataContent = "#" + dataClass;
        $.ajax({
            url: "/PKHKeHoachXuongDetail/GetKHXDetailByKhXuong",
            data: { keHoachXuongId: dataId },
            success: function (data) {
                if (data.IsError === false && (undefined != data.HTML && "" !== data.HTML)) {
                    //location.href = "/PKHKeHoachXuongDetail/Index";
                    $(dataContent).html("");
                    $(dataContent).html(data.HTML);
                } else {
                    //common.notify.showError(data.Message);
                }
            },
            error: function () {
                console.log("Err");
            }
        });
    },
    onClickPaging: function (currentPage, sortColumn, sortDirection) {
        var textSearch = $("#TextSearch").val();
        $.ajax({
            url: "/PKHKeHoachXuongDetail/Search",
            data: {
                currentPage: currentPage, textSearch: textSearch,
                sortColumn: sortColumn, sortDirection: sortDirection
            },
            type: "GET",
            success: function (response) {
                if (response.IsError === false && (undefined != response.HTML && "" !== response.HTML)) {
                    $("#main-pKHKeHoachXuongDetail").html("");
                    $("#main-pKHKeHoachXuongDetail").html(response.HTML);
                }
            }
        });
    },
    onClickSorting: function () {
        $(document).on("click", ".sorting,.sorting_asc,.sorting_desc", function () {
            var sortColumn = $(this).attr("data-column");
            var sortDirection;
            var sortDirectionValue;
            if ($(this).hasClass("sorting_asc")) {
                $(this).removeClass("sorting_asc");
                sortDirection = "sorting_desc";
                sortDirectionValue = 1;
            } else {
                $(this).removeClass("sorting_desc");
                sortDirection = "sorting_asc";
                sortDirectionValue = 0;
            }

            $(this).addClass(sortDirection);
            pKHKeHoachXuongDetailModule.onClickPaging(1, sortColumn, sortDirectionValue);
        });
    },
    onClickActive: function () {
        $(document).on("click", ".active", function () {
            var root = $(this).attr("data-parent");
            var item = $(this);
            var dataId = $(item).attr("data-id");
            var status = $(item).attr("data-status");
            $.ajax({
                url: "/PKHKeHoachXuongDetail/Invisibe",
                type: "POST",
                data: { PKHKeHoachXuongDetailId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        if (status === "True" || parseInt(status) === active) {
                            $(item).attr("data-status", inActive);
                            $(item).find("i").removeClass("icon-eye5").addClass("icon-eye4");
                            $("#" + root).find("span[data-span='inactive_" + dataId + "']").removeClass("hide").addClass("show");
                            $("#" + root).find("span[data-span='active_" + dataId + "']").removeClass("show").addClass("hide");
                        } else {
                            $(item).attr("data-status", active);
                            $(item).find("i").removeClass("icon-eye4").addClass("icon-eye5");
                            $("#" + root).find("span[data-span='active_" + dataId + "']").removeClass("hide").addClass("show");
                            $("#" + root).find("span[data-span='inactive_" + dataId + "']").removeClass("show").addClass("hide");
                        }
                        common.notify.showSuccess(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },
    onClickEditKeHoachXuongChiTiet: function () {
        $(document).on("click", ".editkehoach", function () {
            var maKHXuongChiTiet = $(this).attr("data-id");
            var maVatTu = $(this).attr("data-vattu");
            var soLuong = $(this).attr("data-soluong");
            var ngayCapHang = $(this).attr("data-date");
            var ghiChu = $(this).attr("data-note");
            //var item = $(this);
            $(".callout-danger").hide();
            $("#PKHKeHoachSXNKXHDetailId").val(maKHXuongChiTiet);
            $("#YeuCau").val(soLuong);
            $("#MaVatTu").val(maVatTu);
            $("#NgayCapHangValue").val(ngayCapHang);
            $("#GhiChu").val(ghiChu);
            $("#YeuCau").focus();
        });
    },

    onClickSearch: function () {
        $(document).on("click", "#btnSearch", function () {
            pKHKeHoachXuongDetailModule.onClickPaging(1);
        });
    }
}

function ModuleAction(moduleActionId) {
    this.ModuleActionId = moduleActionId;
}