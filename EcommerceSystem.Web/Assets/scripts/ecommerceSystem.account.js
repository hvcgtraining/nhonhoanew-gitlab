﻿//$(function () {
//    accountModule.init();
//});

var inActive = 0;
var active = 1;

var accountModule = {
    init: function () {
        accountModule.openConfirmDelete();
        accountModule.onClickActive();
        accountModule.onClickSearch();
        accountModule.openChangePassword();
        accountModule.changePassword();
    },

    openChangePassword: function () {
        $(document).on("click", "#changePassword", function () {
            $("#change-pass").css({ "display": "block" });
            common.modal.show("changePass_modal_info");
        });
    },

    changePassword: function () {
        $(document).on("click", "#btn_changePassword", function () {
            var form = $("#frmChangePass").serialize();
            $.ajax({
                url: "/Account/ChangePassword",
                type: "POST",
                data: form,
                success: function (data) {
                    if (data.IsError === false) {
                        common.notify.showSuccess(data.Message);
                        $(".btn-warning").click();
                    } else {
                        if ("" != data.Message && undefined != data.Message) {
                            common.notify.showError(data.Message);
                        } else {
                            $(".modal-backdrop").remove();
                            $("#change-pass").html("");
                            $("#change-pass").html(data.HTML);
                            $("#change-pass").css({ "display": "block" });
                            $.validator.unobtrusive.parse($('frmChangePass'));
                            $("#changePassword").click();
                        }
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },

    openConfirmDelete: function () {
        $(document).on("click", ".btn_remove_account", function () {
            var dataId = $(this).attr("data-id");
            var root = $("#account_modal_info").find(".confirm-yes");
            $(root).attr("onclick", accountModule.executeDelete);
            $(root).attr("data-id", dataId);

            common.modal.show("account_modal_info");
            accountModule.executeDelete();
        });
    },

    executeDelete: function () {
        $(document).on("click", "#btn_account", function () {
            var dataId = $(this).attr("data-id");
            $.ajax({
                url: "/Account/Delete",
                type: "POST",
                data: { userId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        location.href = "/Account/Index";
                    } else {
                        common.notify.showError(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },

    onClickPaging: function (currentPage, sortColumn, sortDirection) {
        var textSearch = $("#UserName").val();
        $.ajax({
            url: "/Account/Search",
            data: {
                currentPage: currentPage, textSearch: textSearch,
                sortColumn: sortColumn, sortDirection: sortDirection
            },
            type: "GET",
            success: function (response) {
                if (response.IsError === false && (undefined != response.HTML && "" !== response.HTML)) {
                    $("#main-account").html("");
                    $("#main-account").html(response.HTML);
                }
            }
        });
    },

    onClickActive: function () {
        $(document).on("click", ".btn_active_account", function () {
            var root = $(this).attr("data-parent");
            var item = $(this);
            var dataId = $(item).attr("data-id");
            var status = $(item).attr("data-status");
            $.ajax({
                url: "/Account/Invisibe",
                type: "POST",
                data: { userId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        if (status === "True" || parseInt(status) === active) {
                            $(item).attr("data-status", inActive);
                            $(item).find("i").removeClass("icon-eye5").addClass("icon-eye4");
                            $("#" + root).find("span[data-span='inactive_" + dataId + "']").removeClass("hide").addClass("show");
                            $("#" + root).find("span[data-span='active_" + dataId + "']").removeClass("show").addClass("hide");
                        } else {
                            $(item).attr("data-status", active);
                            $(item).find("i").removeClass("icon-eye4").addClass("icon-eye5");
                            $("#" + root).find("span[data-span='active_" + dataId + "']").removeClass("hide").addClass("show");
                            $("#" + root).find("span[data-span='inactive_" + dataId + "']").removeClass("show").addClass("hide");
                        }
                        common.notify.showSuccess(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },

    onClickSearch: function () {
        $(document).on("click", "#btnSearch_account", function () {
            accountModule.onClickPaging(1);
        });
    }
}