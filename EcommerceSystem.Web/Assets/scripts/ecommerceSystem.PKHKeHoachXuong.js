﻿

var inActive = 0;
var active = 1;

var pKHKeHoachXuongModule = {
    init: function () {
        pKHKeHoachXuongModule.openConfirmDelete();
        pKHKeHoachXuongModule.onClickActive();
        pKHKeHoachXuongModule.onClickSearch();
        pKHKeHoachXuongModule.onClickSorting();
        pKHKeHoachXuongModule.initCheckpKHKeHoachXuong();
		pKHKeHoachXuongModule.openPopup();
    },
    initCheckpKHKeHoachXuong: function () {
        var checkAll = $(".checkedAll");
        checkAll.each(function (index, element) {
            var rootParent = element.id;

            var isChecked = $(".pKHKeHoachXuong-item").find("input[data-root='" + rootParent + "']").val();
            if ("" != isChecked && (isChecked === "1" || isChecked === 1)) {
                $(element).prop('checked', true);
            } else {
                $(element).prop('checked', false);
            }
        });
    },
	openPopup: function () {
        $(document).on("click", "a.dialog", function () {
            var url = $(this).attr('href');
            var title = $(this).attr('title');
            var dialog = $('<div style="display:none"></div>').appendTo('body');

            dialog.load(url,
                function (responseText, textStatus, XMLHttpRequest) {
                    $.validator.unobtrusive.parse(this);
                    dialog.dialog({
                        modal: true,
                        title: title,
                        width: ($(window).width() * 0.8),
                        resizable: false,
                        close: function (event, ui) {
                            dialog.remove();
                        }
                    });
                });
            return false;
        });
    },
    openConfirmDelete: function () {
        $(document).on("click", ".icon-remove4", function () {
            var dataId = $(this).attr("data-id");
            var root = $("#pKHKeHoachXuong_modal_info").find(".confirm-yes");
            $(root).attr("onclick", pKHKeHoachXuongModule.executeDelete);
            $(root).attr("data-id", dataId);

            common.modal.show("pKHKeHoachXuong_modal_info");
            pKHKeHoachXuongModule.executeDelete();
        });
    },
    
    executeDelete: function () {
        $(document).on("click", "#btn_pKHKeHoachXuong", function () {
            var dataId = $(this).attr("data-id");
            $.ajax({
                url: "/PKHKeHoachXuong/Delete",
                type: "POST",
                data: { PKHKeHoachXuongId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        location.href = "/PKHKeHoachXuong/Index";
                    } else {
                        common.notify.showError(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },
    onClickPaging: function (currentPage, sortColumn, sortDirection) {
        var textSearch = $("#TextSearch").val();
        var textSearch = $("#StartDate").val();
        var textSearch = $("#EndDate").val();
        var textSearch = $("#XuongId").val();
        $.ajax({
            url: "/PKHKeHoachXuong/Search",
            data: {
                currentPage: currentPage, textSearch: textSearch, StartDate: StartDate, EndDate: EndDate, XuongId: XuongId,
                sortColumn: sortColumn, sortDirection: sortDirection
            },
            type: "GET",
            success: function (response) {
                if (response.IsError === false && (undefined != response.HTML && "" !== response.HTML)) {
                    $("#main-pKHKeHoachXuong").html("");
                    $("#main-pKHKeHoachXuong").html(response.HTML);
                }
            }
        });
    },
    onClickSorting: function () {
        $(document).on("click", ".sorting,.sorting_asc,.sorting_desc", function () {
            var sortColumn = $(this).attr("data-column");
            var sortDirection;
            var sortDirectionValue;
            if ($(this).hasClass("sorting_asc")) {
                $(this).removeClass("sorting_asc");
                sortDirection = "sorting_desc";
                sortDirectionValue = 1;
            } else {
                $(this).removeClass("sorting_desc");
                sortDirection = "sorting_asc";
                sortDirectionValue = 0;
            }

            $(this).addClass(sortDirection);
            pKHKeHoachXuongModule.onClickPaging(1, sortColumn, sortDirectionValue);
        });
    },
    onClickActive: function () {
        $(document).on("click", ".active", function () {
            var root = $(this).attr("data-parent");
            var item = $(this);
            var dataId = $(item).attr("data-id");
            var status = $(item).attr("data-status");
            $.ajax({
                url: "/PKHKeHoachXuong/Invisibe",
                type: "POST",
                data: { PKHKeHoachXuongId: dataId },
                success: function (data) {
                    if (data.IsError === false) {
                        if (status === "True" || parseInt(status) === active) {
                            $(item).attr("data-status", inActive);
                            $(item).find("i").removeClass("icon-eye5").addClass("icon-eye4");
                            $("#" + root).find("span[data-span='inactive_" + dataId + "']").removeClass("hide").addClass("show");
                            $("#" + root).find("span[data-span='active_" + dataId + "']").removeClass("show").addClass("hide");
                        } else {
                            $(item).attr("data-status", active);
                            $(item).find("i").removeClass("icon-eye4").addClass("icon-eye5");
                            $("#" + root).find("span[data-span='active_" + dataId + "']").removeClass("hide").addClass("show");
                            $("#" + root).find("span[data-span='inactive_" + dataId + "']").removeClass("show").addClass("hide");
                        }
                        common.notify.showSuccess(data.Message);
                    }
                },
                error: function () {
                    console.log("Err");
                }
            });
        });
    },
    onClickSearch: function () {
        $(document).on("click", "#btnSearch", function () {
            pKHKeHoachXuongModule.onClickPaging(1);
        });
    }
}

function ModuleAction(moduleActionId) {
    this.ModuleActionId = moduleActionId;
}