﻿var system_msg_success = '<div class="alert alert-success system-msg"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><h4>Success!</h4> {0}</div>';
var system_msg_error = '<div class="alert alert-danger system-msg"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><h4>Error!</h4> {0}</div>';
function showMessage(content, type) {
    var msg = '';
    if (type == 'success') {
        if (content == undefined || content == '') {
            content = 'Save successful.!';
        }
        msg = system_msg_success.replace('{0}', content);
    }
    else if (type == 'error') {
        if (content == undefined || content == '') {
            content = 'Error!';
        }
        msg = system_msg_error.replace('{0}', content);
    }
    $('body').append(msg);
    $('body').animate({
        scrollTop: 0
    }, '500',
        function () { } // callback method use this space how you like
    );

    setTimeout(function () {
        $('.system-msg').fadeOut('slow');
    }, 8000);

    setTimeout(function () {
        $('.system-msg').remove();
    }, 8000);

    //$.jGrowl(content, { sticky: true, theme: 'growl-' + type, header: 'Success!' });
}

function SetTimeoutHide(item) {
    setTimeout(function () {
        item.fadeOut('slow');
    }, 1000);

    setTimeout(function () {
        item.hide();
    }, 2000);
}

var autoCompleteJsonValue = [];
function CheckExistItemAutoComplete(item) {
    var ret = false;
    autoCompleteJsonValue.some(function (entry, i) {
        if (entry.IdNumber == item.IdNumber) {
            ret = true;
            return true;
        }
    });
    return ret;
}

function GetAutoCompleteJsonValue() {
    return autoCompleteJsonValue;
}

function SetAutoCompletedEmployee(idinput) {
    $("#" + idinput).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Dashboard/AutocompleteSuggestions",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    //alert(JSON.stringify(data));
                    response($.map(data, function (item) {
                        if (!CheckExistItemAutoComplete(item)) {
                            autoCompleteJsonValue.push(item);
                        }

                        return {
                            //label: item.IdNumber, value: item.IdNumber
                            IdNumber: item.IdNumber,
                            ImageUrl: item.ImageUrl,
                            Name: item.Name,
                            NickName: item.NickName,
                            Department: item.Department,
                            value: item.IdNumber,
                            json: item
                        };
                    }))

                }
            })
        },
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        // here return item for autocomplete text box, Here is the place 
        // where we can modify data as we want to show as autocomplete item
        return $("<li>")
        .append("<table><tr><td><img src='" + item.ImageUrl + "' alt='" + item.Name + "' width='40' height='40'></td><td style='padding-left:10px'>Name:" + item.Name + " -   ID:" + item.IdNumber + "</td></tr></table>").appendTo(ul);
    };
}

function ShowLoading() {
    $('body').append('<div id="show_loadind_form" class="overlay"><div class="opacity"></div><i class="icon-spinner3 spin"></i></div>');
    $('.overlay').fadeIn(150);
}

function EndLoading() {
    $('.overlay').remove();
}


function ShowModalInfo(title,content) {
    $('#modal_info').find('.modal-title-text').html(title);
    $('#modal_info').find('.modal-content-text').html(content);
    $('.modal-info')[0].click();
}

$(document).ajaxError(function () {
    window.location.href = "/PageInfo/Error?query=timeout-error";
});

console.log('hi');
/*Show/Hidden btn*/

var btnShow = document.querySelector("#btnShow");
var addValueForm = document.querySelector("#addValueForm");


btnShow.addEventListener("click", function(){
	addValueForm.classList.toggle("show-form");
})


/*Show/Hidden btn*/

