﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models;
using EcommerceSystem.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceSystem.Services
{
    public static class Mapper
    {
        #region Mapping User
        public static User MapToEntity(this UserModel model)
        {
            var user = new User();
            user.Email = model.Email;
            user.Password = model.Password;
            user.UserName = model.UserName;
            user.CreatedDate = model.CreatedDate;
            user.IsLockedOut = model.IsLockedOut;
            user.IsSupperAdmin = model.IsSupperAdmin;
            user.Status = model.Status;
            return user;
        }
        public static User MapToEntity(this UserModel model, User entity)
        {
            entity.Email = model.Email;
            entity.Password = model.Password;
            return entity;
        }
        public static UserModel MapToModel(this User entity)
        {
            var user = new UserModel();
            user.UserId = entity.UserId;
            user.UserName = entity.UserName;
            user.Email = entity.Email;
            user.CreatedDate = entity.CreatedDate;
            user.Status = entity.Status;
            user.Password = entity.Password;
            user.RoleId = entity.UserRoles.Select(x => x.RoleId).FirstOrDefault();
            user.RoleName = entity.UserRoles.Select(x => x.Role.Name).FirstOrDefault();

            return user;
        }
        public static List<User> MapToEntities(this List<UserModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<UserModel> MapToModels(this List<User> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion

        #region Configuration
        public static ConfigurationModel MaptoModel(this Configuration entity)
        {
            return new ConfigurationModel
            {
                AdminAcceptanceBalancePercentage = entity.AdminAcceptanceBalancePercentage,
                ClientAcceptanceBalancePercentage = entity.ClientAcceptanceBalancePercentage,
                ExchangeRate = entity.ExchangeRate ?? 0
            };
        }
        #endregion

        #region Notification
        public static NotificationModel MaptoModel(this Notification entity)
        {
            return new NotificationModel
            {
                NotificationId = entity.NotificationId,
                Title = entity.Title,
                Message = entity.Note,
                Type = entity.Type,
                CreatedDate = entity.CreatedDate,
                IsActive = entity.IsActive
            };
        }
        public static List<NotificationModel> MaptoModels(this List<Notification> entities)
        {
            return entities != null ? entities.Select(x => x.MaptoModel()).ToList() : null;
        }

        public static NotificationModel MaptoModel(this NotificationRecipient entity)
        {
            return new NotificationModel
            {
                NotificationId = entity.NotificationId,
                Title = entity.Notification.Title,
                Message = entity.Notification != null ? entity.Notification.Note : string.Empty,
                Type = entity.Notification != null ? entity.Notification.Type : 0,
                CreatedDate = entity.CreatedDate,
                IsActive = entity.Notification != null ? entity.Notification.IsActive : false,
                IsRead = entity.IsRead,
                NotificationRecipientId = entity.NotificationRecipientId
            };
        }
        public static List<NotificationModel> MaptoModels(this List<NotificationRecipient> entities)
        {
            return entities != null ? entities.Select(x => x.MaptoModel()).ToList() : null;
        }
        #endregion
    }
}
