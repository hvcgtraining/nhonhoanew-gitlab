﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.LoaiHinhKinhDoanhModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class LoaiHinhKinhDoanhMapper
    {
        #region Mapping LoaiHinhKinhDoanh
        public static LoaiHinhKinhDoanhModel MapToModel(this LoaiHinhKinhDoanh entity)
        {
            return new LoaiHinhKinhDoanhModel
            {
				LoaiHinhKinhDoanhId = entity.LoaiHinhKinhDoanhId,
				Title = entity.Title,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static LoaiHinhKinhDoanhModel MapToModel(this LoaiHinhKinhDoanh entity, LoaiHinhKinhDoanhModel model)
        {
			model.LoaiHinhKinhDoanhId = entity.LoaiHinhKinhDoanhId;
			model.Title = entity.Title;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static LoaiHinhKinhDoanh MapToEntity(this LoaiHinhKinhDoanhModel model)
        {
            return new LoaiHinhKinhDoanh
            {
				LoaiHinhKinhDoanhId = model.LoaiHinhKinhDoanhId,
				Title = model.Title,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static LoaiHinhKinhDoanh MapToEntity(this LoaiHinhKinhDoanhModel model, LoaiHinhKinhDoanh entity)
        {
			entity.LoaiHinhKinhDoanhId = model.LoaiHinhKinhDoanhId;
			entity.Title = model.Title;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<LoaiHinhKinhDoanh> MapToEntities(this List<LoaiHinhKinhDoanhModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<LoaiHinhKinhDoanhModel> MapToModels(this List<LoaiHinhKinhDoanh> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
