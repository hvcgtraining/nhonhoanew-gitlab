﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.NhomYeuCauModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class NhomYeuCauMapper
    {
        #region Mapping NhomYeuCau
        public static NhomYeuCauModel MapToModel(this NhomYeuCau entity)
        {
            return new NhomYeuCauModel
            {
				NhomYeuCauId = entity.NhomYeuCauId,
				Title = entity.Title,
				GhiChu = entity.GhiChu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static NhomYeuCauModel MapToModel(this NhomYeuCau entity, NhomYeuCauModel model)
        {
			model.NhomYeuCauId = entity.NhomYeuCauId;
			model.Title = entity.Title;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static NhomYeuCau MapToEntity(this NhomYeuCauModel model)
        {
            return new NhomYeuCau
            {
				NhomYeuCauId = model.NhomYeuCauId,
				Title = model.Title,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static NhomYeuCau MapToEntity(this NhomYeuCauModel model, NhomYeuCau entity)
        {
			entity.NhomYeuCauId = model.NhomYeuCauId;
			entity.Title = model.Title;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<NhomYeuCau> MapToEntities(this List<NhomYeuCauModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<NhomYeuCauModel> MapToModels(this List<NhomYeuCau> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
