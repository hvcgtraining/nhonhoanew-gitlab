﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.DoiMaVatTuModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class DoiMaVatTuMapper
    {
        #region Mapping DoiMaVatTu
        public static DoiMaVatTuModel MapToModel(this DoiMaVatTu entity)
        {
            return new DoiMaVatTuModel
            {
				DoiMaVatTuId = entity.DoiMaVatTuId,
				MaLoaiVatTu = entity.MaLoaiVatTu,
				Title = entity.Title,
				Cap = entity.Cap,
				GhiChu = entity.GhiChu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static DoiMaVatTuModel MapToModel(this DoiMaVatTu entity, DoiMaVatTuModel model)
        {
			model.DoiMaVatTuId = entity.DoiMaVatTuId;
			model.MaLoaiVatTu = entity.MaLoaiVatTu;
			model.Title = entity.Title;
			model.Cap = entity.Cap;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static DoiMaVatTu MapToEntity(this DoiMaVatTuModel model)
        {
            return new DoiMaVatTu
            {
				DoiMaVatTuId = model.DoiMaVatTuId,
				MaLoaiVatTu = model.MaLoaiVatTu,
				Title = model.Title,
				Cap = model.Cap,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static DoiMaVatTu MapToEntity(this DoiMaVatTuModel model, DoiMaVatTu entity)
        {
			entity.DoiMaVatTuId = model.DoiMaVatTuId;
			entity.MaLoaiVatTu = model.MaLoaiVatTu;
			entity.Title = model.Title;
			entity.Cap = model.Cap;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<DoiMaVatTu> MapToEntities(this List<DoiMaVatTuModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<DoiMaVatTuModel> MapToModels(this List<DoiMaVatTu> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
