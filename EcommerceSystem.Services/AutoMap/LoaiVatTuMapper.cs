﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.LoaiVatTuModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class LoaiVatTuMapper
    {
        #region Mapping LoaiVatTu
        public static LoaiVatTuModel MapToModel(this LoaiVatTu entity)
        {
            return new LoaiVatTuModel
            {
				LoaiVatTuId = entity.LoaiVatTuId,
				MaLoaiVatTu = entity.MaLoaiVatTu,
				Title = entity.Title,
				Cap = entity.Cap,
                Type = entity.Type,
				GhiChu = entity.GhiChu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static LoaiVatTuModel MapToModel(this LoaiVatTu entity, LoaiVatTuModel model)
        {
			model.LoaiVatTuId = entity.LoaiVatTuId;
			model.MaLoaiVatTu = entity.MaLoaiVatTu;
			model.Title = entity.Title;
			model.Cap = entity.Cap;
            model.Type = entity.Type;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static LoaiVatTu MapToEntity(this LoaiVatTuModel model)
        {
            return new LoaiVatTu
            {
				LoaiVatTuId = model.LoaiVatTuId,
				MaLoaiVatTu = model.MaLoaiVatTu,
				Title = model.Title,
				Cap = model.Cap,
                Type = model.Type,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static LoaiVatTu MapToEntity(this LoaiVatTuModel model, LoaiVatTu entity)
        {
			entity.LoaiVatTuId = model.LoaiVatTuId;
			entity.MaLoaiVatTu = model.MaLoaiVatTu;
			entity.Title = model.Title;
			entity.Cap = model.Cap;
            entity.Type = model.Type;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<LoaiVatTu> MapToEntities(this List<LoaiVatTuModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<LoaiVatTuModel> MapToModels(this List<LoaiVatTu> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
