﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.LyDoXuatModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class LyDoXuatMapper
    {
        #region Mapping LyDoXuat
        public static LyDoXuatModel MapToModel(this LyDoXuat entity)
        {
            return new LyDoXuatModel
            {
				LyDoXuatId = entity.LyDoXuatId,
				MaLyDoXuat = entity.MaLyDoXuat,
				Title = entity.Title,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static LyDoXuatModel MapToModel(this LyDoXuat entity, LyDoXuatModel model)
        {
			model.LyDoXuatId = entity.LyDoXuatId;
			model.MaLyDoXuat = entity.MaLyDoXuat;
			model.Title = entity.Title;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static LyDoXuat MapToEntity(this LyDoXuatModel model)
        {
            return new LyDoXuat
            {
				LyDoXuatId = model.LyDoXuatId,
				MaLyDoXuat = model.MaLyDoXuat,
				Title = model.Title,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static LyDoXuat MapToEntity(this LyDoXuatModel model, LyDoXuat entity)
        {
			entity.LyDoXuatId = model.LyDoXuatId;
			entity.MaLyDoXuat = model.MaLyDoXuat;
			entity.Title = model.Title;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<LyDoXuat> MapToEntities(this List<LyDoXuatModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<LyDoXuatModel> MapToModels(this List<LyDoXuat> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
