﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.KhoModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class KhoMapper
    {
        #region Mapping Kho
        public static KhoModel MapToModel(this Kho entity)
        {
            return new KhoModel
            {
				KhoId = entity.KhoId,
				MaKho = entity.MaKho,
				Title = entity.Title,
				NguoiGiao = entity.NguoiGiao,
				ThongTinKho = entity.ThongTinKho,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static KhoModel MapToModel(this Kho entity, KhoModel model)
        {
			model.KhoId = entity.KhoId;
			model.MaKho = entity.MaKho;
			model.Title = entity.Title;
			model.NguoiGiao = entity.NguoiGiao;
			model.ThongTinKho = entity.ThongTinKho;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static Kho MapToEntity(this KhoModel model)
        {
            return new Kho
            {
				KhoId = model.KhoId,
				MaKho = model.MaKho,
				Title = model.Title,
				NguoiGiao = model.NguoiGiao,
				ThongTinKho = model.ThongTinKho,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static Kho MapToEntity(this KhoModel model, Kho entity)
        {
			entity.KhoId = model.KhoId;
			entity.MaKho = model.MaKho;
			entity.Title = model.Title;
			entity.NguoiGiao = model.NguoiGiao;
			entity.ThongTinKho = model.ThongTinKho;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<Kho> MapToEntities(this List<KhoModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<KhoModel> MapToModels(this List<Kho> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
