﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHKeHoachXuatKhauModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHKeHoachXuatKhauMapper
    {
        #region Mapping PKHKeHoachXuatKhau
        public static PKHKeHoachXuatKhauModel MapToModel(this PKHKeHoachXuatKhau entity)
        {
            return new PKHKeHoachXuatKhauModel
            {
				PKHKeHoachXuatKhauId = entity.PKHKeHoachXuatKhauId,
				Title = entity.Title,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,
				MaHopDong = entity.MaHopDong,
				SoHopDong = entity.SoHopDong,
				DanhMucKhachHangId = entity.DanhMucKhachHangId,
				SoLuong = entity.SoLuong,
				NgayXep = entity.NgayXep,
				NgayNhap = entity.NgayNhap,
				NgayCP = entity.NgayCP,
				NgaySon = entity.NgaySon,
				NgayMS = entity.NgayMS,
				NgayBB = entity.NgayBB,
				NgayTuChinh = entity.NgayTuChinh,
				Lan = entity.Lan,
				NgayHopDong = entity.NgayHopDong,
				NgayXuat = entity.NgayXuat,
				NgayLapPhieu = entity.NgayLapPhieu,
				Tong = entity.Tong,

            };
        }
        public static PKHKeHoachXuatKhauModel MapToModel(this PKHKeHoachXuatKhau entity, PKHKeHoachXuatKhauModel model)
        {
			model.PKHKeHoachXuatKhauId = entity.PKHKeHoachXuatKhauId;
			model.Title = entity.Title;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;
			model.MaHopDong = entity.MaHopDong;
			model.SoHopDong = entity.SoHopDong;
			model.DanhMucKhachHangId = entity.DanhMucKhachHangId;
			model.SoLuong = entity.SoLuong;
			model.NgayXep = entity.NgayXep;
			model.NgayNhap = entity.NgayNhap;
			model.NgayCP = entity.NgayCP;
			model.NgaySon = entity.NgaySon;
			model.NgayMS = entity.NgayMS;
			model.NgayBB = entity.NgayBB;
			model.NgayTuChinh = entity.NgayTuChinh;
			model.Lan = entity.Lan;
			model.NgayHopDong = entity.NgayHopDong;
			model.NgayXuat = entity.NgayXuat;
			model.NgayLapPhieu = entity.NgayLapPhieu;
			model.Tong = entity.Tong;

            return model;
        }
        public static PKHKeHoachXuatKhau MapToEntity(this PKHKeHoachXuatKhauModel model)
        {
            return new PKHKeHoachXuatKhau
            {
				PKHKeHoachXuatKhauId = model.PKHKeHoachXuatKhauId,
				Title = model.Title,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,
				MaHopDong = model.MaHopDong,
				SoHopDong = model.SoHopDong,
				DanhMucKhachHangId = model.DanhMucKhachHangId,
				SoLuong = model.SoLuong,
				NgayXep = model.NgayXep,
				NgayNhap = model.NgayNhap,
				NgayCP = model.NgayCP,
				NgaySon = model.NgaySon,
				NgayMS = model.NgayMS,
				NgayBB = model.NgayBB,
				NgayTuChinh = model.NgayTuChinh,
				Lan = model.Lan,
				NgayHopDong = model.NgayHopDong,
				NgayXuat = model.NgayXuat,
				NgayLapPhieu = model.NgayLapPhieu,
				Tong = model.Tong,

            };
        }
        public static PKHKeHoachXuatKhau MapToEntity(this PKHKeHoachXuatKhauModel model, PKHKeHoachXuatKhau entity)
        {
			entity.PKHKeHoachXuatKhauId = model.PKHKeHoachXuatKhauId;
			entity.Title = model.Title;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;
			entity.MaHopDong = model.MaHopDong;
			entity.SoHopDong = model.SoHopDong;
			entity.DanhMucKhachHangId = model.DanhMucKhachHangId;
			entity.SoLuong = model.SoLuong;
			entity.NgayXep = model.NgayXep;
			entity.NgayNhap = model.NgayNhap;
			entity.NgayCP = model.NgayCP;
			entity.NgaySon = model.NgaySon;
			entity.NgayMS = model.NgayMS;
			entity.NgayBB = model.NgayBB;
			entity.NgayTuChinh = model.NgayTuChinh;
			entity.Lan = model.Lan;
			entity.NgayHopDong = model.NgayHopDong;
			entity.NgayXuat = model.NgayXuat;
			entity.NgayLapPhieu = model.NgayLapPhieu;
			entity.Tong = model.Tong;

            return entity;
        }
        public static List<PKHKeHoachXuatKhau> MapToEntities(this List<PKHKeHoachXuatKhauModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHKeHoachXuatKhauModel> MapToModels(this List<PKHKeHoachXuatKhau> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
