﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.KhoiLuongCoPhanModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class KhoiLuongCoPhanMapper
    {
        #region Mapping KhoiLuongCoPhan
        public static KhoiLuongCoPhanModel MapToModel(this KhoiLuongCoPhan entity)
        {
            return new KhoiLuongCoPhanModel
            {
				KhoiLuongCoPhanId = entity.KhoiLuongCoPhanId,
				MaKhoiLuongCoPhan = entity.MaKhoiLuongCoPhan,
				Title = entity.Title,
				KhoiLuong = entity.KhoiLuong,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static KhoiLuongCoPhanModel MapToModel(this KhoiLuongCoPhan entity, KhoiLuongCoPhanModel model)
        {
			model.KhoiLuongCoPhanId = entity.KhoiLuongCoPhanId;
			model.MaKhoiLuongCoPhan = entity.MaKhoiLuongCoPhan;
			model.Title = entity.Title;
			model.KhoiLuong = entity.KhoiLuong;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static KhoiLuongCoPhan MapToEntity(this KhoiLuongCoPhanModel model)
        {
            return new KhoiLuongCoPhan
            {
				KhoiLuongCoPhanId = model.KhoiLuongCoPhanId,
				MaKhoiLuongCoPhan = model.MaKhoiLuongCoPhan,
				Title = model.Title,
				KhoiLuong = model.KhoiLuong,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static KhoiLuongCoPhan MapToEntity(this KhoiLuongCoPhanModel model, KhoiLuongCoPhan entity)
        {
			entity.KhoiLuongCoPhanId = model.KhoiLuongCoPhanId;
			entity.MaKhoiLuongCoPhan = model.MaKhoiLuongCoPhan;
			entity.Title = model.Title;
			entity.KhoiLuong = model.KhoiLuong;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<KhoiLuongCoPhan> MapToEntities(this List<KhoiLuongCoPhanModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<KhoiLuongCoPhanModel> MapToModels(this List<KhoiLuongCoPhan> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
