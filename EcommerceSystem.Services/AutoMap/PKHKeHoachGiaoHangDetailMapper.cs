﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHKeHoachGiaoHangDetailModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHKeHoachGiaoHangDetailMapper
    {
        #region Mapping PKHKeHoachGiaoHangDetail
        public static PKHKeHoachGiaoHangDetailModel MapToModel(this PKHKeHoachGiaoHangDetail entity)
        {
            return new PKHKeHoachGiaoHangDetailModel
            {
				PKHKeHoachGiaoHangDetailId = entity.PKHKeHoachGiaoHangDetailId,
				PKHKeHoachGiaoHangId = entity.PKHKeHoachGiaoHangId,
				LoaiVatTuId = entity.LoaiVatTuId,
				ChiTietVatTuId = entity.ChiTietVatTuId,
				XuongId = entity.XuongId,
				Thu2 = entity.Thu2,
				Thu3 = entity.Thu3,
				Thu4 = entity.Thu4,
				Thu5 = entity.Thu5,
				Thu6 = entity.Thu6,
				Thu7 = entity.Thu7,
				ChuNhat = entity.ChuNhat,
				TongCong = entity.TongCong,
				TongSoCanChuyen = entity.TongSoCanChuyen,
				GhiChu = entity.GhiChu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static PKHKeHoachGiaoHangDetailModel MapToModel(this PKHKeHoachGiaoHangDetail entity, PKHKeHoachGiaoHangDetailModel model)
        {
			model.PKHKeHoachGiaoHangDetailId = entity.PKHKeHoachGiaoHangDetailId;
			model.PKHKeHoachGiaoHangId = entity.PKHKeHoachGiaoHangId;
			model.LoaiVatTuId = entity.LoaiVatTuId;
			model.ChiTietVatTuId = entity.ChiTietVatTuId;
			model.XuongId = entity.XuongId;
			model.Thu2 = entity.Thu2;
			model.Thu3 = entity.Thu3;
			model.Thu4 = entity.Thu4;
			model.Thu5 = entity.Thu5;
			model.Thu6 = entity.Thu6;
			model.Thu7 = entity.Thu7;
			model.ChuNhat = entity.ChuNhat;
			model.TongCong = entity.TongCong;
			model.TongSoCanChuyen = entity.TongSoCanChuyen;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static PKHKeHoachGiaoHangDetail MapToEntity(this PKHKeHoachGiaoHangDetailModel model)
        {
            return new PKHKeHoachGiaoHangDetail
            {
				PKHKeHoachGiaoHangDetailId = model.PKHKeHoachGiaoHangDetailId,
				PKHKeHoachGiaoHangId = model.PKHKeHoachGiaoHangId,
				LoaiVatTuId = model.LoaiVatTuId,
				ChiTietVatTuId = model.ChiTietVatTuId,
				XuongId = model.XuongId,
				Thu2 = model.Thu2,
				Thu3 = model.Thu3,
				Thu4 = model.Thu4,
				Thu5 = model.Thu5,
				Thu6 = model.Thu6,
				Thu7 = model.Thu7,
				ChuNhat = model.ChuNhat,
				TongCong = model.TongCong,
				TongSoCanChuyen = model.TongSoCanChuyen,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHKeHoachGiaoHangDetail MapToEntity(this PKHKeHoachGiaoHangDetailModel model, PKHKeHoachGiaoHangDetail entity)
        {
			entity.PKHKeHoachGiaoHangDetailId = model.PKHKeHoachGiaoHangDetailId;
			entity.PKHKeHoachGiaoHangId = model.PKHKeHoachGiaoHangId;
			entity.LoaiVatTuId = model.LoaiVatTuId;
			entity.ChiTietVatTuId = model.ChiTietVatTuId;
			entity.XuongId = model.XuongId;
			entity.Thu2 = model.Thu2;
			entity.Thu3 = model.Thu3;
			entity.Thu4 = model.Thu4;
			entity.Thu5 = model.Thu5;
			entity.Thu6 = model.Thu6;
			entity.Thu7 = model.Thu7;
			entity.ChuNhat = model.ChuNhat;
			entity.TongCong = model.TongCong;
			entity.TongSoCanChuyen = model.TongSoCanChuyen;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHKeHoachGiaoHangDetail> MapToEntities(this List<PKHKeHoachGiaoHangDetailModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHKeHoachGiaoHangDetailModel> MapToModels(this List<PKHKeHoachGiaoHangDetail> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
