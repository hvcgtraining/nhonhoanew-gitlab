﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.GoiDauModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class GoiDauMapper
    {
        #region Mapping GoiDau
        public static GoiDauModel MapToModel(this GoiDau entity)
        {
            return new GoiDauModel
            {
                GoiDauId = entity.GoiDauId,
                Title = entity.Title,
                VatTuId = entity.VatTuId,
                NgayGoiDau = entity.NgayGoiDau,
                TuanDuPhong = entity.TuanDuPhong,
                PKHKeHoachSXNKXHId = entity.PKHKeHoachSXNKXHId,
                MaKeHoach = entity.MaKeHoach,
                GhiChu = entity.GhiChu,
                CreatedDate = entity.CreatedDate,
                UpdatedDate = entity.UpdatedDate,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                Status = entity.Status,
                MaLoaiVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.MaChiTietVatTu : string.Empty,
                TenLoaiVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.Title : string.Empty,
                TenPKHKeHoachSXNKXH = (entity.PKHKeHoachSXNKXH != null) ? entity.PKHKeHoachSXNKXH.Title : string.Empty,

            };
        }
        public static GoiDauModel MapToModel(this GoiDau entity, GoiDauModel model)
        {
            model.GoiDauId = entity.GoiDauId;
            model.Title = entity.Title;
            model.VatTuId = entity.VatTuId;
            model.NgayGoiDau = entity.NgayGoiDau;
            model.TuanDuPhong = entity.TuanDuPhong;
            model.PKHKeHoachSXNKXHId = entity.PKHKeHoachSXNKXHId;
            model.MaKeHoach = entity.MaKeHoach;
            model.GhiChu = entity.GhiChu;
            model.CreatedDate = entity.CreatedDate;
            model.UpdatedDate = entity.UpdatedDate;
            model.CreatedBy = entity.CreatedBy;
            model.UpdatedBy = entity.UpdatedBy;
            model.Status = entity.Status;
            model.MaLoaiVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.MaChiTietVatTu : string.Empty;
            model.TenLoaiVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.Title : string.Empty;
            model.TenPKHKeHoachSXNKXH = (entity.PKHKeHoachSXNKXH != null) ? entity.PKHKeHoachSXNKXH.Title : string.Empty;

            return model;
        }
        public static GoiDau MapToEntity(this GoiDauModel model)
        {
            return new GoiDau
            {
                GoiDauId = model.GoiDauId,
                Title = model.Title,
                VatTuId = model.VatTuId,
                NgayGoiDau = model.NgayGoiDau,
                TuanDuPhong = model.TuanDuPhong,
                PKHKeHoachSXNKXHId = model.PKHKeHoachSXNKXHId,
                MaKeHoach = model.MaKeHoach,
                GhiChu = model.GhiChu,
                CreatedDate = model.CreatedDate,
                UpdatedDate = model.UpdatedDate,
                CreatedBy = model.CreatedBy,
                UpdatedBy = model.UpdatedBy,
                Status = model.Status,

            };
        }
        public static GoiDau MapToEntity(this GoiDauModel model, GoiDau entity)
        {
            entity.GoiDauId = model.GoiDauId;
            entity.Title = model.Title;
            entity.VatTuId = model.VatTuId;
            entity.NgayGoiDau = model.NgayGoiDau;
            entity.TuanDuPhong = model.TuanDuPhong;
            entity.PKHKeHoachSXNKXHId = model.PKHKeHoachSXNKXHId;
            entity.MaKeHoach = model.MaKeHoach;
            entity.GhiChu = model.GhiChu;
            //entity.CreatedDate = model.CreatedDate;
            entity.UpdatedDate = model.UpdatedDate;
            //entity.CreatedBy = model.CreatedBy;
            entity.UpdatedBy = model.UpdatedBy;
            entity.Status = model.Status;

            return entity;
        }
        public static List<GoiDau> MapToEntities(this List<GoiDauModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<GoiDauModel> MapToModels(this List<GoiDau> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
