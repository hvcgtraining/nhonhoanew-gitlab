﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHDuKienGiaoHangModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHDuKienGiaoHangMapper
    {
        #region Mapping PKHDuKienGiaoHang
        public static PKHDuKienGiaoHangModel MapToModel(this PKHDuKienGiaoHang entity)
        {
            return new PKHDuKienGiaoHangModel
            {
                PKHDuKienGiaoHangId = entity.PKHDuKienGiaoHangId,
                PKHPhieuYeuCauMuaHangId = entity.PKHPhieuYeuCauMuaHangId,
                PKHPhieuYeuCauMuaHangDetailId = entity.PKHPhieuYeuCauMuaHangDetailId,
                Title = entity.Title,
                NgayDuKien = entity.NgayDuKien,
                SoLuongGiaoDuKien = entity.SoLuongGiaoDuKien,
                TongSoLuong = entity.TongSoLuong,
                CreatedDate = entity.CreatedDate,
                UpdatedDate = entity.UpdatedDate,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                Status = entity.Status,
                MaChiTietVatTu = (entity.PKHPhieuYeuCauMuaHangDetail != null) ? entity.PKHPhieuYeuCauMuaHangDetail.ChiTietVatTu.MaChiTietVatTu : string.Empty,

            };
        }
        public static PKHDuKienGiaoHangModel MapToModel(this PKHDuKienGiaoHang entity, PKHDuKienGiaoHangModel model)
        {
			model.PKHDuKienGiaoHangId = entity.PKHDuKienGiaoHangId;
			model.PKHPhieuYeuCauMuaHangId = entity.PKHPhieuYeuCauMuaHangId;
			model.PKHPhieuYeuCauMuaHangDetailId = entity.PKHPhieuYeuCauMuaHangDetailId;
			model.Title = entity.Title;
			model.NgayDuKien = entity.NgayDuKien;
			model.SoLuongGiaoDuKien = entity.SoLuongGiaoDuKien;
			model.TongSoLuong = entity.TongSoLuong;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static PKHDuKienGiaoHang MapToEntity(this PKHDuKienGiaoHangModel model)
        {
            return new PKHDuKienGiaoHang
            {
				PKHDuKienGiaoHangId = model.PKHDuKienGiaoHangId,
				PKHPhieuYeuCauMuaHangId = model.PKHPhieuYeuCauMuaHangId,
				PKHPhieuYeuCauMuaHangDetailId = model.PKHPhieuYeuCauMuaHangDetailId,
				Title = model.Title,
				NgayDuKien = model.NgayDuKien,
				SoLuongGiaoDuKien = model.SoLuongGiaoDuKien,
				TongSoLuong = model.TongSoLuong,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHDuKienGiaoHang MapToEntity(this PKHDuKienGiaoHangModel model, PKHDuKienGiaoHang entity)
        {
			entity.PKHDuKienGiaoHangId = model.PKHDuKienGiaoHangId;
			entity.PKHPhieuYeuCauMuaHangId = model.PKHPhieuYeuCauMuaHangId;
			entity.PKHPhieuYeuCauMuaHangDetailId = model.PKHPhieuYeuCauMuaHangDetailId;
			entity.Title = model.Title;
			entity.NgayDuKien = model.NgayDuKien;
			entity.SoLuongGiaoDuKien = model.SoLuongGiaoDuKien;
			entity.TongSoLuong = model.TongSoLuong;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHDuKienGiaoHang> MapToEntities(this List<PKHDuKienGiaoHangModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHDuKienGiaoHangModel> MapToModels(this List<PKHDuKienGiaoHang> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
