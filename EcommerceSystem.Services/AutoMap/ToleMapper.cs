﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.ToleModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class ToleMapper
    {
        #region Mapping Tole
        public static ToleModel MapToModel(this Tole entity)
        {
            return new ToleModel
            {
                ToleId = entity.ToleId,
                MaTole = entity.MaTole,
                Title = entity.Title,
                DonVi = entity.DonVi,
                Day = entity.Day,
                Ngang = entity.Ngang,
                Dung = entity.Dung,
                KhoiLuong = entity.KhoiLuong,
                DechetId = entity.DechetId,
                GhiChu = entity.GhiChu,
                CreatedDate = entity.CreatedDate,
                UpdatedDate = entity.UpdatedDate,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                Status = entity.Status,
                DechetName = (entity.Dechet != null) ? entity.Dechet.Title : string.Empty,

            };
        }
        public static ToleModel MapToModel(this Tole entity, ToleModel model)
        {
            model.ToleId = entity.ToleId;
            model.MaTole = entity.MaTole;
            model.Title = entity.Title;
            model.DonVi = entity.DonVi;
            model.Day = entity.Day;
            model.Ngang = entity.Ngang;
            model.Dung = entity.Dung;
            model.KhoiLuong = entity.KhoiLuong;
            model.DechetId = entity.DechetId;
            model.GhiChu = entity.GhiChu;
            model.CreatedDate = entity.CreatedDate;
            model.UpdatedDate = entity.UpdatedDate;
            model.CreatedBy = entity.CreatedBy;
            model.UpdatedBy = entity.UpdatedBy;
            model.Status = entity.Status;
            model.DechetName = entity.Dechet.Title;
            return model;
        }
        public static Tole MapToEntity(this ToleModel model)
        {
            return new Tole
            {
                ToleId = model.ToleId,
                MaTole = model.MaTole,
                Title = model.Title,
                DonVi = model.DonVi,
                Day = model.Day,
                Ngang = model.Ngang,
                Dung = model.Dung,
                KhoiLuong = model.KhoiLuong,
                DechetId = model.DechetId,
                GhiChu = model.GhiChu,
                CreatedDate = model.CreatedDate,
                UpdatedDate = model.UpdatedDate,
                CreatedBy = model.CreatedBy,
                UpdatedBy = model.UpdatedBy,
                Status = model.Status,

            };
        }
        public static Tole MapToEntity(this ToleModel model, Tole entity)
        {
            entity.ToleId = model.ToleId;
            entity.MaTole = model.MaTole;
            entity.Title = model.Title;
            entity.DonVi = model.DonVi;
            entity.Day = model.Day;
            entity.Ngang = model.Ngang;
            entity.Dung = model.Dung;
            entity.KhoiLuong = model.KhoiLuong;
            entity.DechetId = model.DechetId;
            entity.GhiChu = model.GhiChu;
            //entity.CreatedDate = model.CreatedDate;
            entity.UpdatedDate = model.UpdatedDate;
            //entity.CreatedBy = model.CreatedBy;
            entity.UpdatedBy = model.UpdatedBy;
            entity.Status = model.Status;

            return entity;
        }
        public static List<Tole> MapToEntities(this List<ToleModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<ToleModel> MapToModels(this List<Tole> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
