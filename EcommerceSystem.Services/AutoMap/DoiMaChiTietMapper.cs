﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.DoiMaChiTietModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class DoiMaChiTietMapper
    {
        #region Mapping DoiMaChiTiet
        public static DoiMaChiTietModel MapToModel(this DoiMaChiTiet entity)
        {
            return new DoiMaChiTietModel
            {
				DoiMaChiTietId = entity.DoiMaChiTietId,
				Title = entity.Title,
				MaChiTietVatTu = entity.MaChiTietVatTu,
				DonVi = entity.DonVi,
				GhiChu = entity.GhiChu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static DoiMaChiTietModel MapToModel(this DoiMaChiTiet entity, DoiMaChiTietModel model)
        {
			model.DoiMaChiTietId = entity.DoiMaChiTietId;
			model.Title = entity.Title;
			model.MaChiTietVatTu = entity.MaChiTietVatTu;
			model.DonVi = entity.DonVi;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static DoiMaChiTiet MapToEntity(this DoiMaChiTietModel model)
        {
            return new DoiMaChiTiet
            {
				DoiMaChiTietId = model.DoiMaChiTietId,
				Title = model.Title,
				MaChiTietVatTu = model.MaChiTietVatTu,
				DonVi = model.DonVi,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static DoiMaChiTiet MapToEntity(this DoiMaChiTietModel model, DoiMaChiTiet entity)
        {
			entity.DoiMaChiTietId = model.DoiMaChiTietId;
			entity.Title = model.Title;
			entity.MaChiTietVatTu = model.MaChiTietVatTu;
			entity.DonVi = model.DonVi;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<DoiMaChiTiet> MapToEntities(this List<DoiMaChiTietModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<DoiMaChiTietModel> MapToModels(this List<DoiMaChiTiet> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
