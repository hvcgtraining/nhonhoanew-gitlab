﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHKeHoachGiaoHangModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHKeHoachGiaoHangMapper
    {
        #region Mapping PKHKeHoachGiaoHang
        public static PKHKeHoachGiaoHangModel MapToModel(this PKHKeHoachGiaoHang entity)
        {
            return new PKHKeHoachGiaoHangModel
            {
                PKHKeHoachGiaoHangId = entity.PKHKeHoachGiaoHangId,
                PKHKeHoachXuongId = entity.PKHKeHoachXuongId,
                PKHKeHoachSXNKXHId = entity.PKHKeHoachSXNKXHId,
                MaTuan = entity.MaTuan,
                StartDate = entity.StartDate,
                EndDate = entity.EndDate,
                XuongId = entity.XuongId,
                TinhTrang = entity.TinhTrang,
                GhiChu = entity.GhiChu,
                CreatedDate = entity.CreatedDate,
                UpdatedDate = entity.UpdatedDate,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                Status = entity.Status,
                XuongName = (entity.Xuong != null) ? entity.Xuong.Title : string.Empty,
                KeHoachXuongName = (entity.PKHKeHoachXuong != null) ? entity.PKHKeHoachXuong.Title : string.Empty,
                KeHoachSXNKXHName=(entity.PKHKeHoachSXNKXH!=null)?entity.PKHKeHoachSXNKXH.Title:string.Empty,
            };
        }
        public static PKHKeHoachGiaoHangModel MapToModel(this PKHKeHoachGiaoHang entity, PKHKeHoachGiaoHangModel model)
        {
			model.PKHKeHoachGiaoHangId = entity.PKHKeHoachGiaoHangId;
			model.PKHKeHoachXuongId = entity.PKHKeHoachXuongId;
			model.PKHKeHoachSXNKXHId = entity.PKHKeHoachSXNKXHId;
			model.MaTuan = entity.MaTuan;
			model.StartDate = entity.StartDate;
			model.EndDate = entity.EndDate;
			model.XuongId = entity.XuongId;
			model.TinhTrang = entity.TinhTrang;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;
            model.XuongName = entity.Xuong.Title;
            model.KeHoachXuongName = entity.PKHKeHoachXuong.Title;
            model.KeHoachSXNKXHName = entity.PKHKeHoachSXNKXH.Title;
            return model;
        }
        public static PKHKeHoachGiaoHang MapToEntity(this PKHKeHoachGiaoHangModel model)
        {
            return new PKHKeHoachGiaoHang
            {
				PKHKeHoachGiaoHangId = model.PKHKeHoachGiaoHangId,
				PKHKeHoachXuongId = model.PKHKeHoachXuongId,
				PKHKeHoachSXNKXHId = model.PKHKeHoachSXNKXHId,
				MaTuan = model.MaTuan,
				StartDate = model.StartDate,
				EndDate = model.EndDate,
				XuongId = model.XuongId,
				TinhTrang = model.TinhTrang,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHKeHoachGiaoHang MapToEntity(this PKHKeHoachGiaoHangModel model, PKHKeHoachGiaoHang entity)
        {
			entity.PKHKeHoachGiaoHangId = model.PKHKeHoachGiaoHangId;
			entity.PKHKeHoachXuongId = model.PKHKeHoachXuongId;
			entity.PKHKeHoachSXNKXHId = model.PKHKeHoachSXNKXHId;
			entity.MaTuan = model.MaTuan;
			entity.StartDate = model.StartDate;
			entity.EndDate = model.EndDate;
			entity.XuongId = model.XuongId;
			entity.TinhTrang = model.TinhTrang;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHKeHoachGiaoHang> MapToEntities(this List<PKHKeHoachGiaoHangModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHKeHoachGiaoHangModel> MapToModels(this List<PKHKeHoachGiaoHang> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
