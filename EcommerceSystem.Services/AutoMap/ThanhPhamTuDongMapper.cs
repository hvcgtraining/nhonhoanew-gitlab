﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.ThanhPhamTuDongModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class ThanhPhamTuDongMapper
    {
        #region Mapping ThanhPhamTuDong
        public static ThanhPhamTuDongModel MapToModel(this ThanhPhamTuDong entity)
        {
            return new ThanhPhamTuDongModel
            {
				ThanhPhamTuDongId = entity.ThanhPhamTuDongId,
				MaCoPhan = entity.MaCoPhan,
				Title = entity.Title,
				GhiChu = entity.GhiChu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static ThanhPhamTuDongModel MapToModel(this ThanhPhamTuDong entity, ThanhPhamTuDongModel model)
        {
			model.ThanhPhamTuDongId = entity.ThanhPhamTuDongId;
			model.MaCoPhan = entity.MaCoPhan;
			model.Title = entity.Title;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static ThanhPhamTuDong MapToEntity(this ThanhPhamTuDongModel model)
        {
            return new ThanhPhamTuDong
            {
				ThanhPhamTuDongId = model.ThanhPhamTuDongId,
				MaCoPhan = model.MaCoPhan,
				Title = model.Title,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static ThanhPhamTuDong MapToEntity(this ThanhPhamTuDongModel model, ThanhPhamTuDong entity)
        {
			entity.ThanhPhamTuDongId = model.ThanhPhamTuDongId;
			entity.MaCoPhan = model.MaCoPhan;
			entity.Title = model.Title;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<ThanhPhamTuDong> MapToEntities(this List<ThanhPhamTuDongModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<ThanhPhamTuDongModel> MapToModels(this List<ThanhPhamTuDong> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
