﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHDieuChuyenVatTuModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHDieuChuyenVatTuMapper
    {
        #region Mapping PKHDieuChuyenVatTu
        public static PKHDieuChuyenVatTuModel MapToModel(this PKHDieuChuyenVatTu entity)
        {
            return new PKHDieuChuyenVatTuModel
            {
                PKHDieuChuyenVatTuId = entity.PKHDieuChuyenVatTuId,
                PKHKeHoachXuongId = entity.PKHKeHoachXuongId,
                PKHKeHoachSXNKXHId = entity.PKHKeHoachSXNKXHId,
                NgayLap = entity.NgayLap,
                SoChungTu = entity.SoChungTu,
                LyDoXuatId = entity.LyDoXuatId,
                LyDoXuat = entity.LyDoXuat,
                LoaiDieuChuyen = entity.LoaiDieuChuyen,
                KhoXuatId = entity.KhoXuatId,
                KhoNhapId = entity.KhoNhapId,
                XuongXuatId = entity.XuongXuatId,
                XuongNhapId = entity.XuongNhapId,
                NguoiNhanId = entity.NguoiNhanId,
                NguoiNhan = entity.NguoiNhan,
                NguoiGiaoId = entity.NguoiGiaoId,
                NguoiGiao = entity.NguoiGiao,
                GhiChu = entity.GhiChu,
                TinhTrang = entity.TinhTrang,
                CreatedDate = entity.CreatedDate,
                UpdatedDate = entity.UpdatedDate,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                Status = entity.Status,
                PKHKeHoachXuongName = (entity.PKHKeHoachXuong != null) ? entity.PKHKeHoachXuong.Title : string.Empty,
                PKHKeHoachSXNKXHName = (entity.PKHKeHoachSXNKXH != null) ? entity.PKHKeHoachSXNKXH.Title : string.Empty,
                LyDoXuatName=(entity.LyDoXuat1!=null)?entity.LyDoXuat1.Title:string.Empty,
                KhoXuatName = (entity.Kho != null) ? entity.Kho.Title : string.Empty,
                KhoNhapName = (entity.Kho1 != null) ? entity.Kho1.Title : string.Empty,
                XuongXuatName = (entity.Xuong != null) ? entity.Xuong.Title : string.Empty,
                XuongNhapName = (entity.Xuong1 != null) ? entity.Xuong1.Title : string.Empty,
                NguoiNhapName = (entity.NguoiNhanKho != null) ? entity.NguoiNhanKho.Title : string.Empty,
                NguoiGiaoName=(entity.NguoiNhanKho1!=null)?entity.NguoiNhanKho1.Title:string.Empty,
            };
        }
        public static PKHDieuChuyenVatTuModel MapToModel(this PKHDieuChuyenVatTu entity, PKHDieuChuyenVatTuModel model)
        {
			model.PKHDieuChuyenVatTuId = entity.PKHDieuChuyenVatTuId;
			model.PKHKeHoachXuongId = entity.PKHKeHoachXuongId;
			model.PKHKeHoachSXNKXHId = entity.PKHKeHoachSXNKXHId;
			model.NgayLap = entity.NgayLap;
			model.SoChungTu = entity.SoChungTu;
			model.LyDoXuatId = entity.LyDoXuatId;
			model.LyDoXuat = entity.LyDoXuat;
			model.LoaiDieuChuyen = entity.LoaiDieuChuyen;
			model.KhoXuatId = entity.KhoXuatId;
			model.KhoNhapId = entity.KhoNhapId;
			model.XuongXuatId = entity.XuongXuatId;
			model.XuongNhapId = entity.XuongNhapId;
			model.NguoiNhanId = entity.NguoiNhanId;
			model.NguoiNhan = entity.NguoiNhan;
			model.NguoiGiaoId = entity.NguoiGiaoId;
			model.NguoiGiao = entity.NguoiGiao;
			model.GhiChu = entity.GhiChu;
			model.TinhTrang = entity.TinhTrang;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;
            model.PKHKeHoachXuongName = entity.PKHKeHoachXuong.Title;
            model.PKHKeHoachSXNKXHName = entity.PKHKeHoachSXNKXH.Title;
            model.LyDoXuatName = entity.LyDoXuat1.Title;
            model.KhoXuatName = entity.Kho.Title;
            model.KhoNhapName = entity.Kho1.Title;
            model.XuongXuatName = entity.Xuong.Title;
            model.XuongNhapName = entity.Xuong1.Title;
            model.NguoiNhapName = entity.NguoiNhanKho.Title;
            model.NguoiGiaoName = entity.NguoiNhanKho1.Title;
            return model;
        }
        public static PKHDieuChuyenVatTu MapToEntity(this PKHDieuChuyenVatTuModel model)
        {
            return new PKHDieuChuyenVatTu
            {
				PKHDieuChuyenVatTuId = model.PKHDieuChuyenVatTuId,
				PKHKeHoachXuongId = model.PKHKeHoachXuongId,
				PKHKeHoachSXNKXHId = model.PKHKeHoachSXNKXHId,
				NgayLap = model.NgayLap,
				SoChungTu = model.SoChungTu,
				LyDoXuatId = model.LyDoXuatId,
				LyDoXuat = model.LyDoXuat,
				LoaiDieuChuyen = model.LoaiDieuChuyen,
				KhoXuatId = model.KhoXuatId,
				KhoNhapId = model.KhoNhapId,
				XuongXuatId = model.XuongXuatId,
				XuongNhapId = model.XuongNhapId,
				NguoiNhanId = model.NguoiNhanId,
				NguoiNhan = model.NguoiNhan,
				NguoiGiaoId = model.NguoiGiaoId,
				NguoiGiao = model.NguoiGiao,
				GhiChu = model.GhiChu,
				TinhTrang = model.TinhTrang,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHDieuChuyenVatTu MapToEntity(this PKHDieuChuyenVatTuModel model, PKHDieuChuyenVatTu entity)
        {
			entity.PKHDieuChuyenVatTuId = model.PKHDieuChuyenVatTuId;
			entity.PKHKeHoachXuongId = model.PKHKeHoachXuongId;
			entity.PKHKeHoachSXNKXHId = model.PKHKeHoachSXNKXHId;
			entity.NgayLap = model.NgayLap;
			entity.SoChungTu = model.SoChungTu;
			entity.LyDoXuatId = model.LyDoXuatId;
			entity.LyDoXuat = model.LyDoXuat;
			entity.LoaiDieuChuyen = model.LoaiDieuChuyen;
			entity.KhoXuatId = model.KhoXuatId;
			entity.KhoNhapId = model.KhoNhapId;
			entity.XuongXuatId = model.XuongXuatId;
			entity.XuongNhapId = model.XuongNhapId;
			entity.NguoiNhanId = model.NguoiNhanId;
			entity.NguoiNhan = model.NguoiNhan;
			entity.NguoiGiaoId = model.NguoiGiaoId;
			entity.NguoiGiao = model.NguoiGiao;
			entity.GhiChu = model.GhiChu;
			entity.TinhTrang = model.TinhTrang;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHDieuChuyenVatTu> MapToEntities(this List<PKHDieuChuyenVatTuModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHDieuChuyenVatTuModel> MapToModels(this List<PKHDieuChuyenVatTu> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
