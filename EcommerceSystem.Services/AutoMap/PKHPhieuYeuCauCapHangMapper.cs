﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHPhieuYeuCauCapHangModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHPhieuYeuCauCapHangMapper
    {
        #region Mapping PKHPhieuYeuCauCapHang
        public static PKHPhieuYeuCauCapHangModel MapToModel(this PKHPhieuYeuCauCapHang entity)
        {
            return new PKHPhieuYeuCauCapHangModel
            {
				PKHPhieuYeuCauCapHangId = entity.PKHPhieuYeuCauCapHangId,
				KeHoachXuongId = entity.KeHoachXuongId,
				PKHKeHoachSXNKXHId = entity.PKHKeHoachSXNKXHId,
				SoCT = entity.SoCT,
				NgayLap = entity.NgayLap,
				LoaiVatTuId = entity.LoaiVatTuId,
				SoLuong = entity.SoLuong != null ? entity.SoLuong : 0,
				NguoiNhanId = entity.NguoiNhanId,
				NguoiNhan = entity.NguoiNhan,
				NguoiGiaoId = entity.NguoiGiaoId,
				NguoiGiao = entity.NguoiGiao,
				LyDoXuatId = entity.LyDoXuatId,
				LyDoXuat = entity.LyDoXuat,
				SoDieuChuyen = entity.SoDieuChuyen,
				TrangThai = entity.TrangThai,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,
				Title = entity.Title,
				KhoId = entity.KhoId,

                MaTP = entity.LoaiVatTu != null ? entity.LoaiVatTu.MaLoaiVatTu : "",
                TenTP = entity.LoaiVatTu != null ? entity.LoaiVatTu.Title : "",
                Kho = entity.Kho != null ? entity.Kho.Title : "",
            };
        }
        public static PKHPhieuYeuCauCapHangModel MapToModel(this PKHPhieuYeuCauCapHang entity, PKHPhieuYeuCauCapHangModel model)
        {
			model.PKHPhieuYeuCauCapHangId = entity.PKHPhieuYeuCauCapHangId;
			model.KeHoachXuongId = entity.KeHoachXuongId;
			model.PKHKeHoachSXNKXHId = entity.PKHKeHoachSXNKXHId;
			model.SoCT = entity.SoCT;
			model.NgayLap = entity.NgayLap;
			model.LoaiVatTuId = entity.LoaiVatTuId;
			model.SoLuong = entity.SoLuong;
			model.NguoiNhanId = entity.NguoiNhanId;
			model.NguoiNhan = entity.NguoiNhan;
			model.NguoiGiaoId = entity.NguoiGiaoId;
			model.NguoiGiao = entity.NguoiGiao;
			model.LyDoXuatId = entity.LyDoXuatId;
			model.LyDoXuat = entity.LyDoXuat;
			model.SoDieuChuyen = entity.SoDieuChuyen;
			model.TrangThai = entity.TrangThai;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;
			model.Title = entity.Title;
			model.KhoId = entity.KhoId;

            return model;
        }
        public static PKHPhieuYeuCauCapHang MapToEntity(this PKHPhieuYeuCauCapHangModel model)
        {
            return new PKHPhieuYeuCauCapHang
            {
				PKHPhieuYeuCauCapHangId = model.PKHPhieuYeuCauCapHangId,
				KeHoachXuongId = model.KeHoachXuongId,
				PKHKeHoachSXNKXHId = model.PKHKeHoachSXNKXHId,
				SoCT = model.SoCT,
				NgayLap = model.NgayLap,
				LoaiVatTuId = model.LoaiVatTuId,
				SoLuong = model.SoLuong,
				NguoiNhanId = model.NguoiNhanId,
				NguoiNhan = model.NguoiNhan,
				NguoiGiaoId = model.NguoiGiaoId,
				NguoiGiao = model.NguoiGiao,
				LyDoXuatId = model.LyDoXuatId,
				LyDoXuat = model.LyDoXuat,
				SoDieuChuyen = model.SoDieuChuyen,
				TrangThai = model.TrangThai,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,
				Title = model.Title,
				KhoId = model.KhoId,

            };
        }
        public static PKHPhieuYeuCauCapHang MapToEntity(this PKHPhieuYeuCauCapHangModel model, PKHPhieuYeuCauCapHang entity)
        {
			entity.PKHPhieuYeuCauCapHangId = model.PKHPhieuYeuCauCapHangId;
			entity.KeHoachXuongId = model.KeHoachXuongId;
			entity.PKHKeHoachSXNKXHId = model.PKHKeHoachSXNKXHId;
			entity.SoCT = model.SoCT;
			entity.NgayLap = model.NgayLap;
			entity.LoaiVatTuId = model.LoaiVatTuId;
			entity.SoLuong = model.SoLuong;
			entity.NguoiNhanId = model.NguoiNhanId;
			entity.NguoiNhan = model.NguoiNhan;
			entity.NguoiGiaoId = model.NguoiGiaoId;
			entity.NguoiGiao = model.NguoiGiao;
			entity.LyDoXuatId = model.LyDoXuatId;
			entity.LyDoXuat = model.LyDoXuat;
			entity.SoDieuChuyen = model.SoDieuChuyen;
			entity.TrangThai = model.TrangThai;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;
			entity.Title = model.Title;
			entity.KhoId = model.KhoId;

            return entity;
        }
        public static List<PKHPhieuYeuCauCapHang> MapToEntities(this List<PKHPhieuYeuCauCapHangModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHPhieuYeuCauCapHangModel> MapToModels(this List<PKHPhieuYeuCauCapHang> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
