﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.NhomVatTuNhapRiengModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class NhomVatTuNhapRiengMapper
    {
        #region Mapping NhomVatTuNhapRieng
        public static NhomVatTuNhapRiengModel MapToModel(this NhomVatTuNhapRieng entity)
        {
            return new NhomVatTuNhapRiengModel
            {
				NhomVatTuNhapRiengId = entity.NhomVatTuNhapRiengId,
				Title = entity.Title,
				KhoId = entity.KhoId,
				DangMa = entity.DangMa,
				TenNhom = entity.TenNhom,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static NhomVatTuNhapRiengModel MapToModel(this NhomVatTuNhapRieng entity, NhomVatTuNhapRiengModel model)
        {
			model.NhomVatTuNhapRiengId = entity.NhomVatTuNhapRiengId;
			model.Title = entity.Title;
			model.KhoId = entity.KhoId;
			model.DangMa = entity.DangMa;
			model.TenNhom = entity.TenNhom;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static NhomVatTuNhapRieng MapToEntity(this NhomVatTuNhapRiengModel model)
        {
            return new NhomVatTuNhapRieng
            {
				NhomVatTuNhapRiengId = model.NhomVatTuNhapRiengId,
				Title = model.Title,
				KhoId = model.KhoId,
				DangMa = model.DangMa,
				TenNhom = model.TenNhom,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static NhomVatTuNhapRieng MapToEntity(this NhomVatTuNhapRiengModel model, NhomVatTuNhapRieng entity)
        {
			entity.NhomVatTuNhapRiengId = model.NhomVatTuNhapRiengId;
			entity.Title = model.Title;
			entity.KhoId = model.KhoId;
			entity.DangMa = model.DangMa;
			entity.TenNhom = model.TenNhom;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<NhomVatTuNhapRieng> MapToEntities(this List<NhomVatTuNhapRiengModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<NhomVatTuNhapRiengModel> MapToModels(this List<NhomVatTuNhapRieng> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
