﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHKeHoachSXTuanDetailModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHKeHoachSXTuanDetailMapper
    {
        #region Mapping PKHKeHoachSXTuanDetail
        public static PKHKeHoachSXTuanDetailModel MapToModel(this PKHKeHoachSXTuanDetail entity)
        {
            return new PKHKeHoachSXTuanDetailModel
            {
				PKHKeHoachSXTuanDetailId = entity.PKHKeHoachSXTuanDetailId,
                PKHKeHoachXuongDetailId = entity.PKHKeHoachXuongDetailId,
				PKHKeHoachXuongId = entity.PKHKeHoachXuongId,
				SoLuongSanXuat = entity.SoLuongSanXuat,
				Title = entity.Title,
				ThuHai = entity.ThuHai,
				ThuBa = entity.ThuBa,
				ThuTu = entity.ThuTu,
				ThuNam = entity.ThuNam,
				ThuSau = entity.ThuSau,
				ThuBay = entity.ThuBay,
				ChuNhat = entity.ChuNhat,
				GhiChu = entity.GhiChu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static PKHKeHoachSXTuanDetailModel MapToModel(this PKHKeHoachSXTuanDetail entity, PKHKeHoachSXTuanDetailModel model)
        {
			model.PKHKeHoachSXTuanDetailId = entity.PKHKeHoachSXTuanDetailId;
			model.PKHKeHoachXuongDetailId = entity.PKHKeHoachXuongDetailId;
			model.PKHKeHoachXuongId = entity.PKHKeHoachXuongId;
			model.SoLuongSanXuat = entity.SoLuongSanXuat;
			model.Title = entity.Title;
			model.ThuHai = entity.ThuHai;
			model.ThuBa = entity.ThuBa;
			model.ThuTu = entity.ThuTu;
			model.ThuNam = entity.ThuNam;
			model.ThuSau = entity.ThuSau;
			model.ThuBay = entity.ThuBay;
			model.ChuNhat = entity.ChuNhat;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static PKHKeHoachSXTuanDetail MapToEntity(this PKHKeHoachSXTuanDetailModel model)
        {
            return new PKHKeHoachSXTuanDetail
            {
				PKHKeHoachSXTuanDetailId = model.PKHKeHoachSXTuanDetailId,
                PKHKeHoachXuongDetailId = model.PKHKeHoachXuongDetailId,
				PKHKeHoachXuongId = model.PKHKeHoachXuongId,
				SoLuongSanXuat = model.SoLuongSanXuat,
				Title = model.Title,
				ThuHai = model.ThuHai,
				ThuBa = model.ThuBa,
				ThuTu = model.ThuTu,
				ThuNam = model.ThuNam,
				ThuSau = model.ThuSau,
				ThuBay = model.ThuBay,
				ChuNhat = model.ChuNhat,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHKeHoachSXTuanDetail MapToEntity(this PKHKeHoachSXTuanDetailModel model, PKHKeHoachSXTuanDetail entity)
        {
			entity.PKHKeHoachSXTuanDetailId = model.PKHKeHoachSXTuanDetailId;
			entity.PKHKeHoachXuongDetailId = model.PKHKeHoachXuongDetailId;
			entity.PKHKeHoachXuongId = model.PKHKeHoachXuongId;
			entity.SoLuongSanXuat = model.SoLuongSanXuat;
			entity.Title = model.Title;
			entity.ThuHai = model.ThuHai;
			entity.ThuBa = model.ThuBa;
			entity.ThuTu = model.ThuTu;
			entity.ThuNam = model.ThuNam;
			entity.ThuSau = model.ThuSau;
			entity.ThuBay = model.ThuBay;
			entity.ChuNhat = model.ChuNhat;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHKeHoachSXTuanDetail> MapToEntities(this List<PKHKeHoachSXTuanDetailModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHKeHoachSXTuanDetailModel> MapToModels(this List<PKHKeHoachSXTuanDetail> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
