﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHKiemHoaModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHKiemHoaMapper
    {
        #region Mapping PKHKiemHoa
        public static PKHKiemHoaModel MapToModel(this PKHKiemHoa entity)
        {
            return new PKHKiemHoaModel
            {
				PKHKiemHoaId = entity.PKHKiemHoaId,
				LoaiKiemHoa = entity.LoaiKiemHoa,
				SoKiemHoa = entity.SoKiemHoa,
				NgayLapPhieu = entity.NgayLapPhieu,
				KhoId = entity.KhoId,
				Title = entity.Title,
				MaKiemHoa = entity.MaKiemHoa,
				PhieuNhap = entity.PhieuNhap,
				SoHD = entity.SoHD,
				TrangThai = entity.TrangThai,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static PKHKiemHoaModel MapToModel(this PKHKiemHoa entity, PKHKiemHoaModel model)
        {
			model.PKHKiemHoaId = entity.PKHKiemHoaId;
			model.LoaiKiemHoa = entity.LoaiKiemHoa;
			model.SoKiemHoa = entity.SoKiemHoa;
			model.NgayLapPhieu = entity.NgayLapPhieu;
			model.KhoId = entity.KhoId;
			model.Title = entity.Title;
			model.MaKiemHoa = entity.MaKiemHoa;
			model.PhieuNhap = entity.PhieuNhap;
			model.SoHD = entity.SoHD;
			model.TrangThai = entity.TrangThai;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static PKHKiemHoa MapToEntity(this PKHKiemHoaModel model)
        {
            return new PKHKiemHoa
            {
				PKHKiemHoaId = model.PKHKiemHoaId,
				LoaiKiemHoa = model.LoaiKiemHoa,
				SoKiemHoa = model.SoKiemHoa,
				NgayLapPhieu = model.NgayLapPhieu,
				KhoId = model.KhoId,
				Title = model.Title,
				MaKiemHoa = model.MaKiemHoa,
				PhieuNhap = model.PhieuNhap,
				SoHD = model.SoHD,
				TrangThai = model.TrangThai,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHKiemHoa MapToEntity(this PKHKiemHoaModel model, PKHKiemHoa entity)
        {
			entity.PKHKiemHoaId = model.PKHKiemHoaId;
			entity.LoaiKiemHoa = model.LoaiKiemHoa;
			entity.SoKiemHoa = model.SoKiemHoa;
			entity.NgayLapPhieu = model.NgayLapPhieu;
			entity.KhoId = model.KhoId;
			entity.Title = model.Title;
			entity.MaKiemHoa = model.MaKiemHoa;
			entity.PhieuNhap = model.PhieuNhap;
			entity.SoHD = model.SoHD;
			entity.TrangThai = model.TrangThai;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHKiemHoa> MapToEntities(this List<PKHKiemHoaModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHKiemHoaModel> MapToModels(this List<PKHKiemHoa> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
