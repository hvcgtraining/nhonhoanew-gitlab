﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.DechetModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class DechetMapper
    {
        #region Mapping Dechet
        public static DechetModel MapToModel(this Dechet entity)
        {
            return new DechetModel
            {
				DechetId = entity.DechetId,
				MaCoPhan = entity.MaCoPhan,
				Title = entity.Title,
				GhiChu = entity.GhiChu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static DechetModel MapToModel(this Dechet entity, DechetModel model)
        {
			model.DechetId = entity.DechetId;
			model.MaCoPhan = entity.MaCoPhan;
			model.Title = entity.Title;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static Dechet MapToEntity(this DechetModel model)
        {
            return new Dechet
            {
				DechetId = model.DechetId,
				MaCoPhan = model.MaCoPhan,
				Title = model.Title,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static Dechet MapToEntity(this DechetModel model, Dechet entity)
        {
			entity.DechetId = model.DechetId;
			entity.MaCoPhan = model.MaCoPhan;
			entity.Title = model.Title;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<Dechet> MapToEntities(this List<DechetModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<DechetModel> MapToModels(this List<Dechet> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
