﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.NhomSanPhamModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class NhomSanPhamMapper
    {
        #region Mapping NhomSanPham
        public static NhomSanPhamModel MapToModel(this NhomSanPham entity)
        {
            return new NhomSanPhamModel
            {
				NhomSanPhamId = entity.NhomSanPhamId,
				MaNhomSanPham = entity.MaNhomSanPham,
				Title = entity.Title,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static NhomSanPhamModel MapToModel(this NhomSanPham entity, NhomSanPhamModel model)
        {
			model.NhomSanPhamId = entity.NhomSanPhamId;
			model.MaNhomSanPham = entity.MaNhomSanPham;
			model.Title = entity.Title;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static NhomSanPham MapToEntity(this NhomSanPhamModel model)
        {
            return new NhomSanPham
            {
				NhomSanPhamId = model.NhomSanPhamId,
				MaNhomSanPham = model.MaNhomSanPham,
				Title = model.Title,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static NhomSanPham MapToEntity(this NhomSanPhamModel model, NhomSanPham entity)
        {
			entity.NhomSanPhamId = model.NhomSanPhamId;
			entity.MaNhomSanPham = model.MaNhomSanPham;
			entity.Title = model.Title;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<NhomSanPham> MapToEntities(this List<NhomSanPhamModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<NhomSanPhamModel> MapToModels(this List<NhomSanPham> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
