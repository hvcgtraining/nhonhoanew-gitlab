﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.DanhMucKhachHangModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class DanhMucKhachHangMapper
    {
        #region Mapping DanhMucKhachHang
        public static DanhMucKhachHangModel MapToModel(this DanhMucKhachHang entity)
        {
            return new DanhMucKhachHangModel
            {
                DanhMucKhachHangId = entity.DanhMucKhachHangId,
                Title = entity.Title,
                CreatedDate = entity.CreatedDate,
                UpdatedDate = entity.UpdatedDate,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                Status = entity.Status,
                MaNCC = entity.MaNCC,
                TenNCC = entity.TenNCC,
                MaSoThue = entity.MaSoThue,
                NganhNghe = entity.NganhNghe,
                SoDienThoai = entity.SoDienThoai,
                SoFax = entity.SoFax,
                Email = entity.Email,
                LoaiHinhKinhDoanhId = entity.LoaiHinhKinhDoanhId,
                NhomKhachHangId = entity.NhomKhachHangId,
                GhiChu = entity.GhiChu,

                TenLoaiHinhKinhDoanh = (entity.LoaiHinhKinhDoanh != null) ? entity.LoaiHinhKinhDoanh.Title : string.Empty,
                TenNhomKhachHang=(entity.NhomKhachHang!=null)?entity.NhomKhachHang.Title:string.Empty,
            };
        }
        public static DanhMucKhachHangModel MapToModel(this DanhMucKhachHang entity, DanhMucKhachHangModel model)
        {
			model.DanhMucKhachHangId = entity.DanhMucKhachHangId;
			model.Title = entity.Title;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;
			model.MaNCC = entity.MaNCC;
			model.TenNCC = entity.TenNCC;
			model.MaSoThue = entity.MaSoThue;
			model.NganhNghe = entity.NganhNghe;
			model.SoDienThoai = entity.SoDienThoai;
			model.SoFax = entity.SoFax;
			model.Email = entity.Email;
			model.LoaiHinhKinhDoanhId = entity.LoaiHinhKinhDoanhId;
			model.NhomKhachHangId = entity.NhomKhachHangId;
			model.GhiChu = entity.GhiChu;
            model.TenLoaiHinhKinhDoanh = entity.LoaiHinhKinhDoanh.Title;
            model.TenNhomKhachHang = entity.NhomKhachHang.Title;
            return model;
        }
        public static DanhMucKhachHang MapToEntity(this DanhMucKhachHangModel model)
        {
            return new DanhMucKhachHang
            {
				DanhMucKhachHangId = model.DanhMucKhachHangId,
				Title = model.Title,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,
				MaNCC = model.MaNCC,
				TenNCC = model.TenNCC,
				MaSoThue = model.MaSoThue,
				NganhNghe = model.NganhNghe,
				SoDienThoai = model.SoDienThoai,
				SoFax = model.SoFax,
				Email = model.Email,
				LoaiHinhKinhDoanhId = model.LoaiHinhKinhDoanhId,
				NhomKhachHangId = model.NhomKhachHangId,
				GhiChu = model.GhiChu,

            };
        }
        public static DanhMucKhachHang MapToEntity(this DanhMucKhachHangModel model, DanhMucKhachHang entity)
        {
			entity.DanhMucKhachHangId = model.DanhMucKhachHangId;
			entity.Title = model.Title;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;
			entity.MaNCC = model.MaNCC;
			entity.TenNCC = model.TenNCC;
			entity.MaSoThue = model.MaSoThue;
			entity.NganhNghe = model.NganhNghe;
			entity.SoDienThoai = model.SoDienThoai;
			entity.SoFax = model.SoFax;
			entity.Email = model.Email;
			entity.LoaiHinhKinhDoanhId = model.LoaiHinhKinhDoanhId;
			entity.NhomKhachHangId = model.NhomKhachHangId;
			entity.GhiChu = model.GhiChu;

            return entity;
        }
        public static List<DanhMucKhachHang> MapToEntities(this List<DanhMucKhachHangModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<DanhMucKhachHangModel> MapToModels(this List<DanhMucKhachHang> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
