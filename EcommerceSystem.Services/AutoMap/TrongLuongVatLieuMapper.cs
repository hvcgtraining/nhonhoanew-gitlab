﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.TrongLuongVatLieuModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class TrongLuongVatLieuMapper
    {
        #region Mapping TrongLuongVatLieu
        public static TrongLuongVatLieuModel MapToModel(this TrongLuongVatLieu entity)
        {
            return new TrongLuongVatLieuModel
            {
				TrongLuongVatLieuId = entity.TrongLuongVatLieuId,
				MaNguyenVatLieu = entity.MaNguyenVatLieu,
				Title = entity.Title,
				KichThuoc = entity.KichThuoc,
				KhoiLuong = entity.KhoiLuong,
				GhiChu = entity.GhiChu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static TrongLuongVatLieuModel MapToModel(this TrongLuongVatLieu entity, TrongLuongVatLieuModel model)
        {
			model.TrongLuongVatLieuId = entity.TrongLuongVatLieuId;
			model.MaNguyenVatLieu = entity.MaNguyenVatLieu;
			model.Title = entity.Title;
			model.KichThuoc = entity.KichThuoc;
			model.KhoiLuong = entity.KhoiLuong;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static TrongLuongVatLieu MapToEntity(this TrongLuongVatLieuModel model)
        {
            return new TrongLuongVatLieu
            {
				TrongLuongVatLieuId = model.TrongLuongVatLieuId,
				MaNguyenVatLieu = model.MaNguyenVatLieu,
				Title = model.Title,
				KichThuoc = model.KichThuoc,
				KhoiLuong = model.KhoiLuong,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static TrongLuongVatLieu MapToEntity(this TrongLuongVatLieuModel model, TrongLuongVatLieu entity)
        {
			entity.TrongLuongVatLieuId = model.TrongLuongVatLieuId;
			entity.MaNguyenVatLieu = model.MaNguyenVatLieu;
			entity.Title = model.Title;
			entity.KichThuoc = model.KichThuoc;
			entity.KhoiLuong = model.KhoiLuong;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<TrongLuongVatLieu> MapToEntities(this List<TrongLuongVatLieuModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<TrongLuongVatLieuModel> MapToModels(this List<TrongLuongVatLieu> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
