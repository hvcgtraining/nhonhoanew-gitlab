﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHPhieuNhapVatTuModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHPhieuNhapVatTuMapper
    {
        #region Mapping PKHPhieuNhapVatTu
        public static PKHPhieuNhapVatTuModel MapToModel(this PKHPhieuNhapVatTu entity)
        {
            return new PKHPhieuNhapVatTuModel
            {
                PKHPhieuNhapVatTuId = entity.PKHPhieuNhapVatTuId,
                SoChungTu = entity.SoChungTu,
                NgayLapPhieu = entity.NgayLapPhieu,
                KhoId = entity.KhoId,
                PKHKiemHoaId = entity.PKHKiemHoaId,
                Title = entity.Title,
                DanhMucKhachHangId = entity.DanhMucKhachHangId,
                SoKiemHoa = entity.SoKiemHoa,
                SoDC = entity.SoDC,
                TrangThai = entity.TrangThai,
                CreatedDate = entity.CreatedDate,
                UpdatedDate = entity.UpdatedDate,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                Status = entity.Status,
                KhoName = (entity.Kho != null) ? entity.Kho.Title : string.Empty,
                KiemHoaName = (entity.PKHKiemHoa != null) ? entity.PKHKiemHoa.Title : string.Empty,
                DanhMucKhachHangName=(entity.DanhMucKhachHang!=null)?entity.DanhMucKhachHang.Title:string.Empty,

            };
        }
        public static PKHPhieuNhapVatTuModel MapToModel(this PKHPhieuNhapVatTu entity, PKHPhieuNhapVatTuModel model)
        {
			model.PKHPhieuNhapVatTuId = entity.PKHPhieuNhapVatTuId;
			model.SoChungTu = entity.SoChungTu;
			model.NgayLapPhieu = entity.NgayLapPhieu;
			model.KhoId = entity.KhoId;
			model.PKHKiemHoaId = entity.PKHKiemHoaId;
			model.Title = entity.Title;
			model.DanhMucKhachHangId = entity.DanhMucKhachHangId;
			model.SoKiemHoa = entity.SoKiemHoa;
			model.SoDC = entity.SoDC;
			model.TrangThai = entity.TrangThai;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;
            model.KhoName = entity.Kho.Title;
            model.KiemHoaName = entity.PKHKiemHoa.Title;
            model.DanhMucKhachHangName = entity.DanhMucKhachHang.Title;
            return model;
        }
        public static PKHPhieuNhapVatTu MapToEntity(this PKHPhieuNhapVatTuModel model)
        {
            return new PKHPhieuNhapVatTu
            {
				PKHPhieuNhapVatTuId = model.PKHPhieuNhapVatTuId,
				SoChungTu = model.SoChungTu,
				NgayLapPhieu = model.NgayLapPhieu,
				KhoId = model.KhoId,
				PKHKiemHoaId = model.PKHKiemHoaId,
				Title = model.Title,
				DanhMucKhachHangId = model.DanhMucKhachHangId,
				SoKiemHoa = model.SoKiemHoa,
				SoDC = model.SoDC,
				TrangThai = model.TrangThai,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHPhieuNhapVatTu MapToEntity(this PKHPhieuNhapVatTuModel model, PKHPhieuNhapVatTu entity)
        {
			entity.PKHPhieuNhapVatTuId = model.PKHPhieuNhapVatTuId;
			entity.SoChungTu = model.SoChungTu;
			entity.NgayLapPhieu = model.NgayLapPhieu;
			entity.KhoId = model.KhoId;
			entity.PKHKiemHoaId = model.PKHKiemHoaId;
			entity.Title = model.Title;
			entity.DanhMucKhachHangId = model.DanhMucKhachHangId;
			entity.SoKiemHoa = model.SoKiemHoa;
			entity.SoDC = model.SoDC;
			entity.TrangThai = model.TrangThai;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHPhieuNhapVatTu> MapToEntities(this List<PKHPhieuNhapVatTuModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHPhieuNhapVatTuModel> MapToModels(this List<PKHPhieuNhapVatTu> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
