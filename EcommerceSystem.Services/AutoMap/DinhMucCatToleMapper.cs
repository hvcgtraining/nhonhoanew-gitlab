﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.DinhMucCatToleModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class DinhMucCatToleMapper
    {
        #region Mapping DinhMucCatTole
        public static DinhMucCatToleModel MapToModel(this DinhMucCatTole entity)
        {
            return new DinhMucCatToleModel
            {
				DinhMucCatToleId = entity.DinhMucCatToleId,
				Title = entity.Title,
				SoChungTu = entity.SoChungTu,
				Day = entity.Day,
				PhuongAn = entity.PhuongAn,
				MaCT = entity.MaCT,
				SLCT = entity.SLCT,
				ChiTietVatTuId = entity.ChiTietVatTuId,
				ToleId = entity.ToleId,
				GhiChu = entity.GhiChu,
				Loai = entity.Loai,
				HinhAnh = entity.HinhAnh,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,
                MaTole = entity.Tole != null ? entity.Tole.MaTole : "",
                TenTole = entity.Tole != null ? entity.Tole.Title : "",
                DoDay = entity.Tole != null ? entity.Tole.Day : 0,
                MaChiTiet = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.MaChiTietVatTu : "",
            };
        }
        public static DinhMucCatToleModel MapToModel(this DinhMucCatTole entity, DinhMucCatToleModel model)
        {
			model.DinhMucCatToleId = entity.DinhMucCatToleId;
			model.Title = entity.Title;
			model.SoChungTu = entity.SoChungTu;
			model.Day = entity.Day;
			model.PhuongAn = entity.PhuongAn;
			model.MaCT = entity.MaCT;
			model.SLCT = entity.SLCT;
			model.ChiTietVatTuId = entity.ChiTietVatTuId;
			model.ToleId = entity.ToleId;
			model.GhiChu = entity.GhiChu;
			model.Loai = entity.Loai;
			model.HinhAnh = entity.HinhAnh;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static DinhMucCatTole MapToEntity(this DinhMucCatToleModel model)
        {
            return new DinhMucCatTole
            {
				DinhMucCatToleId = model.DinhMucCatToleId,
				Title = model.Title,
				SoChungTu = model.SoChungTu,
				Day = model.Day,
				PhuongAn = model.PhuongAn,
				MaCT = model.MaCT,
				SLCT = model.SLCT,
				ChiTietVatTuId = model.ChiTietVatTuId,
				ToleId = model.ToleId,
				GhiChu = model.GhiChu,
				Loai = model.Loai,
				HinhAnh = model.HinhAnh,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static DinhMucCatTole MapToEntity(this DinhMucCatToleModel model, DinhMucCatTole entity)
        {
			entity.DinhMucCatToleId = model.DinhMucCatToleId;
			entity.Title = model.Title;
			entity.SoChungTu = model.SoChungTu;
			entity.Day = model.Day;
			entity.PhuongAn = model.PhuongAn;
			entity.MaCT = model.MaCT;
			entity.SLCT = model.SLCT;
			entity.ChiTietVatTuId = model.ChiTietVatTuId;
			entity.ToleId = model.ToleId;
			entity.GhiChu = model.GhiChu;
			entity.Loai = model.Loai;
			entity.HinhAnh = model.HinhAnh;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<DinhMucCatTole> MapToEntities(this List<DinhMucCatToleModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<DinhMucCatToleModel> MapToModels(this List<DinhMucCatTole> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
