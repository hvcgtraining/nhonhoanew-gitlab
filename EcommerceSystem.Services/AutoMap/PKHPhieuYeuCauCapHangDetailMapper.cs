﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHPhieuYeuCauCapHangDetailModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHPhieuYeuCauCapHangDetailMapper
    {
        #region Mapping PKHPhieuYeuCauCapHangDetail
        public static PKHPhieuYeuCauCapHangDetailModel MapToModel(this PKHPhieuYeuCauCapHangDetail entity)
        {
            return new PKHPhieuYeuCauCapHangDetailModel
            {
				PKHPhieuYeuCauCapHangDetailId = entity.PKHPhieuYeuCauCapHangDetailId,
				PKHPhieuYeuCauCapHangId = entity.PKHPhieuYeuCauCapHangId,
				ChiTietVatTuId = entity.ChiTietVatTuId,
				SoLuongXuat = entity.SoLuongXuat,
				SoTam = entity.SoTam,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,
				Title = entity.Title,
                MaChiTiet = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.MaChiTietVatTu : "",
                TenChiTiet = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.Title : ""

            };
        }
        public static PKHPhieuYeuCauCapHangDetailModel MapToModel(this PKHPhieuYeuCauCapHangDetail entity, PKHPhieuYeuCauCapHangDetailModel model)
        {
			model.PKHPhieuYeuCauCapHangDetailId = entity.PKHPhieuYeuCauCapHangDetailId;
			model.PKHPhieuYeuCauCapHangId = entity.PKHPhieuYeuCauCapHangId;
			model.ChiTietVatTuId = entity.ChiTietVatTuId;
			model.SoLuongXuat = entity.SoLuongXuat;
			model.SoTam = entity.SoTam;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;
			model.Title = entity.Title;

            return model;
        }
        public static PKHPhieuYeuCauCapHangDetail MapToEntity(this PKHPhieuYeuCauCapHangDetailModel model)
        {
            return new PKHPhieuYeuCauCapHangDetail
            {
				PKHPhieuYeuCauCapHangDetailId = model.PKHPhieuYeuCauCapHangDetailId,
				PKHPhieuYeuCauCapHangId = model.PKHPhieuYeuCauCapHangId,
				ChiTietVatTuId = model.ChiTietVatTuId,
				SoLuongXuat = model.SoLuongXuat,
				SoTam = model.SoTam,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,
				Title = model.Title,

            };
        }
        public static PKHPhieuYeuCauCapHangDetail MapToEntity(this PKHPhieuYeuCauCapHangDetailModel model, PKHPhieuYeuCauCapHangDetail entity)
        {
			entity.PKHPhieuYeuCauCapHangDetailId = model.PKHPhieuYeuCauCapHangDetailId;
			entity.PKHPhieuYeuCauCapHangId = model.PKHPhieuYeuCauCapHangId;
			entity.ChiTietVatTuId = model.ChiTietVatTuId;
			entity.SoLuongXuat = model.SoLuongXuat;
			entity.SoTam = model.SoTam;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;
			entity.Title = model.Title;

            return entity;
        }
        public static List<PKHPhieuYeuCauCapHangDetail> MapToEntities(this List<PKHPhieuYeuCauCapHangDetailModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHPhieuYeuCauCapHangDetailModel> MapToModels(this List<PKHPhieuYeuCauCapHangDetail> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
