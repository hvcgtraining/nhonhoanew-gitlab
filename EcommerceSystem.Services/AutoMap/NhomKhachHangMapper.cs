﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.NhomKhachHangModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class NhomKhachHangMapper
    {
        #region Mapping NhomKhachHang
        public static NhomKhachHangModel MapToModel(this NhomKhachHang entity)
        {
            return new NhomKhachHangModel
            {
				NhomKhachHangId = entity.NhomKhachHangId,
				Title = entity.Title,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static NhomKhachHangModel MapToModel(this NhomKhachHang entity, NhomKhachHangModel model)
        {
			model.NhomKhachHangId = entity.NhomKhachHangId;
			model.Title = entity.Title;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static NhomKhachHang MapToEntity(this NhomKhachHangModel model)
        {
            return new NhomKhachHang
            {
				NhomKhachHangId = model.NhomKhachHangId,
				Title = model.Title,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static NhomKhachHang MapToEntity(this NhomKhachHangModel model, NhomKhachHang entity)
        {
			entity.NhomKhachHangId = model.NhomKhachHangId;
			entity.Title = model.Title;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<NhomKhachHang> MapToEntities(this List<NhomKhachHangModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<NhomKhachHangModel> MapToModels(this List<NhomKhachHang> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
