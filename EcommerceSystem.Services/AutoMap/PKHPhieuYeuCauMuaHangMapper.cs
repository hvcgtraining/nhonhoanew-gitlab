﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHPhieuYeuCauMuaHangModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHPhieuYeuCauMuaHangMapper
    {
        #region Mapping PKHPhieuYeuCauMuaHang
        public static PKHPhieuYeuCauMuaHangModel MapToModel(this PKHPhieuYeuCauMuaHang entity)
        {
            return new PKHPhieuYeuCauMuaHangModel
            {
				PKHPhieuYeuCauMuaHangId = entity.PKHPhieuYeuCauMuaHangId,
				Title = entity.Title,
				NgayYeuCau = entity.NgayYeuCau,
				NgayDuKien = entity.NgayDuKien,
				NgayHoanTat = entity.NgayHoanTat,
				TrangThai = entity.TrangThai,
				YeuCauKT = entity.YeuCauKT,
				NoiDung = entity.NoiDung,
				DiaDiem = entity.DiaDiem,
				MucDich = entity.MucDich,
				GhiChu = entity.GhiChu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static PKHPhieuYeuCauMuaHangModel MapToModel(this PKHPhieuYeuCauMuaHang entity, PKHPhieuYeuCauMuaHangModel model)
        {
			model.PKHPhieuYeuCauMuaHangId = entity.PKHPhieuYeuCauMuaHangId;
			model.Title = entity.Title;
			model.NgayYeuCau = entity.NgayYeuCau;
			model.NgayDuKien = entity.NgayDuKien;
			model.NgayHoanTat = entity.NgayHoanTat;
			model.TrangThai = entity.TrangThai;
			model.YeuCauKT = entity.YeuCauKT;
			model.NoiDung = entity.NoiDung;
			model.DiaDiem = entity.DiaDiem;
			model.MucDich = entity.MucDich;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static PKHPhieuYeuCauMuaHang MapToEntity(this PKHPhieuYeuCauMuaHangModel model)
        {
            return new PKHPhieuYeuCauMuaHang
            {
				PKHPhieuYeuCauMuaHangId = model.PKHPhieuYeuCauMuaHangId,
				Title = model.Title,
				NgayYeuCau = model.NgayYeuCau,
				NgayDuKien = model.NgayDuKien,
				NgayHoanTat = model.NgayHoanTat,
				TrangThai = model.TrangThai,
				YeuCauKT = model.YeuCauKT,
				NoiDung = model.NoiDung,
				DiaDiem = model.DiaDiem,
				MucDich = model.MucDich,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHPhieuYeuCauMuaHang MapToEntity(this PKHPhieuYeuCauMuaHangModel model, PKHPhieuYeuCauMuaHang entity)
        {
			entity.PKHPhieuYeuCauMuaHangId = model.PKHPhieuYeuCauMuaHangId;
			entity.Title = model.Title;
			entity.NgayYeuCau = model.NgayYeuCau;
			entity.NgayDuKien = model.NgayDuKien;
			entity.NgayHoanTat = model.NgayHoanTat;
			entity.TrangThai = model.TrangThai;
			entity.YeuCauKT = model.YeuCauKT;
			entity.NoiDung = model.NoiDung;
			entity.DiaDiem = model.DiaDiem;
			entity.MucDich = model.MucDich;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHPhieuYeuCauMuaHang> MapToEntities(this List<PKHPhieuYeuCauMuaHangModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHPhieuYeuCauMuaHangModel> MapToModels(this List<PKHPhieuYeuCauMuaHang> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
