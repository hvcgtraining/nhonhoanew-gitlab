﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.ThanhPhamXuongModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class ThanhPhamXuongMapper
    {
        #region Mapping ThanhPhamXuong
        public static ThanhPhamXuongModel MapToModel(this ThanhPhamXuong entity)
        {
            return new ThanhPhamXuongModel
            {
                ThanhPhamXuongId = entity.ThanhPhamXuongId,
                Title = entity.Title,
                XuongId = entity.XuongId,
                VatTuId = entity.VatTuId,
                NhomSanPhamId = entity.NhomSanPhamId,
                Loai = entity.Loai,
                NangSuatTuan = entity.NangSuatTuan,
                LoaiVatLieu = entity.LoaiVatLieu,
                QuyCach = entity.QuyCach,
                CreatedDate = entity.CreatedDate,
                UpdatedDate = entity.UpdatedDate,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                Status = entity.Status,
                MaXuong = (entity.Xuong != null) ? entity.Xuong.MaXuong : string.Empty,
                MaVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.MaChiTietVatTu : string.Empty,
                TenVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.Title : string.Empty,
                TenNhomSanPham = (entity.NhomSanPham != null) ? entity.NhomSanPham.Title : string.Empty,
            };
        }
        public static ThanhPhamXuongModel MapToModel(this ThanhPhamXuong entity, ThanhPhamXuongModel model)
        {
            model.ThanhPhamXuongId = entity.ThanhPhamXuongId;
            model.Title = entity.Title;
            model.XuongId = entity.XuongId;
            model.VatTuId = entity.VatTuId;
            model.NhomSanPhamId = entity.NhomSanPhamId;
            model.Loai = entity.Loai;
            model.NangSuatTuan = entity.NangSuatTuan;
            model.LoaiVatLieu = entity.LoaiVatLieu;
            model.QuyCach = entity.QuyCach;
            model.CreatedDate = entity.CreatedDate;
            model.UpdatedDate = entity.UpdatedDate;
            model.CreatedBy = entity.CreatedBy;
            model.UpdatedBy = entity.UpdatedBy;
            model.Status = entity.Status;

            return model;
        }
        public static ThanhPhamXuong MapToEntity(this ThanhPhamXuongModel model)
        {
            return new ThanhPhamXuong
            {
                ThanhPhamXuongId = model.ThanhPhamXuongId,
                Title = model.Title,
                XuongId = model.XuongId,
                VatTuId = model.VatTuId,
                NhomSanPhamId = model.NhomSanPhamId,
                Loai = model.Loai,
                NangSuatTuan = model.NangSuatTuan,
                LoaiVatLieu = model.LoaiVatLieu,
                QuyCach = model.QuyCach,
                CreatedDate = model.CreatedDate,
                UpdatedDate = model.UpdatedDate,
                CreatedBy = model.CreatedBy,
                UpdatedBy = model.UpdatedBy,
                Status = model.Status,

            };
        }
        public static ThanhPhamXuong MapToEntity(this ThanhPhamXuongModel model, ThanhPhamXuong entity)
        {
            entity.ThanhPhamXuongId = model.ThanhPhamXuongId;
            entity.Title = model.Title;
            entity.XuongId = model.XuongId;
            entity.VatTuId = model.VatTuId;
            entity.NhomSanPhamId = model.NhomSanPhamId;
            entity.Loai = model.Loai;
            entity.NangSuatTuan = model.NangSuatTuan;
            entity.LoaiVatLieu = model.LoaiVatLieu;
            entity.QuyCach = model.QuyCach;
            //entity.CreatedDate = model.CreatedDate;
            entity.UpdatedDate = model.UpdatedDate;
            //entity.CreatedBy = model.CreatedBy;
            entity.UpdatedBy = model.UpdatedBy;
            entity.Status = model.Status;

            return entity;
        }
        public static List<ThanhPhamXuong> MapToEntities(this List<ThanhPhamXuongModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<ThanhPhamXuongModel> MapToModels(this List<ThanhPhamXuong> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
