﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.DinhMucSanXuatModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class DinhMucSanXuatMapper
    {
        #region Mapping DinhMucSanXuat
        public static DinhMucSanXuatModel MapToModel(this DinhMucSanXuat entity)
        {
            return new DinhMucSanXuatModel
            {
				DinhMucSanXuatId = entity.DinhMucSanXuatId,
				Title = entity.Title,
				MaDinhMucSanXuat = entity.MaDinhMucSanXuat,
				SoChungTu = entity.SoChungTu,
				ChiTietVatTuId = entity.ChiTietVatTuId,
                MaThanhPham = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.MaChiTietVatTu : string.Empty,
				TenThanhPham = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.Title : string.Empty,
                MaCapPhat = entity.MaCapPhat,
				Loai = entity.Loai,
				DienGiai = entity.DienGiai,
				GhiChu = entity.GhiChu,
				KhoQuyetToanId = entity.KhoQuyetToanId,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static DinhMucSanXuatModel MapToModel(this DinhMucSanXuat entity, DinhMucSanXuatModel model)
        {
			model.DinhMucSanXuatId = entity.DinhMucSanXuatId;
			model.Title = entity.Title;
			model.MaDinhMucSanXuat = entity.MaDinhMucSanXuat;
			model.SoChungTu = entity.SoChungTu;
			model.ChiTietVatTuId = entity.ChiTietVatTuId;
            model.MaThanhPham = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.MaChiTietVatTu : string.Empty;
            model.TenThanhPham = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.Title : string.Empty;
			model.MaCapPhat = entity.MaCapPhat;
			model.Loai = entity.Loai;
			model.DienGiai = entity.DienGiai;
			model.GhiChu = entity.GhiChu;
			model.KhoQuyetToanId = entity.KhoQuyetToanId;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static DinhMucSanXuat MapToEntity(this DinhMucSanXuatModel model)
        {
            return new DinhMucSanXuat
            {
				DinhMucSanXuatId = model.DinhMucSanXuatId,
				Title = model.Title,
				MaDinhMucSanXuat = model.MaDinhMucSanXuat,
				SoChungTu = model.SoChungTu,
                ChiTietVatTuId = model.ChiTietVatTuId,
				//TenThanhPham = model.TenThanhPham,
				MaCapPhat = model.MaCapPhat,
				Loai = model.Loai,
				DienGiai = model.DienGiai,
				GhiChu = model.GhiChu,
				KhoQuyetToanId = model.KhoQuyetToanId,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static DinhMucSanXuat MapToEntity(this DinhMucSanXuatModel model, DinhMucSanXuat entity)
        {
			entity.DinhMucSanXuatId = model.DinhMucSanXuatId;
			entity.Title = model.Title;
			entity.MaDinhMucSanXuat = model.MaDinhMucSanXuat;
			entity.SoChungTu = model.SoChungTu;
			entity.ChiTietVatTuId = model.ChiTietVatTuId;
			//entity.TenThanhPham = model.TenThanhPham;
			entity.MaCapPhat = model.MaCapPhat;
			entity.Loai = model.Loai;
			entity.DienGiai = model.DienGiai;
			entity.GhiChu = model.GhiChu;
			entity.KhoQuyetToanId = model.KhoQuyetToanId;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<DinhMucSanXuat> MapToEntities(this List<DinhMucSanXuatModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<DinhMucSanXuatModel> MapToModels(this List<DinhMucSanXuat> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
