﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.NguoiNhanKhoModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class NguoiNhanKhoMapper
    {
        #region Mapping NguoiNhanKho
        public static NguoiNhanKhoModel MapToModel(this NguoiNhanKho entity)
        {
            return new NguoiNhanKhoModel
            {
				NguoiNhanKhoId = entity.NguoiNhanKhoId,
				Title = entity.Title,
				KhoId = entity.KhoId,
				MaTen = entity.MaTen,
				TenGiaoNhan = entity.TenGiaoNhan,
				GhiChu = entity.GhiChu,
				MaTruongDonVi = entity.MaTruongDonVi,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,
                KhoName = (entity.Kho != null) ? entity.Kho.Title : string.Empty,
            };
        }
        public static NguoiNhanKhoModel MapToModel(this NguoiNhanKho entity, NguoiNhanKhoModel model)
        {
			model.NguoiNhanKhoId = entity.NguoiNhanKhoId;
			model.Title = entity.Title;
			model.KhoId = entity.KhoId;
			model.MaTen = entity.MaTen;
			model.TenGiaoNhan = entity.TenGiaoNhan;
			model.GhiChu = entity.GhiChu;
			model.MaTruongDonVi = entity.MaTruongDonVi;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;
            model.KhoName = entity.Kho.Title;
            return model;
        }
        public static NguoiNhanKho MapToEntity(this NguoiNhanKhoModel model)
        {
            return new NguoiNhanKho
            {
				NguoiNhanKhoId = model.NguoiNhanKhoId,
				Title = model.Title,
				KhoId = model.KhoId,
				MaTen = model.MaTen,
				TenGiaoNhan = model.TenGiaoNhan,
				GhiChu = model.GhiChu,
				MaTruongDonVi = model.MaTruongDonVi,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static NguoiNhanKho MapToEntity(this NguoiNhanKhoModel model, NguoiNhanKho entity)
        {
			entity.NguoiNhanKhoId = model.NguoiNhanKhoId;
			entity.Title = model.Title;
			entity.KhoId = model.KhoId;
			entity.MaTen = model.MaTen;
			entity.TenGiaoNhan = model.TenGiaoNhan;
			entity.GhiChu = model.GhiChu;
			entity.MaTruongDonVi = model.MaTruongDonVi;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<NguoiNhanKho> MapToEntities(this List<NguoiNhanKhoModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<NguoiNhanKhoModel> MapToModels(this List<NguoiNhanKho> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
