﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.QuyDoiDinhMucModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class QuyDoiDinhMucMapper
    {
        #region Mapping QuyDoiDinhMuc
        public static QuyDoiDinhMucModel MapToModel(this QuyDoiDinhMuc entity)
        {
            return new QuyDoiDinhMucModel
            {
				QuyDoiDinhMucId = entity.QuyDoiDinhMucId,
				MaQuyDoiDinhMuc = entity.MaQuyDoiDinhMuc,
				Title = entity.Title,
				DinhMuc = entity.DinhMuc,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static QuyDoiDinhMucModel MapToModel(this QuyDoiDinhMuc entity, QuyDoiDinhMucModel model)
        {
			model.QuyDoiDinhMucId = entity.QuyDoiDinhMucId;
			model.MaQuyDoiDinhMuc = entity.MaQuyDoiDinhMuc;
			model.Title = entity.Title;
			model.DinhMuc = entity.DinhMuc;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static QuyDoiDinhMuc MapToEntity(this QuyDoiDinhMucModel model)
        {
            return new QuyDoiDinhMuc
            {
				QuyDoiDinhMucId = model.QuyDoiDinhMucId,
				MaQuyDoiDinhMuc = model.MaQuyDoiDinhMuc,
				Title = model.Title,
				DinhMuc = model.DinhMuc,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static QuyDoiDinhMuc MapToEntity(this QuyDoiDinhMucModel model, QuyDoiDinhMuc entity)
        {
			entity.QuyDoiDinhMucId = model.QuyDoiDinhMucId;
			entity.MaQuyDoiDinhMuc = model.MaQuyDoiDinhMuc;
			entity.Title = model.Title;
			entity.DinhMuc = model.DinhMuc;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<QuyDoiDinhMuc> MapToEntities(this List<QuyDoiDinhMucModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<QuyDoiDinhMucModel> MapToModels(this List<QuyDoiDinhMuc> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
