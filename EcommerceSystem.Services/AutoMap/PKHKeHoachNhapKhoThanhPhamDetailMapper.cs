﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHKeHoachNhapKhoThanhPhamDetailModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHKeHoachNhapKhoThanhPhamDetailMapper
    {
        #region Mapping PKHKeHoachNhapKhoThanhPhamDetail
        public static PKHKeHoachNhapKhoThanhPhamDetailModel MapToModel(this PKHKeHoachNhapKhoThanhPhamDetail entity)
        {
            return new PKHKeHoachNhapKhoThanhPhamDetailModel
            {
				PKHKeHoachNhapKhoThanhPhamDetailId = entity.PKHKeHoachNhapKhoThanhPhamDetailId,
				PKHKeHoachXuongId = entity.PKHKeHoachXuongId,
                PKHKeHoachXuongDetailId = entity.PKHKeHoachXuongDetailId,
                VatTuId = entity.VatTuId,
				Title = entity.Title,
				ThuHai = entity.ThuHai,
				ThuBa = entity.ThuBa,
				ThuTu = entity.ThuTu,
				ThuNam = entity.ThuNam,
				ThuSau = entity.ThuSau,
				ThuBay = entity.ThuBay,
				ChuNhat = entity.ChuNhat,
				TongNhapKho = entity.TongNhapKho,
				GhiChu = entity.GhiChu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,
                TenKeHoachXuong = (entity.PKHKeHoachXuong != null) ? entity.PKHKeHoachXuong.Title : string.Empty,
                TenXuong = (entity.PKHKeHoachXuong.Xuong != null) ? entity.PKHKeHoachXuong.Xuong.Title : string.Empty,
            };
        }
        public static PKHKeHoachNhapKhoThanhPhamDetailModel MapToModel(this PKHKeHoachNhapKhoThanhPhamDetail entity, PKHKeHoachNhapKhoThanhPhamDetailModel model)
        {
			model.PKHKeHoachNhapKhoThanhPhamDetailId = entity.PKHKeHoachNhapKhoThanhPhamDetailId;
			model.PKHKeHoachXuongId = entity.PKHKeHoachXuongId;
            model.PKHKeHoachXuongDetailId = entity.PKHKeHoachXuongDetailId;
            model.VatTuId = entity.VatTuId;
			model.Title = entity.Title;
			model.ThuHai = entity.ThuHai;
			model.ThuBa = entity.ThuBa;
			model.ThuTu = entity.ThuTu;
			model.ThuNam = entity.ThuNam;
			model.ThuSau = entity.ThuSau;
			model.ThuBay = entity.ThuBay;
			model.ChuNhat = entity.ChuNhat;
			model.TongNhapKho = entity.TongNhapKho;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static PKHKeHoachNhapKhoThanhPhamDetail MapToEntity(this PKHKeHoachNhapKhoThanhPhamDetailModel model)
        {
            return new PKHKeHoachNhapKhoThanhPhamDetail
            {
				PKHKeHoachNhapKhoThanhPhamDetailId = model.PKHKeHoachNhapKhoThanhPhamDetailId,
				PKHKeHoachXuongId = model.PKHKeHoachXuongId,
                PKHKeHoachXuongDetailId = model.PKHKeHoachXuongDetailId,
                VatTuId = model.VatTuId,
				Title = model.Title,
				ThuHai = model.ThuHai,
				ThuBa = model.ThuBa,
				ThuTu = model.ThuTu,
				ThuNam = model.ThuNam,
				ThuSau = model.ThuSau,
				ThuBay = model.ThuBay,
				ChuNhat = model.ChuNhat,
				TongNhapKho = model.TongNhapKho,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHKeHoachNhapKhoThanhPhamDetail MapToEntity(this PKHKeHoachNhapKhoThanhPhamDetailModel model, PKHKeHoachNhapKhoThanhPhamDetail entity)
        {
			entity.PKHKeHoachNhapKhoThanhPhamDetailId = model.PKHKeHoachNhapKhoThanhPhamDetailId;
			entity.PKHKeHoachXuongId = model.PKHKeHoachXuongId;
            entity.PKHKeHoachXuongDetailId = model.PKHKeHoachXuongDetailId;
            entity.VatTuId = model.VatTuId;
			entity.Title = model.Title;
			entity.ThuHai = model.ThuHai;
			entity.ThuBa = model.ThuBa;
			entity.ThuTu = model.ThuTu;
			entity.ThuNam = model.ThuNam;
			entity.ThuSau = model.ThuSau;
			entity.ThuBay = model.ThuBay;
			entity.ChuNhat = model.ChuNhat;
			entity.TongNhapKho = model.TongNhapKho;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHKeHoachNhapKhoThanhPhamDetail> MapToEntities(this List<PKHKeHoachNhapKhoThanhPhamDetailModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHKeHoachNhapKhoThanhPhamDetailModel> MapToModels(this List<PKHKeHoachNhapKhoThanhPhamDetail> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
