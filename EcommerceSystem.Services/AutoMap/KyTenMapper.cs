﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.KyTenModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class KyTenMapper
    {
        #region Mapping KyTen
        public static KyTenModel MapToModel(this KyTen entity)
        {
            return new KyTenModel
            {
				KyTenId = entity.KyTenId,
				MaKyTen = entity.MaKyTen,
				Title = entity.Title,
				NhanVienId = entity.NhanVienId,
				TenThuKho = entity.TenThuKho,
				GhiChu = entity.GhiChu,
				XuongId = entity.XuongId,
				LoaiKyTen = entity.LoaiKyTen,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static KyTenModel MapToModel(this KyTen entity, KyTenModel model)
        {
			model.KyTenId = entity.KyTenId;
			model.MaKyTen = entity.MaKyTen;
			model.Title = entity.Title;
			model.NhanVienId = entity.NhanVienId;
			model.TenThuKho = entity.TenThuKho;
			model.GhiChu = entity.GhiChu;
			model.XuongId = entity.XuongId;
			model.LoaiKyTen = entity.LoaiKyTen;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static KyTen MapToEntity(this KyTenModel model)
        {
            return new KyTen
            {
				KyTenId = model.KyTenId,
				MaKyTen = model.MaKyTen,
				Title = model.Title,
				NhanVienId = model.NhanVienId,
				TenThuKho = model.TenThuKho,
				GhiChu = model.GhiChu,
				XuongId = model.XuongId,
				LoaiKyTen = model.LoaiKyTen,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static KyTen MapToEntity(this KyTenModel model, KyTen entity)
        {
			entity.KyTenId = model.KyTenId;
			entity.MaKyTen = model.MaKyTen;
			entity.Title = model.Title;
			entity.NhanVienId = model.NhanVienId;
			entity.TenThuKho = model.TenThuKho;
			entity.GhiChu = model.GhiChu;
			entity.XuongId = model.XuongId;
			entity.LoaiKyTen = model.LoaiKyTen;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<KyTen> MapToEntities(this List<KyTenModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<KyTenModel> MapToModels(this List<KyTen> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
