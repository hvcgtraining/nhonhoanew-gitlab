﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.DanhSachChiTietModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class DanhSachChiTietMapper
    {
        #region Mapping DanhSachChiTiet
        public static DanhSachChiTietModel MapToModel(this DanhSachChiTiet entity)
        {
            return new DanhSachChiTietModel
            {
				DanhSachChiTietId = entity.DanhSachChiTietId,
				Title = entity.Title,
				ChiTietVatTuId = entity.ChiTietVatTuId,
				SoLuongCoPhanId = entity.SoLuongCoPhanId,
				SoLuong = entity.SoLuong,
				GhiChu = entity.GhiChu,
				HasChild = entity.HasChild.HasValue ? entity.HasChild.Value: false,
				ParentId = entity.ParentId,
				DinhMucCatToleId = entity.DinhMucCatToleId,
				DinhMucSanXuatId = entity.DinhMucSanXuatId,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

                MaChiTietVatTu = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.MaChiTietVatTu : string.Empty,
                TenChiTietVatTu = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.Title : string.Empty,
                DonVi = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.DonVi : "",

            };
        }
        public static DanhSachChiTietModel MapToModel(this DanhSachChiTiet entity, DanhSachChiTietModel model)
        {
			model.DanhSachChiTietId = entity.DanhSachChiTietId;
			model.Title = entity.Title;
			model.ChiTietVatTuId = entity.ChiTietVatTuId;
			model.SoLuongCoPhanId = entity.SoLuongCoPhanId;
			model.SoLuong = entity.SoLuong;
			model.GhiChu = entity.GhiChu;
            model.HasChild = entity.HasChild.HasValue ? entity.HasChild.Value : false;
			model.ParentId = entity.ParentId;
			model.DinhMucCatToleId = entity.DinhMucCatToleId;
			model.DinhMucSanXuatId = entity.DinhMucSanXuatId;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            model.MaChiTietVatTu = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.MaChiTietVatTu : string.Empty;
            model.TenChiTietVatTu = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.Title : string.Empty;
            model.DonVi = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.DonVi : "";

            return model;
        }
        public static DanhSachChiTiet MapToEntity(this DanhSachChiTietModel model)
        {
            return new DanhSachChiTiet
            {
				DanhSachChiTietId = model.DanhSachChiTietId,
				Title = model.Title,
				ChiTietVatTuId = model.ChiTietVatTuId,
				SoLuongCoPhanId = model.SoLuongCoPhanId,
				SoLuong = model.SoLuong,
				GhiChu = model.GhiChu,
				HasChild = model.HasChild,
				ParentId = model.ParentId,
				DinhMucCatToleId = model.DinhMucCatToleId,
				DinhMucSanXuatId = model.DinhMucSanXuatId,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static DanhSachChiTiet MapToEntity(this DanhSachChiTietModel model, DanhSachChiTiet entity)
        {
			entity.DanhSachChiTietId = model.DanhSachChiTietId;
			entity.Title = model.Title;
			entity.ChiTietVatTuId = model.ChiTietVatTuId;
			entity.SoLuongCoPhanId = model.SoLuongCoPhanId;
			entity.SoLuong = model.SoLuong;
			entity.GhiChu = model.GhiChu;
			entity.HasChild = model.HasChild;
			entity.ParentId = model.ParentId;
			entity.DinhMucCatToleId = model.DinhMucCatToleId;
			entity.DinhMucSanXuatId = model.DinhMucSanXuatId;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<DanhSachChiTiet> MapToEntities(this List<DanhSachChiTietModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<DanhSachChiTietModel> MapToModels(this List<DanhSachChiTiet> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
