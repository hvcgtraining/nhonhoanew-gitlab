﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.ChiTietVatTuModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class ChiTietVatTuMapper
    {
        #region Mapping ChiTietVatTu
        public static ChiTietVatTuModel MapToModel(this ChiTietVatTu entity)
        {
            return new ChiTietVatTuModel
            {
                ChiTietVatTuId = entity.ChiTietVatTuId,
                MaChiTietVatTu = entity.MaChiTietVatTu,
                Title = entity.Title,
                DonVi = entity.DonVi,
                GhiChu = entity.GhiChu,
                CreatedDate = entity.CreatedDate,
                UpdatedDate = entity.UpdatedDate,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                Status = entity.Status,
                LoaiVatTuId = entity.LoaiVatTuId,
                MaLoaiVatTu = entity.LoaiVatTu != null ? entity.LoaiVatTu.MaLoaiVatTu : "",
                TenLoaiVatTu = entity.LoaiVatTu != null ? entity.LoaiVatTu.Title : "",
            };
        }
        public static ChiTietVatTuModel MapToModel(this ChiTietVatTu entity, ChiTietVatTuModel model)
        {
			model.ChiTietVatTuId = entity.ChiTietVatTuId;
			model.MaChiTietVatTu = entity.MaChiTietVatTu;
			model.Title = entity.Title;
			model.DonVi = entity.DonVi;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;
            model.LoaiVatTuId = entity.LoaiVatTuId;
            return model;
        }
        public static ChiTietVatTu MapToEntity(this ChiTietVatTuModel model)
        {
            return new ChiTietVatTu
            {
				ChiTietVatTuId = model.ChiTietVatTuId,
				MaChiTietVatTu = model.MaChiTietVatTu,
				Title = model.Title,
				DonVi = model.DonVi,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,
                LoaiVatTuId = model.LoaiVatTuId
            };
        }
        public static ChiTietVatTu MapToEntity(this ChiTietVatTuModel model, ChiTietVatTu entity)
        {
			entity.ChiTietVatTuId = model.ChiTietVatTuId;
			entity.MaChiTietVatTu = model.MaChiTietVatTu;
			entity.Title = model.Title;
			entity.DonVi = model.DonVi;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;
            entity.LoaiVatTuId = model.LoaiVatTuId;
            return entity;
        }
        public static List<ChiTietVatTu> MapToEntities(this List<ChiTietVatTuModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<ChiTietVatTuModel> MapToModels(this List<ChiTietVatTu> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
