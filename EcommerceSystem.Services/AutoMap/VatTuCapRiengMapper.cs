﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.VatTuCapRiengModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class VatTuCapRiengMapper
    {
        #region Mapping VatTuCapRieng
        public static VatTuCapRiengModel MapToModel(this VatTuCapRieng entity)
        {
            return new VatTuCapRiengModel
            {
				VatTuCapRiengId = entity.VatTuCapRiengId,
				Title = entity.Title,
				VatTuId = entity.VatTuId,
				NhomSanPhamId = entity.NhomSanPhamId,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static VatTuCapRiengModel MapToModel(this VatTuCapRieng entity, VatTuCapRiengModel model)
        {
			model.VatTuCapRiengId = entity.VatTuCapRiengId;
			model.Title = entity.Title;
			model.VatTuId = entity.VatTuId;
			model.NhomSanPhamId = entity.NhomSanPhamId;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static VatTuCapRieng MapToEntity(this VatTuCapRiengModel model)
        {
            return new VatTuCapRieng
            {
				VatTuCapRiengId = model.VatTuCapRiengId,
				Title = model.Title,
				VatTuId = model.VatTuId,
				NhomSanPhamId = model.NhomSanPhamId,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static VatTuCapRieng MapToEntity(this VatTuCapRiengModel model, VatTuCapRieng entity)
        {
			entity.VatTuCapRiengId = model.VatTuCapRiengId;
			entity.Title = model.Title;
			entity.VatTuId = model.VatTuId;
			entity.NhomSanPhamId = model.NhomSanPhamId;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<VatTuCapRieng> MapToEntities(this List<VatTuCapRiengModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<VatTuCapRiengModel> MapToModels(this List<VatTuCapRieng> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
