﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHKeHoachSXNKXHDetailModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHKeHoachSXNKXHDetailMapper
    {
        #region Mapping PKHKeHoachSXNKXHDetail
        public static PKHKeHoachSXNKXHDetailModel MapToModel(this PKHKeHoachSXNKXHDetail entity)
        {
            return new PKHKeHoachSXNKXHDetailModel
            {
				PKHKeHoachSXNKXHDetailId = entity.PKHKeHoachSXNKXHDetailId,
				PKHKeHoachSXNKXHId = entity.PKHKeHoachSXNKXHId,
				VatTuId = entity.VatTuId,
				Title = entity.Title,
				TonKho = entity.TonKho,
				NhapKhoTuanTruoc = entity.NhapKhoTuanTruoc,
				XuatHaNoi = entity.XuatHaNoi,
				XuatKho = entity.XuatKho,
				TongTonKho = entity.TongTonKho,
				TongNhapKho = entity.TongNhapKho,
				XuatDaiLy = entity.XuatDaiLy,
				DuKienXuatKho = entity.DuKienXuatKho,
				DuKienTonKho = entity.DuKienTonKho,
				SoLuongCan = entity.SoLuongCan,
				SoLuongBaoBi = entity.SoLuongBaoBi,
				DieuChinhCan = entity.DieuChinhCan,
				DieuChinhBaoBi = entity.DieuChinhBaoBi,
				TongSoCan = entity.TongSoCan,
				Xuong4 = entity.Xuong4,
				Xuong5 = entity.Xuong5,
				Xuong6 = entity.Xuong6,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,
				XuongEx = entity.XuongEx,
                MaVatTu = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.MaChiTietVatTu: "",
                TenVatTu = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.Title: ""
            };
        }
        public static PKHKeHoachSXNKXHDetailModel MapToModel(this PKHKeHoachSXNKXHDetail entity, PKHKeHoachSXNKXHDetailModel model)
        {
			model.PKHKeHoachSXNKXHDetailId = entity.PKHKeHoachSXNKXHDetailId;
			model.PKHKeHoachSXNKXHId = entity.PKHKeHoachSXNKXHId;
			model.VatTuId = entity.VatTuId;
			model.Title = entity.Title;
			model.TonKho = entity.TonKho;
			model.NhapKhoTuanTruoc = entity.NhapKhoTuanTruoc;
			model.XuatHaNoi = entity.XuatHaNoi;
			model.XuatKho = entity.XuatKho;
			model.TongTonKho = entity.TongTonKho;
			model.TongNhapKho = entity.TongNhapKho;
			model.XuatDaiLy = entity.XuatDaiLy;
			model.DuKienXuatKho = entity.DuKienXuatKho;
			model.DuKienTonKho = entity.DuKienTonKho;
			model.SoLuongCan = entity.SoLuongCan;
			model.SoLuongBaoBi = entity.SoLuongBaoBi;
			model.DieuChinhCan = entity.DieuChinhCan;
			model.DieuChinhBaoBi = entity.DieuChinhBaoBi;
			model.TongSoCan = entity.TongSoCan;
			model.Xuong4 = entity.Xuong4;
			model.Xuong5 = entity.Xuong5;
			model.Xuong6 = entity.Xuong6;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;
			model.XuongEx = entity.XuongEx;

            return model;
        }
        public static PKHKeHoachSXNKXHDetail MapToEntity(this PKHKeHoachSXNKXHDetailModel model)
        {
            return new PKHKeHoachSXNKXHDetail
            {
				PKHKeHoachSXNKXHDetailId = model.PKHKeHoachSXNKXHDetailId,
				PKHKeHoachSXNKXHId = model.PKHKeHoachSXNKXHId,
				VatTuId = model.VatTuId,
				Title = model.Title,
				TonKho = model.TonKho,
				NhapKhoTuanTruoc = model.NhapKhoTuanTruoc,
				XuatHaNoi = model.XuatHaNoi,
				XuatKho = model.XuatKho,
				TongTonKho = model.TongTonKho,
				TongNhapKho = model.TongNhapKho,
				XuatDaiLy = model.XuatDaiLy,
				DuKienXuatKho = model.DuKienXuatKho,
				DuKienTonKho = model.DuKienTonKho,
				SoLuongCan = model.SoLuongCan,
				SoLuongBaoBi = model.SoLuongBaoBi,
				DieuChinhCan = model.DieuChinhCan,
				DieuChinhBaoBi = model.DieuChinhBaoBi,
				TongSoCan = model.TongSoCan,
				Xuong4 = model.Xuong4,
				Xuong5 = model.Xuong5,
				Xuong6 = model.Xuong6,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,
				XuongEx = model.XuongEx,

            };
        }
        public static PKHKeHoachSXNKXHDetail MapToEntity(this PKHKeHoachSXNKXHDetailModel model, PKHKeHoachSXNKXHDetail entity)
        {
			entity.PKHKeHoachSXNKXHDetailId = model.PKHKeHoachSXNKXHDetailId;
			entity.PKHKeHoachSXNKXHId = model.PKHKeHoachSXNKXHId;
			entity.VatTuId = model.VatTuId;
			entity.Title = model.Title;
			entity.TonKho = model.TonKho;
			entity.NhapKhoTuanTruoc = model.NhapKhoTuanTruoc;
			entity.XuatHaNoi = model.XuatHaNoi;
			entity.XuatKho = model.XuatKho;
			entity.TongTonKho = model.TongTonKho;
			entity.TongNhapKho = model.TongNhapKho;
			entity.XuatDaiLy = model.XuatDaiLy;
			entity.DuKienXuatKho = model.DuKienXuatKho;
			entity.DuKienTonKho = model.DuKienTonKho;
			entity.SoLuongCan = model.SoLuongCan;
			entity.SoLuongBaoBi = model.SoLuongBaoBi;
			entity.DieuChinhCan = model.DieuChinhCan;
			entity.DieuChinhBaoBi = model.DieuChinhBaoBi;
			entity.TongSoCan = model.TongSoCan;
			entity.Xuong4 = model.Xuong4;
			entity.Xuong5 = model.Xuong5;
			entity.Xuong6 = model.Xuong6;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;
			entity.XuongEx = model.XuongEx;

            return entity;
        }
        public static List<PKHKeHoachSXNKXHDetail> MapToEntities(this List<PKHKeHoachSXNKXHDetailModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHKeHoachSXNKXHDetailModel> MapToModels(this List<PKHKeHoachSXNKXHDetail> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
