﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.QuanLySoNModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class QuanLySoNMapper
    {
        #region Mapping QuanLySoN
        public static QuanLySoNModel MapToModel(this QuanLySoN entity)
        {
            return new QuanLySoNModel
            {
				QuanLySoNId = entity.QuanLySoNId,
				Title = entity.Title,
				SoTong = entity.SoTong,
				SoN = entity.SoN,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,
				VatTuId = entity.VatTuId,

            };
        }
        public static QuanLySoNModel MapToModel(this QuanLySoN entity, QuanLySoNModel model)
        {
			model.QuanLySoNId = entity.QuanLySoNId;
			model.Title = entity.Title;
			model.SoTong = entity.SoTong;
			model.SoN = entity.SoN;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;
			model.VatTuId = entity.VatTuId;

            return model;
        }
        public static QuanLySoN MapToEntity(this QuanLySoNModel model)
        {
            return new QuanLySoN
            {
				QuanLySoNId = model.QuanLySoNId,
				Title = model.Title,
				SoTong = model.SoTong,
				SoN = model.SoN,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,
				VatTuId = model.VatTuId,

            };
        }
        public static QuanLySoN MapToEntity(this QuanLySoNModel model, QuanLySoN entity)
        {
			entity.QuanLySoNId = model.QuanLySoNId;
			entity.Title = model.Title;
			entity.SoTong = model.SoTong;
			entity.SoN = model.SoN;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;
			entity.VatTuId = model.VatTuId;

            return entity;
        }
        public static List<QuanLySoN> MapToEntities(this List<QuanLySoNModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<QuanLySoNModel> MapToModels(this List<QuanLySoN> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
