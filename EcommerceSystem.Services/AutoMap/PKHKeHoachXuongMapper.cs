﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHKeHoachXuongModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHKeHoachXuongMapper
    {
        #region Mapping PKHKeHoachXuong
        public static PKHKeHoachXuongModel MapToModel(this PKHKeHoachXuong entity)
        {
            return new PKHKeHoachXuongModel
            {
                PKHKeHoachXuongId = entity.PKHKeHoachXuongId,
                Title = entity.Title,
                PKHKeHoachSXNKXHId = entity.PKHKeHoachSXNKXHId,
                XuongId = entity.XuongId,
                TongSoLuong = entity.TongSoLuong,
                ThemGio = entity.ThemGio,
                TinhTrang = entity.TinhTrang,
                GhiChu = entity.GhiChu,
                CreatedDate = entity.CreatedDate,
                UpdatedDate = entity.UpdatedDate,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                Status = entity.Status,
                TenXuong = entity.Xuong != null ? entity.Xuong.Title : ""
            };
        }
        public static PKHKeHoachXuongModel MapToModel(this PKHKeHoachXuong entity, PKHKeHoachXuongModel model)
        {
			model.PKHKeHoachXuongId = entity.PKHKeHoachXuongId;
			model.Title = entity.Title;
			model.PKHKeHoachSXNKXHId = entity.PKHKeHoachSXNKXHId;
			model.XuongId = entity.XuongId;
			model.TongSoLuong = entity.TongSoLuong;
			model.ThemGio = entity.ThemGio;
			model.TinhTrang = entity.TinhTrang;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static PKHKeHoachXuong MapToEntity(this PKHKeHoachXuongModel model)
        {
            return new PKHKeHoachXuong
            {
				PKHKeHoachXuongId = model.PKHKeHoachXuongId,
				Title = model.Title,
				PKHKeHoachSXNKXHId = model.PKHKeHoachSXNKXHId,
				XuongId = model.XuongId,
				TongSoLuong = model.TongSoLuong,
				ThemGio = model.ThemGio,
				TinhTrang = model.TinhTrang,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHKeHoachXuong MapToEntity(this PKHKeHoachXuongModel model, PKHKeHoachXuong entity)
        {
			entity.PKHKeHoachXuongId = model.PKHKeHoachXuongId;
			entity.Title = model.Title;
			entity.PKHKeHoachSXNKXHId = model.PKHKeHoachSXNKXHId;
			entity.XuongId = model.XuongId;
			entity.TongSoLuong = model.TongSoLuong;
			entity.ThemGio = model.ThemGio;
			entity.TinhTrang = model.TinhTrang;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHKeHoachXuong> MapToEntities(this List<PKHKeHoachXuongModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHKeHoachXuongModel> MapToModels(this List<PKHKeHoachXuong> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
