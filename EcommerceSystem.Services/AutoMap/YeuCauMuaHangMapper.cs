﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.YeuCauMuaHangModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class YeuCauMuaHangMapper
    {
        #region Mapping YeuCauMuaHang
        public static YeuCauMuaHangModel MapToModel(this YeuCauMuaHang entity)
        {
            return new YeuCauMuaHangModel
            {
                YeuCauMuaHangId = entity.YeuCauMuaHangId,
                XuongId = entity.XuongId,
                KhoId = entity.KhoId,
                ChiTietVatTuId = entity.ChiTietVatTuId,
                PKHPhieuYeuCauMuaHangId = entity.PKHPhieuYeuCauMuaHangId,
                Title = entity.Title,
                SoPR = entity.SoPR,
                NgayYeuCau = entity.NgayYeuCau,
                NgayKetThucDuKien = entity.NgayKetThucDuKien,
                TongSoLuong = entity.TongSoLuong,
                NoiDung = entity.NoiDung,
                GhiChu = entity.GhiChu,
                TrangThai = entity.TrangThai,
                CreatedDate = entity.CreatedDate,
                UpdatedDate = entity.UpdatedDate,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                Status = entity.Status,
                Xuong = (entity.Xuong != null) ? entity.Xuong.Title : string.Empty,
                Kho = (entity.Kho != null) ? entity.Kho.Title : string.Empty,
                TenChiTietVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.Title : string.Empty,
                TenPhieuYeuCauMuaHang = (entity.PKHPhieuYeuCauMuaHang != null) ? entity.PKHPhieuYeuCauMuaHang.Title : string.Empty,
            };
        }
        public static YeuCauMuaHangModel MapToModel(this YeuCauMuaHang entity, YeuCauMuaHangModel model)
        {
            model.YeuCauMuaHangId = entity.YeuCauMuaHangId;
            model.XuongId = entity.XuongId;
            model.KhoId = entity.KhoId;
            model.ChiTietVatTuId = entity.ChiTietVatTuId;
            model.PKHPhieuYeuCauMuaHangId = entity.PKHPhieuYeuCauMuaHangId;
            model.Title = entity.Title;
            model.SoPR = entity.SoPR;
            model.NgayYeuCau = entity.NgayYeuCau;
            model.NgayKetThucDuKien = entity.NgayKetThucDuKien;
            model.TongSoLuong = entity.TongSoLuong;
            model.NoiDung = entity.NoiDung;
            model.GhiChu = entity.GhiChu;
            model.TrangThai = entity.TrangThai;
            model.CreatedDate = entity.CreatedDate;
            model.UpdatedDate = entity.UpdatedDate;
            model.CreatedBy = entity.CreatedBy;
            model.UpdatedBy = entity.UpdatedBy;
            model.Status = entity.Status;
            model.Xuong = (entity.Xuong != null) ? entity.Xuong.Title : string.Empty;
            model.Kho = (entity.Kho != null) ? entity.Kho.Title : string.Empty;
            model.TenChiTietVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.Title : string.Empty;
            model.TenPhieuYeuCauMuaHang = (entity.PKHPhieuYeuCauMuaHang != null) ? entity.PKHPhieuYeuCauMuaHang.Title : string.Empty;

            return model;
        }
        public static YeuCauMuaHang MapToEntity(this YeuCauMuaHangModel model)
        {
            return new YeuCauMuaHang
            {
                YeuCauMuaHangId = model.YeuCauMuaHangId,
                XuongId = model.XuongId,
                KhoId = model.KhoId,
                ChiTietVatTuId = model.ChiTietVatTuId,
                PKHPhieuYeuCauMuaHangId = model.PKHPhieuYeuCauMuaHangId,
                Title = model.Title,
                SoPR = model.SoPR,
                NgayYeuCau = model.NgayYeuCau,
                NgayKetThucDuKien = model.NgayKetThucDuKien,
                TongSoLuong = model.TongSoLuong,
                NoiDung = model.NoiDung,
                GhiChu = model.GhiChu,
                TrangThai = model.TrangThai,
                CreatedDate = model.CreatedDate,
                UpdatedDate = model.UpdatedDate,
                CreatedBy = model.CreatedBy,
                UpdatedBy = model.UpdatedBy,
                Status = model.Status,

            };
        }
        public static YeuCauMuaHang MapToEntity(this YeuCauMuaHangModel model, YeuCauMuaHang entity)
        {
            entity.YeuCauMuaHangId = model.YeuCauMuaHangId;
            entity.XuongId = model.XuongId;
            entity.KhoId = model.KhoId;
            entity.ChiTietVatTuId = model.ChiTietVatTuId;
            entity.PKHPhieuYeuCauMuaHangId = model.PKHPhieuYeuCauMuaHangId;
            entity.Title = model.Title;
            entity.SoPR = model.SoPR;
            entity.NgayYeuCau = model.NgayYeuCau;
            entity.NgayKetThucDuKien = model.NgayKetThucDuKien;
            entity.TongSoLuong = model.TongSoLuong;
            entity.NoiDung = model.NoiDung;
            entity.GhiChu = model.GhiChu;
            entity.TrangThai = model.TrangThai;
            //entity.CreatedDate = model.CreatedDate;
            entity.UpdatedDate = model.UpdatedDate;
            //entity.CreatedBy = model.CreatedBy;
            entity.UpdatedBy = model.UpdatedBy;
            entity.Status = model.Status;

            return entity;
        }
        public static List<YeuCauMuaHang> MapToEntities(this List<YeuCauMuaHangModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<YeuCauMuaHangModel> MapToModels(this List<YeuCauMuaHang> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
