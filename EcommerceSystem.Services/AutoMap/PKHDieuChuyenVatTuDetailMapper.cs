﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHDieuChuyenVatTuDetailModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHDieuChuyenVatTuDetailMapper
    {
        #region Mapping PKHDieuChuyenVatTuDetail
        public static PKHDieuChuyenVatTuDetailModel MapToModel(this PKHDieuChuyenVatTuDetail entity)
        {
            return new PKHDieuChuyenVatTuDetailModel
            {
				PKHDieuChuyenVatTuDetailId = entity.PKHDieuChuyenVatTuDetailId,
				PKHDieuChuyenVatTuId = entity.PKHDieuChuyenVatTuId,
				ChiTietVatTuId = entity.ChiTietVatTuId,
				LoaiVatTuId = entity.LoaiVatTuId,
				SoLuong = entity.SoLuong,
				GhiChu = entity.GhiChu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static PKHDieuChuyenVatTuDetailModel MapToModel(this PKHDieuChuyenVatTuDetail entity, PKHDieuChuyenVatTuDetailModel model)
        {
			model.PKHDieuChuyenVatTuDetailId = entity.PKHDieuChuyenVatTuDetailId;
			model.PKHDieuChuyenVatTuId = entity.PKHDieuChuyenVatTuId;
			model.ChiTietVatTuId = entity.ChiTietVatTuId;
			model.LoaiVatTuId = entity.LoaiVatTuId;
			model.SoLuong = entity.SoLuong;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static PKHDieuChuyenVatTuDetail MapToEntity(this PKHDieuChuyenVatTuDetailModel model)
        {
            return new PKHDieuChuyenVatTuDetail
            {
				PKHDieuChuyenVatTuDetailId = model.PKHDieuChuyenVatTuDetailId,
				PKHDieuChuyenVatTuId = model.PKHDieuChuyenVatTuId,
				ChiTietVatTuId = model.ChiTietVatTuId,
				LoaiVatTuId = model.LoaiVatTuId,
				SoLuong = model.SoLuong,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHDieuChuyenVatTuDetail MapToEntity(this PKHDieuChuyenVatTuDetailModel model, PKHDieuChuyenVatTuDetail entity)
        {
			entity.PKHDieuChuyenVatTuDetailId = model.PKHDieuChuyenVatTuDetailId;
			entity.PKHDieuChuyenVatTuId = model.PKHDieuChuyenVatTuId;
			entity.ChiTietVatTuId = model.ChiTietVatTuId;
			entity.LoaiVatTuId = model.LoaiVatTuId;
			entity.SoLuong = model.SoLuong;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHDieuChuyenVatTuDetail> MapToEntities(this List<PKHDieuChuyenVatTuDetailModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHDieuChuyenVatTuDetailModel> MapToModels(this List<PKHDieuChuyenVatTuDetail> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
