﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.XuongModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class XuongMapper
    {
        #region Mapping Xuong
        public static XuongModel MapToModel(this Xuong entity)
        {
            return new XuongModel
            {
				XuongId = entity.XuongId,
				MaXuong = entity.MaXuong,
				Title = entity.Title,
				ThongTinXuong = entity.ThongTinXuong,
				GhiChu = entity.GhiChu,
				Type = entity.Type,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static XuongModel MapToModel(this Xuong entity, XuongModel model)
        {
			model.XuongId = entity.XuongId;
			model.MaXuong = entity.MaXuong;
			model.Title = entity.Title;
			model.ThongTinXuong = entity.ThongTinXuong;
			model.GhiChu = entity.GhiChu;
			model.Type = entity.Type;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static Xuong MapToEntity(this XuongModel model)
        {
            return new Xuong
            {
				XuongId = model.XuongId,
				MaXuong = model.MaXuong,
				Title = model.Title,
				ThongTinXuong = model.ThongTinXuong,
				GhiChu = model.GhiChu,
				Type = model.Type,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static Xuong MapToEntity(this XuongModel model, Xuong entity)
        {
			entity.XuongId = model.XuongId;
			entity.MaXuong = model.MaXuong;
			entity.Title = model.Title;
			entity.ThongTinXuong = model.ThongTinXuong;
			entity.GhiChu = model.GhiChu;
			entity.Type = model.Type;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<Xuong> MapToEntities(this List<XuongModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<XuongModel> MapToModels(this List<Xuong> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
