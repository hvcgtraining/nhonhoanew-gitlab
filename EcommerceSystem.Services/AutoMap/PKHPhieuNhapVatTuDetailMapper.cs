﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHPhieuNhapVatTuDetailModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHPhieuNhapVatTuDetailMapper
    {
        #region Mapping PKHPhieuNhapVatTuDetail
        public static PKHPhieuNhapVatTuDetailModel MapToModel(this PKHPhieuNhapVatTuDetail entity)
        {
            return new PKHPhieuNhapVatTuDetailModel
            {
				PKHPhieuNhapVatTuDetailId = entity.PKHPhieuNhapVatTuDetailId,
				PKHPhieuNhapVatTuId = entity.PKHPhieuNhapVatTuId,
				KhoId = entity.KhoId,
				CustomerId = entity.CustomerId,
				Title = entity.Title,
				ChiTietVatTuId = entity.ChiTietVatTuId,
				SoLuong = entity.SoLuong,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static PKHPhieuNhapVatTuDetailModel MapToModel(this PKHPhieuNhapVatTuDetail entity, PKHPhieuNhapVatTuDetailModel model)
        {
			model.PKHPhieuNhapVatTuDetailId = entity.PKHPhieuNhapVatTuDetailId;
			model.PKHPhieuNhapVatTuId = entity.PKHPhieuNhapVatTuId;
			model.KhoId = entity.KhoId;
			model.CustomerId = entity.CustomerId;
			model.Title = entity.Title;
			model.ChiTietVatTuId = entity.ChiTietVatTuId;
			model.SoLuong = entity.SoLuong;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static PKHPhieuNhapVatTuDetail MapToEntity(this PKHPhieuNhapVatTuDetailModel model)
        {
            return new PKHPhieuNhapVatTuDetail
            {
				PKHPhieuNhapVatTuDetailId = model.PKHPhieuNhapVatTuDetailId,
				PKHPhieuNhapVatTuId = model.PKHPhieuNhapVatTuId,
				KhoId = model.KhoId,
				CustomerId = model.CustomerId,
				Title = model.Title,
				ChiTietVatTuId = model.ChiTietVatTuId,
				SoLuong = model.SoLuong,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHPhieuNhapVatTuDetail MapToEntity(this PKHPhieuNhapVatTuDetailModel model, PKHPhieuNhapVatTuDetail entity)
        {
			entity.PKHPhieuNhapVatTuDetailId = model.PKHPhieuNhapVatTuDetailId;
			entity.PKHPhieuNhapVatTuId = model.PKHPhieuNhapVatTuId;
			entity.KhoId = model.KhoId;
			entity.CustomerId = model.CustomerId;
			entity.Title = model.Title;
			entity.ChiTietVatTuId = model.ChiTietVatTuId;
			entity.SoLuong = model.SoLuong;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHPhieuNhapVatTuDetail> MapToEntities(this List<PKHPhieuNhapVatTuDetailModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHPhieuNhapVatTuDetailModel> MapToModels(this List<PKHPhieuNhapVatTuDetail> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
