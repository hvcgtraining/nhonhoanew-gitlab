﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.KhoTinhTonModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class KhoTinhTonMapper
    {
        #region Mapping KhoTinhTon
        public static KhoTinhTonModel MapToModel(this KhoTinhTon entity)
        {
            return new KhoTinhTonModel
            {
				KhoTinhTonId = entity.KhoTinhTonId,
				Title = entity.Title,
				KhoId = entity.KhoId,
				NhomSanPhamId = entity.NhomSanPhamId,
				GhiChu = entity.GhiChu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,
                KhoName = (entity.Kho != null) ? entity.Kho.Title : string.Empty,
                NhomSanPhamName=(entity.NhomSanPham!=null)?entity.NhomSanPham.Title:string.Empty,
            };
        }
        public static KhoTinhTonModel MapToModel(this KhoTinhTon entity, KhoTinhTonModel model)
        {
			model.KhoTinhTonId = entity.KhoTinhTonId;
			model.Title = entity.Title;
			model.KhoId = entity.KhoId;
			model.NhomSanPhamId = entity.NhomSanPhamId;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;
            model.KhoName = entity.Kho.Title;
            model.NhomSanPhamName = entity.NhomSanPham.Title;
            return model;
        }
        public static KhoTinhTon MapToEntity(this KhoTinhTonModel model)
        {
            return new KhoTinhTon
            {
				KhoTinhTonId = model.KhoTinhTonId,
				Title = model.Title,
				KhoId = model.KhoId,
				NhomSanPhamId = model.NhomSanPhamId,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static KhoTinhTon MapToEntity(this KhoTinhTonModel model, KhoTinhTon entity)
        {
			entity.KhoTinhTonId = model.KhoTinhTonId;
			entity.Title = model.Title;
			entity.KhoId = model.KhoId;
			entity.NhomSanPhamId = model.NhomSanPhamId;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<KhoTinhTon> MapToEntities(this List<KhoTinhTonModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<KhoTinhTonModel> MapToModels(this List<KhoTinhTon> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
