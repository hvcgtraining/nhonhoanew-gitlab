﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.SoDuBanDauModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class SoDuBanDauMapper
    {
        #region Mapping SoDuBanDau
        public static SoDuBanDauModel MapToModel(this SoDuBanDau entity)
        {
            return new SoDuBanDauModel
            {
                SoDuBanDauId = entity.SoDuBanDauId,
                Title = entity.Title,
                KhoId = entity.KhoId,
                VatTuId = entity.VatTuId,
                SoDuDauKy = entity.SoDuDauKy,
                CreatedDate = entity.CreatedDate,
                UpdatedDate = entity.UpdatedDate,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                Status = entity.Status,
                MaKho = (entity.Kho != null) ? entity.Kho.MaKho : string.Empty,
                MaLoaiVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.MaChiTietVatTu : string.Empty,
                TenVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.Title : string.Empty,
            };
        }
        public static SoDuBanDauModel MapToModel(this SoDuBanDau entity, SoDuBanDauModel model)
        {
			model.SoDuBanDauId = entity.SoDuBanDauId;
			model.Title = entity.Title;
			model.KhoId = entity.KhoId;
			model.VatTuId = entity.VatTuId;
			model.SoDuDauKy = entity.SoDuDauKy;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;


            return model;
        }
        public static SoDuBanDau MapToEntity(this SoDuBanDauModel model)
        {
            return new SoDuBanDau
            {
				SoDuBanDauId = model.SoDuBanDauId,
				Title = model.Title,
				KhoId = model.KhoId,
				VatTuId = model.VatTuId,
				SoDuDauKy = model.SoDuDauKy,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static SoDuBanDau MapToEntity(this SoDuBanDauModel model, SoDuBanDau entity)
        {
			entity.SoDuBanDauId = model.SoDuBanDauId;
			entity.Title = model.Title;
			entity.KhoId = model.KhoId;
			entity.VatTuId = model.VatTuId;
			entity.SoDuDauKy = model.SoDuDauKy;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<SoDuBanDau> MapToEntities(this List<SoDuBanDauModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<SoDuBanDauModel> MapToModels(this List<SoDuBanDau> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
