﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.DinhMucTuanModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class DinhMucTuanMapper
    {
        #region Mapping DinhMucTuan
        public static DinhMucTuanModel MapToModel(this DinhMucTuan entity)
        {
            return new DinhMucTuanModel
            {
				DinhMucTuanId = entity.DinhMucTuanId,
				Title = entity.Title,
				VatTuId = entity.VatTuId,
				DinhMuc = entity.DinhMuc,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

                MaLoaiVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.MaChiTietVatTu : string.Empty,
                TenLoaiVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.Title : string.Empty,

            };
        }
        public static DinhMucTuanModel MapToModel(this DinhMucTuan entity, DinhMucTuanModel model)
        {
			model.DinhMucTuanId = entity.DinhMucTuanId;
			model.Title = entity.Title;
			model.VatTuId = entity.VatTuId;
			model.DinhMuc = entity.DinhMuc;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            model.MaLoaiVatTu = entity.ChiTietVatTu.MaChiTietVatTu;
            model.TenLoaiVatTu = entity.ChiTietVatTu.Title;

            return model;
        }
        public static DinhMucTuan MapToEntity(this DinhMucTuanModel model)
        {
            return new DinhMucTuan
            {
				DinhMucTuanId = model.DinhMucTuanId,
				Title = model.Title,
				VatTuId = model.VatTuId,
				DinhMuc = model.DinhMuc,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static DinhMucTuan MapToEntity(this DinhMucTuanModel model, DinhMucTuan entity)
        {
			entity.DinhMucTuanId = model.DinhMucTuanId;
			entity.Title = model.Title;
			entity.VatTuId = model.VatTuId;
			entity.DinhMuc = model.DinhMuc;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<DinhMucTuan> MapToEntities(this List<DinhMucTuanModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<DinhMucTuanModel> MapToModels(this List<DinhMucTuan> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
