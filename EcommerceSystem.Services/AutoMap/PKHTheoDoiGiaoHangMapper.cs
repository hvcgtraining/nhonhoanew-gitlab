﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHTheoDoiGiaoHangModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHTheoDoiGiaoHangMapper
    {
        #region Mapping PKHTheoDoiGiaoHang
        public static PKHTheoDoiGiaoHangModel MapToModel(this PKHTheoDoiGiaoHang entity)
        {
            return new PKHTheoDoiGiaoHangModel
            {
				PKHTheoDoiGiaoHangId = entity.PKHTheoDoiGiaoHangId,
				PKHPhieuYeuCauMuaHangId = entity.PKHPhieuYeuCauMuaHangId,
				PKHPhieuYeuCauMuaHangDetailId = entity.PKHPhieuYeuCauMuaHangDetailId,
				PKHDuKienGiaoHangId = entity.PKHDuKienGiaoHangId,
				PKHKiemHoaId = entity.PKHKiemHoaId,
				NgayGiaoHang = entity.NgayGiaoHang,
				ChiTietVatTuId = entity.ChiTietVatTuId,
				CustomerId = entity.CustomerId,
				Title = entity.Title,
				SoLuongGiaoDuKien = entity.SoLuongGiaoDuKien,
				SoLuongGiaoThucTe = entity.SoLuongGiaoThucTe,
				SoLuongConThieu = entity.SoLuongConThieu,
				KiemHoaFlag = entity.KiemHoaFlag,
				SoLuongDat = entity.SoLuongDat,
				SoLuongKhongDat = entity.SoLuongKhongDat,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static PKHTheoDoiGiaoHangModel MapToModel(this PKHTheoDoiGiaoHang entity, PKHTheoDoiGiaoHangModel model)
        {
			model.PKHTheoDoiGiaoHangId = entity.PKHTheoDoiGiaoHangId;
			model.PKHPhieuYeuCauMuaHangId = entity.PKHPhieuYeuCauMuaHangId;
			model.PKHPhieuYeuCauMuaHangDetailId = entity.PKHPhieuYeuCauMuaHangDetailId;
			model.PKHDuKienGiaoHangId = entity.PKHDuKienGiaoHangId;
			model.PKHKiemHoaId = entity.PKHKiemHoaId;
			model.NgayGiaoHang = entity.NgayGiaoHang;
			model.ChiTietVatTuId = entity.ChiTietVatTuId;
			model.CustomerId = entity.CustomerId;
			model.Title = entity.Title;
			model.SoLuongGiaoDuKien = entity.SoLuongGiaoDuKien;
			model.SoLuongGiaoThucTe = entity.SoLuongGiaoThucTe;
			model.SoLuongConThieu = entity.SoLuongConThieu;
			model.KiemHoaFlag = entity.KiemHoaFlag;
			model.SoLuongDat = entity.SoLuongDat;
			model.SoLuongKhongDat = entity.SoLuongKhongDat;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static PKHTheoDoiGiaoHang MapToEntity(this PKHTheoDoiGiaoHangModel model)
        {
            return new PKHTheoDoiGiaoHang
            {
				PKHTheoDoiGiaoHangId = model.PKHTheoDoiGiaoHangId,
				PKHPhieuYeuCauMuaHangId = model.PKHPhieuYeuCauMuaHangId,
				PKHPhieuYeuCauMuaHangDetailId = model.PKHPhieuYeuCauMuaHangDetailId,
				PKHDuKienGiaoHangId = model.PKHDuKienGiaoHangId,
				PKHKiemHoaId = model.PKHKiemHoaId,
				NgayGiaoHang = model.NgayGiaoHang,
				ChiTietVatTuId = model.ChiTietVatTuId,
				CustomerId = model.CustomerId,
				Title = model.Title,
				SoLuongGiaoDuKien = model.SoLuongGiaoDuKien,
				SoLuongGiaoThucTe = model.SoLuongGiaoThucTe,
				SoLuongConThieu = model.SoLuongConThieu,
				KiemHoaFlag = model.KiemHoaFlag,
				SoLuongDat = model.SoLuongDat,
				SoLuongKhongDat = model.SoLuongKhongDat,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHTheoDoiGiaoHang MapToEntity(this PKHTheoDoiGiaoHangModel model, PKHTheoDoiGiaoHang entity)
        {
			entity.PKHTheoDoiGiaoHangId = model.PKHTheoDoiGiaoHangId;
			entity.PKHPhieuYeuCauMuaHangId = model.PKHPhieuYeuCauMuaHangId;
			entity.PKHPhieuYeuCauMuaHangDetailId = model.PKHPhieuYeuCauMuaHangDetailId;
			entity.PKHDuKienGiaoHangId = model.PKHDuKienGiaoHangId;
			entity.PKHKiemHoaId = model.PKHKiemHoaId;
			entity.NgayGiaoHang = model.NgayGiaoHang;
			entity.ChiTietVatTuId = model.ChiTietVatTuId;
			entity.CustomerId = model.CustomerId;
			entity.Title = model.Title;
			entity.SoLuongGiaoDuKien = model.SoLuongGiaoDuKien;
			entity.SoLuongGiaoThucTe = model.SoLuongGiaoThucTe;
			entity.SoLuongConThieu = model.SoLuongConThieu;
			entity.KiemHoaFlag = model.KiemHoaFlag;
			entity.SoLuongDat = model.SoLuongDat;
			entity.SoLuongKhongDat = model.SoLuongKhongDat;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHTheoDoiGiaoHang> MapToEntities(this List<PKHTheoDoiGiaoHangModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHTheoDoiGiaoHangModel> MapToModels(this List<PKHTheoDoiGiaoHang> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
