﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PhuLucIsoModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PhuLucIsoMapper
    {
        #region Mapping PhuLucIso
        public static PhuLucIsoModel MapToModel(this PhuLucIso entity)
        {
            return new PhuLucIsoModel
            {
				PhuLucIsoId = entity.PhuLucIsoId,
				MaPhuLucIso = entity.MaPhuLucIso,
				Title = entity.Title,
				SoPhuLuc = entity.SoPhuLuc,
				GhiChu = entity.GhiChu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static PhuLucIsoModel MapToModel(this PhuLucIso entity, PhuLucIsoModel model)
        {
			model.PhuLucIsoId = entity.PhuLucIsoId;
			model.MaPhuLucIso = entity.MaPhuLucIso;
			model.Title = entity.Title;
			model.SoPhuLuc = entity.SoPhuLuc;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static PhuLucIso MapToEntity(this PhuLucIsoModel model)
        {
            return new PhuLucIso
            {
				PhuLucIsoId = model.PhuLucIsoId,
				MaPhuLucIso = model.MaPhuLucIso,
				Title = model.Title,
				SoPhuLuc = model.SoPhuLuc,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PhuLucIso MapToEntity(this PhuLucIsoModel model, PhuLucIso entity)
        {
			entity.PhuLucIsoId = model.PhuLucIsoId;
			entity.MaPhuLucIso = model.MaPhuLucIso;
			entity.Title = model.Title;
			entity.SoPhuLuc = model.SoPhuLuc;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PhuLucIso> MapToEntities(this List<PhuLucIsoModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PhuLucIsoModel> MapToModels(this List<PhuLucIso> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
