﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.CoPhanCapXuongModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class CoPhanCapXuongMapper
    {
        #region Mapping CoPhanCapXuong
        public static CoPhanCapXuongModel MapToModel(this CoPhanCapXuong entity)
        {
            return new CoPhanCapXuongModel
            {
				CoPhanCapXuongId = entity.CoPhanCapXuongId,
				Title = entity.Title,
				KhoNhapId = entity.KhoNhapId,
				KhoXuatId = entity.KhoXuatId,
				VatTuId = entity.VatTuId,
				NhomSanPhamId = entity.NhomSanPhamId,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,
                MaKhoNhap = (entity.Kho !=null)? entity.Kho.MaKho : string.Empty,
                MaKhoXuat = (entity.Kho1 != null) ? entity.Kho1.MaKho : string.Empty,
                MaVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.MaChiTietVatTu : string.Empty,
                TenVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.Title : string.Empty,
                MaNhom = (entity.NhomSanPham != null) ? entity.NhomSanPham.MaNhomSanPham : string.Empty,
            };
        }
        public static CoPhanCapXuongModel MapToModel(this CoPhanCapXuong entity, CoPhanCapXuongModel model)
        {
			model.CoPhanCapXuongId = entity.CoPhanCapXuongId;
			model.Title = entity.Title;
			model.KhoNhapId = entity.KhoNhapId;
			model.KhoXuatId = entity.KhoXuatId;
			model.VatTuId = entity.VatTuId;
			model.NhomSanPhamId = entity.NhomSanPhamId;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;
            model.MaKhoXuat = (entity.Kho1 != null) ? entity.Kho1.MaKho : string.Empty;
            return model;
        }
        public static CoPhanCapXuong MapToEntity(this CoPhanCapXuongModel model)
        {
            return new CoPhanCapXuong
            {
				CoPhanCapXuongId = model.CoPhanCapXuongId,
				Title = model.Title,
				KhoNhapId = model.KhoNhapId,
				KhoXuatId = model.KhoXuatId,
                VatTuId = model.VatTuId,
				NhomSanPhamId = model.NhomSanPhamId,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static CoPhanCapXuong MapToEntity(this CoPhanCapXuongModel model, CoPhanCapXuong entity)
        {
			entity.CoPhanCapXuongId = model.CoPhanCapXuongId;
			entity.Title = model.Title;
			entity.KhoNhapId = model.KhoNhapId;
			entity.KhoXuatId = model.KhoXuatId;
			entity.VatTuId = model.VatTuId;
			entity.NhomSanPhamId = model.NhomSanPhamId;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<CoPhanCapXuong> MapToEntities(this List<CoPhanCapXuongModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<CoPhanCapXuongModel> MapToModels(this List<CoPhanCapXuong> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
