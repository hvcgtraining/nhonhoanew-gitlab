﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHPhieuYeuCauMuaHangDetailModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHPhieuYeuCauMuaHangDetailMapper
    {
        #region Mapping PKHPhieuYeuCauMuaHangDetail
        public static PKHPhieuYeuCauMuaHangDetailModel MapToModel(this PKHPhieuYeuCauMuaHangDetail entity)
        {
            return new PKHPhieuYeuCauMuaHangDetailModel
            {
				PKHPhieuYeuCauMuaHangDetailId = entity.PKHPhieuYeuCauMuaHangDetailId,
				PKHPhieuYeuCauMuaHangId = entity.PKHPhieuYeuCauMuaHangId,
				ChiTietVatTuId = entity.ChiTietVatTuId,
				Title = entity.Title,
				SoLuong = entity.SoLuong,
				DonGiaThamKhao = entity.DonGiaThamKhao,
				QuyCach = entity.QuyCach,
				DonGia = entity.DonGia,
				NhomYeuCauId = entity.NhomYeuCauId,
				GhiChu = entity.GhiChu,
				TongSoLuong = entity.TongSoLuong,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,
                MaChiTietVatTu = (entity.ChiTietVatTu != null)? entity.ChiTietVatTu.MaChiTietVatTu : string.Empty,
                TenChiTietVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.Title : string.Empty,
                DonViTinh = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.DonVi : null,

            };
        }
        public static PKHPhieuYeuCauMuaHangDetailModel MapToModel(this PKHPhieuYeuCauMuaHangDetail entity, PKHPhieuYeuCauMuaHangDetailModel model)
        {
			model.PKHPhieuYeuCauMuaHangDetailId = entity.PKHPhieuYeuCauMuaHangDetailId;
			model.PKHPhieuYeuCauMuaHangId = entity.PKHPhieuYeuCauMuaHangId;
			model.ChiTietVatTuId = entity.ChiTietVatTuId;
			model.Title = entity.Title;
			model.SoLuong = entity.SoLuong;
			model.DonGiaThamKhao = entity.DonGiaThamKhao;
			model.QuyCach = entity.QuyCach;
			model.DonGia = entity.DonGia;
			model.NhomYeuCauId = entity.NhomYeuCauId;
			model.GhiChu = entity.GhiChu;
			model.TongSoLuong = entity.TongSoLuong;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static PKHPhieuYeuCauMuaHangDetail MapToEntity(this PKHPhieuYeuCauMuaHangDetailModel model)
        {
            return new PKHPhieuYeuCauMuaHangDetail
            {
				PKHPhieuYeuCauMuaHangDetailId = model.PKHPhieuYeuCauMuaHangDetailId,
				PKHPhieuYeuCauMuaHangId = model.PKHPhieuYeuCauMuaHangId,
				ChiTietVatTuId = model.ChiTietVatTuId,
				Title = model.Title,
				SoLuong = model.SoLuong,
				DonGiaThamKhao = model.DonGiaThamKhao,
				QuyCach = model.QuyCach,
				DonGia = model.DonGia,
				NhomYeuCauId = model.NhomYeuCauId,
				GhiChu = model.GhiChu,
				TongSoLuong = model.TongSoLuong,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHPhieuYeuCauMuaHangDetail MapToEntity(this PKHPhieuYeuCauMuaHangDetailModel model, PKHPhieuYeuCauMuaHangDetail entity)
        {
			entity.PKHPhieuYeuCauMuaHangDetailId = model.PKHPhieuYeuCauMuaHangDetailId;
			entity.PKHPhieuYeuCauMuaHangId = model.PKHPhieuYeuCauMuaHangId;
			entity.ChiTietVatTuId = model.ChiTietVatTuId;
			entity.Title = model.Title;
			entity.SoLuong = model.SoLuong;
			entity.DonGiaThamKhao = model.DonGiaThamKhao;
			entity.QuyCach = model.QuyCach;
			entity.DonGia = model.DonGia;
			entity.NhomYeuCauId = model.NhomYeuCauId;
			entity.GhiChu = model.GhiChu;
			entity.TongSoLuong = model.TongSoLuong;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHPhieuYeuCauMuaHangDetail> MapToEntities(this List<PKHPhieuYeuCauMuaHangDetailModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHPhieuYeuCauMuaHangDetailModel> MapToModels(this List<PKHPhieuYeuCauMuaHangDetail> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
