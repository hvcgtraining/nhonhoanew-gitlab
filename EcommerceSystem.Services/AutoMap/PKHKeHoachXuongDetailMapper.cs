﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHKeHoachXuongDetailModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHKeHoachXuongDetailMapper
    {
        #region Mapping PKHKeHoachXuongDetail
        public static PKHKeHoachXuongDetailModel MapToModel(this PKHKeHoachXuongDetail entity)
        {
            return new PKHKeHoachXuongDetailModel
            {
                PKHKeHoachXuongDetailId = entity.PKHKeHoachXuongDetailId,
                PKHKeHoachSXNKXHDetailId = entity.PKHKeHoachSXNKXHDetailId,
                ChiTietVatTuId = entity.ChiTietVatTuId,
                KhoId = entity.KhoId,
                Title = entity.Title,
                DinhMucSanXuatId = entity.DinhMucSanXuatId,
                PKHKeHoachXuongId = entity.PKHKeHoachXuongId,
                DinhMuc = entity.DinhMuc,
                ThoiLuong = entity.ThoiLuong,
                YeuCau = entity.YeuCau,
                ChenhLech = entity.ChenhLech,
                UuTien = entity.UuTien,
                TonKho = entity.TonKho,
                ChiTietTon = entity.ChiTietTon,
                TonDuKien = entity.TonDuKien,
                DauKy = entity.DauKy,
                NgayCapHang = entity.NgayCapHang,
                GhiChu = entity.GhiChu,
                CreatedDate = entity.CreatedDate,
                UpdatedDate = entity.UpdatedDate,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                Status = entity.Status,
                MaVatTu = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.MaChiTietVatTu : "",
                TenVatTu = entity.ChiTietVatTu != null ? entity.ChiTietVatTu.Title : "",
                TenKho = entity.Kho != null ? entity.Kho.Title : "",
                NgayCapHangValue = entity.NgayCapHang.HasValue ? entity.NgayCapHang.Value.ToString("dd/MM/yyyy"): ""
            };
        }
        public static PKHKeHoachXuongDetailModel MapToModel(this PKHKeHoachXuongDetail entity, PKHKeHoachXuongDetailModel model)
        {
			model.PKHKeHoachXuongDetailId = entity.PKHKeHoachXuongDetailId;
			model.PKHKeHoachSXNKXHDetailId = entity.PKHKeHoachSXNKXHDetailId;
			model.ChiTietVatTuId = entity.ChiTietVatTuId;
			model.KhoId = entity.KhoId;
			model.Title = entity.Title;
			model.DinhMucSanXuatId = entity.DinhMucSanXuatId;
			model.PKHKeHoachXuongId = entity.PKHKeHoachXuongId;
			model.DinhMuc = entity.DinhMuc;
			model.ThoiLuong = entity.ThoiLuong;
			model.YeuCau = entity.YeuCau;
			model.ChenhLech = entity.ChenhLech;
			model.UuTien = entity.UuTien;
			model.TonKho = entity.TonKho;
			model.ChiTietTon = entity.ChiTietTon;
			model.TonDuKien = entity.TonDuKien;
			model.DauKy = entity.DauKy;
			model.NgayCapHang = entity.NgayCapHang;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static PKHKeHoachXuongDetail MapToEntity(this PKHKeHoachXuongDetailModel model)
        {
            return new PKHKeHoachXuongDetail
            {
				PKHKeHoachXuongDetailId = model.PKHKeHoachXuongDetailId,
				PKHKeHoachSXNKXHDetailId = model.PKHKeHoachSXNKXHDetailId,
				ChiTietVatTuId = model.ChiTietVatTuId,
				KhoId = model.KhoId,
				Title = model.Title,
				DinhMucSanXuatId = model.DinhMucSanXuatId,
				PKHKeHoachXuongId = model.PKHKeHoachXuongId,
				DinhMuc = model.DinhMuc,
				ThoiLuong = model.ThoiLuong,
				YeuCau = model.YeuCau,
				ChenhLech = model.ChenhLech,
				UuTien = model.UuTien,
				TonKho = model.TonKho,
				ChiTietTon = model.ChiTietTon,
				TonDuKien = model.TonDuKien,
				DauKy = model.DauKy,
				NgayCapHang = model.NgayCapHang,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHKeHoachXuongDetail MapToEntity(this PKHKeHoachXuongDetailModel model, PKHKeHoachXuongDetail entity)
        {
			entity.PKHKeHoachXuongDetailId = model.PKHKeHoachXuongDetailId;
			entity.PKHKeHoachSXNKXHDetailId = model.PKHKeHoachSXNKXHDetailId;
			entity.ChiTietVatTuId = model.ChiTietVatTuId;
			entity.KhoId = model.KhoId;
			entity.Title = model.Title;
			entity.DinhMucSanXuatId = model.DinhMucSanXuatId;
			entity.PKHKeHoachXuongId = model.PKHKeHoachXuongId;
			entity.DinhMuc = model.DinhMuc;
			entity.ThoiLuong = model.ThoiLuong;
			entity.YeuCau = model.YeuCau;
			entity.ChenhLech = model.ChenhLech;
			entity.UuTien = model.UuTien;
			entity.TonKho = model.TonKho;
			entity.ChiTietTon = model.ChiTietTon;
			entity.TonDuKien = model.TonDuKien;
			entity.DauKy = model.DauKy;
			entity.NgayCapHang = model.NgayCapHang;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHKeHoachXuongDetail> MapToEntities(this List<PKHKeHoachXuongDetailModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHKeHoachXuongDetailModel> MapToModels(this List<PKHKeHoachXuongDetail> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
