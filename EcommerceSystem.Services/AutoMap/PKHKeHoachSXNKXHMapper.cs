﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHKeHoachSXNKXHModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHKeHoachSXNKXHMapper
    {
        #region Mapping PKHKeHoachSXNKXH
        public static PKHKeHoachSXNKXHModel MapToModel(this PKHKeHoachSXNKXH entity)
        {
            return new PKHKeHoachSXNKXHModel
            {
                PKHKeHoachSXNKXHId = entity.PKHKeHoachSXNKXHId,
                Title = entity.Title,
                MaTuan = entity.MaTuan,
                StartDate = entity.StartDate,
                EndDate = entity.EndDate,
                NgayTonKho = entity.NgayTonKho,
                SoNgayXuatKho = entity.SoNgayXuatKho,
                SoLuongCan = entity.SoLuongCan,
                SoLuongTon = entity.SoLuongTon,
                TinhTrang = entity.TinhTrang,
                GhiChu = entity.GhiChu,
                CreatedDate = entity.CreatedDate,
                UpdatedDate = entity.UpdatedDate,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                Status = entity.Status,
                StartDateValue = entity.StartDate.ToString("dd/MM/yyyy"),
                EndDateValue = entity.EndDate.ToString("dd/MM/yyyy"),
                NgayTonKhoValue = entity.NgayTonKho.ToString("dd/MM/yyyy"),
            };
        }
        public static PKHKeHoachSXNKXHModel MapToModel(this PKHKeHoachSXNKXH entity, PKHKeHoachSXNKXHModel model)
        {
			model.PKHKeHoachSXNKXHId = entity.PKHKeHoachSXNKXHId;
			model.Title = entity.Title;
			model.MaTuan = entity.MaTuan;
			model.StartDate = entity.StartDate;
			model.EndDate = entity.EndDate;
			model.NgayTonKho = entity.NgayTonKho;
			model.SoNgayXuatKho = entity.SoNgayXuatKho;
			model.SoLuongCan = entity.SoLuongCan;
			model.SoLuongTon = entity.SoLuongTon;
			model.TinhTrang = entity.TinhTrang;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static PKHKeHoachSXNKXH MapToEntity(this PKHKeHoachSXNKXHModel model)
        {
            return new PKHKeHoachSXNKXH
            {
				PKHKeHoachSXNKXHId = model.PKHKeHoachSXNKXHId,
				Title = model.Title,
				MaTuan = model.MaTuan,
				StartDate = model.StartDate,
				EndDate = model.EndDate,
				NgayTonKho = model.NgayTonKho,
				SoNgayXuatKho = model.SoNgayXuatKho,
				SoLuongCan = model.SoLuongCan,
				SoLuongTon = model.SoLuongTon,
				TinhTrang = model.TinhTrang,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHKeHoachSXNKXH MapToEntity(this PKHKeHoachSXNKXHModel model, PKHKeHoachSXNKXH entity)
        {
			entity.PKHKeHoachSXNKXHId = model.PKHKeHoachSXNKXHId;
			entity.Title = model.Title;
			entity.MaTuan = model.MaTuan;
			entity.StartDate = model.StartDate;
			entity.EndDate = model.EndDate;
			entity.NgayTonKho = model.NgayTonKho;
			entity.SoNgayXuatKho = model.SoNgayXuatKho;
			entity.SoLuongCan = model.SoLuongCan;
			entity.SoLuongTon = model.SoLuongTon;
			entity.TinhTrang = model.TinhTrang;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHKeHoachSXNKXH> MapToEntities(this List<PKHKeHoachSXNKXHModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHKeHoachSXNKXHModel> MapToModels(this List<PKHKeHoachSXNKXH> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
