﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.DieuChuyenQuyetToanModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class DieuChuyenQuyetToanMapper
    {
        #region Mapping DieuChuyenQuyetToan
        public static DieuChuyenQuyetToanModel MapToModel(this DieuChuyenQuyetToan entity)
        {
            return new DieuChuyenQuyetToanModel
            {
				DieuChuyenQuyetToanId = entity.DieuChuyenQuyetToanId,
				Title = entity.Title,
				SoChungTu = entity.SoChungTu,
				KhoNhapId = entity.KhoNhapId,
				KhoXuatId = entity.KhoXuatId,
				DaXem = entity.DaXem,
				LoaiDieuChuyenQuyetToan = entity.LoaiDieuChuyenQuyetToan,
				LyDoXuat = entity.LyDoXuat,
				NguoiNhan = entity.NguoiNhan,
				CanCu = entity.CanCu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,
                MaKhoNhap = (entity.Kho != null)? entity.Kho.MaKho : string.Empty,
                MaKhoXuat = (entity.Kho1 != null) ? entity.Kho1.MaKho : string.Empty

            };
        }
        public static DieuChuyenQuyetToanModel MapToModel(this DieuChuyenQuyetToan entity, DieuChuyenQuyetToanModel model)
        {
			model.DieuChuyenQuyetToanId = entity.DieuChuyenQuyetToanId;
			model.Title = entity.Title;
			model.SoChungTu = entity.SoChungTu;
			model.KhoNhapId = entity.KhoNhapId;
			model.KhoXuatId = entity.KhoXuatId;
			model.DaXem = entity.DaXem;
			model.LoaiDieuChuyenQuyetToan = entity.LoaiDieuChuyenQuyetToan;
			model.LyDoXuat = entity.LyDoXuat;
			model.NguoiNhan = entity.NguoiNhan;
			model.CanCu = entity.CanCu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;
            model.MaKhoNhap = (entity.Kho != null) ? entity.Kho.MaKho : string.Empty;
            model.MaKhoXuat = (entity.Kho1 != null) ? entity.Kho1.MaKho : string.Empty;
            return model;
        }
        public static DieuChuyenQuyetToan MapToEntity(this DieuChuyenQuyetToanModel model)
        {
            return new DieuChuyenQuyetToan
            {
				DieuChuyenQuyetToanId = model.DieuChuyenQuyetToanId,
				Title = model.Title,
				SoChungTu = model.SoChungTu,
				KhoNhapId = model.KhoNhapId,
				KhoXuatId = model.KhoXuatId,
				DaXem = model.DaXem,
				LoaiDieuChuyenQuyetToan = model.LoaiDieuChuyenQuyetToan,
				LyDoXuat = model.LyDoXuat,
				NguoiNhan = model.NguoiNhan,
				CanCu = model.CanCu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static DieuChuyenQuyetToan MapToEntity(this DieuChuyenQuyetToanModel model, DieuChuyenQuyetToan entity)
        {
			entity.DieuChuyenQuyetToanId = model.DieuChuyenQuyetToanId;
			entity.Title = model.Title;
			entity.SoChungTu = model.SoChungTu;
			entity.KhoNhapId = model.KhoNhapId;
			entity.KhoXuatId = model.KhoXuatId;
			entity.DaXem = model.DaXem;
			entity.LoaiDieuChuyenQuyetToan = model.LoaiDieuChuyenQuyetToan;
			entity.LyDoXuat = model.LyDoXuat;
			entity.NguoiNhan = model.NguoiNhan;
			entity.CanCu = model.CanCu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<DieuChuyenQuyetToan> MapToEntities(this List<DieuChuyenQuyetToanModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<DieuChuyenQuyetToanModel> MapToModels(this List<DieuChuyenQuyetToan> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
