﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHKiemHoaDetailModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHKiemHoaDetailMapper
    {
        #region Mapping PKHKiemHoaDetail
        public static PKHKiemHoaDetailModel MapToModel(this PKHKiemHoaDetail entity)
        {
            return new PKHKiemHoaDetailModel
            {
				PKHKiemHoaDetailId = entity.PKHKiemHoaDetailId,
				PKHKiemHoaId = entity.PKHKiemHoaId,
				MaPYC = entity.MaPYC,
				CustomerId = entity.CustomerId,
				ChiTietVatTuId = entity.ChiTietVatTuId,
				Title = entity.Title,
				SoLuong = entity.SoLuong,
				SoLuongDat = entity.SoLuongDat,
				SoLuongKhongDat = entity.SoLuongKhongDat,
				CheckFlag = entity.CheckFlag,
				GhiChu = entity.GhiChu,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static PKHKiemHoaDetailModel MapToModel(this PKHKiemHoaDetail entity, PKHKiemHoaDetailModel model)
        {
			model.PKHKiemHoaDetailId = entity.PKHKiemHoaDetailId;
			model.PKHKiemHoaId = entity.PKHKiemHoaId;
			model.MaPYC = entity.MaPYC;
			model.CustomerId = entity.CustomerId;
			model.ChiTietVatTuId = entity.ChiTietVatTuId;
			model.Title = entity.Title;
			model.SoLuong = entity.SoLuong;
			model.SoLuongDat = entity.SoLuongDat;
			model.SoLuongKhongDat = entity.SoLuongKhongDat;
			model.CheckFlag = entity.CheckFlag;
			model.GhiChu = entity.GhiChu;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static PKHKiemHoaDetail MapToEntity(this PKHKiemHoaDetailModel model)
        {
            return new PKHKiemHoaDetail
            {
				PKHKiemHoaDetailId = model.PKHKiemHoaDetailId,
				PKHKiemHoaId = model.PKHKiemHoaId,
				MaPYC = model.MaPYC,
				CustomerId = model.CustomerId,
				ChiTietVatTuId = model.ChiTietVatTuId,
				Title = model.Title,
				SoLuong = model.SoLuong,
				SoLuongDat = model.SoLuongDat,
				SoLuongKhongDat = model.SoLuongKhongDat,
				CheckFlag = model.CheckFlag,
				GhiChu = model.GhiChu,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static PKHKiemHoaDetail MapToEntity(this PKHKiemHoaDetailModel model, PKHKiemHoaDetail entity)
        {
			entity.PKHKiemHoaDetailId = model.PKHKiemHoaDetailId;
			entity.PKHKiemHoaId = model.PKHKiemHoaId;
			entity.MaPYC = model.MaPYC;
			entity.CustomerId = model.CustomerId;
			entity.ChiTietVatTuId = model.ChiTietVatTuId;
			entity.Title = model.Title;
			entity.SoLuong = model.SoLuong;
			entity.SoLuongDat = model.SoLuongDat;
			entity.SoLuongKhongDat = model.SoLuongKhongDat;
			entity.CheckFlag = model.CheckFlag;
			entity.GhiChu = model.GhiChu;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<PKHKiemHoaDetail> MapToEntities(this List<PKHKiemHoaDetailModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHKiemHoaDetailModel> MapToModels(this List<PKHKiemHoaDetail> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
