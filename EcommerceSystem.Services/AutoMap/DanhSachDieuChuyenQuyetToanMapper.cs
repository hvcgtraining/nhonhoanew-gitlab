﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.DanhSachDieuChuyenQuyetToanModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class DanhSachDieuChuyenQuyetToanMapper
    {
        #region Mapping DanhSachDieuChuyenQuyetToan
        public static DanhSachDieuChuyenQuyetToanModel MapToModel(this DanhSachDieuChuyenQuyetToan entity)
        {
            return new DanhSachDieuChuyenQuyetToanModel
            {
                DanhSachDieuChuyenQuyetToanId = entity.DanhSachDieuChuyenQuyetToanId,
                Title = entity.Title,
                DieuChuyenQuyetToanId = entity.DieuChuyenQuyetToanId,
                SoChungTu = entity.SoChungTu,
                VatTuDiId = entity.VatTuDiId,
                SoLuongDi = entity.SoLuongDi,
                VatTuVeId = entity.VatTuDiId,
                SoLuongVeId = entity.SoLuongVeId,
                DonViTinh = entity.DonViTinh,
                CreatedDate = entity.CreatedDate,
                UpdatedDate = entity.UpdatedDate,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                Status = entity.Status,
                DonViChiTietVatTu = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.DonVi : string.Empty,
                MaVatTuDi = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.MaChiTietVatTu : string.Empty,
                TenVatTuDi = (entity.ChiTietVatTu != null) ? entity.ChiTietVatTu.Title : string.Empty,
                MaVatTuVe = (entity.ChiTietVatTu1 != null) ? entity.ChiTietVatTu1.MaChiTietVatTu : string.Empty,
                TenVatTuVe = (entity.ChiTietVatTu1 != null) ? entity.ChiTietVatTu1.Title : string.Empty,
                TenDieuChuyenQuyetToan = (entity.DieuChuyenQuyetToan != null) ? entity.DieuChuyenQuyetToan.Title : string.Empty,
            };
        }
        public static DanhSachDieuChuyenQuyetToanModel MapToModel(this DanhSachDieuChuyenQuyetToan entity, DanhSachDieuChuyenQuyetToanModel model)
        {
            model.DanhSachDieuChuyenQuyetToanId = entity.DanhSachDieuChuyenQuyetToanId;
            model.Title = entity.Title;
            model.DieuChuyenQuyetToanId = entity.DieuChuyenQuyetToanId;
            model.SoChungTu = entity.SoChungTu;
            model.VatTuDiId = entity.VatTuDiId;
            model.SoLuongDi = entity.SoLuongDi;
            model.VatTuVeId = entity.VatTuVeId;
            model.SoLuongVeId = entity.SoLuongVeId;
            model.DonViTinh = entity.DonViTinh;
            model.CreatedDate = entity.CreatedDate;
            model.UpdatedDate = entity.UpdatedDate;
            model.CreatedBy = entity.CreatedBy;
            model.UpdatedBy = entity.UpdatedBy;
            model.Status = entity.Status;


            return model;
        }
        public static DanhSachDieuChuyenQuyetToan MapToEntity(this DanhSachDieuChuyenQuyetToanModel model)
        {
            return new DanhSachDieuChuyenQuyetToan
            {
                DanhSachDieuChuyenQuyetToanId = model.DanhSachDieuChuyenQuyetToanId,
                Title = model.Title,
                DieuChuyenQuyetToanId = model.DieuChuyenQuyetToanId,
                SoChungTu = model.SoChungTu,
                VatTuDiId = model.VatTuDiId,
                SoLuongDi = model.SoLuongDi,
                VatTuVeId = model.VatTuVeId,
                SoLuongVeId = model.SoLuongVeId,
                DonViTinh = model.DonViTinh,
                CreatedDate = model.CreatedDate,
                UpdatedDate = model.UpdatedDate,
                CreatedBy = model.CreatedBy,
                UpdatedBy = model.UpdatedBy,
                Status = model.Status,

            };
        }
        public static DanhSachDieuChuyenQuyetToan MapToEntity(this DanhSachDieuChuyenQuyetToanModel model, DanhSachDieuChuyenQuyetToan entity)
        {
            entity.DanhSachDieuChuyenQuyetToanId = model.DanhSachDieuChuyenQuyetToanId;
            entity.Title = model.Title;
            entity.DieuChuyenQuyetToanId = model.DieuChuyenQuyetToanId;
            entity.SoChungTu = model.SoChungTu;
            entity.VatTuDiId = model.VatTuDiId;
            entity.SoLuongDi = model.SoLuongDi;
            entity.VatTuVeId = model.VatTuVeId;
            entity.SoLuongVeId = model.SoLuongVeId;
            entity.DonViTinh = model.DonViTinh;
            //entity.CreatedDate = model.CreatedDate;
            entity.UpdatedDate = model.UpdatedDate;
            //entity.CreatedBy = model.CreatedBy;
            entity.UpdatedBy = model.UpdatedBy;
            entity.Status = model.Status;

            return entity;
        }
        public static List<DanhSachDieuChuyenQuyetToan> MapToEntities(this List<DanhSachDieuChuyenQuyetToanModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<DanhSachDieuChuyenQuyetToanModel> MapToModels(this List<DanhSachDieuChuyenQuyetToan> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
