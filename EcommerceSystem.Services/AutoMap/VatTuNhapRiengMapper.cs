﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.VatTuNhapRiengModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class VatTuNhapRiengMapper
    {
        #region Mapping VatTuNhapRieng
        public static VatTuNhapRiengModel MapToModel(this VatTuNhapRieng entity)
        {
            return new VatTuNhapRiengModel
            {
				VatTuNhapRiengId = entity.VatTuNhapRiengId,
				Title = entity.Title,
				KhoId = entity.KhoId,
				VatTuId = entity.VatTuId,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,

            };
        }
        public static VatTuNhapRiengModel MapToModel(this VatTuNhapRieng entity, VatTuNhapRiengModel model)
        {
			model.VatTuNhapRiengId = entity.VatTuNhapRiengId;
			model.Title = entity.Title;
			model.KhoId = entity.KhoId;
			model.VatTuId = entity.VatTuId;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;

            return model;
        }
        public static VatTuNhapRieng MapToEntity(this VatTuNhapRiengModel model)
        {
            return new VatTuNhapRieng
            {
				VatTuNhapRiengId = model.VatTuNhapRiengId,
				Title = model.Title,
				KhoId = model.KhoId,
				VatTuId = model.VatTuId,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,

            };
        }
        public static VatTuNhapRieng MapToEntity(this VatTuNhapRiengModel model, VatTuNhapRieng entity)
        {
			entity.VatTuNhapRiengId = model.VatTuNhapRiengId;
			entity.Title = model.Title;
			entity.KhoId = model.KhoId;
			entity.VatTuId = model.VatTuId;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;

            return entity;
        }
        public static List<VatTuNhapRieng> MapToEntities(this List<VatTuNhapRiengModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<VatTuNhapRiengModel> MapToModels(this List<VatTuNhapRieng> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
