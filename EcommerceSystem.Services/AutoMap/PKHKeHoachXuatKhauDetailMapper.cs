﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.Models.PKHKeHoachXuatKhauDetailModel;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services.AutoMap
{
    public static class PKHKeHoachXuatKhauDetailMapper
    {
        #region Mapping PKHKeHoachXuatKhauDetail
        public static PKHKeHoachXuatKhauDetailModel MapToModel(this PKHKeHoachXuatKhauDetail entity)
        {
            return new PKHKeHoachXuatKhauDetailModel
            {
				PKHKeHoachXuatKhauDetailId = entity.PKHKeHoachXuatKhauDetailId,
				Title = entity.Title,
				CreatedDate = entity.CreatedDate,
				UpdatedDate = entity.UpdatedDate,
				CreatedBy = entity.CreatedBy,
				UpdatedBy = entity.UpdatedBy,
				Status = entity.Status,
				PKHKeHoachXuatKhauId = entity.PKHKeHoachXuatKhauId,
				VatTuId = entity.VatTuId,
				SoLuong = entity.SoLuong,
				TongSo = entity.TongSo,
				XuongId = entity.XuongId,
				LoaiCan = entity.LoaiCan,
				GhiChu = entity.GhiChu,

            };
        }
        public static PKHKeHoachXuatKhauDetailModel MapToModel(this PKHKeHoachXuatKhauDetail entity, PKHKeHoachXuatKhauDetailModel model)
        {
			model.PKHKeHoachXuatKhauDetailId = entity.PKHKeHoachXuatKhauDetailId;
			model.Title = entity.Title;
			model.CreatedDate = entity.CreatedDate;
			model.UpdatedDate = entity.UpdatedDate;
			model.CreatedBy = entity.CreatedBy;
			model.UpdatedBy = entity.UpdatedBy;
			model.Status = entity.Status;
			model.PKHKeHoachXuatKhauId = entity.PKHKeHoachXuatKhauId;
			model.VatTuId = entity.VatTuId;
			model.SoLuong = entity.SoLuong;
			model.TongSo = entity.TongSo;
			model.XuongId = entity.XuongId;
			model.LoaiCan = entity.LoaiCan;
			model.GhiChu = entity.GhiChu;

            return model;
        }
        public static PKHKeHoachXuatKhauDetail MapToEntity(this PKHKeHoachXuatKhauDetailModel model)
        {
            return new PKHKeHoachXuatKhauDetail
            {
				PKHKeHoachXuatKhauDetailId = model.PKHKeHoachXuatKhauDetailId,
				Title = model.Title,
				CreatedDate = model.CreatedDate,
				UpdatedDate = model.UpdatedDate,
				CreatedBy = model.CreatedBy,
				UpdatedBy = model.UpdatedBy,
				Status = model.Status,
				PKHKeHoachXuatKhauId = model.PKHKeHoachXuatKhauId,
				VatTuId = model.VatTuId,
				SoLuong = model.SoLuong,
				TongSo = model.TongSo,
				XuongId = model.XuongId,
				LoaiCan = model.LoaiCan,
				GhiChu = model.GhiChu,

            };
        }
        public static PKHKeHoachXuatKhauDetail MapToEntity(this PKHKeHoachXuatKhauDetailModel model, PKHKeHoachXuatKhauDetail entity)
        {
			entity.PKHKeHoachXuatKhauDetailId = model.PKHKeHoachXuatKhauDetailId;
			entity.Title = model.Title;
			//entity.CreatedDate = model.CreatedDate;
			entity.UpdatedDate = model.UpdatedDate;
			//entity.CreatedBy = model.CreatedBy;
			entity.UpdatedBy = model.UpdatedBy;
			entity.Status = model.Status;
			entity.PKHKeHoachXuatKhauId = model.PKHKeHoachXuatKhauId;
			entity.VatTuId = model.VatTuId;
			entity.SoLuong = model.SoLuong;
			entity.TongSo = model.TongSo;
			entity.XuongId = model.XuongId;
			entity.LoaiCan = model.LoaiCan;
			entity.GhiChu = model.GhiChu;

            return entity;
        }
        public static List<PKHKeHoachXuatKhauDetail> MapToEntities(this List<PKHKeHoachXuatKhauDetailModel> models)
        {
            return models.Select(x => x.MapToEntity()).ToList();
        }

        public static List<PKHKeHoachXuatKhauDetailModel> MapToModels(this List<PKHKeHoachXuatKhauDetail> entities)
        {
            return entities.Select(x => x.MapToModel()).ToList();
        }
        #endregion
    }
}
