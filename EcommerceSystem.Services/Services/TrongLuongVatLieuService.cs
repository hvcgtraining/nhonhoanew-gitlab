﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.TrongLuongVatLieuModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface ITrongLuongVatLieuService : IEntityService<TrongLuongVatLieu>
    {
        List<TrongLuongVatLieuModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateTrongLuongVatLieu(TrongLuongVatLieuModel TrongLuongVatLieuModel, out string message);
        bool Delete(int trongLuongVatLieuId, out string message);
        List<TrongLuongVatLieuModel> GetAllTrongLuongVatLieus();
        bool CreateTrongLuongVatLieu(TrongLuongVatLieuModel model);
		bool ChangeStatus(int trongLuongVatLieuId, out string message);

    }
    public class TrongLuongVatLieuService : EntityService<TrongLuongVatLieu>, ITrongLuongVatLieuService
    {
        private readonly ITrongLuongVatLieuRepository _trongLuongVatLieuRepository;
        public TrongLuongVatLieuService(IUnitOfWork unitOfWork, ITrongLuongVatLieuRepository trongLuongVatLieuRepository)
            : base(unitOfWork, trongLuongVatLieuRepository)
        {
            _trongLuongVatLieuRepository = trongLuongVatLieuRepository;
        }

        public List<TrongLuongVatLieuModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var trongLuongVatLieuEntities = _trongLuongVatLieuRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (trongLuongVatLieuEntities != null)
				{
					return trongLuongVatLieuEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search TrongLuongVatLieu error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateTrongLuongVatLieu(TrongLuongVatLieuModel trongLuongVatLieuModel, out string message)
        {
            try
            {
                var trongLuongVatLieuEntity = _trongLuongVatLieuRepository.GetById(trongLuongVatLieuModel.TrongLuongVatLieuId);
				if (trongLuongVatLieuEntity != null)
				{
					trongLuongVatLieuEntity = trongLuongVatLieuModel.MapToEntity(trongLuongVatLieuEntity);
					trongLuongVatLieuEntity.MapToModel(trongLuongVatLieuModel);

					_trongLuongVatLieuRepository.Update(trongLuongVatLieuEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update TrongLuongVatLieu error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateTrongLuongVatLieu(TrongLuongVatLieuModel model)
        {
            try
            {
                var createdTrongLuongVatLieu = _trongLuongVatLieuRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdTrongLuongVatLieu == null)
                {
                    Log.Error("Create trongLuongVatLieu error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create TrongLuongVatLieu error", ex);
                return false;
            }

        }

        public bool Delete(int trongLuongVatLieuId, out string message)
        {
            

            try
            {
                var entity = _trongLuongVatLieuRepository.GetById(trongLuongVatLieuId);
				if (entity != null)
				{
					_trongLuongVatLieuRepository.Delete(trongLuongVatLieuId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete TrongLuongVatLieu error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<TrongLuongVatLieuModel> GetAllTrongLuongVatLieus()
        {
            //Igrone trongLuongVatLieu system
            return _trongLuongVatLieuRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int trongLuongVatLieuId, out string message)
        {
            try
            {
                var entity = _trongLuongVatLieuRepository.Query(c => c.TrongLuongVatLieuId == trongLuongVatLieuId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_trongLuongVatLieuRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete TrongLuongVatLieu error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
