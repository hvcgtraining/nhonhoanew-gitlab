﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.QuyDoiDinhMucModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IQuyDoiDinhMucService : IEntityService<QuyDoiDinhMuc>
    {
        List<QuyDoiDinhMucModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateQuyDoiDinhMuc(QuyDoiDinhMucModel QuyDoiDinhMucModel, out string message);
        bool Delete(int quyDoiDinhMucId, out string message);
        List<QuyDoiDinhMucModel> GetAllQuyDoiDinhMucs();
        bool CreateQuyDoiDinhMuc(QuyDoiDinhMucModel model);
		bool ChangeStatus(int quyDoiDinhMucId, out string message);

    }
    public class QuyDoiDinhMucService : EntityService<QuyDoiDinhMuc>, IQuyDoiDinhMucService
    {
        private readonly IQuyDoiDinhMucRepository _quyDoiDinhMucRepository;
        public QuyDoiDinhMucService(IUnitOfWork unitOfWork, IQuyDoiDinhMucRepository quyDoiDinhMucRepository)
            : base(unitOfWork, quyDoiDinhMucRepository)
        {
            _quyDoiDinhMucRepository = quyDoiDinhMucRepository;
        }

        public List<QuyDoiDinhMucModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var quyDoiDinhMucEntities = _quyDoiDinhMucRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (quyDoiDinhMucEntities != null)
				{
					return quyDoiDinhMucEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search QuyDoiDinhMuc error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateQuyDoiDinhMuc(QuyDoiDinhMucModel quyDoiDinhMucModel, out string message)
        {
            try
            {
                var quyDoiDinhMucEntity = _quyDoiDinhMucRepository.GetById(quyDoiDinhMucModel.QuyDoiDinhMucId);
				if (quyDoiDinhMucEntity != null)
				{
					quyDoiDinhMucEntity = quyDoiDinhMucModel.MapToEntity(quyDoiDinhMucEntity);
					quyDoiDinhMucEntity.MapToModel(quyDoiDinhMucModel);

					_quyDoiDinhMucRepository.Update(quyDoiDinhMucEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update QuyDoiDinhMuc error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateQuyDoiDinhMuc(QuyDoiDinhMucModel model)
        {
            try
            {
                var createdQuyDoiDinhMuc = _quyDoiDinhMucRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdQuyDoiDinhMuc == null)
                {
                    Log.Error("Create quyDoiDinhMuc error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create QuyDoiDinhMuc error", ex);
                return false;
            }

        }

        public bool Delete(int quyDoiDinhMucId, out string message)
        {
            

            try
            {
                var entity = _quyDoiDinhMucRepository.GetById(quyDoiDinhMucId);
				if (entity != null)
				{
					_quyDoiDinhMucRepository.Delete(quyDoiDinhMucId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete QuyDoiDinhMuc error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<QuyDoiDinhMucModel> GetAllQuyDoiDinhMucs()
        {
            //Igrone quyDoiDinhMuc system
            return _quyDoiDinhMucRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int quyDoiDinhMucId, out string message)
        {
            try
            {
                var entity = _quyDoiDinhMucRepository.Query(c => c.QuyDoiDinhMucId == quyDoiDinhMucId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_quyDoiDinhMucRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete QuyDoiDinhMuc error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
