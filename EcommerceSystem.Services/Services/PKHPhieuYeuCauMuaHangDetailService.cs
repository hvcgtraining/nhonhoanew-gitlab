﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHPhieuYeuCauMuaHangDetailModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHPhieuYeuCauMuaHangDetailService : IEntityService<PKHPhieuYeuCauMuaHangDetail>
    {
        List<PKHPhieuYeuCauMuaHangDetailModel> Search(int currentPage, int pageSize, string textSearch, int PKHPhieuYeuCauMuaHangId, int? maNhom, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHPhieuYeuCauMuaHangDetail(PKHPhieuYeuCauMuaHangDetailModel PKHPhieuYeuCauMuaHangDetailModel, out string message);
        bool Delete(int pKHPhieuYeuCauMuaHangDetailId, out string message);
        List<PKHPhieuYeuCauMuaHangDetailModel> GetAllPKHPhieuYeuCauMuaHangDetails();
        bool CreatePKHPhieuYeuCauMuaHangDetail(PKHPhieuYeuCauMuaHangDetailModel model);
		bool ChangeStatus(int pKHPhieuYeuCauMuaHangDetailId, out string message);

    }
    public class PKHPhieuYeuCauMuaHangDetailService : EntityService<PKHPhieuYeuCauMuaHangDetail>, IPKHPhieuYeuCauMuaHangDetailService
    {
        private readonly IPKHPhieuYeuCauMuaHangDetailRepository _pKHPhieuYeuCauMuaHangDetailRepository;
        public PKHPhieuYeuCauMuaHangDetailService(IUnitOfWork unitOfWork, IPKHPhieuYeuCauMuaHangDetailRepository pKHPhieuYeuCauMuaHangDetailRepository)
            : base(unitOfWork, pKHPhieuYeuCauMuaHangDetailRepository)
        {
            _pKHPhieuYeuCauMuaHangDetailRepository = pKHPhieuYeuCauMuaHangDetailRepository;
        }

        public List<PKHPhieuYeuCauMuaHangDetailModel> Search(int currentPage, int pageSize, string textSearch, int PKHPhieuYeuCauMuaHangId, int? maNhom, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHPhieuYeuCauMuaHangDetailEntities = _pKHPhieuYeuCauMuaHangDetailRepository.Search(currentPage, pageSize, textSearch, PKHPhieuYeuCauMuaHangId, maNhom, sortColumn, sortDirection, out totalPage);
				if (pKHPhieuYeuCauMuaHangDetailEntities != null)
				{
					return pKHPhieuYeuCauMuaHangDetailEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHPhieuYeuCauMuaHangDetail error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHPhieuYeuCauMuaHangDetail(PKHPhieuYeuCauMuaHangDetailModel pKHPhieuYeuCauMuaHangDetailModel, out string message)
        {
            try
            {
                var pKHPhieuYeuCauMuaHangDetailEntity = _pKHPhieuYeuCauMuaHangDetailRepository.GetById(pKHPhieuYeuCauMuaHangDetailModel.PKHPhieuYeuCauMuaHangDetailId);
				if (pKHPhieuYeuCauMuaHangDetailEntity != null)
				{
					pKHPhieuYeuCauMuaHangDetailEntity = pKHPhieuYeuCauMuaHangDetailModel.MapToEntity(pKHPhieuYeuCauMuaHangDetailEntity);
					pKHPhieuYeuCauMuaHangDetailEntity.MapToModel(pKHPhieuYeuCauMuaHangDetailModel);

					_pKHPhieuYeuCauMuaHangDetailRepository.Update(pKHPhieuYeuCauMuaHangDetailEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHPhieuYeuCauMuaHangDetail error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHPhieuYeuCauMuaHangDetail(PKHPhieuYeuCauMuaHangDetailModel model)
        {
            try
            {
                var createdPKHPhieuYeuCauMuaHangDetail = _pKHPhieuYeuCauMuaHangDetailRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHPhieuYeuCauMuaHangDetail == null)
                {
                    Log.Error("Create pKHPhieuYeuCauMuaHangDetail error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHPhieuYeuCauMuaHangDetail error", ex);
                return false;
            }

        }

        public bool Delete(int pKHPhieuYeuCauMuaHangDetailId, out string message)
        {
            

            try
            {
                var entity = _pKHPhieuYeuCauMuaHangDetailRepository.GetById(pKHPhieuYeuCauMuaHangDetailId);
				if (entity != null)
				{
					_pKHPhieuYeuCauMuaHangDetailRepository.Delete(pKHPhieuYeuCauMuaHangDetailId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHPhieuYeuCauMuaHangDetail error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHPhieuYeuCauMuaHangDetailModel> GetAllPKHPhieuYeuCauMuaHangDetails()
        {
            //Igrone pKHPhieuYeuCauMuaHangDetail system
            return _pKHPhieuYeuCauMuaHangDetailRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHPhieuYeuCauMuaHangDetailId, out string message)
        {
            try
            {
                var entity = _pKHPhieuYeuCauMuaHangDetailRepository.Query(c => c.PKHPhieuYeuCauMuaHangDetailId == pKHPhieuYeuCauMuaHangDetailId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHPhieuYeuCauMuaHangDetailRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHPhieuYeuCauMuaHangDetail error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
