﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models;
using EcommerceSystem.Models.PKHKeHoachXuongModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHKeHoachXuongService : IEntityService<PKHKeHoachXuong>
    {
        List<PKHKeHoachXuongModel> Search(int currentPage, int pageSize, string textSearch, DateTime? startDate, DateTime? endDate, int? xuongId, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHKeHoachXuong(PKHKeHoachXuongModel PKHKeHoachXuongModel, out string message);
        bool Delete(int pKHKeHoachXuongId, out string message);
        List<PKHKeHoachXuongModel> GetAllPKHKeHoachXuongs();
        bool CreatePKHKeHoachXuong(PKHKeHoachXuongModel model);
		bool ChangeStatus(int pKHKeHoachXuongId, out string message);
        List<PKHKeHoachXuongModel> GetAllXuongKeHoachSX(long khSXId, int xuongType);

    }
    public class PKHKeHoachXuongService : EntityService<PKHKeHoachXuong>, IPKHKeHoachXuongService
    {
        private readonly IPKHKeHoachXuongRepository _pKHKeHoachXuongRepository;
        public PKHKeHoachXuongService(IUnitOfWork unitOfWork, IPKHKeHoachXuongRepository pKHKeHoachXuongRepository)
            : base(unitOfWork, pKHKeHoachXuongRepository)
        {
            _pKHKeHoachXuongRepository = pKHKeHoachXuongRepository;
        }

        public List<PKHKeHoachXuongModel> Search(int currentPage, int pageSize, string textSearch, DateTime? startDate, DateTime? endDate, int? xuongId, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHKeHoachXuongEntities = _pKHKeHoachXuongRepository.Search(currentPage, pageSize, textSearch, startDate, endDate, xuongId, sortColumn, sortDirection, out totalPage);
				if (pKHKeHoachXuongEntities != null)
				{
					return pKHKeHoachXuongEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHKeHoachXuong error", ex);
            }
            totalPage = 0;
            return null;
        }

        public List<PKHKeHoachXuongModel> GetAllXuongKeHoachSX(long khSXId, int xuongType)
        {
            try
            {
                var pKHKeHoachXuongEntities = _pKHKeHoachXuongRepository.GetAllXuongKeHoachSX(khSXId, xuongType);
                if (pKHKeHoachXuongEntities != null)
                {
                    return pKHKeHoachXuongEntities.MapToModels();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHKeHoachXuong error", ex);
            }
            return null;
        }

        public bool UpdatePKHKeHoachXuong(PKHKeHoachXuongModel pKHKeHoachXuongModel, out string message)
        {
            try
            {
                var pKHKeHoachXuongEntity = _pKHKeHoachXuongRepository.GetById(pKHKeHoachXuongModel.PKHKeHoachXuongId);
				if (pKHKeHoachXuongEntity != null)
				{
					pKHKeHoachXuongEntity = pKHKeHoachXuongModel.MapToEntity(pKHKeHoachXuongEntity);
					pKHKeHoachXuongEntity.MapToModel(pKHKeHoachXuongModel);

					_pKHKeHoachXuongRepository.Update(pKHKeHoachXuongEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHKeHoachXuong error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHKeHoachXuong(PKHKeHoachXuongModel model)
        {
            try
            {
                var createdPKHKeHoachXuong = _pKHKeHoachXuongRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHKeHoachXuong == null)
                {
                    Log.Error("Create pKHKeHoachXuong error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHKeHoachXuong error", ex);
                return false;
            }

        }

        public bool UpdateKhsxStatus(int pKHKeHoachXuongId, out string message)
        {
            try
            {
                var entity = _pKHKeHoachXuongRepository.GetById(pKHKeHoachXuongId);
                if (entity != null)
                {
                    entity.TinhTrang = (byte)TrangThaiKeHoachXuong.DangThucHien;
                    _pKHKeHoachXuongRepository.Update(entity);
                    UnitOfWork.SaveChanges();

                    message = "Cập nhật bản ghi thành công";
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachXuongDetail error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }
        public bool Delete(int pKHKeHoachXuongId, out string message)
        {
            try
            {
                var entity = _pKHKeHoachXuongRepository.GetById(pKHKeHoachXuongId);
				if (entity != null)
				{
					_pKHKeHoachXuongRepository.Delete(pKHKeHoachXuongId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachXuong error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHKeHoachXuongModel> GetAllPKHKeHoachXuongs()
        {
            //Igrone pKHKeHoachXuong system
            return _pKHKeHoachXuongRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHKeHoachXuongId, out string message)
        {
            try
            {
                var entity = _pKHKeHoachXuongRepository.Query(c => c.PKHKeHoachXuongId == pKHKeHoachXuongId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHKeHoachXuongRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachXuong error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
