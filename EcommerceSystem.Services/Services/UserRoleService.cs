﻿using EcommerceSystem.DataAccess;

namespace EcommerceSystem.Services
{
    public interface IUserRoleService : IEntityService<UserRole>
    {
    }
}
