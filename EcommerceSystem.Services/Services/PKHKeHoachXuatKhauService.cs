﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHKeHoachXuatKhauModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHKeHoachXuatKhauService : IEntityService<PKHKeHoachXuatKhau>
    {
        List<PKHKeHoachXuatKhauModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHKeHoachXuatKhau(PKHKeHoachXuatKhauModel PKHKeHoachXuatKhauModel, out string message);
        bool Delete(int pKHKeHoachXuatKhauId, out string message);
        List<PKHKeHoachXuatKhauModel> GetAllPKHKeHoachXuatKhaus();
        bool CreatePKHKeHoachXuatKhau(PKHKeHoachXuatKhauModel model);
		bool ChangeStatus(int pKHKeHoachXuatKhauId, out string message);

    }
    public class PKHKeHoachXuatKhauService : EntityService<PKHKeHoachXuatKhau>, IPKHKeHoachXuatKhauService
    {
        private readonly IPKHKeHoachXuatKhauRepository _pKHKeHoachXuatKhauRepository;
        public PKHKeHoachXuatKhauService(IUnitOfWork unitOfWork, IPKHKeHoachXuatKhauRepository pKHKeHoachXuatKhauRepository)
            : base(unitOfWork, pKHKeHoachXuatKhauRepository)
        {
            _pKHKeHoachXuatKhauRepository = pKHKeHoachXuatKhauRepository;
        }

        public List<PKHKeHoachXuatKhauModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHKeHoachXuatKhauEntities = _pKHKeHoachXuatKhauRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHKeHoachXuatKhauEntities != null)
				{
					return pKHKeHoachXuatKhauEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHKeHoachXuatKhau error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHKeHoachXuatKhau(PKHKeHoachXuatKhauModel pKHKeHoachXuatKhauModel, out string message)
        {
            try
            {
                var pKHKeHoachXuatKhauEntity = _pKHKeHoachXuatKhauRepository.GetById(pKHKeHoachXuatKhauModel.PKHKeHoachXuatKhauId);
				if (pKHKeHoachXuatKhauEntity != null)
				{
					pKHKeHoachXuatKhauEntity = pKHKeHoachXuatKhauModel.MapToEntity(pKHKeHoachXuatKhauEntity);
					pKHKeHoachXuatKhauEntity.MapToModel(pKHKeHoachXuatKhauModel);

					_pKHKeHoachXuatKhauRepository.Update(pKHKeHoachXuatKhauEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHKeHoachXuatKhau error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHKeHoachXuatKhau(PKHKeHoachXuatKhauModel model)
        {
            try
            {
                var createdPKHKeHoachXuatKhau = _pKHKeHoachXuatKhauRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHKeHoachXuatKhau == null)
                {
                    Log.Error("Create pKHKeHoachXuatKhau error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHKeHoachXuatKhau error", ex);
                return false;
            }

        }

        public bool Delete(int pKHKeHoachXuatKhauId, out string message)
        {
            

            try
            {
                var entity = _pKHKeHoachXuatKhauRepository.GetById(pKHKeHoachXuatKhauId);
				if (entity != null)
				{
					_pKHKeHoachXuatKhauRepository.Delete(pKHKeHoachXuatKhauId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachXuatKhau error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHKeHoachXuatKhauModel> GetAllPKHKeHoachXuatKhaus()
        {
            //Igrone pKHKeHoachXuatKhau system
            return _pKHKeHoachXuatKhauRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHKeHoachXuatKhauId, out string message)
        {
            try
            {
                var entity = _pKHKeHoachXuatKhauRepository.Query(c => c.PKHKeHoachXuatKhauId == pKHKeHoachXuatKhauId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHKeHoachXuatKhauRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachXuatKhau error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
