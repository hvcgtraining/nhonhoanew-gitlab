﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.DinhMucCatToleModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace EcommerceSystem.Services
{
    public interface IDinhMucCatToleService : IEntityService<DinhMucCatTole>
    {
        List<DinhMucCatToleModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateDinhMucCatTole(DinhMucCatToleModel DinhMucCatToleModel, out string message);
        bool Delete(int dinhMucCatToleId, out string message);
        List<DinhMucCatToleModel> GetAllDinhMucCatToles();
        bool CreateDinhMucCatTole(DinhMucCatToleModel model);
		bool ChangeStatus(int dinhMucCatToleId, out string message);

        List<SelectListItem> GetListChiTietVatTu();
        List<SelectListItem> GetListTole();

    }
    public class DinhMucCatToleService : EntityService<DinhMucCatTole>, IDinhMucCatToleService
    {
        private readonly IDinhMucCatToleRepository _dinhMucCatToleRepository;
        private readonly IChiTietVatTuRepository _chiTietVatTuRepository;
        private readonly IToleRepository _toleRepository;

        public DinhMucCatToleService(IUnitOfWork unitOfWork, IDinhMucCatToleRepository dinhMucCatToleRepository,
            IChiTietVatTuRepository chiTietVatTuRepository, IToleRepository toleRepository)
            : base(unitOfWork, dinhMucCatToleRepository)
        {
            _dinhMucCatToleRepository = dinhMucCatToleRepository;
            _chiTietVatTuRepository = chiTietVatTuRepository;
            _toleRepository = toleRepository;
        }

        public List<DinhMucCatToleModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var dinhMucCatToleEntities = _dinhMucCatToleRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (dinhMucCatToleEntities != null)
				{
					return dinhMucCatToleEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search DinhMucCatTole error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateDinhMucCatTole(DinhMucCatToleModel dinhMucCatToleModel, out string message)
        {
            try
            {
                var dinhMucCatToleEntity = _dinhMucCatToleRepository.GetById(dinhMucCatToleModel.DinhMucCatToleId);
				if (dinhMucCatToleEntity != null)
				{
					dinhMucCatToleEntity = dinhMucCatToleModel.MapToEntity(dinhMucCatToleEntity);
					//dinhMucCatToleEntity.MapToModel(dinhMucCatToleModel);

					_dinhMucCatToleRepository.Update(dinhMucCatToleEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update DinhMucCatTole error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateDinhMucCatTole(DinhMucCatToleModel model)
        {
            try
            {
                var createdDinhMucCatTole = _dinhMucCatToleRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdDinhMucCatTole == null)
                {
                    Log.Error("Create dinhMucCatTole error");
                    return false;
                }
                model.DinhMucCatToleId = createdDinhMucCatTole.DinhMucCatToleId;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create DinhMucCatTole error", ex);
                return false;
            }

        }

        public bool Delete(int dinhMucCatToleId, out string message)
        {
            

            try
            {
                var entity = _dinhMucCatToleRepository.GetById(dinhMucCatToleId);
				if (entity != null)
				{
					_dinhMucCatToleRepository.Delete(dinhMucCatToleId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DinhMucCatTole error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<DinhMucCatToleModel> GetAllDinhMucCatToles()
        {
            //Igrone dinhMucCatTole system
            return _dinhMucCatToleRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int dinhMucCatToleId, out string message)
        {
            try
            {
                var entity = _dinhMucCatToleRepository.Query(c => c.DinhMucCatToleId == dinhMucCatToleId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_dinhMucCatToleRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DinhMucCatTole error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }

        public List<SelectListItem> GetListChiTietVatTu()
        {
            var listChiTietVatTu = _chiTietVatTuRepository.GetAll().Select(x => new SelectListItem
            {
                Value = x.ChiTietVatTuId.ToString(),
                Text = x.Title,
            }).ToList();

            return listChiTietVatTu;
        }

        public List<SelectListItem> GetListTole()
        {
            var listTole = _toleRepository.GetAll().Select(x => new SelectListItem
            {
                Value = x.ToleId.ToString(),
                Text = x.Title,
            }).ToList();

            return listTole;
        }
    }
}
