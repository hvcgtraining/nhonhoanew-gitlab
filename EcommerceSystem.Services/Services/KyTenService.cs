﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.KyTenModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IKyTenService : IEntityService<KyTen>
    {
        List<KyTenModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateKyTen(KyTenModel KyTenModel, out string message);
        bool Delete(int kyTenId, out string message);
        List<KyTenModel> GetAllKyTens();
        bool CreateKyTen(KyTenModel model);
		bool ChangeStatus(int kyTenId, out string message);

    }
    public class KyTenService : EntityService<KyTen>, IKyTenService
    {
        private readonly IKyTenRepository _kyTenRepository;
        public KyTenService(IUnitOfWork unitOfWork, IKyTenRepository kyTenRepository)
            : base(unitOfWork, kyTenRepository)
        {
            _kyTenRepository = kyTenRepository;
        }

        public List<KyTenModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var kyTenEntities = _kyTenRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (kyTenEntities != null)
				{
					return kyTenEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search KyTen error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateKyTen(KyTenModel kyTenModel, out string message)
        {
            try
            {
                var kyTenEntity = _kyTenRepository.GetById(kyTenModel.KyTenId);
				if (kyTenEntity != null)
				{
					kyTenEntity = kyTenModel.MapToEntity(kyTenEntity);
					kyTenEntity.MapToModel(kyTenModel);

					_kyTenRepository.Update(kyTenEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update KyTen error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateKyTen(KyTenModel model)
        {
            try
            {
                var createdKyTen = _kyTenRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdKyTen == null)
                {
                    Log.Error("Create kyTen error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create KyTen error", ex);
                return false;
            }

        }

        public bool Delete(int kyTenId, out string message)
        {
            

            try
            {
                var entity = _kyTenRepository.GetById(kyTenId);
				if (entity != null)
				{
					_kyTenRepository.Delete(kyTenId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete KyTen error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<KyTenModel> GetAllKyTens()
        {
            //Igrone kyTen system
            return _kyTenRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int kyTenId, out string message)
        {
            try
            {
                var entity = _kyTenRepository.Query(c => c.KyTenId == kyTenId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_kyTenRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete KyTen error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
