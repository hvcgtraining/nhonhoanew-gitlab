﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.SoDuBanDauModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface ISoDuBanDauService : IEntityService<SoDuBanDau>
    {
        List<SoDuBanDauModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateSoDuBanDau(SoDuBanDauModel SoDuBanDauModel, out string message);
        bool Delete(int soDuBanDauId, out string message);
        List<SoDuBanDauModel> GetAllSoDuBanDaus();
        bool CreateSoDuBanDau(SoDuBanDauModel model);
		bool ChangeStatus(int soDuBanDauId, out string message);

    }
    public class SoDuBanDauService : EntityService<SoDuBanDau>, ISoDuBanDauService
    {
        private readonly ISoDuBanDauRepository _soDuBanDauRepository;
        public SoDuBanDauService(IUnitOfWork unitOfWork, ISoDuBanDauRepository soDuBanDauRepository)
            : base(unitOfWork, soDuBanDauRepository)
        {
            _soDuBanDauRepository = soDuBanDauRepository;
        }

        public List<SoDuBanDauModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var soDuBanDauEntities = _soDuBanDauRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (soDuBanDauEntities != null)
				{
					return soDuBanDauEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search SoDuBanDau error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateSoDuBanDau(SoDuBanDauModel soDuBanDauModel, out string message)
        {
            try
            {
                var soDuBanDauEntity = _soDuBanDauRepository.GetById(soDuBanDauModel.SoDuBanDauId);
				if (soDuBanDauEntity != null)
				{
					soDuBanDauEntity = soDuBanDauModel.MapToEntity(soDuBanDauEntity);
					soDuBanDauEntity.MapToModel(soDuBanDauModel);

					_soDuBanDauRepository.Update(soDuBanDauEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update SoDuBanDau error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateSoDuBanDau(SoDuBanDauModel model)
        {
            try
            {
                var createdSoDuBanDau = _soDuBanDauRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdSoDuBanDau == null)
                {
                    Log.Error("Create soDuBanDau error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create SoDuBanDau error", ex);
                return false;
            }

        }

        public bool Delete(int soDuBanDauId, out string message)
        {
            

            try
            {
                var entity = _soDuBanDauRepository.GetById(soDuBanDauId);
				if (entity != null)
				{
					_soDuBanDauRepository.Delete(soDuBanDauId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete SoDuBanDau error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<SoDuBanDauModel> GetAllSoDuBanDaus()
        {
            //Igrone soDuBanDau system
            return _soDuBanDauRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int soDuBanDauId, out string message)
        {
            try
            {
                var entity = _soDuBanDauRepository.Query(c => c.SoDuBanDauId == soDuBanDauId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_soDuBanDauRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete SoDuBanDau error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
