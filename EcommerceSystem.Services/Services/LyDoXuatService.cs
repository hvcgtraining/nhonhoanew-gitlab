﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.LyDoXuatModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface ILyDoXuatService : IEntityService<LyDoXuat>
    {
        List<LyDoXuatModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateLyDoXuat(LyDoXuatModel LyDoXuatModel, out string message);
        bool Delete(int lyDoXuatId, out string message);
        List<LyDoXuatModel> GetAllLyDoXuats();
        bool CreateLyDoXuat(LyDoXuatModel model);
		bool ChangeStatus(int lyDoXuatId, out string message);

    }
    public class LyDoXuatService : EntityService<LyDoXuat>, ILyDoXuatService
    {
        private readonly ILyDoXuatRepository _lyDoXuatRepository;
        public LyDoXuatService(IUnitOfWork unitOfWork, ILyDoXuatRepository lyDoXuatRepository)
            : base(unitOfWork, lyDoXuatRepository)
        {
            _lyDoXuatRepository = lyDoXuatRepository;
        }

        public List<LyDoXuatModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var lyDoXuatEntities = _lyDoXuatRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (lyDoXuatEntities != null)
				{
					return lyDoXuatEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search LyDoXuat error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateLyDoXuat(LyDoXuatModel lyDoXuatModel, out string message)
        {
            try
            {
                var lyDoXuatEntity = _lyDoXuatRepository.GetById(lyDoXuatModel.LyDoXuatId);
				if (lyDoXuatEntity != null)
				{
					lyDoXuatEntity = lyDoXuatModel.MapToEntity(lyDoXuatEntity);
					lyDoXuatEntity.MapToModel(lyDoXuatModel);

					_lyDoXuatRepository.Update(lyDoXuatEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update LyDoXuat error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateLyDoXuat(LyDoXuatModel model)
        {
            try
            {
                var createdLyDoXuat = _lyDoXuatRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdLyDoXuat == null)
                {
                    Log.Error("Create lyDoXuat error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create LyDoXuat error", ex);
                return false;
            }

        }

        public bool Delete(int lyDoXuatId, out string message)
        {
            

            try
            {
                var entity = _lyDoXuatRepository.GetById(lyDoXuatId);
				if (entity != null)
				{
					_lyDoXuatRepository.Delete(lyDoXuatId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete LyDoXuat error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<LyDoXuatModel> GetAllLyDoXuats()
        {
            //Igrone lyDoXuat system
            return _lyDoXuatRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int lyDoXuatId, out string message)
        {
            try
            {
                var entity = _lyDoXuatRepository.Query(c => c.LyDoXuatId == lyDoXuatId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_lyDoXuatRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete LyDoXuat error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
