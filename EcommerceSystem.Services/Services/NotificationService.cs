﻿using EcommerceSystem.Core;
using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceSystem.Services
{
    public interface INotificationService : IEntityService<Notification>
    {
        void SendNotification(int? userId, int notificationId, int notificationType, string formatedDateTime, string title, string content);
        int AddNotification(int sendTouserId, string title, string message);
        bool AddSystemNotification(NotificationModel model);
        List<NotificationModel> SearchNotification(string keyword, int? type, int pageIndex, int pageSize, out int totalRecords);
        List<NotificationModel> GetCustomerMessage(int userId, bool isRead = false);
        List<NotificationModel> GetCustomerSystemNotification(int userId);
        NotificationModel RetrieveNotification(int notificationtId, int userId);
    }

    public class NotificationService : EntityService<Notification>, INotificationService
    {
        #region Members
        private readonly INotificationRepository _notificationRepository;
        private readonly INotificationRecipientRepository _notificationRecipientRepository;
        #endregion

        #region Ctors
        public NotificationService(IUnitOfWork unitOfWork, INotificationRepository notificationRepository, INotificationRecipientRepository notificationRecipientRepository)
            : base(unitOfWork, notificationRepository)
        {
            _notificationRecipientRepository = notificationRecipientRepository;
            _notificationRepository = notificationRepository;
        }
        #endregion

        #region Public Methods

        public List<NotificationModel> GetCustomerMessage(int userId, bool isRead = false)
        {
            var notifications = _notificationRecipientRepository.GetAll().Include(x => x.Notification)
                 .Where(x => x.UserId == userId && x.Notification.Type == (int)NotificationType.Message);
            if (!isRead)
            {
                notifications = notifications.Where(x=>!x.IsRead).OrderByDescending(x => x.NotificationId);
            }
            else
            {
                notifications = notifications.OrderByDescending(x => x.NotificationId);
            }
            var results = notifications.ToList();
            if (results != null)
                return results.MaptoModels();
            return null;
        }

        public NotificationModel RetrieveNotification(int notificationtId, int userId)
        {
            var notification = _notificationRepository.Find(x => x.NotificationId == notificationtId);
            if (notification != null)
            {
                MarkNotificationAsRead(notificationtId, userId);
                return notification.MaptoModel();
            }
            return null;
        }

        public void MarkNotificationAsRead(int notificationtId, int userId)
        {
            try
            {
                var notification = _notificationRecipientRepository.Find(x => x.NotificationId == notificationtId && x.UserId == userId);
                if (notification != null)
                {
                    if (!notification.IsRead)
                    {
                        notification.IsRead = true;
                        UnitOfWork.SaveChanges();
                    }
                    return;
                }
                notification = new NotificationRecipient()
                {
                    IsRead = true,
                    UserId = userId,
                    NotificationId = notificationtId,
                    CreatedDate = DateTime.Now,
                };
                _notificationRecipientRepository.Insert(notification);
                UnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }
        //public NotificationModel RetrieveSystemNotification(int notificationId)
        //{
        //    var systemNotification = _notificationRepository.Find(x => x.Type == (int)NotificationType.System && x.NotificationId == notificationId && !x.IsDeleted);
        //    var notification = _notificationRecipientRepository.Find(x => x.NotificationId == notificationId, y => y.Notification);
        //    if(systemNotification != null)
        //    {
        //        var model = systemNotification.MaptoModel();
        //        model.IsRead = notification != null ? notification.IsRead : false;
        //        return model;
        //    }
        //    return null;
        //}
        public List<NotificationModel> GetCustomerSystemNotification(int userId)
        {
            var systemNotifications = _notificationRepository.FindAll(x => x.Type == (int)NotificationType.System && x.IsActive == true && !x.IsDeleted);

            var customernotifications = _notificationRecipientRepository.FindAll(x => x.UserId == userId && x.Notification.Type == (int)NotificationType.System, y => y.Notification);
            if (systemNotifications != null)
            {
                var results = systemNotifications.Select(x => new NotificationModel()
                {
                    NotificationId = x.NotificationId,
                    Title = x.Title,
                    Message = x.Note,
                    Type = x.Type,
                    CreatedDate = x.CreatedDate,
                    IsActive = x.IsActive,
                    IsRead = customernotifications != null ? customernotifications.Any(y => y.NotificationId == x.NotificationId) : false
                }).OrderByDescending(x => x.NotificationId).ToList();
                return results;
            }
            return null;
        }
        public List<NotificationModel> SearchNotification(string keyword, int? type, int pageIndex, int pageSize, out int totalRecords)
        {
            var notifications = _notificationRepository.Search(keyword, type, pageIndex, pageSize, out totalRecords);
            return notifications != null ? notifications.MaptoModels() : null;
        }
        public bool AddSystemNotification(NotificationModel model)
        {
            try
            {
                var notification = new Notification()
                {
                    CreatedDate = DateTime.Now,
                    Note = model.Message,
                    Title = model.Title,
                    Type = (int)NotificationType.System,
                    IsActive = true
                };
                _notificationRepository.Insert(notification);
                UnitOfWork.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
        }
        public int AddNotification(int sendTouserId, string title, string message)
        {
            try
            {
                var notification = new Notification()
                {
                    Type = (short)NotificationType.Message,
                    Note = message,
                    CreatedDate = DateTime.Now,
                    Title = title,
                    IsActive = true
                };
                _notificationRepository.Insert(notification);
                UnitOfWork.SaveChanges();

                var notificationRecipient = new NotificationRecipient()
                {
                    CreatedDate = DateTime.Now,
                    UserId = sendTouserId,
                    NotificationId = notification.NotificationId
                };
                _notificationRecipientRepository.Insert(notificationRecipient);
                UnitOfWork.SaveChanges();
                SendNotification(sendTouserId, notification.NotificationId, notification.Type, notificationRecipient.CreatedDate.ToString("dd/MM/yyyy HH:mm"), title, message);

                return notificationRecipient.NotificationId;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return -1;
            }

        }

        public void SendNotification(int? userId, int notificationId, int notificationType, string formatedDateTime, string title, string content)
        {
            GlobalHost.ConnectionManager.GetHubContext<NotificationHub>().Clients.All.sendNotification(userId, notificationId, notificationType, formatedDateTime, title, content);
        }
        #endregion

    }
}
