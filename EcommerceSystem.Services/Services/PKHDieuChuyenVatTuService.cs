﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHDieuChuyenVatTuModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHDieuChuyenVatTuService : IEntityService<PKHDieuChuyenVatTu>
    {
        List<PKHDieuChuyenVatTuModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHDieuChuyenVatTu(PKHDieuChuyenVatTuModel PKHDieuChuyenVatTuModel, out string message);
        bool Delete(int pKHDieuChuyenVatTuId, out string message);
        List<PKHDieuChuyenVatTuModel> GetAllPKHDieuChuyenVatTus();
        bool CreatePKHDieuChuyenVatTu(PKHDieuChuyenVatTuModel model);
		bool ChangeStatus(int pKHDieuChuyenVatTuId, out string message);

    }
    public class PKHDieuChuyenVatTuService : EntityService<PKHDieuChuyenVatTu>, IPKHDieuChuyenVatTuService
    {
        private readonly IPKHDieuChuyenVatTuRepository _pKHDieuChuyenVatTuRepository;
        public PKHDieuChuyenVatTuService(IUnitOfWork unitOfWork, IPKHDieuChuyenVatTuRepository pKHDieuChuyenVatTuRepository)
            : base(unitOfWork, pKHDieuChuyenVatTuRepository)
        {
            _pKHDieuChuyenVatTuRepository = pKHDieuChuyenVatTuRepository;
        }

        public List<PKHDieuChuyenVatTuModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHDieuChuyenVatTuEntities = _pKHDieuChuyenVatTuRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHDieuChuyenVatTuEntities != null)
				{
					return pKHDieuChuyenVatTuEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHDieuChuyenVatTu error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHDieuChuyenVatTu(PKHDieuChuyenVatTuModel pKHDieuChuyenVatTuModel, out string message)
        {
            try
            {
                var pKHDieuChuyenVatTuEntity = _pKHDieuChuyenVatTuRepository.GetById(pKHDieuChuyenVatTuModel.PKHDieuChuyenVatTuId);
				if (pKHDieuChuyenVatTuEntity != null)
				{
					pKHDieuChuyenVatTuEntity = pKHDieuChuyenVatTuModel.MapToEntity(pKHDieuChuyenVatTuEntity);
					pKHDieuChuyenVatTuEntity.MapToModel(pKHDieuChuyenVatTuModel);

					_pKHDieuChuyenVatTuRepository.Update(pKHDieuChuyenVatTuEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHDieuChuyenVatTu error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHDieuChuyenVatTu(PKHDieuChuyenVatTuModel model)
        {
            try
            {
                var createdPKHDieuChuyenVatTu = _pKHDieuChuyenVatTuRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHDieuChuyenVatTu == null)
                {
                    Log.Error("Create pKHDieuChuyenVatTu error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHDieuChuyenVatTu error", ex);
                return false;
            }

        }

        public bool Delete(int pKHDieuChuyenVatTuId, out string message)
        {
            

            try
            {
                var entity = _pKHDieuChuyenVatTuRepository.GetById(pKHDieuChuyenVatTuId);
				if (entity != null)
				{
					_pKHDieuChuyenVatTuRepository.Delete(pKHDieuChuyenVatTuId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHDieuChuyenVatTu error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHDieuChuyenVatTuModel> GetAllPKHDieuChuyenVatTus()
        {
            //Igrone pKHDieuChuyenVatTu system
            return _pKHDieuChuyenVatTuRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHDieuChuyenVatTuId, out string message)
        {
            try
            {
                var entity = _pKHDieuChuyenVatTuRepository.Query(c => c.PKHDieuChuyenVatTuId == pKHDieuChuyenVatTuId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHDieuChuyenVatTuRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHDieuChuyenVatTu error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
