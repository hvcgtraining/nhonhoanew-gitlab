﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.DieuChuyenQuyetToanModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IDieuChuyenQuyetToanService : IEntityService<DieuChuyenQuyetToan>
    {
        List<DieuChuyenQuyetToanModel> Search(int currentPage, int pageSize, string textSearch, int loaiDieuChuyen, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateDieuChuyenQuyetToan(DieuChuyenQuyetToanModel DieuChuyenQuyetToanModel, out string message);
        bool Delete(int dieuChuyenQuyetToanId, out string message);
        List<DieuChuyenQuyetToanModel> GetAllDieuChuyenQuyetToans();
        List<DieuChuyenQuyetToanModel> GetDieuChuyenQuyetToansByLoaiDieuChuyen(int? loaiDieuChuyenQuyetToan);
        bool CreateDieuChuyenQuyetToan(DieuChuyenQuyetToanModel model);
		bool ChangeStatus(int dieuChuyenQuyetToanId, out string message);

    }
    public class DieuChuyenQuyetToanService : EntityService<DieuChuyenQuyetToan>, IDieuChuyenQuyetToanService
    {
        private readonly IDieuChuyenQuyetToanRepository _dieuChuyenQuyetToanRepository;
        public DieuChuyenQuyetToanService(IUnitOfWork unitOfWork, IDieuChuyenQuyetToanRepository dieuChuyenQuyetToanRepository)
            : base(unitOfWork, dieuChuyenQuyetToanRepository)
        {
            _dieuChuyenQuyetToanRepository = dieuChuyenQuyetToanRepository;
        }

        public List<DieuChuyenQuyetToanModel> Search(int currentPage, int pageSize, string textSearch, int loaiDieuChuyen, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var dieuChuyenQuyetToanEntities = _dieuChuyenQuyetToanRepository.Search(currentPage, pageSize, textSearch, loaiDieuChuyen, sortColumn, sortDirection, out totalPage);
				if (dieuChuyenQuyetToanEntities != null)
				{
					return dieuChuyenQuyetToanEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search DieuChuyenQuyetToan error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateDieuChuyenQuyetToan(DieuChuyenQuyetToanModel dieuChuyenQuyetToanModel, out string message)
        {
            try
            {
                var dieuChuyenQuyetToanEntity = _dieuChuyenQuyetToanRepository.GetById(dieuChuyenQuyetToanModel.DieuChuyenQuyetToanId);
				if (dieuChuyenQuyetToanEntity != null)
				{
					dieuChuyenQuyetToanEntity = dieuChuyenQuyetToanModel.MapToEntity(dieuChuyenQuyetToanEntity);
					dieuChuyenQuyetToanEntity.MapToModel(dieuChuyenQuyetToanModel);

					_dieuChuyenQuyetToanRepository.Update(dieuChuyenQuyetToanEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update DieuChuyenQuyetToan error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateDieuChuyenQuyetToan(DieuChuyenQuyetToanModel model)
        {
            try
            {
                var createdDieuChuyenQuyetToan = _dieuChuyenQuyetToanRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdDieuChuyenQuyetToan == null)
                {
                    Log.Error("Create dieuChuyenQuyetToan error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create DieuChuyenQuyetToan error", ex);
                return false;
            }

        }

        public bool Delete(int dieuChuyenQuyetToanId, out string message)
        {
            

            try
            {
                var entity = _dieuChuyenQuyetToanRepository.GetById(dieuChuyenQuyetToanId);
				if (entity != null)
				{
					_dieuChuyenQuyetToanRepository.Delete(dieuChuyenQuyetToanId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DieuChuyenQuyetToan error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<DieuChuyenQuyetToanModel> GetAllDieuChuyenQuyetToans()
        {
            //Igrone dieuChuyenQuyetToan system
            return _dieuChuyenQuyetToanRepository.GetAll().ToList().MapToModels();
        }
        public List<DieuChuyenQuyetToanModel> GetDieuChuyenQuyetToansByLoaiDieuChuyen(int? loaiDieuChuyenQuyetToan)
        {
            return _dieuChuyenQuyetToanRepository.GetAll().Where(c=>c.LoaiDieuChuyenQuyetToan == loaiDieuChuyenQuyetToan).ToList().MapToModels();
        }

        public bool ChangeStatus(int dieuChuyenQuyetToanId, out string message)
        {
            try
            {
                var entity = _dieuChuyenQuyetToanRepository.Query(c => c.DieuChuyenQuyetToanId == dieuChuyenQuyetToanId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_dieuChuyenQuyetToanRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DieuChuyenQuyetToan error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
