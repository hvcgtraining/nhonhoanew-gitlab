﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.XuongModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IXuongService : IEntityService<Xuong>
    {
        List<XuongModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateXuong(XuongModel XuongModel, out string message);
        bool Delete(int xuongId, out string message);
        List<XuongModel> GetAllXuongs();
        bool CreateXuong(XuongModel model);
		bool ChangeStatus(int xuongId, out string message);
        List<XuongModel> GetAXuongsByType(int type);

    }
    public class XuongService : EntityService<Xuong>, IXuongService
    {
        private readonly IXuongRepository _xuongRepository;
        public XuongService(IUnitOfWork unitOfWork, IXuongRepository xuongRepository)
            : base(unitOfWork, xuongRepository)
        {
            _xuongRepository = xuongRepository;
        }

        public List<XuongModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var xuongEntities = _xuongRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (xuongEntities != null)
				{
					return xuongEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search Xuong error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateXuong(XuongModel xuongModel, out string message)
        {
            try
            {
                var xuongEntity = _xuongRepository.GetById(xuongModel.XuongId);
				if (xuongEntity != null)
				{
					xuongEntity = xuongModel.MapToEntity(xuongEntity);
					xuongEntity.MapToModel(xuongModel);

					_xuongRepository.Update(xuongEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update Xuong error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateXuong(XuongModel model)
        {
            try
            {
                var createdXuong = _xuongRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdXuong == null)
                {
                    Log.Error("Create xuong error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create Xuong error", ex);
                return false;
            }

        }

        public bool Delete(int xuongId, out string message)
        {
            

            try
            {
                var entity = _xuongRepository.GetById(xuongId);
				if (entity != null)
				{
					_xuongRepository.Delete(xuongId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete Xuong error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<XuongModel> GetAllXuongs()
        {
            //Igrone xuong system
            return _xuongRepository.GetAll().ToList().MapToModels();
        }

        public List<XuongModel> GetAXuongsByType(int type)
        {
            //Igrone xuong system
            return _xuongRepository.GetAll().Where(x=>x.Type == type).ToList().MapToModels();
        }

        public bool ChangeStatus(int xuongId, out string message)
        {
            try
            {
                var entity = _xuongRepository.Query(c => c.XuongId == xuongId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status.HasValue && entity.Status.Value)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_xuongRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete Xuong error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
