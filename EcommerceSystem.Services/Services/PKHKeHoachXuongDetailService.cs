﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHKeHoachXuongDetailModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHKeHoachXuongDetailService : IEntityService<PKHKeHoachXuongDetail>
    {
        List<PKHKeHoachXuongDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHKeHoachXuongDetail(PKHKeHoachXuongDetailModel PKHKeHoachXuongDetailModel, out string message);
        bool UpdatePKHKeHoachXuongDetail(long maKHXuongChiTiet, string maVatTu, int soLuong, DateTime ngayCapHang, string ghiChu, out string message);
        bool Delete(int pKHKeHoachXuongDetailId, out string message);
        List<PKHKeHoachXuongDetailModel> GetAllPKHKeHoachXuongDetails();
        bool CreatePKHKeHoachXuongDetail(PKHKeHoachXuongDetailModel model);
		bool ChangeStatus(int pKHKeHoachXuongDetailId, out string message);
        List<PKHKeHoachXuongDetailModel> GetAllByParent(long khXuongId);

    }
    public class PKHKeHoachXuongDetailService : EntityService<PKHKeHoachXuongDetail>, IPKHKeHoachXuongDetailService
    {
        private readonly IPKHKeHoachXuongDetailRepository _pKHKeHoachXuongDetailRepository;
        public PKHKeHoachXuongDetailService(IUnitOfWork unitOfWork, IPKHKeHoachXuongDetailRepository pKHKeHoachXuongDetailRepository)
            : base(unitOfWork, pKHKeHoachXuongDetailRepository)
        {
            _pKHKeHoachXuongDetailRepository = pKHKeHoachXuongDetailRepository;
        }

        public List<PKHKeHoachXuongDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHKeHoachXuongDetailEntities = _pKHKeHoachXuongDetailRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHKeHoachXuongDetailEntities != null)
				{
					return pKHKeHoachXuongDetailEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHKeHoachXuongDetail error", ex);
            }
            totalPage = 0;
            return null;
        }

        public List<PKHKeHoachXuongDetailModel> GetAllByParent(long khXuongId)
        {
            try
            {
                var pKHKeHoachXuongDetailEntities = _pKHKeHoachXuongDetailRepository.GetAllByParent(khXuongId);
                if (pKHKeHoachXuongDetailEntities != null)
                {
                    return pKHKeHoachXuongDetailEntities.MapToModels();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHKeHoachXuongDetail error", ex);
            }
            return null;
        }

        public bool UpdatePKHKeHoachXuongDetail(PKHKeHoachXuongDetailModel pKHKeHoachXuongDetailModel, out string message)
        {
            try
            {
                var pKHKeHoachXuongDetailEntity = _pKHKeHoachXuongDetailRepository.GetById(pKHKeHoachXuongDetailModel.PKHKeHoachXuongDetailId);
				if (pKHKeHoachXuongDetailEntity != null)
				{
					pKHKeHoachXuongDetailEntity = pKHKeHoachXuongDetailModel.MapToEntity(pKHKeHoachXuongDetailEntity);
					pKHKeHoachXuongDetailEntity.MapToModel(pKHKeHoachXuongDetailModel);

					_pKHKeHoachXuongDetailRepository.Update(pKHKeHoachXuongDetailEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHKeHoachXuongDetail error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHKeHoachXuongDetail(PKHKeHoachXuongDetailModel model)
        {
            try
            {
                var createdPKHKeHoachXuongDetail = _pKHKeHoachXuongDetailRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHKeHoachXuongDetail == null)
                {
                    Log.Error("Create pKHKeHoachXuongDetail error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHKeHoachXuongDetail error", ex);
                return false;
            }

        }

        public bool UpdatePKHKeHoachXuongDetail(long maKHXuongChiTiet, string maVatTu, int soLuong, DateTime ngayCapHang, string ghiChu, out string message)
        {
            try
            {
                var pKHKeHoachXuongDetailEntity = _pKHKeHoachXuongDetailRepository.GetById(maKHXuongChiTiet);
                if (pKHKeHoachXuongDetailEntity != null)
                {
                    pKHKeHoachXuongDetailEntity.YeuCau = soLuong;
                    pKHKeHoachXuongDetailEntity.NgayCapHang = ngayCapHang;
                    pKHKeHoachXuongDetailEntity.GhiChu = ghiChu;

                    _pKHKeHoachXuongDetailRepository.Update(pKHKeHoachXuongDetailEntity);
                    UnitOfWork.SaveChanges();

                    message = "Cập nhật thành công";
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHKeHoachXuongDetail error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool Delete(int pKHKeHoachXuongDetailId, out string message)
        {
            

            try
            {
                var entity = _pKHKeHoachXuongDetailRepository.GetById(pKHKeHoachXuongDetailId);
				if (entity != null)
				{
					_pKHKeHoachXuongDetailRepository.Delete(pKHKeHoachXuongDetailId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachXuongDetail error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }


        public List<PKHKeHoachXuongDetailModel> GetAllPKHKeHoachXuongDetails()
        {
            //Igrone pKHKeHoachXuongDetail system
            return _pKHKeHoachXuongDetailRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHKeHoachXuongDetailId, out string message)
        {
            try
            {
                var entity = _pKHKeHoachXuongDetailRepository.Query(c => c.PKHKeHoachXuongDetailId == pKHKeHoachXuongDetailId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHKeHoachXuongDetailRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachXuongDetail error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
