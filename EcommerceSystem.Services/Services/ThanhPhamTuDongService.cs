﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.ThanhPhamTuDongModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IThanhPhamTuDongService : IEntityService<ThanhPhamTuDong>
    {
        List<ThanhPhamTuDongModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateThanhPhamTuDong(ThanhPhamTuDongModel ThanhPhamTuDongModel, out string message);
        bool Delete(int thanhPhamTuDongId, out string message);
        List<ThanhPhamTuDongModel> GetAllThanhPhamTuDongs();
        bool CreateThanhPhamTuDong(ThanhPhamTuDongModel model);
		bool ChangeStatus(int thanhPhamTuDongId, out string message);

    }
    public class ThanhPhamTuDongService : EntityService<ThanhPhamTuDong>, IThanhPhamTuDongService
    {
        private readonly IThanhPhamTuDongRepository _thanhPhamTuDongRepository;
        public ThanhPhamTuDongService(IUnitOfWork unitOfWork, IThanhPhamTuDongRepository thanhPhamTuDongRepository)
            : base(unitOfWork, thanhPhamTuDongRepository)
        {
            _thanhPhamTuDongRepository = thanhPhamTuDongRepository;
        }

        public List<ThanhPhamTuDongModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var thanhPhamTuDongEntities = _thanhPhamTuDongRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (thanhPhamTuDongEntities != null)
				{
					return thanhPhamTuDongEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search ThanhPhamTuDong error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateThanhPhamTuDong(ThanhPhamTuDongModel thanhPhamTuDongModel, out string message)
        {
            try
            {
                var thanhPhamTuDongEntity = _thanhPhamTuDongRepository.GetById(thanhPhamTuDongModel.ThanhPhamTuDongId);
				if (thanhPhamTuDongEntity != null)
				{
					thanhPhamTuDongEntity = thanhPhamTuDongModel.MapToEntity(thanhPhamTuDongEntity);
					thanhPhamTuDongEntity.MapToModel(thanhPhamTuDongModel);

					_thanhPhamTuDongRepository.Update(thanhPhamTuDongEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update ThanhPhamTuDong error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateThanhPhamTuDong(ThanhPhamTuDongModel model)
        {
            try
            {
                var createdThanhPhamTuDong = _thanhPhamTuDongRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdThanhPhamTuDong == null)
                {
                    Log.Error("Create thanhPhamTuDong error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create ThanhPhamTuDong error", ex);
                return false;
            }

        }

        public bool Delete(int thanhPhamTuDongId, out string message)
        {
            

            try
            {
                var entity = _thanhPhamTuDongRepository.GetById(thanhPhamTuDongId);
				if (entity != null)
				{
					_thanhPhamTuDongRepository.Delete(thanhPhamTuDongId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete ThanhPhamTuDong error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<ThanhPhamTuDongModel> GetAllThanhPhamTuDongs()
        {
            //Igrone thanhPhamTuDong system
            return _thanhPhamTuDongRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int thanhPhamTuDongId, out string message)
        {
            try
            {
                var entity = _thanhPhamTuDongRepository.Query(c => c.ThanhPhamTuDongId == thanhPhamTuDongId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_thanhPhamTuDongRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete ThanhPhamTuDong error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
