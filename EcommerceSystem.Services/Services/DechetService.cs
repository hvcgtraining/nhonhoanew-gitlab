﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.DechetModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IDechetService : IEntityService<Dechet>
    {
        List<DechetModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateDechet(DechetModel DechetModel, out string message);
        bool Delete(int dechetId, out string message);
        List<DechetModel> GetAllDechets();
        bool CreateDechet(DechetModel model);
		bool ChangeStatus(int dechetId, out string message);

    }
    public class DechetService : EntityService<Dechet>, IDechetService
    {
        private readonly IDechetRepository _dechetRepository;
        public DechetService(IUnitOfWork unitOfWork, IDechetRepository dechetRepository)
            : base(unitOfWork, dechetRepository)
        {
            _dechetRepository = dechetRepository;
        }

        public List<DechetModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var dechetEntities = _dechetRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (dechetEntities != null)
				{
					return dechetEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search Dechet error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateDechet(DechetModel dechetModel, out string message)
        {
            try
            {
                var dechetEntity = _dechetRepository.GetById(dechetModel.DechetId);
				if (dechetEntity != null)
				{
					dechetEntity = dechetModel.MapToEntity(dechetEntity);
					dechetEntity.MapToModel(dechetModel);

					_dechetRepository.Update(dechetEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update Dechet error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateDechet(DechetModel model)
        {
            try
            {
                var createdDechet = _dechetRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdDechet == null)
                {
                    Log.Error("Create dechet error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create Dechet error", ex);
                return false;
            }

        }

        public bool Delete(int dechetId, out string message)
        {
            

            try
            {
                var entity = _dechetRepository.GetById(dechetId);
				if (entity != null)
				{
					_dechetRepository.Delete(dechetId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete Dechet error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<DechetModel> GetAllDechets()
        {
            //Igrone dechet system
            return _dechetRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int dechetId, out string message)
        {
            try
            {
                var entity = _dechetRepository.Query(c => c.DechetId == dechetId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_dechetRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete Dechet error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
