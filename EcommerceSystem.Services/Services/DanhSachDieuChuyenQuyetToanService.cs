﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.DanhSachDieuChuyenQuyetToanModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IDanhSachDieuChuyenQuyetToanService : IEntityService<DanhSachDieuChuyenQuyetToan>
    {
        List<DanhSachDieuChuyenQuyetToanModel> Search(int currentPage, int pageSize, string textSearch, int loaiDieuChuyen, int quyetToanId, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateDanhSachDieuChuyenQuyetToan(DanhSachDieuChuyenQuyetToanModel DanhSachDieuChuyenQuyetToanModel, out string message);
        bool Delete(int danhSachDieuChuyenQuyetToanId, out string message);
        List<DanhSachDieuChuyenQuyetToanModel> GetAllDanhSachDieuChuyenQuyetToans();
        bool CreateDanhSachDieuChuyenQuyetToan(DanhSachDieuChuyenQuyetToanModel model);
		bool ChangeStatus(int danhSachDieuChuyenQuyetToanId, out string message);

    }
    public class DanhSachDieuChuyenQuyetToanService : EntityService<DanhSachDieuChuyenQuyetToan>, IDanhSachDieuChuyenQuyetToanService
    {
        private readonly IDanhSachDieuChuyenQuyetToanRepository _danhSachDieuChuyenQuyetToanRepository;
        public DanhSachDieuChuyenQuyetToanService(IUnitOfWork unitOfWork, IDanhSachDieuChuyenQuyetToanRepository danhSachDieuChuyenQuyetToanRepository)
            : base(unitOfWork, danhSachDieuChuyenQuyetToanRepository)
        {
            _danhSachDieuChuyenQuyetToanRepository = danhSachDieuChuyenQuyetToanRepository;
        }

        public List<DanhSachDieuChuyenQuyetToanModel> Search(int currentPage, int pageSize, string textSearch, int loaiDieuChuyen, int quyetToanId, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var danhSachDieuChuyenQuyetToanEntities = _danhSachDieuChuyenQuyetToanRepository.Search(currentPage, pageSize, textSearch, loaiDieuChuyen, quyetToanId, sortColumn, sortDirection, out totalPage);
				if (danhSachDieuChuyenQuyetToanEntities != null)
				{
					return danhSachDieuChuyenQuyetToanEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search DanhSachDieuChuyenQuyetToan error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateDanhSachDieuChuyenQuyetToan(DanhSachDieuChuyenQuyetToanModel danhSachDieuChuyenQuyetToanModel, out string message)
        {
            try
            {
                var danhSachDieuChuyenQuyetToanEntity = _danhSachDieuChuyenQuyetToanRepository.GetById(danhSachDieuChuyenQuyetToanModel.DanhSachDieuChuyenQuyetToanId);
				if (danhSachDieuChuyenQuyetToanEntity != null)
				{
					danhSachDieuChuyenQuyetToanEntity = danhSachDieuChuyenQuyetToanModel.MapToEntity(danhSachDieuChuyenQuyetToanEntity);
					danhSachDieuChuyenQuyetToanEntity.MapToModel(danhSachDieuChuyenQuyetToanModel);

					_danhSachDieuChuyenQuyetToanRepository.Update(danhSachDieuChuyenQuyetToanEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update DanhSachDieuChuyenQuyetToan error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateDanhSachDieuChuyenQuyetToan(DanhSachDieuChuyenQuyetToanModel model)
        {
            try
            {
                var createdDanhSachDieuChuyenQuyetToan = _danhSachDieuChuyenQuyetToanRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdDanhSachDieuChuyenQuyetToan == null)
                {
                    Log.Error("Create danhSachDieuChuyenQuyetToan error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create DanhSachDieuChuyenQuyetToan error", ex);
                return false;
            }

        }

        public bool Delete(int danhSachDieuChuyenQuyetToanId, out string message)
        {
            

            try
            {
                var entity = _danhSachDieuChuyenQuyetToanRepository.GetById(danhSachDieuChuyenQuyetToanId);
				if (entity != null)
				{
					_danhSachDieuChuyenQuyetToanRepository.Delete(danhSachDieuChuyenQuyetToanId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DanhSachDieuChuyenQuyetToan error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<DanhSachDieuChuyenQuyetToanModel> GetAllDanhSachDieuChuyenQuyetToans()
        {
            //Igrone danhSachDieuChuyenQuyetToan system
            return _danhSachDieuChuyenQuyetToanRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int danhSachDieuChuyenQuyetToanId, out string message)
        {
            try
            {
                var entity = _danhSachDieuChuyenQuyetToanRepository.Query(c => c.DanhSachDieuChuyenQuyetToanId == danhSachDieuChuyenQuyetToanId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_danhSachDieuChuyenQuyetToanRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DanhSachDieuChuyenQuyetToan error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
