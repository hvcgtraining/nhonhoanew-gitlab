﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.NhomSanPhamModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface INhomSanPhamService : IEntityService<NhomSanPham>
    {
        List<NhomSanPhamModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateNhomSanPham(NhomSanPhamModel NhomSanPhamModel, out string message);
        bool Delete(int nhomSanPhamId, out string message);
        List<NhomSanPhamModel> GetAllNhomSanPhams();
        bool CreateNhomSanPham(NhomSanPhamModel model);
		bool ChangeStatus(int nhomSanPhamId, out string message);

    }
    public class NhomSanPhamService : EntityService<NhomSanPham>, INhomSanPhamService
    {
        private readonly INhomSanPhamRepository _nhomSanPhamRepository;
        public NhomSanPhamService(IUnitOfWork unitOfWork, INhomSanPhamRepository nhomSanPhamRepository)
            : base(unitOfWork, nhomSanPhamRepository)
        {
            _nhomSanPhamRepository = nhomSanPhamRepository;
        }

        public List<NhomSanPhamModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var nhomSanPhamEntities = _nhomSanPhamRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (nhomSanPhamEntities != null)
				{
					return nhomSanPhamEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search NhomSanPham error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateNhomSanPham(NhomSanPhamModel nhomSanPhamModel, out string message)
        {
            try
            {
                var nhomSanPhamEntity = _nhomSanPhamRepository.GetById(nhomSanPhamModel.NhomSanPhamId);
				if (nhomSanPhamEntity != null)
				{
					nhomSanPhamEntity = nhomSanPhamModel.MapToEntity(nhomSanPhamEntity);
					nhomSanPhamEntity.MapToModel(nhomSanPhamModel);

					_nhomSanPhamRepository.Update(nhomSanPhamEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update NhomSanPham error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateNhomSanPham(NhomSanPhamModel model)
        {
            try
            {
                var createdNhomSanPham = _nhomSanPhamRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdNhomSanPham == null)
                {
                    Log.Error("Create nhomSanPham error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create NhomSanPham error", ex);
                return false;
            }

        }

        public bool Delete(int nhomSanPhamId, out string message)
        {
            

            try
            {
                var entity = _nhomSanPhamRepository.GetById(nhomSanPhamId);
				if (entity != null)
				{
					_nhomSanPhamRepository.Delete(nhomSanPhamId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete NhomSanPham error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<NhomSanPhamModel> GetAllNhomSanPhams()
        {
            //Igrone nhomSanPham system
            return _nhomSanPhamRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int nhomSanPhamId, out string message)
        {
            try
            {
                var entity = _nhomSanPhamRepository.Query(c => c.NhomSanPhamId == nhomSanPhamId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_nhomSanPhamRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete NhomSanPham error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
