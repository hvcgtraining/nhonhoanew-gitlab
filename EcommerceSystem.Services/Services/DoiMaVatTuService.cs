﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.DoiMaVatTuModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IDoiMaVatTuService : IEntityService<DoiMaVatTu>
    {
        List<DoiMaVatTuModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateDoiMaVatTu(DoiMaVatTuModel DoiMaVatTuModel, out string message);
        bool Delete(int doiMaVatTuId, out string message);
        List<DoiMaVatTuModel> GetAllDoiMaVatTus();
        bool CreateDoiMaVatTu(DoiMaVatTuModel model);
		bool ChangeStatus(int doiMaVatTuId, out string message);

    }
    public class DoiMaVatTuService : EntityService<DoiMaVatTu>, IDoiMaVatTuService
    {
        private readonly IDoiMaVatTuRepository _doiMaVatTuRepository;
        public DoiMaVatTuService(IUnitOfWork unitOfWork, IDoiMaVatTuRepository doiMaVatTuRepository)
            : base(unitOfWork, doiMaVatTuRepository)
        {
            _doiMaVatTuRepository = doiMaVatTuRepository;
        }

        public List<DoiMaVatTuModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var doiMaVatTuEntities = _doiMaVatTuRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (doiMaVatTuEntities != null)
				{
					return doiMaVatTuEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search DoiMaVatTu error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateDoiMaVatTu(DoiMaVatTuModel doiMaVatTuModel, out string message)
        {
            try
            {
                var doiMaVatTuEntity = _doiMaVatTuRepository.GetById(doiMaVatTuModel.DoiMaVatTuId);
				if (doiMaVatTuEntity != null)
				{
					doiMaVatTuEntity = doiMaVatTuModel.MapToEntity(doiMaVatTuEntity);
					doiMaVatTuEntity.MapToModel(doiMaVatTuModel);

					_doiMaVatTuRepository.Update(doiMaVatTuEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update DoiMaVatTu error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateDoiMaVatTu(DoiMaVatTuModel model)
        {
            try
            {
                var createdDoiMaVatTu = _doiMaVatTuRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdDoiMaVatTu == null)
                {
                    Log.Error("Create doiMaVatTu error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create DoiMaVatTu error", ex);
                return false;
            }

        }

        public bool Delete(int doiMaVatTuId, out string message)
        {
            

            try
            {
                var entity = _doiMaVatTuRepository.GetById(doiMaVatTuId);
				if (entity != null)
				{
					_doiMaVatTuRepository.Delete(doiMaVatTuId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DoiMaVatTu error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<DoiMaVatTuModel> GetAllDoiMaVatTus()
        {
            //Igrone doiMaVatTu system
            return _doiMaVatTuRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int doiMaVatTuId, out string message)
        {
            try
            {
                var entity = _doiMaVatTuRepository.Query(c => c.DoiMaVatTuId == doiMaVatTuId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_doiMaVatTuRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DoiMaVatTu error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
