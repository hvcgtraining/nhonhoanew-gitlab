﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.VatTuNhapRiengModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IVatTuNhapRiengService : IEntityService<VatTuNhapRieng>
    {
        List<VatTuNhapRiengModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateVatTuNhapRieng(VatTuNhapRiengModel VatTuNhapRiengModel, out string message);
        bool Delete(int vatTuNhapRiengId, out string message);
        List<VatTuNhapRiengModel> GetAllVatTuNhapRiengs();
        bool CreateVatTuNhapRieng(VatTuNhapRiengModel model);
		bool ChangeStatus(int vatTuNhapRiengId, out string message);

    }
    public class VatTuNhapRiengService : EntityService<VatTuNhapRieng>, IVatTuNhapRiengService
    {
        private readonly IVatTuNhapRiengRepository _vatTuNhapRiengRepository;
        public VatTuNhapRiengService(IUnitOfWork unitOfWork, IVatTuNhapRiengRepository vatTuNhapRiengRepository)
            : base(unitOfWork, vatTuNhapRiengRepository)
        {
            _vatTuNhapRiengRepository = vatTuNhapRiengRepository;
        }

        public List<VatTuNhapRiengModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var vatTuNhapRiengEntities = _vatTuNhapRiengRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (vatTuNhapRiengEntities != null)
				{
					return vatTuNhapRiengEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search VatTuNhapRieng error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateVatTuNhapRieng(VatTuNhapRiengModel vatTuNhapRiengModel, out string message)
        {
            try
            {
                var vatTuNhapRiengEntity = _vatTuNhapRiengRepository.GetById(vatTuNhapRiengModel.VatTuNhapRiengId);
				if (vatTuNhapRiengEntity != null)
				{
					vatTuNhapRiengEntity = vatTuNhapRiengModel.MapToEntity(vatTuNhapRiengEntity);
					vatTuNhapRiengEntity.MapToModel(vatTuNhapRiengModel);

					_vatTuNhapRiengRepository.Update(vatTuNhapRiengEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update VatTuNhapRieng error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateVatTuNhapRieng(VatTuNhapRiengModel model)
        {
            try
            {
                var createdVatTuNhapRieng = _vatTuNhapRiengRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdVatTuNhapRieng == null)
                {
                    Log.Error("Create vatTuNhapRieng error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create VatTuNhapRieng error", ex);
                return false;
            }

        }

        public bool Delete(int vatTuNhapRiengId, out string message)
        {
            

            try
            {
                var entity = _vatTuNhapRiengRepository.GetById(vatTuNhapRiengId);
				if (entity != null)
				{
					_vatTuNhapRiengRepository.Delete(vatTuNhapRiengId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete VatTuNhapRieng error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<VatTuNhapRiengModel> GetAllVatTuNhapRiengs()
        {
            //Igrone vatTuNhapRieng system
            return _vatTuNhapRiengRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int vatTuNhapRiengId, out string message)
        {
            try
            {
                var entity = _vatTuNhapRiengRepository.Query(c => c.VatTuNhapRiengId == vatTuNhapRiengId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_vatTuNhapRiengRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete VatTuNhapRieng error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
