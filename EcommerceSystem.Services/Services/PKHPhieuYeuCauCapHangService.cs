﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHPhieuYeuCauCapHangModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHPhieuYeuCauCapHangService : IEntityService<PKHPhieuYeuCauCapHang>
    {
        List<PKHPhieuYeuCauCapHangModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHPhieuYeuCauCapHang(PKHPhieuYeuCauCapHangModel PKHPhieuYeuCauCapHangModel, out string message);
        bool Delete(int pKHPhieuYeuCauCapHangId, out string message);
        List<PKHPhieuYeuCauCapHangModel> GetAllPKHPhieuYeuCauCapHangs();
        bool CreatePKHPhieuYeuCauCapHang(PKHPhieuYeuCauCapHangModel model);
		bool ChangeStatus(int pKHPhieuYeuCauCapHangId, out string message);

    }
    public class PKHPhieuYeuCauCapHangService : EntityService<PKHPhieuYeuCauCapHang>, IPKHPhieuYeuCauCapHangService
    {
        private readonly IPKHPhieuYeuCauCapHangRepository _pKHPhieuYeuCauCapHangRepository;
        private readonly IPKHPhieuYeuCauCapHangDetailRepository _pKHPhieuYeuCauCapHangDetailRepository;
        public PKHPhieuYeuCauCapHangService(IUnitOfWork unitOfWork, IPKHPhieuYeuCauCapHangRepository pKHPhieuYeuCauCapHangRepository, IPKHPhieuYeuCauCapHangDetailRepository pKHPhieuYeuCauCapHangDetailRepository)
            : base(unitOfWork, pKHPhieuYeuCauCapHangRepository)
        {
            _pKHPhieuYeuCauCapHangRepository = pKHPhieuYeuCauCapHangRepository;
            _pKHPhieuYeuCauCapHangDetailRepository = pKHPhieuYeuCauCapHangDetailRepository;
        }

        public List<PKHPhieuYeuCauCapHangModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHPhieuYeuCauCapHangEntities = _pKHPhieuYeuCauCapHangRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHPhieuYeuCauCapHangEntities != null)
				{
					return pKHPhieuYeuCauCapHangEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHPhieuYeuCauCapHang error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHPhieuYeuCauCapHang(PKHPhieuYeuCauCapHangModel pKHPhieuYeuCauCapHangModel, out string message)
        {
            try
            {
                var pKHPhieuYeuCauCapHangEntity = _pKHPhieuYeuCauCapHangRepository.GetById(pKHPhieuYeuCauCapHangModel.PKHPhieuYeuCauCapHangId);
				if (pKHPhieuYeuCauCapHangEntity != null)
				{
					pKHPhieuYeuCauCapHangEntity = pKHPhieuYeuCauCapHangModel.MapToEntity(pKHPhieuYeuCauCapHangEntity);
					pKHPhieuYeuCauCapHangEntity.MapToModel(pKHPhieuYeuCauCapHangModel);

					_pKHPhieuYeuCauCapHangRepository.Update(pKHPhieuYeuCauCapHangEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHPhieuYeuCauCapHang error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHPhieuYeuCauCapHang(PKHPhieuYeuCauCapHangModel model)
        {
            try
            {
                var createdPKHPhieuYeuCauCapHang = _pKHPhieuYeuCauCapHangRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHPhieuYeuCauCapHang == null)
                {
                    Log.Error("Create pKHPhieuYeuCauCapHang error");
                    return false;
                }
                model.PKHPhieuYeuCauCapHangId = createdPKHPhieuYeuCauCapHang.PKHPhieuYeuCauCapHangId;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHPhieuYeuCauCapHang error", ex);
                return false;
            }

        }

        public bool Delete(int pKHPhieuYeuCauCapHangId, out string message)
        {
            

            try
            {
                var entity = _pKHPhieuYeuCauCapHangRepository.GetById(pKHPhieuYeuCauCapHangId);
				if (entity != null)
				{
                    var detailEntities = _pKHPhieuYeuCauCapHangDetailRepository.GetAll().Where(p => p.PKHPhieuYeuCauCapHangId == pKHPhieuYeuCauCapHangId);
                    if (detailEntities != null && detailEntities.Any())
                    {
                        _pKHPhieuYeuCauCapHangDetailRepository.DeleteMulti(detailEntities.ToList());
                    }
                    _pKHPhieuYeuCauCapHangRepository.Delete(pKHPhieuYeuCauCapHangId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHPhieuYeuCauCapHang error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHPhieuYeuCauCapHangModel> GetAllPKHPhieuYeuCauCapHangs()
        {
            //Igrone pKHPhieuYeuCauCapHang system
            return _pKHPhieuYeuCauCapHangRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHPhieuYeuCauCapHangId, out string message)
        {
            try
            {
                var entity = _pKHPhieuYeuCauCapHangRepository.Query(c => c.PKHPhieuYeuCauCapHangId == pKHPhieuYeuCauCapHangId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHPhieuYeuCauCapHangRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHPhieuYeuCauCapHang error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
