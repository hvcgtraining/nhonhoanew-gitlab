﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHPhieuNhapVatTuModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHPhieuNhapVatTuService : IEntityService<PKHPhieuNhapVatTu>
    {
        List<PKHPhieuNhapVatTuModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHPhieuNhapVatTu(PKHPhieuNhapVatTuModel PKHPhieuNhapVatTuModel, out string message);
        bool Delete(int pKHPhieuNhapVatTuId, out string message);
        List<PKHPhieuNhapVatTuModel> GetAllPKHPhieuNhapVatTus();
        bool CreatePKHPhieuNhapVatTu(PKHPhieuNhapVatTuModel model);
		bool ChangeStatus(int pKHPhieuNhapVatTuId, out string message);

    }
    public class PKHPhieuNhapVatTuService : EntityService<PKHPhieuNhapVatTu>, IPKHPhieuNhapVatTuService
    {
        private readonly IPKHPhieuNhapVatTuRepository _pKHPhieuNhapVatTuRepository;
        public PKHPhieuNhapVatTuService(IUnitOfWork unitOfWork, IPKHPhieuNhapVatTuRepository pKHPhieuNhapVatTuRepository)
            : base(unitOfWork, pKHPhieuNhapVatTuRepository)
        {
            _pKHPhieuNhapVatTuRepository = pKHPhieuNhapVatTuRepository;
        }

        public List<PKHPhieuNhapVatTuModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHPhieuNhapVatTuEntities = _pKHPhieuNhapVatTuRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHPhieuNhapVatTuEntities != null)
				{
					return pKHPhieuNhapVatTuEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHPhieuNhapVatTu error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHPhieuNhapVatTu(PKHPhieuNhapVatTuModel pKHPhieuNhapVatTuModel, out string message)
        {
            try
            {
                var pKHPhieuNhapVatTuEntity = _pKHPhieuNhapVatTuRepository.GetById(pKHPhieuNhapVatTuModel.PKHPhieuNhapVatTuId);
				if (pKHPhieuNhapVatTuEntity != null)
				{
					pKHPhieuNhapVatTuEntity = pKHPhieuNhapVatTuModel.MapToEntity(pKHPhieuNhapVatTuEntity);
					pKHPhieuNhapVatTuEntity.MapToModel(pKHPhieuNhapVatTuModel);

					_pKHPhieuNhapVatTuRepository.Update(pKHPhieuNhapVatTuEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHPhieuNhapVatTu error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHPhieuNhapVatTu(PKHPhieuNhapVatTuModel model)
        {
            try
            {
                var createdPKHPhieuNhapVatTu = _pKHPhieuNhapVatTuRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHPhieuNhapVatTu == null)
                {
                    Log.Error("Create pKHPhieuNhapVatTu error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHPhieuNhapVatTu error", ex);
                return false;
            }

        }

        public bool Delete(int pKHPhieuNhapVatTuId, out string message)
        {
            

            try
            {
                var entity = _pKHPhieuNhapVatTuRepository.GetById(pKHPhieuNhapVatTuId);
				if (entity != null)
				{
					_pKHPhieuNhapVatTuRepository.Delete(pKHPhieuNhapVatTuId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHPhieuNhapVatTu error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHPhieuNhapVatTuModel> GetAllPKHPhieuNhapVatTus()
        {
            //Igrone pKHPhieuNhapVatTu system
            return _pKHPhieuNhapVatTuRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHPhieuNhapVatTuId, out string message)
        {
            try
            {
                var entity = _pKHPhieuNhapVatTuRepository.Query(c => c.PKHPhieuNhapVatTuId == pKHPhieuNhapVatTuId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHPhieuNhapVatTuRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHPhieuNhapVatTu error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
