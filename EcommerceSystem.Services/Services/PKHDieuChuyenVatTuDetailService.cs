﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHDieuChuyenVatTuDetailModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHDieuChuyenVatTuDetailService : IEntityService<PKHDieuChuyenVatTuDetail>
    {
        List<PKHDieuChuyenVatTuDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHDieuChuyenVatTuDetail(PKHDieuChuyenVatTuDetailModel PKHDieuChuyenVatTuDetailModel, out string message);
        bool Delete(int pKHDieuChuyenVatTuDetailId, out string message);
        List<PKHDieuChuyenVatTuDetailModel> GetAllPKHDieuChuyenVatTuDetails();
        bool CreatePKHDieuChuyenVatTuDetail(PKHDieuChuyenVatTuDetailModel model);
		bool ChangeStatus(int pKHDieuChuyenVatTuDetailId, out string message);

    }
    public class PKHDieuChuyenVatTuDetailService : EntityService<PKHDieuChuyenVatTuDetail>, IPKHDieuChuyenVatTuDetailService
    {
        private readonly IPKHDieuChuyenVatTuDetailRepository _pKHDieuChuyenVatTuDetailRepository;
        public PKHDieuChuyenVatTuDetailService(IUnitOfWork unitOfWork, IPKHDieuChuyenVatTuDetailRepository pKHDieuChuyenVatTuDetailRepository)
            : base(unitOfWork, pKHDieuChuyenVatTuDetailRepository)
        {
            _pKHDieuChuyenVatTuDetailRepository = pKHDieuChuyenVatTuDetailRepository;
        }

        public List<PKHDieuChuyenVatTuDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHDieuChuyenVatTuDetailEntities = _pKHDieuChuyenVatTuDetailRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHDieuChuyenVatTuDetailEntities != null)
				{
					return pKHDieuChuyenVatTuDetailEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHDieuChuyenVatTuDetail error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHDieuChuyenVatTuDetail(PKHDieuChuyenVatTuDetailModel pKHDieuChuyenVatTuDetailModel, out string message)
        {
            try
            {
                var pKHDieuChuyenVatTuDetailEntity = _pKHDieuChuyenVatTuDetailRepository.GetById(pKHDieuChuyenVatTuDetailModel.PKHDieuChuyenVatTuDetailId);
				if (pKHDieuChuyenVatTuDetailEntity != null)
				{
					pKHDieuChuyenVatTuDetailEntity = pKHDieuChuyenVatTuDetailModel.MapToEntity(pKHDieuChuyenVatTuDetailEntity);
					pKHDieuChuyenVatTuDetailEntity.MapToModel(pKHDieuChuyenVatTuDetailModel);

					_pKHDieuChuyenVatTuDetailRepository.Update(pKHDieuChuyenVatTuDetailEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHDieuChuyenVatTuDetail error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHDieuChuyenVatTuDetail(PKHDieuChuyenVatTuDetailModel model)
        {
            try
            {
                var createdPKHDieuChuyenVatTuDetail = _pKHDieuChuyenVatTuDetailRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHDieuChuyenVatTuDetail == null)
                {
                    Log.Error("Create pKHDieuChuyenVatTuDetail error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHDieuChuyenVatTuDetail error", ex);
                return false;
            }

        }

        public bool Delete(int pKHDieuChuyenVatTuDetailId, out string message)
        {
            

            try
            {
                var entity = _pKHDieuChuyenVatTuDetailRepository.GetById(pKHDieuChuyenVatTuDetailId);
				if (entity != null)
				{
					_pKHDieuChuyenVatTuDetailRepository.Delete(pKHDieuChuyenVatTuDetailId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHDieuChuyenVatTuDetail error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHDieuChuyenVatTuDetailModel> GetAllPKHDieuChuyenVatTuDetails()
        {
            //Igrone pKHDieuChuyenVatTuDetail system
            return _pKHDieuChuyenVatTuDetailRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHDieuChuyenVatTuDetailId, out string message)
        {
            try
            {
                var entity = _pKHDieuChuyenVatTuDetailRepository.Query(c => c.PKHDieuChuyenVatTuDetailId == pKHDieuChuyenVatTuDetailId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHDieuChuyenVatTuDetailRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHDieuChuyenVatTuDetail error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
