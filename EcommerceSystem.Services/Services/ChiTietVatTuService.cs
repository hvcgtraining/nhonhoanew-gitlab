﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.ChiTietVatTuModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IChiTietVatTuService : IEntityService<ChiTietVatTu>
    {
        List<ChiTietVatTuModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateChiTietVatTu(ChiTietVatTuModel ChiTietVatTuModel, out string message);
        bool Delete(int VatTuId, out string message);
        List<ChiTietVatTuModel> GetAllChiTietVatTus();
        bool CreateChiTietVatTu(ChiTietVatTuModel model);
		bool ChangeStatus(int VatTuId, out string message);

    }
    public class ChiTietVatTuService : EntityService<ChiTietVatTu>, IChiTietVatTuService
    {
        private readonly IChiTietVatTuRepository _chiTietVatTuRepository;
        public ChiTietVatTuService(IUnitOfWork unitOfWork, IChiTietVatTuRepository chiTietVatTuRepository)
            : base(unitOfWork, chiTietVatTuRepository)
        {
            _chiTietVatTuRepository = chiTietVatTuRepository;
        }

        public List<ChiTietVatTuModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var chiTietVatTuEntities = _chiTietVatTuRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (chiTietVatTuEntities != null)
				{
					return chiTietVatTuEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search ChiTietVatTu error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateChiTietVatTu(ChiTietVatTuModel chiTietVatTuModel, out string message)
        {
            try
            {
                var chiTietVatTuEntity = _chiTietVatTuRepository.GetById(chiTietVatTuModel.ChiTietVatTuId);
				if (chiTietVatTuEntity != null)
				{
					chiTietVatTuEntity = chiTietVatTuModel.MapToEntity(chiTietVatTuEntity);
					chiTietVatTuEntity.MapToModel(chiTietVatTuModel);

					_chiTietVatTuRepository.Update(chiTietVatTuEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update ChiTietVatTu error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateChiTietVatTu(ChiTietVatTuModel model)
        {
            try
            {
                var createdChiTietVatTu = _chiTietVatTuRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdChiTietVatTu == null)
                {
                    Log.Error("Create chiTietVatTu error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create ChiTietVatTu error", ex);
                return false;
            }

        }

        public bool Delete(int VatTuId, out string message)
        {
            

            try
            {
                var entity = _chiTietVatTuRepository.GetById(VatTuId);
				if (entity != null)
				{
					_chiTietVatTuRepository.Delete(VatTuId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete ChiTietVatTu error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<ChiTietVatTuModel> GetAllChiTietVatTus()
        {
            //Igrone chiTietVatTu system
            return _chiTietVatTuRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int VatTuId, out string message)
        {
            try
            {
                var entity = _chiTietVatTuRepository.Query(c => c.ChiTietVatTuId == VatTuId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_chiTietVatTuRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete ChiTietVatTu error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
