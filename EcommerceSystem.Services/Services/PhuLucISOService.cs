﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PhuLucIsoModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPhuLucIsoService : IEntityService<PhuLucIso>
    {
        List<PhuLucIsoModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePhuLucIso(PhuLucIsoModel PhuLucIsoModel, out string message);
        bool Delete(int phuLucIsoId, out string message);
        List<PhuLucIsoModel> GetAllPhuLucIsos();
        bool CreatePhuLucIso(PhuLucIsoModel model);
		bool ChangeStatus(int phuLucIsoId, out string message);

    }
    public class PhuLucIsoService : EntityService<PhuLucIso>, IPhuLucIsoService
    {
        private readonly IPhuLucIsoRepository _phuLucIsoRepository;
        public PhuLucIsoService(IUnitOfWork unitOfWork, IPhuLucIsoRepository phuLucIsoRepository)
            : base(unitOfWork, phuLucIsoRepository)
        {
            _phuLucIsoRepository = phuLucIsoRepository;
        }

        public List<PhuLucIsoModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var phuLucIsoEntities = _phuLucIsoRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (phuLucIsoEntities != null)
				{
					return phuLucIsoEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PhuLucIso error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePhuLucIso(PhuLucIsoModel phuLucIsoModel, out string message)
        {
            try
            {
                var phuLucIsoEntity = _phuLucIsoRepository.GetById(phuLucIsoModel.PhuLucIsoId);
				if (phuLucIsoEntity != null)
				{
					phuLucIsoEntity = phuLucIsoModel.MapToEntity(phuLucIsoEntity);
					phuLucIsoEntity.MapToModel(phuLucIsoModel);

					_phuLucIsoRepository.Update(phuLucIsoEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PhuLucIso error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePhuLucIso(PhuLucIsoModel model)
        {
            try
            {
                var createdPhuLucIso = _phuLucIsoRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPhuLucIso == null)
                {
                    Log.Error("Create phuLucIso error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PhuLucIso error", ex);
                return false;
            }

        }

        public bool Delete(int phuLucIsoId, out string message)
        {
            

            try
            {
                var entity = _phuLucIsoRepository.GetById(phuLucIsoId);
				if (entity != null)
				{
					_phuLucIsoRepository.Delete(phuLucIsoId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PhuLucIso error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PhuLucIsoModel> GetAllPhuLucIsos()
        {
            //Igrone phuLucIso system
            return _phuLucIsoRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int phuLucIsoId, out string message)
        {
            try
            {
                var entity = _phuLucIsoRepository.Query(c => c.PhuLucIsoId == phuLucIsoId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_phuLucIsoRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PhuLucIso error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
