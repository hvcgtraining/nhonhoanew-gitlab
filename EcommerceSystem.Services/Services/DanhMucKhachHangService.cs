﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.DanhMucKhachHangModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IDanhMucKhachHangService : IEntityService<DanhMucKhachHang>
    {
        List<DanhMucKhachHangModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateDanhMucKhachHang(DanhMucKhachHangModel DanhMucKhachHangModel, out string message);
        bool Delete(int danhMucKhachHangId, out string message);
        List<DanhMucKhachHangModel> GetAllDanhMucKhachHangs();
        bool CreateDanhMucKhachHang(DanhMucKhachHangModel model);
		bool ChangeStatus(int danhMucKhachHangId, out string message);

    }
    public class DanhMucKhachHangService : EntityService<DanhMucKhachHang>, IDanhMucKhachHangService
    {
        private readonly IDanhMucKhachHangRepository _danhMucKhachHangRepository;
        public DanhMucKhachHangService(IUnitOfWork unitOfWork, IDanhMucKhachHangRepository danhMucKhachHangRepository)
            : base(unitOfWork, danhMucKhachHangRepository)
        {
            _danhMucKhachHangRepository = danhMucKhachHangRepository;
        }

        public List<DanhMucKhachHangModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var danhMucKhachHangEntities = _danhMucKhachHangRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (danhMucKhachHangEntities != null)
				{
					return danhMucKhachHangEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search DanhMucKhachHang error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateDanhMucKhachHang(DanhMucKhachHangModel danhMucKhachHangModel, out string message)
        {
            try
            {
                var danhMucKhachHangEntity = _danhMucKhachHangRepository.GetById(danhMucKhachHangModel.DanhMucKhachHangId);
				if (danhMucKhachHangEntity != null)
				{
					danhMucKhachHangEntity = danhMucKhachHangModel.MapToEntity(danhMucKhachHangEntity);
					danhMucKhachHangEntity.MapToModel(danhMucKhachHangModel);

					_danhMucKhachHangRepository.Update(danhMucKhachHangEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update DanhMucKhachHang error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateDanhMucKhachHang(DanhMucKhachHangModel model)
        {
            try
            {
                var createdDanhMucKhachHang = _danhMucKhachHangRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdDanhMucKhachHang == null)
                {
                    Log.Error("Create danhMucKhachHang error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create DanhMucKhachHang error", ex);
                return false;
            }

        }

        public bool Delete(int danhMucKhachHangId, out string message)
        {
            

            try
            {
                var entity = _danhMucKhachHangRepository.GetById(danhMucKhachHangId);
				if (entity != null)
				{
					_danhMucKhachHangRepository.Delete(danhMucKhachHangId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DanhMucKhachHang error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<DanhMucKhachHangModel> GetAllDanhMucKhachHangs()
        {
            //Igrone danhMucKhachHang system
            return _danhMucKhachHangRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int danhMucKhachHangId, out string message)
        {
            try
            {
                var entity = _danhMucKhachHangRepository.Query(c => c.DanhMucKhachHangId == danhMucKhachHangId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_danhMucKhachHangRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DanhMucKhachHang error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
