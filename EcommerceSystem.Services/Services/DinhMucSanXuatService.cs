﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.DinhMucSanXuatModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace EcommerceSystem.Services
{
    public interface IDinhMucSanXuatService : IEntityService<DinhMucSanXuat>
    {
        List<DinhMucSanXuatModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateDinhMucSanXuat(DinhMucSanXuatModel DinhMucSanXuatModel, out string message);
        bool Delete(int dinhMucSanXuatId, out string message);
        List<DinhMucSanXuatModel> GetAllDinhMucSanXuats();
        bool CreateDinhMucSanXuat(DinhMucSanXuatModel model);
		bool ChangeStatus(int dinhMucSanXuatId, out string message);

        List<SelectListItem> GetListKho();

        List<SelectListItem> GetListChiTietVatTu();
    }
    public class DinhMucSanXuatService : EntityService<DinhMucSanXuat>, IDinhMucSanXuatService
    {
        private readonly IDinhMucSanXuatRepository _dinhMucSanXuatRepository;
        private readonly IKhoRepository _khoRepository;
        private readonly IChiTietVatTuRepository _chiTietVatTuRepository;

        public DinhMucSanXuatService(IUnitOfWork unitOfWork, IDinhMucSanXuatRepository dinhMucSanXuatRepository, IKhoRepository khoRepository, IChiTietVatTuRepository chiTietVatTuRepository)
            : base(unitOfWork, dinhMucSanXuatRepository)
        {
            _dinhMucSanXuatRepository = dinhMucSanXuatRepository;
            _khoRepository = khoRepository;
            _chiTietVatTuRepository = chiTietVatTuRepository;
        }

        public List<DinhMucSanXuatModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var dinhMucSanXuatEntities = _dinhMucSanXuatRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (dinhMucSanXuatEntities != null)
				{
					return dinhMucSanXuatEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search DinhMucSanXuat error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateDinhMucSanXuat(DinhMucSanXuatModel dinhMucSanXuatModel, out string message)
        {
            try
            {
                var dinhMucSanXuatEntity = _dinhMucSanXuatRepository.GetById(dinhMucSanXuatModel.DinhMucSanXuatId);
				if (dinhMucSanXuatEntity != null)
				{
					dinhMucSanXuatEntity = dinhMucSanXuatModel.MapToEntity(dinhMucSanXuatEntity);

					_dinhMucSanXuatRepository.Update(dinhMucSanXuatEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update DinhMucSanXuat error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateDinhMucSanXuat(DinhMucSanXuatModel model)
        {
            try
            {
                var createdDinhMucSanXuat = _dinhMucSanXuatRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdDinhMucSanXuat == null)
                {
                    Log.Error("Create dinhMucSanXuat error");
                    return false;
                }
                model.DinhMucSanXuatId = createdDinhMucSanXuat.DinhMucSanXuatId;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create DinhMucSanXuat error", ex);
                return false;
            }

        }

        public bool Delete(int dinhMucSanXuatId, out string message)
        {
            

            try
            {
                var entity = _dinhMucSanXuatRepository.GetById(dinhMucSanXuatId);
				if (entity != null)
				{
					_dinhMucSanXuatRepository.Delete(dinhMucSanXuatId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DinhMucSanXuat error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<DinhMucSanXuatModel> GetAllDinhMucSanXuats()
        {
            //Igrone dinhMucSanXuat system
            return _dinhMucSanXuatRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int dinhMucSanXuatId, out string message)
        {
            try
            {
                var entity = _dinhMucSanXuatRepository.Query(c => c.DinhMucSanXuatId == dinhMucSanXuatId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_dinhMucSanXuatRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DinhMucSanXuat error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }

        public List<SelectListItem> GetListKho()
        {
            var listKho = _khoRepository.GetAll().Select(x => new SelectListItem
            {
                Value = x.KhoId.ToString(),
                Text = x.Title
            })
            .ToList();

            return listKho;
        }

        public List<SelectListItem> GetListChiTietVatTu()
        {
            var listChiTietVatTu = _chiTietVatTuRepository.GetAll().Select(x => new SelectListItem
                {
                    Value = x.ChiTietVatTuId.ToString(),
                    Text = x.Title
                })
                .ToList();

            return listChiTietVatTu;
        }
    }
}
