﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHTheoDoiGiaoHangModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHTheoDoiGiaoHangService : IEntityService<PKHTheoDoiGiaoHang>
    {
        List<PKHTheoDoiGiaoHangModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHTheoDoiGiaoHang(PKHTheoDoiGiaoHangModel PKHTheoDoiGiaoHangModel, out string message);
        bool Delete(int pKHTheoDoiGiaoHangId, out string message);
        List<PKHTheoDoiGiaoHangModel> GetAllPKHTheoDoiGiaoHangs();
        bool CreatePKHTheoDoiGiaoHang(PKHTheoDoiGiaoHangModel model);
		bool ChangeStatus(int pKHTheoDoiGiaoHangId, out string message);

    }
    public class PKHTheoDoiGiaoHangService : EntityService<PKHTheoDoiGiaoHang>, IPKHTheoDoiGiaoHangService
    {
        private readonly IPKHTheoDoiGiaoHangRepository _pKHTheoDoiGiaoHangRepository;
        public PKHTheoDoiGiaoHangService(IUnitOfWork unitOfWork, IPKHTheoDoiGiaoHangRepository pKHTheoDoiGiaoHangRepository)
            : base(unitOfWork, pKHTheoDoiGiaoHangRepository)
        {
            _pKHTheoDoiGiaoHangRepository = pKHTheoDoiGiaoHangRepository;
        }

        public List<PKHTheoDoiGiaoHangModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHTheoDoiGiaoHangEntities = _pKHTheoDoiGiaoHangRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHTheoDoiGiaoHangEntities != null)
				{
					return pKHTheoDoiGiaoHangEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHTheoDoiGiaoHang error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHTheoDoiGiaoHang(PKHTheoDoiGiaoHangModel pKHTheoDoiGiaoHangModel, out string message)
        {
            try
            {
                var pKHTheoDoiGiaoHangEntity = _pKHTheoDoiGiaoHangRepository.GetById(pKHTheoDoiGiaoHangModel.PKHTheoDoiGiaoHangId);
				if (pKHTheoDoiGiaoHangEntity != null)
				{
					pKHTheoDoiGiaoHangEntity = pKHTheoDoiGiaoHangModel.MapToEntity(pKHTheoDoiGiaoHangEntity);
					pKHTheoDoiGiaoHangEntity.MapToModel(pKHTheoDoiGiaoHangModel);

					_pKHTheoDoiGiaoHangRepository.Update(pKHTheoDoiGiaoHangEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHTheoDoiGiaoHang error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHTheoDoiGiaoHang(PKHTheoDoiGiaoHangModel model)
        {
            try
            {
                var createdPKHTheoDoiGiaoHang = _pKHTheoDoiGiaoHangRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHTheoDoiGiaoHang == null)
                {
                    Log.Error("Create pKHTheoDoiGiaoHang error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHTheoDoiGiaoHang error", ex);
                return false;
            }

        }

        public bool Delete(int pKHTheoDoiGiaoHangId, out string message)
        {
            

            try
            {
                var entity = _pKHTheoDoiGiaoHangRepository.GetById(pKHTheoDoiGiaoHangId);
				if (entity != null)
				{
					_pKHTheoDoiGiaoHangRepository.Delete(pKHTheoDoiGiaoHangId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHTheoDoiGiaoHang error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHTheoDoiGiaoHangModel> GetAllPKHTheoDoiGiaoHangs()
        {
            //Igrone pKHTheoDoiGiaoHang system
            return _pKHTheoDoiGiaoHangRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHTheoDoiGiaoHangId, out string message)
        {
            try
            {
                var entity = _pKHTheoDoiGiaoHangRepository.Query(c => c.PKHTheoDoiGiaoHangId == pKHTheoDoiGiaoHangId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHTheoDoiGiaoHangRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHTheoDoiGiaoHang error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
