﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHKeHoachXuatKhauDetailModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHKeHoachXuatKhauDetailService : IEntityService<PKHKeHoachXuatKhauDetail>
    {
        List<PKHKeHoachXuatKhauDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHKeHoachXuatKhauDetail(PKHKeHoachXuatKhauDetailModel PKHKeHoachXuatKhauDetailModel, out string message);
        bool Delete(int pKHKeHoachXuatKhauDetailId, out string message);
        List<PKHKeHoachXuatKhauDetailModel> GetAllPKHKeHoachXuatKhauDetails();
        bool CreatePKHKeHoachXuatKhauDetail(PKHKeHoachXuatKhauDetailModel model);
		bool ChangeStatus(int pKHKeHoachXuatKhauDetailId, out string message);

    }
    public class PKHKeHoachXuatKhauDetailService : EntityService<PKHKeHoachXuatKhauDetail>, IPKHKeHoachXuatKhauDetailService
    {
        private readonly IPKHKeHoachXuatKhauDetailRepository _pKHKeHoachXuatKhauDetailRepository;
        public PKHKeHoachXuatKhauDetailService(IUnitOfWork unitOfWork, IPKHKeHoachXuatKhauDetailRepository pKHKeHoachXuatKhauDetailRepository)
            : base(unitOfWork, pKHKeHoachXuatKhauDetailRepository)
        {
            _pKHKeHoachXuatKhauDetailRepository = pKHKeHoachXuatKhauDetailRepository;
        }

        public List<PKHKeHoachXuatKhauDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHKeHoachXuatKhauDetailEntities = _pKHKeHoachXuatKhauDetailRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHKeHoachXuatKhauDetailEntities != null)
				{
					return pKHKeHoachXuatKhauDetailEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHKeHoachXuatKhauDetail error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHKeHoachXuatKhauDetail(PKHKeHoachXuatKhauDetailModel pKHKeHoachXuatKhauDetailModel, out string message)
        {
            try
            {
                var pKHKeHoachXuatKhauDetailEntity = _pKHKeHoachXuatKhauDetailRepository.GetById(pKHKeHoachXuatKhauDetailModel.PKHKeHoachXuatKhauDetailId);
				if (pKHKeHoachXuatKhauDetailEntity != null)
				{
					pKHKeHoachXuatKhauDetailEntity = pKHKeHoachXuatKhauDetailModel.MapToEntity(pKHKeHoachXuatKhauDetailEntity);
					pKHKeHoachXuatKhauDetailEntity.MapToModel(pKHKeHoachXuatKhauDetailModel);

					_pKHKeHoachXuatKhauDetailRepository.Update(pKHKeHoachXuatKhauDetailEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHKeHoachXuatKhauDetail error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHKeHoachXuatKhauDetail(PKHKeHoachXuatKhauDetailModel model)
        {
            try
            {
                var createdPKHKeHoachXuatKhauDetail = _pKHKeHoachXuatKhauDetailRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHKeHoachXuatKhauDetail == null)
                {
                    Log.Error("Create pKHKeHoachXuatKhauDetail error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHKeHoachXuatKhauDetail error", ex);
                return false;
            }

        }

        public bool Delete(int pKHKeHoachXuatKhauDetailId, out string message)
        {
            

            try
            {
                var entity = _pKHKeHoachXuatKhauDetailRepository.GetById(pKHKeHoachXuatKhauDetailId);
				if (entity != null)
				{
					_pKHKeHoachXuatKhauDetailRepository.Delete(pKHKeHoachXuatKhauDetailId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachXuatKhauDetail error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHKeHoachXuatKhauDetailModel> GetAllPKHKeHoachXuatKhauDetails()
        {
            //Igrone pKHKeHoachXuatKhauDetail system
            return _pKHKeHoachXuatKhauDetailRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHKeHoachXuatKhauDetailId, out string message)
        {
            try
            {
                var entity = _pKHKeHoachXuatKhauDetailRepository.Query(c => c.PKHKeHoachXuatKhauDetailId == pKHKeHoachXuatKhauDetailId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHKeHoachXuatKhauDetailRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachXuatKhauDetail error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
