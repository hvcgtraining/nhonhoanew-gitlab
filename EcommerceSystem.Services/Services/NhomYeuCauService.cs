﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.NhomYeuCauModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface INhomYeuCauService : IEntityService<NhomYeuCau>
    {
        List<NhomYeuCauModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateNhomYeuCau(NhomYeuCauModel NhomYeuCauModel, out string message);
        bool Delete(int nhomYeuCauId, out string message);
        List<NhomYeuCauModel> GetAllNhomYeuCaus();
        bool CreateNhomYeuCau(NhomYeuCauModel model, out string message);
		bool ChangeStatus(int nhomYeuCauId, out string message);

    }
    public class NhomYeuCauService : EntityService<NhomYeuCau>, INhomYeuCauService
    {
        private readonly INhomYeuCauRepository _nhomYeuCauRepository;
        public NhomYeuCauService(IUnitOfWork unitOfWork, INhomYeuCauRepository nhomYeuCauRepository)
            : base(unitOfWork, nhomYeuCauRepository)
        {
            _nhomYeuCauRepository = nhomYeuCauRepository;
        }

        public List<NhomYeuCauModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var nhomYeuCauEntities = _nhomYeuCauRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (nhomYeuCauEntities != null)
				{
					return nhomYeuCauEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search NhomYeuCau error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateNhomYeuCau(NhomYeuCauModel nhomYeuCauModel, out string message)
        {
            try
            {
                var nhomYeuCauEntity = _nhomYeuCauRepository.GetById(nhomYeuCauModel.NhomYeuCauId);
				if (nhomYeuCauEntity != null)
				{
					nhomYeuCauEntity = nhomYeuCauModel.MapToEntity(nhomYeuCauEntity);
					nhomYeuCauEntity.MapToModel(nhomYeuCauModel);

					_nhomYeuCauRepository.Update(nhomYeuCauEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update NhomYeuCau error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateNhomYeuCau(NhomYeuCauModel model, out string message)
        {
            message = "";
            try
            {

                var entity = _nhomYeuCauRepository.GetAll().FirstOrDefault(n=>n.Title.ToLower().Trim() == model.Title.ToLower().Trim());
                if(entity != null)
                {
                    message = "Nhóm đã tồn tại, Xin vui lòng nhập nhóm mới";
                    return false;
                }
                var createdNhomYeuCau = _nhomYeuCauRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdNhomYeuCau == null)
                {
                    message = "Xin vui lòng thử lại";
                    Log.Error("Create nhomYeuCau error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create NhomYeuCau error", ex);
                return false;
            }

        }

        public bool Delete(int nhomYeuCauId, out string message)
        {
            

            try
            {
                var entity = _nhomYeuCauRepository.GetById(nhomYeuCauId);
				if (entity != null)
				{
					_nhomYeuCauRepository.Delete(nhomYeuCauId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete NhomYeuCau error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<NhomYeuCauModel> GetAllNhomYeuCaus()
        {
            //Igrone nhomYeuCau system
            return _nhomYeuCauRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int nhomYeuCauId, out string message)
        {
            try
            {
                var entity = _nhomYeuCauRepository.Query(c => c.NhomYeuCauId == nhomYeuCauId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_nhomYeuCauRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete NhomYeuCau error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
