﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.ToleModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace EcommerceSystem.Services
{
    public interface IToleService : IEntityService<Tole>
    {
        List<ToleModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateTole(ToleModel ToleModel, out string message);
        bool Delete(int toleId, out string message);
        List<ToleModel> GetAllToles();
        bool CreateTole(ToleModel model);
		bool ChangeStatus(int toleId, out string message);

        List<SelectListItem> GetListDechet();

    }
    public class ToleService : EntityService<Tole>, IToleService
    {
        private readonly IToleRepository _toleRepository;
        private readonly IDechetRepository _dechetRepository;

        public ToleService(IUnitOfWork unitOfWork, IToleRepository toleRepository, IDechetRepository dechetRepository)
            : base(unitOfWork, toleRepository)
        {
            _toleRepository = toleRepository;
            _dechetRepository = dechetRepository;
        }

        public List<ToleModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var toleEntities = _toleRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
                if (toleEntities != null)
				{
					return toleEntities.MapToModels();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Search Tole error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateTole(ToleModel toleModel, out string message)
        {
            try
            {
                var toleEntity = _toleRepository.GetById(toleModel.ToleId);
				if (toleEntity != null)
				{
					toleEntity = toleModel.MapToEntity(toleEntity);

					_toleRepository.Update(toleEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update Tole error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateTole(ToleModel model)
        {
            try
            {
                var createdTole = _toleRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdTole == null)
                {
                    Log.Error("Create tole error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create Tole error", ex);
                return false;
            }

        }

        public bool Delete(int toleId, out string message)
        {
            

            try
            {
                var entity = _toleRepository.GetById(toleId);
				if (entity != null)
				{
					_toleRepository.Delete(toleId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete Tole error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<ToleModel> GetAllToles()
        {
            //Igrone tole system
            return _toleRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int toleId, out string message)
        {
            try
            {
                var entity = _toleRepository.Query(c => c.ToleId == toleId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_toleRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete Tole error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }

        public List<SelectListItem> GetListDechet()
        {
            var listDechet = _dechetRepository.GetAll().Select(x => new SelectListItem
            {
                Value = x.DechetId.ToString(),
                Text = x.Title
            }).ToList();

            return listDechet;
        }
    }
}
