﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.DanhSachChiTietModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace EcommerceSystem.Services
{
    public interface IDanhSachChiTietService : IEntityService<DanhSachChiTiet>
    {
        List<DanhSachChiTietModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        List<DanhSachChiTietModel> GetAllByParentId(int dinhMucSacXuatId);
        List<DanhSachChiTietModel> GetAllByDinhMucCatToleId(int dinhMucSacXuatId);
        bool UpdateDanhSachChiTiet(DanhSachChiTietModel DanhSachChiTietModel, out string message);
        bool Delete(int danhSachChiTietId, out string message);
        List<DanhSachChiTietModel> GetAllDanhSachChiTiets();
        bool CreateDanhSachChiTiet(DanhSachChiTietModel model);
		bool ChangeStatus(int danhSachChiTietId, out string message);

        List<SelectListItem> GetListChiTietVatTu();
    }
    public class DanhSachChiTietService : EntityService<DanhSachChiTiet>, IDanhSachChiTietService
    {
        private readonly IDanhSachChiTietRepository _danhSachChiTietRepository;
        private readonly IChiTietVatTuRepository _chiTietVatTuRepository;

        public DanhSachChiTietService(IUnitOfWork unitOfWork, IDanhSachChiTietRepository danhSachChiTietRepository, IChiTietVatTuRepository chiTietVatTuRepository)
            : base(unitOfWork, danhSachChiTietRepository)
        {
            _danhSachChiTietRepository = danhSachChiTietRepository;
            _chiTietVatTuRepository = chiTietVatTuRepository;
        }

        public List<DanhSachChiTietModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var danhSachChiTietEntities = _danhSachChiTietRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (danhSachChiTietEntities != null)
				{
					return danhSachChiTietEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search DanhSachChiTiet error", ex);
            }
            totalPage = 0;
            return null;
        }

        public List<DanhSachChiTietModel> GetAllByParentId(int dinhMucSanXuatId)
        {
            try
            {
                var danhSachChiTiets = _danhSachChiTietRepository.GetAllByParentId(dinhMucSanXuatId);
                if (danhSachChiTiets != null)
                {
                    return danhSachChiTiets.MapToModels();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Search Danh sach chi tiet error", ex);
            }
            return null;
        }

        public List<DanhSachChiTietModel> GetAllByDinhMucCatToleId(int dinhMucCatToleId)
        {
            try
            {
                var danhSachChiTiets = _danhSachChiTietRepository.GetAllByDinhMucCatToleId(dinhMucCatToleId);
                if (danhSachChiTiets != null)
                {
                    return danhSachChiTiets.MapToModels();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Search Danh sach chi tiet error", ex);
            }
            return null;
        }

        public bool UpdateDanhSachChiTiet(DanhSachChiTietModel danhSachChiTietModel, out string message)
        {
            try
            {
                var danhSachChiTietEntity = _danhSachChiTietRepository.GetById(danhSachChiTietModel.DanhSachChiTietId);
				if (danhSachChiTietEntity != null)
				{
					danhSachChiTietEntity = danhSachChiTietModel.MapToEntity(danhSachChiTietEntity);
					//danhSachChiTietEntity.MapToModel(danhSachChiTietModel);

					_danhSachChiTietRepository.Update(danhSachChiTietEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update DanhSachChiTiet error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateDanhSachChiTiet(DanhSachChiTietModel model)
        {
            try
            {
                var createdDanhSachChiTiet = _danhSachChiTietRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdDanhSachChiTiet == null)
                {
                    Log.Error("Create danhSachChiTiet error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create DanhSachChiTiet error", ex);
                return false;
            }

        }

        public bool Delete(int danhSachChiTietId, out string message)
        {
            

            try
            {
                var entity = _danhSachChiTietRepository.GetById(danhSachChiTietId);
				if (entity != null)
				{
					_danhSachChiTietRepository.Delete(danhSachChiTietId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DanhSachChiTiet error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<DanhSachChiTietModel> GetAllDanhSachChiTiets()
        {
            //Igrone danhSachChiTiet system
            return _danhSachChiTietRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int danhSachChiTietId, out string message)
        {
            try
            {
                var entity = _danhSachChiTietRepository.Query(c => c.DanhSachChiTietId == danhSachChiTietId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_danhSachChiTietRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DanhSachChiTiet error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }

        public List<SelectListItem> GetListChiTietVatTu()
        {
            var listChiTietVatTu = _chiTietVatTuRepository.GetAll().Select(x => new SelectListItem()
            {
                Value = x.ChiTietVatTuId.ToString(),
                Text = x.Title
            })
            .ToList();

            return listChiTietVatTu;
        }
    }
}
