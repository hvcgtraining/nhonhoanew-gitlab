﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models;
using EcommerceSystem.Models.PKHKeHoachSXNKXHDetailModel;
using EcommerceSystem.Models.PKHKeHoachXuongDetailModel;
using EcommerceSystem.Models.PKHKeHoachXuongModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace EcommerceSystem.Services
{
    public interface IPKHKeHoachSXNKXHDetailService : IEntityService<PKHKeHoachSXNKXHDetail>
    {
        List<PKHKeHoachSXNKXHDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        List<PKHKeHoachSXNKXHDetailModel> GetAllByParentId(long keHoachId);
        bool UpdatePKHKeHoachSXNKXHDetail(PKHKeHoachSXNKXHDetailModel PKHKeHoachSXNKXHDetailModel, out string message);
        bool UpdateSoLuongPKHKeHoachSX(PKHKeHoachSXNKXHDetailModel PKHKeHoachSXNKXHDetailModel, out string message);
        bool Delete(long pKHKeHoachSXNKXHDetailId, out string message);
        List<PKHKeHoachSXNKXHDetailModel> GetAllPKHKeHoachSXNKXHDetails();
        bool CreatePKHKeHoachSXNKXHDetail(PKHKeHoachSXNKXHDetailModel model, out long keHoachSXNKXHDetailId, out string message);
		bool ChangeStatus(int pKHKeHoachSXNKXHDetailId, out string message);
        void UpdateKHSXXuong(int parentVatTuId, long pKHKeHoachSXNKXHId, long pKHKeHoachSXNKXHDetailId, int tongNhapKho, LoaiXuong loaiXuong, string currentUser);
    }
    public class PKHKeHoachSXNKXHDetailService : EntityService<PKHKeHoachSXNKXHDetail>, IPKHKeHoachSXNKXHDetailService
    {
        private readonly IPKHKeHoachSXNKXHDetailRepository _pKHKeHoachSXNKXHDetailRepository;
        private readonly IDinhMucSanXuatRepository _dinhMucSanXuatRepository;
        private readonly IDanhSachChiTietRepository _danhSachChiTietRepository;
        private readonly IThanhPhamXuongRepository _thanhPhamXuongRepository;
        private readonly IPKHKeHoachXuongRepository _pKHKeHoachXuongRepository;
        private readonly IPKHKeHoachXuongDetailRepository _pKHKeHoachXuongDetailRepository;
        private readonly ICoPhanCapXuongRepository _coPhanCapXuongRepository;
        private readonly IXuongRepository _xuongRepository;
        public PKHKeHoachSXNKXHDetailService(IUnitOfWork unitOfWork, IPKHKeHoachSXNKXHDetailRepository pKHKeHoachSXNKXHDetailRepository,
            IDinhMucSanXuatRepository dinhMucSanXuatRepository, IDanhSachChiTietRepository danhSachChiTietRepository, IThanhPhamXuongRepository thanhPhamXuongRepository,
            IPKHKeHoachXuongRepository pKHKeHoachXuongRepository, IPKHKeHoachXuongDetailRepository pKHKeHoachXuongDetailRepository, ICoPhanCapXuongRepository coPhanCapXuongRepository, IXuongRepository xuongRepository)
            : base(unitOfWork, pKHKeHoachSXNKXHDetailRepository)
        {
            _pKHKeHoachSXNKXHDetailRepository = pKHKeHoachSXNKXHDetailRepository;
            _dinhMucSanXuatRepository = dinhMucSanXuatRepository;
            _danhSachChiTietRepository = danhSachChiTietRepository;
            _thanhPhamXuongRepository = thanhPhamXuongRepository;
            _pKHKeHoachXuongRepository = pKHKeHoachXuongRepository;
            _pKHKeHoachXuongDetailRepository = pKHKeHoachXuongDetailRepository;
            _coPhanCapXuongRepository = coPhanCapXuongRepository;
            _xuongRepository = xuongRepository;
        }

        public List<PKHKeHoachSXNKXHDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHKeHoachSXNKXHDetailEntities = _pKHKeHoachSXNKXHDetailRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHKeHoachSXNKXHDetailEntities != null)
				{
					return pKHKeHoachSXNKXHDetailEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHKeHoachSXNKXHDetail error", ex);
            }
            totalPage = 0;
            return null;
        }

        public List<PKHKeHoachSXNKXHDetailModel> GetAllByParentId(long keHoachId)
        {
            try
            {
                var pKHKeHoachSXNKXHDetailEntities = _pKHKeHoachSXNKXHDetailRepository.GetAllByParentId(keHoachId);
                if (pKHKeHoachSXNKXHDetailEntities != null)
                {
                    return pKHKeHoachSXNKXHDetailEntities.MapToModels();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHKeHoachSXNKXHDetail error", ex);
            }
            return null;
        }

        public bool UpdateSoLuongPKHKeHoachSX(PKHKeHoachSXNKXHDetailModel pKHKeHoachSXNKXHDetailModel, out string message)
        {
            try
            {
                var pKHKeHoachSXNKXHDetailEntity = _pKHKeHoachSXNKXHDetailRepository.GetById(pKHKeHoachSXNKXHDetailModel.PKHKeHoachSXNKXHDetailId);
                if (pKHKeHoachSXNKXHDetailEntity != null)
                {
                    pKHKeHoachSXNKXHDetailEntity.SoLuongCan = pKHKeHoachSXNKXHDetailModel.SoLuongCan;
                    pKHKeHoachSXNKXHDetailEntity.SoLuongBaoBi = pKHKeHoachSXNKXHDetailModel.SoLuongBaoBi;
                    pKHKeHoachSXNKXHDetailEntity.TongNhapKho = pKHKeHoachSXNKXHDetailModel.SoLuongCan;
                    pKHKeHoachSXNKXHDetailEntity.Xuong4 = pKHKeHoachSXNKXHDetailEntity.Xuong4.HasValue ? pKHKeHoachSXNKXHDetailModel.SoLuongCan : pKHKeHoachSXNKXHDetailEntity.Xuong4;
                    pKHKeHoachSXNKXHDetailEntity.Xuong5 = pKHKeHoachSXNKXHDetailEntity.Xuong5.HasValue ? pKHKeHoachSXNKXHDetailModel.SoLuongCan : pKHKeHoachSXNKXHDetailEntity.Xuong5;
                    pKHKeHoachSXNKXHDetailEntity.Xuong6 = pKHKeHoachSXNKXHDetailEntity.Xuong6.HasValue ? pKHKeHoachSXNKXHDetailModel.SoLuongCan : pKHKeHoachSXNKXHDetailEntity.Xuong6;
                    _pKHKeHoachSXNKXHDetailRepository.Update(pKHKeHoachSXNKXHDetailEntity);
                    UnitOfWork.SaveChanges();

                    message = "Cập nhật thành công";
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHKeHoachSXNKXHDetail error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool UpdatePKHKeHoachSXNKXHDetail(PKHKeHoachSXNKXHDetailModel pKHKeHoachSXNKXHDetailModel, out string message)
        {
            try
            {
                var pKHKeHoachSXNKXHDetailEntity = _pKHKeHoachSXNKXHDetailRepository.GetById(pKHKeHoachSXNKXHDetailModel.PKHKeHoachSXNKXHDetailId);
				if (pKHKeHoachSXNKXHDetailEntity != null)
				{
					pKHKeHoachSXNKXHDetailEntity = pKHKeHoachSXNKXHDetailModel.MapToEntity(pKHKeHoachSXNKXHDetailEntity);
					pKHKeHoachSXNKXHDetailEntity.MapToModel(pKHKeHoachSXNKXHDetailModel);

					_pKHKeHoachSXNKXHDetailRepository.Update(pKHKeHoachSXNKXHDetailEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHKeHoachSXNKXHDetail error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHKeHoachSXNKXHDetail(PKHKeHoachSXNKXHDetailModel model, out long keHoachSXNKXHDetailId, out string message)
        {
            keHoachSXNKXHDetailId = 0;
            message = "";
            try
            {
                var checkExistedKHDetail = _pKHKeHoachSXNKXHDetailRepository.GetAll().FirstOrDefault(k=>k.PKHKeHoachSXNKXHId == model.PKHKeHoachSXNKXHId && k.VatTuId == model.VatTuId);
                if (checkExistedKHDetail != null)
                {
                    message = "Vật tư đã tồn tại.";
                    return false;
                }
                    
                var createdPKHKeHoachSXNKXHDetail = _pKHKeHoachSXNKXHDetailRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHKeHoachSXNKXHDetail == null)
                {
                    Log.Error("Create pKHKeHoachSXNKXHDetail error");
                    return false;
                }
                keHoachSXNKXHDetailId = createdPKHKeHoachSXNKXHDetail.PKHKeHoachSXNKXHDetailId;
                //Thread myNewThread = new Thread(() => UpdateKHSXXuong(model.VatTuId, model.PKHKeHoachSXNKXHId, createdPKHKeHoachSXNKXHDetail.PKHKeHoachSXNKXHDetailId, LoaiXuong.XuongLapRap));
                //myNewThread.Start();

                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHKeHoachSXNKXHDetail error", ex);
                return false;
            }
        }

        public void UpdateKHSXXuong(int parentVatTuId, long pKHKeHoachSXNKXHId, long pKHKeHoachSXNKXHDetailId, int tongNhapKho, LoaiXuong loaiXuong, string currentUser)
        {
            //update Xuong, Xuong detail
            //1. Get dinh muc SX (VatTuId) --> DinhMucId --> DetailDinhMucIds --> ChiTietVatTuIds, Vatus --> ThanhPhamXuong, Xuong, CoPhanCapXuong(Kho)
            try
            {
                _pKHKeHoachSXNKXHDetailRepository.UpdateKHSXXuong(parentVatTuId, pKHKeHoachSXNKXHId, pKHKeHoachSXNKXHDetailId, tongNhapKho, (int)loaiXuong, currentUser);
            }
            catch (Exception ex)
            {
                Log.Error("UpdateKHSXXuong error", ex);
            }
        } 

        public bool Delete(long pKHKeHoachSXNKXHDetailId, out string message)
        {
            try
            {
                var entity = _pKHKeHoachSXNKXHDetailRepository.GetById(pKHKeHoachSXNKXHDetailId);
				if (entity != null)
				{
					_pKHKeHoachSXNKXHDetailRepository.Delete(pKHKeHoachSXNKXHDetailId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachSXNKXHDetail error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHKeHoachSXNKXHDetailModel> GetAllPKHKeHoachSXNKXHDetails()
        {
            //Igrone pKHKeHoachSXNKXHDetail system
            return _pKHKeHoachSXNKXHDetailRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHKeHoachSXNKXHDetailId, out string message)
        {
            try
            {
                var entity = _pKHKeHoachSXNKXHDetailRepository.Query(c => c.PKHKeHoachSXNKXHDetailId == pKHKeHoachSXNKXHDetailId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHKeHoachSXNKXHDetailRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachSXNKXHDetail error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
