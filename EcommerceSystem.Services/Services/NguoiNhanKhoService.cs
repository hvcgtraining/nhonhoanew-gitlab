﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.NguoiNhanKhoModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface INguoiNhanKhoService : IEntityService<NguoiNhanKho>
    {
        List<NguoiNhanKhoModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateNguoiNhanKho(NguoiNhanKhoModel NguoiNhanKhoModel, out string message);
        bool Delete(int nguoiNhanKhoId, out string message);
        List<NguoiNhanKhoModel> GetAllNguoiNhanKhos();
        bool CreateNguoiNhanKho(NguoiNhanKhoModel model);
		bool ChangeStatus(int nguoiNhanKhoId, out string message);

    }
    public class NguoiNhanKhoService : EntityService<NguoiNhanKho>, INguoiNhanKhoService
    {
        private readonly INguoiNhanKhoRepository _nguoiNhanKhoRepository;
       
        public NguoiNhanKhoService(IUnitOfWork unitOfWork, INguoiNhanKhoRepository nguoiNhanKhoRepository)
            : base(unitOfWork, nguoiNhanKhoRepository)
        {
            _nguoiNhanKhoRepository = nguoiNhanKhoRepository;
        }

        public List<NguoiNhanKhoModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var nguoiNhanKhoEntities = _nguoiNhanKhoRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (nguoiNhanKhoEntities != null)
				{
					return nguoiNhanKhoEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search NguoiNhanKho error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateNguoiNhanKho(NguoiNhanKhoModel nguoiNhanKhoModel, out string message)
        {
            try
            {
                var nguoiNhanKhoEntity = _nguoiNhanKhoRepository.GetById(nguoiNhanKhoModel.NguoiNhanKhoId);
				if (nguoiNhanKhoEntity != null)
				{
					nguoiNhanKhoEntity = nguoiNhanKhoModel.MapToEntity(nguoiNhanKhoEntity);
					nguoiNhanKhoEntity.MapToModel(nguoiNhanKhoModel);

					_nguoiNhanKhoRepository.Update(nguoiNhanKhoEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update NguoiNhanKho error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateNguoiNhanKho(NguoiNhanKhoModel model)
        {
            try
            {
                var createdNguoiNhanKho = _nguoiNhanKhoRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdNguoiNhanKho == null)
                {
                    Log.Error("Create nguoiNhanKho error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create NguoiNhanKho error", ex);
                return false;
            }

        }

        public bool Delete(int nguoiNhanKhoId, out string message)
        {
            

            try
            {
                var entity = _nguoiNhanKhoRepository.GetById(nguoiNhanKhoId);
				if (entity != null)
				{
					_nguoiNhanKhoRepository.Delete(nguoiNhanKhoId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete NguoiNhanKho error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<NguoiNhanKhoModel> GetAllNguoiNhanKhos()
        {

            //Igrone nguoiNhanKho system
           
            return _nguoiNhanKhoRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int nguoiNhanKhoId, out string message)
        {
            try
            {
                var entity = _nguoiNhanKhoRepository.Query(c => c.NguoiNhanKhoId == nguoiNhanKhoId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_nguoiNhanKhoRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete NguoiNhanKho error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
