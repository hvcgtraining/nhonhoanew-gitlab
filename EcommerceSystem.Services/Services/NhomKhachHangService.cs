﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.NhomKhachHangModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface INhomKhachHangService : IEntityService<NhomKhachHang>
    {
        List<NhomKhachHangModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateNhomKhachHang(NhomKhachHangModel NhomKhachHangModel, out string message);
        bool Delete(int nhomKhachHangId, out string message);
        List<NhomKhachHangModel> GetAllNhomKhachHangs();
        bool CreateNhomKhachHang(NhomKhachHangModel model);
		bool ChangeStatus(int nhomKhachHangId, out string message);

    }
    public class NhomKhachHangService : EntityService<NhomKhachHang>, INhomKhachHangService
    {
        private readonly INhomKhachHangRepository _nhomKhachHangRepository;
        public NhomKhachHangService(IUnitOfWork unitOfWork, INhomKhachHangRepository nhomKhachHangRepository)
            : base(unitOfWork, nhomKhachHangRepository)
        {
            _nhomKhachHangRepository = nhomKhachHangRepository;
        }

        public List<NhomKhachHangModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var nhomKhachHangEntities = _nhomKhachHangRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (nhomKhachHangEntities != null)
				{
					return nhomKhachHangEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search NhomKhachHang error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateNhomKhachHang(NhomKhachHangModel nhomKhachHangModel, out string message)
        {
            try
            {
                var nhomKhachHangEntity = _nhomKhachHangRepository.GetById(nhomKhachHangModel.NhomKhachHangId);
				if (nhomKhachHangEntity != null)
				{
					nhomKhachHangEntity = nhomKhachHangModel.MapToEntity(nhomKhachHangEntity);
					nhomKhachHangEntity.MapToModel(nhomKhachHangModel);

					_nhomKhachHangRepository.Update(nhomKhachHangEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update NhomKhachHang error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateNhomKhachHang(NhomKhachHangModel model)
        {
            try
            {
                var createdNhomKhachHang = _nhomKhachHangRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdNhomKhachHang == null)
                {
                    Log.Error("Create nhomKhachHang error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create NhomKhachHang error", ex);
                return false;
            }

        }

        public bool Delete(int nhomKhachHangId, out string message)
        {
            

            try
            {
                var entity = _nhomKhachHangRepository.GetById(nhomKhachHangId);
				if (entity != null)
				{
					_nhomKhachHangRepository.Delete(nhomKhachHangId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete NhomKhachHang error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<NhomKhachHangModel> GetAllNhomKhachHangs()
        {
            //Igrone nhomKhachHang system
            return _nhomKhachHangRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int nhomKhachHangId, out string message)
        {
            try
            {
                var entity = _nhomKhachHangRepository.Query(c => c.NhomKhachHangId == nhomKhachHangId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_nhomKhachHangRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete NhomKhachHang error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
