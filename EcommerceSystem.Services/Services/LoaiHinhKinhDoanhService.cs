﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.LoaiHinhKinhDoanhModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface ILoaiHinhKinhDoanhService : IEntityService<LoaiHinhKinhDoanh>
    {
        List<LoaiHinhKinhDoanhModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateLoaiHinhKinhDoanh(LoaiHinhKinhDoanhModel LoaiHinhKinhDoanhModel, out string message);
        bool Delete(int loaiHinhKinhDoanhId, out string message);
        List<LoaiHinhKinhDoanhModel> GetAllLoaiHinhKinhDoanhs();
        bool CreateLoaiHinhKinhDoanh(LoaiHinhKinhDoanhModel model);
		bool ChangeStatus(int loaiHinhKinhDoanhId, out string message);

    }
    public class LoaiHinhKinhDoanhService : EntityService<LoaiHinhKinhDoanh>, ILoaiHinhKinhDoanhService
    {
        private readonly ILoaiHinhKinhDoanhRepository _loaiHinhKinhDoanhRepository;
        public LoaiHinhKinhDoanhService(IUnitOfWork unitOfWork, ILoaiHinhKinhDoanhRepository loaiHinhKinhDoanhRepository)
            : base(unitOfWork, loaiHinhKinhDoanhRepository)
        {
            _loaiHinhKinhDoanhRepository = loaiHinhKinhDoanhRepository;
        }

        public List<LoaiHinhKinhDoanhModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var loaiHinhKinhDoanhEntities = _loaiHinhKinhDoanhRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (loaiHinhKinhDoanhEntities != null)
				{
					return loaiHinhKinhDoanhEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search LoaiHinhKinhDoanh error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateLoaiHinhKinhDoanh(LoaiHinhKinhDoanhModel loaiHinhKinhDoanhModel, out string message)
        {
            try
            {
                var loaiHinhKinhDoanhEntity = _loaiHinhKinhDoanhRepository.GetById(loaiHinhKinhDoanhModel.LoaiHinhKinhDoanhId);
				if (loaiHinhKinhDoanhEntity != null)
				{
					loaiHinhKinhDoanhEntity = loaiHinhKinhDoanhModel.MapToEntity(loaiHinhKinhDoanhEntity);
					loaiHinhKinhDoanhEntity.MapToModel(loaiHinhKinhDoanhModel);

					_loaiHinhKinhDoanhRepository.Update(loaiHinhKinhDoanhEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update LoaiHinhKinhDoanh error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateLoaiHinhKinhDoanh(LoaiHinhKinhDoanhModel model)
        {
            try
            {
                var createdLoaiHinhKinhDoanh = _loaiHinhKinhDoanhRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdLoaiHinhKinhDoanh == null)
                {
                    Log.Error("Create loaiHinhKinhDoanh error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create LoaiHinhKinhDoanh error", ex);
                return false;
            }

        }

        public bool Delete(int loaiHinhKinhDoanhId, out string message)
        {
            

            try
            {
                var entity = _loaiHinhKinhDoanhRepository.GetById(loaiHinhKinhDoanhId);
				if (entity != null)
				{
					_loaiHinhKinhDoanhRepository.Delete(loaiHinhKinhDoanhId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete LoaiHinhKinhDoanh error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<LoaiHinhKinhDoanhModel> GetAllLoaiHinhKinhDoanhs()
        {
            //Igrone loaiHinhKinhDoanh system
            return _loaiHinhKinhDoanhRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int loaiHinhKinhDoanhId, out string message)
        {
            try
            {
                var entity = _loaiHinhKinhDoanhRepository.Query(c => c.LoaiHinhKinhDoanhId == loaiHinhKinhDoanhId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_loaiHinhKinhDoanhRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete LoaiHinhKinhDoanh error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
