﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.KhoTinhTonModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IKhoTinhTonService : IEntityService<KhoTinhTon>
    {
        List<KhoTinhTonModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateKhoTinhTon(KhoTinhTonModel KhoTinhTonModel, out string message);
        bool Delete(int khoTinhTonId, out string message);
        List<KhoTinhTonModel> GetAllKhoTinhTons();
        bool CreateKhoTinhTon(KhoTinhTonModel model);
		bool ChangeStatus(int khoTinhTonId, out string message);

    }
    public class KhoTinhTonService : EntityService<KhoTinhTon>, IKhoTinhTonService
    {
        private readonly IKhoTinhTonRepository _khoTinhTonRepository;
        public KhoTinhTonService(IUnitOfWork unitOfWork, IKhoTinhTonRepository khoTinhTonRepository)
            : base(unitOfWork, khoTinhTonRepository)
        {
            _khoTinhTonRepository = khoTinhTonRepository;
        }

        public List<KhoTinhTonModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var khoTinhTonEntities = _khoTinhTonRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (khoTinhTonEntities != null)
				{
					return khoTinhTonEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search KhoTinhTon error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateKhoTinhTon(KhoTinhTonModel khoTinhTonModel, out string message)
        {
            try
            {
                var khoTinhTonEntity = _khoTinhTonRepository.GetById(khoTinhTonModel.KhoTinhTonId);
				if (khoTinhTonEntity != null)
				{
					khoTinhTonEntity = khoTinhTonModel.MapToEntity(khoTinhTonEntity);
					khoTinhTonEntity.MapToModel(khoTinhTonModel);

					_khoTinhTonRepository.Update(khoTinhTonEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update KhoTinhTon error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateKhoTinhTon(KhoTinhTonModel model)
        {
            try
            {
                var createdKhoTinhTon = _khoTinhTonRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdKhoTinhTon == null)
                {
                    Log.Error("Create khoTinhTon error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create KhoTinhTon error", ex);
                return false;
            }

        }

        public bool Delete(int khoTinhTonId, out string message)
        {
            

            try
            {
                var entity = _khoTinhTonRepository.GetById(khoTinhTonId);
				if (entity != null)
				{
					_khoTinhTonRepository.Delete(khoTinhTonId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete KhoTinhTon error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<KhoTinhTonModel> GetAllKhoTinhTons()
        {
            //Igrone khoTinhTon system
            return _khoTinhTonRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int khoTinhTonId, out string message)
        {
            try
            {
                var entity = _khoTinhTonRepository.Query(c => c.KhoTinhTonId == khoTinhTonId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_khoTinhTonRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete KhoTinhTon error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
