﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.DoiMaChiTietModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IDoiMaChiTietService : IEntityService<DoiMaChiTiet>
    {
        List<DoiMaChiTietModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateDoiMaChiTiet(DoiMaChiTietModel DoiMaChiTietModel, out string message);
        bool Delete(int doiMaChiTietId, out string message);
        List<DoiMaChiTietModel> GetAllDoiMaChiTiets();
        bool CreateDoiMaChiTiet(DoiMaChiTietModel model);
		bool ChangeStatus(int doiMaChiTietId, out string message);

    }
    public class DoiMaChiTietService : EntityService<DoiMaChiTiet>, IDoiMaChiTietService
    {
        private readonly IDoiMaChiTietRepository _doiMaChiTietRepository;
        public DoiMaChiTietService(IUnitOfWork unitOfWork, IDoiMaChiTietRepository doiMaChiTietRepository)
            : base(unitOfWork, doiMaChiTietRepository)
        {
            _doiMaChiTietRepository = doiMaChiTietRepository;
        }

        public List<DoiMaChiTietModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var doiMaChiTietEntities = _doiMaChiTietRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (doiMaChiTietEntities != null)
				{
					return doiMaChiTietEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search DoiMaChiTiet error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateDoiMaChiTiet(DoiMaChiTietModel doiMaChiTietModel, out string message)
        {
            try
            {
                var doiMaChiTietEntity = _doiMaChiTietRepository.GetById(doiMaChiTietModel.DoiMaChiTietId);
				if (doiMaChiTietEntity != null)
				{
					doiMaChiTietEntity = doiMaChiTietModel.MapToEntity(doiMaChiTietEntity);
					doiMaChiTietEntity.MapToModel(doiMaChiTietModel);

					_doiMaChiTietRepository.Update(doiMaChiTietEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update DoiMaChiTiet error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateDoiMaChiTiet(DoiMaChiTietModel model)
        {
            try
            {
                var createdDoiMaChiTiet = _doiMaChiTietRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdDoiMaChiTiet == null)
                {
                    Log.Error("Create doiMaChiTiet error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create DoiMaChiTiet error", ex);
                return false;
            }

        }

        public bool Delete(int doiMaChiTietId, out string message)
        {
            

            try
            {
                var entity = _doiMaChiTietRepository.GetById(doiMaChiTietId);
				if (entity != null)
				{
					_doiMaChiTietRepository.Delete(doiMaChiTietId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DoiMaChiTiet error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<DoiMaChiTietModel> GetAllDoiMaChiTiets()
        {
            //Igrone doiMaChiTiet system
            return _doiMaChiTietRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int doiMaChiTietId, out string message)
        {
            try
            {
                var entity = _doiMaChiTietRepository.Query(c => c.DoiMaChiTietId == doiMaChiTietId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_doiMaChiTietRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DoiMaChiTiet error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
