﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.VatTuCapRiengModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IVatTuCapRiengService : IEntityService<VatTuCapRieng>
    {
        List<VatTuCapRiengModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateVatTuCapRieng(VatTuCapRiengModel VatTuCapRiengModel, out string message);
        bool Delete(int vatTuCapRiengId, out string message);
        List<VatTuCapRiengModel> GetAllVatTuCapRiengs();
        bool CreateVatTuCapRieng(VatTuCapRiengModel model);
		bool ChangeStatus(int vatTuCapRiengId, out string message);

    }
    public class VatTuCapRiengService : EntityService<VatTuCapRieng>, IVatTuCapRiengService
    {
        private readonly IVatTuCapRiengRepository _vatTuCapRiengRepository;
        public VatTuCapRiengService(IUnitOfWork unitOfWork, IVatTuCapRiengRepository vatTuCapRiengRepository)
            : base(unitOfWork, vatTuCapRiengRepository)
        {
            _vatTuCapRiengRepository = vatTuCapRiengRepository;
        }

        public List<VatTuCapRiengModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var vatTuCapRiengEntities = _vatTuCapRiengRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (vatTuCapRiengEntities != null)
				{
					return vatTuCapRiengEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search VatTuCapRieng error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateVatTuCapRieng(VatTuCapRiengModel vatTuCapRiengModel, out string message)
        {
            try
            {
                var vatTuCapRiengEntity = _vatTuCapRiengRepository.GetById(vatTuCapRiengModel.VatTuCapRiengId);
				if (vatTuCapRiengEntity != null)
				{
					vatTuCapRiengEntity = vatTuCapRiengModel.MapToEntity(vatTuCapRiengEntity);
					vatTuCapRiengEntity.MapToModel(vatTuCapRiengModel);

					_vatTuCapRiengRepository.Update(vatTuCapRiengEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update VatTuCapRieng error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateVatTuCapRieng(VatTuCapRiengModel model)
        {
            try
            {
                var createdVatTuCapRieng = _vatTuCapRiengRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdVatTuCapRieng == null)
                {
                    Log.Error("Create vatTuCapRieng error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create VatTuCapRieng error", ex);
                return false;
            }

        }

        public bool Delete(int vatTuCapRiengId, out string message)
        {
            

            try
            {
                var entity = _vatTuCapRiengRepository.GetById(vatTuCapRiengId);
				if (entity != null)
				{
					_vatTuCapRiengRepository.Delete(vatTuCapRiengId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete VatTuCapRieng error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<VatTuCapRiengModel> GetAllVatTuCapRiengs()
        {
            //Igrone vatTuCapRieng system
            return _vatTuCapRiengRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int vatTuCapRiengId, out string message)
        {
            try
            {
                var entity = _vatTuCapRiengRepository.Query(c => c.VatTuCapRiengId == vatTuCapRiengId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_vatTuCapRiengRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete VatTuCapRieng error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
