﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHKeHoachGiaoHangDetailModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHKeHoachGiaoHangDetailService : IEntityService<PKHKeHoachGiaoHangDetail>
    {
        List<PKHKeHoachGiaoHangDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHKeHoachGiaoHangDetail(PKHKeHoachGiaoHangDetailModel PKHKeHoachGiaoHangDetailModel, out string message);
        bool Delete(int pKHKeHoachGiaoHangDetailId, out string message);
        List<PKHKeHoachGiaoHangDetailModel> GetAllPKHKeHoachGiaoHangDetails();
        bool CreatePKHKeHoachGiaoHangDetail(PKHKeHoachGiaoHangDetailModel model);
		bool ChangeStatus(int pKHKeHoachGiaoHangDetailId, out string message);

    }
    public class PKHKeHoachGiaoHangDetailService : EntityService<PKHKeHoachGiaoHangDetail>, IPKHKeHoachGiaoHangDetailService
    {
        private readonly IPKHKeHoachGiaoHangDetailRepository _pKHKeHoachGiaoHangDetailRepository;
        public PKHKeHoachGiaoHangDetailService(IUnitOfWork unitOfWork, IPKHKeHoachGiaoHangDetailRepository pKHKeHoachGiaoHangDetailRepository)
            : base(unitOfWork, pKHKeHoachGiaoHangDetailRepository)
        {
            _pKHKeHoachGiaoHangDetailRepository = pKHKeHoachGiaoHangDetailRepository;
        }

        public List<PKHKeHoachGiaoHangDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHKeHoachGiaoHangDetailEntities = _pKHKeHoachGiaoHangDetailRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHKeHoachGiaoHangDetailEntities != null)
				{
					return pKHKeHoachGiaoHangDetailEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHKeHoachGiaoHangDetail error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHKeHoachGiaoHangDetail(PKHKeHoachGiaoHangDetailModel pKHKeHoachGiaoHangDetailModel, out string message)
        {
            try
            {
                var pKHKeHoachGiaoHangDetailEntity = _pKHKeHoachGiaoHangDetailRepository.GetById(pKHKeHoachGiaoHangDetailModel.PKHKeHoachGiaoHangDetailId);
				if (pKHKeHoachGiaoHangDetailEntity != null)
				{
					pKHKeHoachGiaoHangDetailEntity = pKHKeHoachGiaoHangDetailModel.MapToEntity(pKHKeHoachGiaoHangDetailEntity);
					pKHKeHoachGiaoHangDetailEntity.MapToModel(pKHKeHoachGiaoHangDetailModel);

					_pKHKeHoachGiaoHangDetailRepository.Update(pKHKeHoachGiaoHangDetailEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHKeHoachGiaoHangDetail error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHKeHoachGiaoHangDetail(PKHKeHoachGiaoHangDetailModel model)
        {
            try
            {
                var createdPKHKeHoachGiaoHangDetail = _pKHKeHoachGiaoHangDetailRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHKeHoachGiaoHangDetail == null)
                {
                    Log.Error("Create pKHKeHoachGiaoHangDetail error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHKeHoachGiaoHangDetail error", ex);
                return false;
            }

        }

        public bool Delete(int pKHKeHoachGiaoHangDetailId, out string message)
        {
            

            try
            {
                var entity = _pKHKeHoachGiaoHangDetailRepository.GetById(pKHKeHoachGiaoHangDetailId);
				if (entity != null)
				{
					_pKHKeHoachGiaoHangDetailRepository.Delete(pKHKeHoachGiaoHangDetailId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachGiaoHangDetail error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHKeHoachGiaoHangDetailModel> GetAllPKHKeHoachGiaoHangDetails()
        {
            //Igrone pKHKeHoachGiaoHangDetail system
            return _pKHKeHoachGiaoHangDetailRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHKeHoachGiaoHangDetailId, out string message)
        {
            try
            {
                var entity = _pKHKeHoachGiaoHangDetailRepository.Query(c => c.PKHKeHoachGiaoHangDetailId == pKHKeHoachGiaoHangDetailId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHKeHoachGiaoHangDetailRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachGiaoHangDetail error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
