﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHDuKienGiaoHangModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHDuKienGiaoHangService : IEntityService<PKHDuKienGiaoHang>
    {
        List<PKHDuKienGiaoHangModel> Search(int currentPage, int pageSize, string textSearch, int PKHPhieuYeuCauMuaHangId, int PKHPhieuYeuCauMuaHangDetailId, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHDuKienGiaoHang(PKHDuKienGiaoHangModel PKHDuKienGiaoHangModel, out string message);
        bool Delete(int pKHDuKienGiaoHangId, out string message);
        List<PKHDuKienGiaoHangModel> GetAllPKHDuKienGiaoHangs();
        bool CreatePKHDuKienGiaoHang(PKHDuKienGiaoHangModel model);
		bool ChangeStatus(int pKHDuKienGiaoHangId, out string message);

    }
    public class PKHDuKienGiaoHangService : EntityService<PKHDuKienGiaoHang>, IPKHDuKienGiaoHangService
    {
        private readonly IPKHDuKienGiaoHangRepository _pKHDuKienGiaoHangRepository;
        public PKHDuKienGiaoHangService(IUnitOfWork unitOfWork, IPKHDuKienGiaoHangRepository pKHDuKienGiaoHangRepository)
            : base(unitOfWork, pKHDuKienGiaoHangRepository)
        {
            _pKHDuKienGiaoHangRepository = pKHDuKienGiaoHangRepository;
        }

        public List<PKHDuKienGiaoHangModel> Search(int currentPage, int pageSize, string textSearch, int PKHPhieuYeuCauMuaHangId, int PKHPhieuYeuCauMuaHangDetailId, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHDuKienGiaoHangEntities = _pKHDuKienGiaoHangRepository.Search(currentPage, pageSize, textSearch, PKHPhieuYeuCauMuaHangId, PKHPhieuYeuCauMuaHangDetailId, sortColumn, sortDirection, out totalPage);
				if (pKHDuKienGiaoHangEntities != null)
				{
					return pKHDuKienGiaoHangEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHDuKienGiaoHang error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHDuKienGiaoHang(PKHDuKienGiaoHangModel pKHDuKienGiaoHangModel, out string message)
        {
            try
            {
                var pKHDuKienGiaoHangEntity = _pKHDuKienGiaoHangRepository.GetById(pKHDuKienGiaoHangModel.PKHDuKienGiaoHangId);
				if (pKHDuKienGiaoHangEntity != null)
				{
					pKHDuKienGiaoHangEntity = pKHDuKienGiaoHangModel.MapToEntity(pKHDuKienGiaoHangEntity);
					pKHDuKienGiaoHangEntity.MapToModel(pKHDuKienGiaoHangModel);

					_pKHDuKienGiaoHangRepository.Update(pKHDuKienGiaoHangEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHDuKienGiaoHang error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHDuKienGiaoHang(PKHDuKienGiaoHangModel model)
        {
            try
            {
                var createdPKHDuKienGiaoHang = _pKHDuKienGiaoHangRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHDuKienGiaoHang == null)
                {
                    Log.Error("Create pKHDuKienGiaoHang error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHDuKienGiaoHang error", ex);
                return false;
            }

        }

        public bool Delete(int pKHDuKienGiaoHangId, out string message)
        {
            

            try
            {
                var entity = _pKHDuKienGiaoHangRepository.GetById(pKHDuKienGiaoHangId);
				if (entity != null)
				{
					_pKHDuKienGiaoHangRepository.Delete(pKHDuKienGiaoHangId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHDuKienGiaoHang error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHDuKienGiaoHangModel> GetAllPKHDuKienGiaoHangs()
        {
            //Igrone pKHDuKienGiaoHang system
            return _pKHDuKienGiaoHangRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHDuKienGiaoHangId, out string message)
        {
            try
            {
                var entity = _pKHDuKienGiaoHangRepository.Query(c => c.PKHDuKienGiaoHangId == pKHDuKienGiaoHangId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHDuKienGiaoHangRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHDuKienGiaoHang error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
