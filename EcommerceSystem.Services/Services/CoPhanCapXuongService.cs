﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.CoPhanCapXuongModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface ICoPhanCapXuongService : IEntityService<CoPhanCapXuong>
    {
        List<CoPhanCapXuongModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateCoPhanCapXuong(CoPhanCapXuongModel CoPhanCapXuongModel, out string message);
        bool Delete(int coPhanCapXuongId, out string message);
        List<CoPhanCapXuongModel> GetAllCoPhanCapXuongs();
        bool CreateCoPhanCapXuong(CoPhanCapXuongModel model);
		bool ChangeStatus(int coPhanCapXuongId, out string message);

    }
    public class CoPhanCapXuongService : EntityService<CoPhanCapXuong>, ICoPhanCapXuongService
    {
        private readonly ICoPhanCapXuongRepository _coPhanCapXuongRepository;
        public CoPhanCapXuongService(IUnitOfWork unitOfWork, ICoPhanCapXuongRepository coPhanCapXuongRepository)
            : base(unitOfWork, coPhanCapXuongRepository)
        {
            _coPhanCapXuongRepository = coPhanCapXuongRepository;
        }

        public List<CoPhanCapXuongModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var coPhanCapXuongEntities = _coPhanCapXuongRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (coPhanCapXuongEntities != null)
				{
					return coPhanCapXuongEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search CoPhanCapXuong error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateCoPhanCapXuong(CoPhanCapXuongModel coPhanCapXuongModel, out string message)
        {
            try
            {
                var coPhanCapXuongEntity = _coPhanCapXuongRepository.GetById(coPhanCapXuongModel.CoPhanCapXuongId);
				if (coPhanCapXuongEntity != null)
				{
					coPhanCapXuongEntity = coPhanCapXuongModel.MapToEntity(coPhanCapXuongEntity);
					coPhanCapXuongEntity.MapToModel(coPhanCapXuongModel);

					_coPhanCapXuongRepository.Update(coPhanCapXuongEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update CoPhanCapXuong error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateCoPhanCapXuong(CoPhanCapXuongModel model)
        {
            try
            {
                var createdCoPhanCapXuong = _coPhanCapXuongRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdCoPhanCapXuong == null)
                {
                    Log.Error("Create coPhanCapXuong error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create CoPhanCapXuong error", ex);
                return false;
            }

        }

        public bool Delete(int coPhanCapXuongId, out string message)
        {
            

            try
            {
                var entity = _coPhanCapXuongRepository.GetById(coPhanCapXuongId);
				if (entity != null)
				{
					_coPhanCapXuongRepository.Delete(coPhanCapXuongId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete CoPhanCapXuong error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<CoPhanCapXuongModel> GetAllCoPhanCapXuongs()
        {
            //Igrone coPhanCapXuong system
            return _coPhanCapXuongRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int coPhanCapXuongId, out string message)
        {
            try
            {
                var entity = _coPhanCapXuongRepository.Query(c => c.CoPhanCapXuongId == coPhanCapXuongId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_coPhanCapXuongRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete CoPhanCapXuong error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
