﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.KhoModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IKhoService : IEntityService<Kho>
    {
        List<KhoModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateKho(KhoModel KhoModel, out string message);
        bool Delete(int khoId, out string message);
        List<KhoModel> GetAllKhos();
        bool CreateKho(KhoModel model);
		bool ChangeStatus(int khoId, out string message);

    }
    public class KhoService : EntityService<Kho>, IKhoService
    {
        private readonly IKhoRepository _khoRepository;
        public KhoService(IUnitOfWork unitOfWork, IKhoRepository khoRepository)
            : base(unitOfWork, khoRepository)
        {
            _khoRepository = khoRepository;
        }

        public List<KhoModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var khoEntities = _khoRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (khoEntities != null)
				{
					return khoEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search Kho error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateKho(KhoModel khoModel, out string message)
        {
            try
            {
                var khoEntity = _khoRepository.GetById(khoModel.KhoId);
				if (khoEntity != null)
				{
					khoEntity = khoModel.MapToEntity(khoEntity);
					khoEntity.MapToModel(khoModel);

					_khoRepository.Update(khoEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update Kho error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateKho(KhoModel model)
        {
            try
            {
                var createdKho = _khoRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdKho == null)
                {
                    Log.Error("Create kho error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create Kho error", ex);
                return false;
            }

        }

        public bool Delete(int khoId, out string message)
        {
            

            try
            {
                var entity = _khoRepository.GetById(khoId);
				if (entity != null)
				{
					_khoRepository.Delete(khoId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete Kho error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<KhoModel> GetAllKhos()
        {
            //Igrone kho system
            return _khoRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int khoId, out string message)
        {
            try
            {
                var entity = _khoRepository.Query(c => c.KhoId == khoId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_khoRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete Kho error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
