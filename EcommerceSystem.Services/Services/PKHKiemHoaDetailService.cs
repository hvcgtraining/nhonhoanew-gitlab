﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHKiemHoaDetailModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHKiemHoaDetailService : IEntityService<PKHKiemHoaDetail>
    {
        List<PKHKiemHoaDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHKiemHoaDetail(PKHKiemHoaDetailModel PKHKiemHoaDetailModel, out string message);
        bool Delete(int pKHKiemHoaDetailId, out string message);
        List<PKHKiemHoaDetailModel> GetAllPKHKiemHoaDetails();
        bool CreatePKHKiemHoaDetail(PKHKiemHoaDetailModel model);
		bool ChangeStatus(int pKHKiemHoaDetailId, out string message);

    }
    public class PKHKiemHoaDetailService : EntityService<PKHKiemHoaDetail>, IPKHKiemHoaDetailService
    {
        private readonly IPKHKiemHoaDetailRepository _pKHKiemHoaDetailRepository;
        public PKHKiemHoaDetailService(IUnitOfWork unitOfWork, IPKHKiemHoaDetailRepository pKHKiemHoaDetailRepository)
            : base(unitOfWork, pKHKiemHoaDetailRepository)
        {
            _pKHKiemHoaDetailRepository = pKHKiemHoaDetailRepository;
        }

        public List<PKHKiemHoaDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHKiemHoaDetailEntities = _pKHKiemHoaDetailRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHKiemHoaDetailEntities != null)
				{
					return pKHKiemHoaDetailEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHKiemHoaDetail error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHKiemHoaDetail(PKHKiemHoaDetailModel pKHKiemHoaDetailModel, out string message)
        {
            try
            {
                var pKHKiemHoaDetailEntity = _pKHKiemHoaDetailRepository.GetById(pKHKiemHoaDetailModel.PKHKiemHoaDetailId);
				if (pKHKiemHoaDetailEntity != null)
				{
					pKHKiemHoaDetailEntity = pKHKiemHoaDetailModel.MapToEntity(pKHKiemHoaDetailEntity);
					pKHKiemHoaDetailEntity.MapToModel(pKHKiemHoaDetailModel);

					_pKHKiemHoaDetailRepository.Update(pKHKiemHoaDetailEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHKiemHoaDetail error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHKiemHoaDetail(PKHKiemHoaDetailModel model)
        {
            try
            {
                var createdPKHKiemHoaDetail = _pKHKiemHoaDetailRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHKiemHoaDetail == null)
                {
                    Log.Error("Create pKHKiemHoaDetail error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHKiemHoaDetail error", ex);
                return false;
            }

        }

        public bool Delete(int pKHKiemHoaDetailId, out string message)
        {
            

            try
            {
                var entity = _pKHKiemHoaDetailRepository.GetById(pKHKiemHoaDetailId);
				if (entity != null)
				{
					_pKHKiemHoaDetailRepository.Delete(pKHKiemHoaDetailId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKiemHoaDetail error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHKiemHoaDetailModel> GetAllPKHKiemHoaDetails()
        {
            //Igrone pKHKiemHoaDetail system
            return _pKHKiemHoaDetailRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHKiemHoaDetailId, out string message)
        {
            try
            {
                var entity = _pKHKiemHoaDetailRepository.Query(c => c.PKHKiemHoaDetailId == pKHKiemHoaDetailId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHKiemHoaDetailRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKiemHoaDetail error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
