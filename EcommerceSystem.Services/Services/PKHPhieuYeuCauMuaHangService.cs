﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHPhieuYeuCauMuaHangModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHPhieuYeuCauMuaHangService : IEntityService<PKHPhieuYeuCauMuaHang>
    {
        List<PKHPhieuYeuCauMuaHangModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHPhieuYeuCauMuaHang(PKHPhieuYeuCauMuaHangModel PKHPhieuYeuCauMuaHangModel, out string message);
        bool Delete(int pKHPhieuYeuCauMuaHangId, out string message);
        List<PKHPhieuYeuCauMuaHangModel> GetAllPKHPhieuYeuCauMuaHangs();
        long CreatePKHPhieuYeuCauMuaHang(PKHPhieuYeuCauMuaHangModel model);
		bool ChangeStatus(int pKHPhieuYeuCauMuaHangId, out string message);

    }
    public class PKHPhieuYeuCauMuaHangService : EntityService<PKHPhieuYeuCauMuaHang>, IPKHPhieuYeuCauMuaHangService
    {
        private readonly IPKHPhieuYeuCauMuaHangRepository _pKHPhieuYeuCauMuaHangRepository;
        public PKHPhieuYeuCauMuaHangService(IUnitOfWork unitOfWork, IPKHPhieuYeuCauMuaHangRepository pKHPhieuYeuCauMuaHangRepository)
            : base(unitOfWork, pKHPhieuYeuCauMuaHangRepository)
        {
            _pKHPhieuYeuCauMuaHangRepository = pKHPhieuYeuCauMuaHangRepository;
        }

        public List<PKHPhieuYeuCauMuaHangModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHPhieuYeuCauMuaHangEntities = _pKHPhieuYeuCauMuaHangRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHPhieuYeuCauMuaHangEntities != null)
				{
					return pKHPhieuYeuCauMuaHangEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHPhieuYeuCauMuaHang error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHPhieuYeuCauMuaHang(PKHPhieuYeuCauMuaHangModel pKHPhieuYeuCauMuaHangModel, out string message)
        {
            try
            {
                var pKHPhieuYeuCauMuaHangEntity = _pKHPhieuYeuCauMuaHangRepository.GetById(pKHPhieuYeuCauMuaHangModel.PKHPhieuYeuCauMuaHangId);
				if (pKHPhieuYeuCauMuaHangEntity != null)
				{
					pKHPhieuYeuCauMuaHangEntity = pKHPhieuYeuCauMuaHangModel.MapToEntity(pKHPhieuYeuCauMuaHangEntity);
					pKHPhieuYeuCauMuaHangEntity.MapToModel(pKHPhieuYeuCauMuaHangModel);

					_pKHPhieuYeuCauMuaHangRepository.Update(pKHPhieuYeuCauMuaHangEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHPhieuYeuCauMuaHang error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }
        
        public long CreatePKHPhieuYeuCauMuaHang(PKHPhieuYeuCauMuaHangModel model)
        {
            try
            {
                var createdPKHPhieuYeuCauMuaHang = _pKHPhieuYeuCauMuaHangRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHPhieuYeuCauMuaHang == null)
                {
                    Log.Error("Create pKHPhieuYeuCauMuaHang error");
                    return 0;
                }
                return createdPKHPhieuYeuCauMuaHang.PKHPhieuYeuCauMuaHangId;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHPhieuYeuCauMuaHang error", ex);
                return 0;
            }

        }

        public bool Delete(int pKHPhieuYeuCauMuaHangId, out string message)
        {
            

            try
            {
                var entity = _pKHPhieuYeuCauMuaHangRepository.GetById(pKHPhieuYeuCauMuaHangId);
				if (entity != null)
				{
					_pKHPhieuYeuCauMuaHangRepository.Delete(pKHPhieuYeuCauMuaHangId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHPhieuYeuCauMuaHang error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHPhieuYeuCauMuaHangModel> GetAllPKHPhieuYeuCauMuaHangs()
        {
            //Igrone pKHPhieuYeuCauMuaHang system
            return _pKHPhieuYeuCauMuaHangRepository.GetAll().ToList().MapToModels();
        }
        public bool ChangeStatus(int pKHPhieuYeuCauMuaHangId, out string message)
        {
            try
            {
                var entity = _pKHPhieuYeuCauMuaHangRepository.Query(c => c.PKHPhieuYeuCauMuaHangId == pKHPhieuYeuCauMuaHangId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHPhieuYeuCauMuaHangRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHPhieuYeuCauMuaHang error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
