﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.KhoiLuongCoPhanModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IKhoiLuongCoPhanService : IEntityService<KhoiLuongCoPhan>
    {
        List<KhoiLuongCoPhanModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateKhoiLuongCoPhan(KhoiLuongCoPhanModel KhoiLuongCoPhanModel, out string message);
        bool Delete(int khoiLuongCoPhanId, out string message);
        List<KhoiLuongCoPhanModel> GetAllKhoiLuongCoPhans();
        bool CreateKhoiLuongCoPhan(KhoiLuongCoPhanModel model);
		bool ChangeStatus(int khoiLuongCoPhanId, out string message);

    }
    public class KhoiLuongCoPhanService : EntityService<KhoiLuongCoPhan>, IKhoiLuongCoPhanService
    {
        private readonly IKhoiLuongCoPhanRepository _khoiLuongCoPhanRepository;
        public KhoiLuongCoPhanService(IUnitOfWork unitOfWork, IKhoiLuongCoPhanRepository khoiLuongCoPhanRepository)
            : base(unitOfWork, khoiLuongCoPhanRepository)
        {
            _khoiLuongCoPhanRepository = khoiLuongCoPhanRepository;
        }

        public List<KhoiLuongCoPhanModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var khoiLuongCoPhanEntities = _khoiLuongCoPhanRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (khoiLuongCoPhanEntities != null)
				{
					return khoiLuongCoPhanEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search KhoiLuongCoPhan error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateKhoiLuongCoPhan(KhoiLuongCoPhanModel khoiLuongCoPhanModel, out string message)
        {
            try
            {
                var khoiLuongCoPhanEntity = _khoiLuongCoPhanRepository.GetById(khoiLuongCoPhanModel.KhoiLuongCoPhanId);
				if (khoiLuongCoPhanEntity != null)
				{
					khoiLuongCoPhanEntity = khoiLuongCoPhanModel.MapToEntity(khoiLuongCoPhanEntity);
					khoiLuongCoPhanEntity.MapToModel(khoiLuongCoPhanModel);

					_khoiLuongCoPhanRepository.Update(khoiLuongCoPhanEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update KhoiLuongCoPhan error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateKhoiLuongCoPhan(KhoiLuongCoPhanModel model)
        {
            try
            {
                var createdKhoiLuongCoPhan = _khoiLuongCoPhanRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdKhoiLuongCoPhan == null)
                {
                    Log.Error("Create khoiLuongCoPhan error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create KhoiLuongCoPhan error", ex);
                return false;
            }

        }

        public bool Delete(int khoiLuongCoPhanId, out string message)
        {
            

            try
            {
                var entity = _khoiLuongCoPhanRepository.GetById(khoiLuongCoPhanId);
				if (entity != null)
				{
					_khoiLuongCoPhanRepository.Delete(khoiLuongCoPhanId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete KhoiLuongCoPhan error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<KhoiLuongCoPhanModel> GetAllKhoiLuongCoPhans()
        {
            //Igrone khoiLuongCoPhan system
            return _khoiLuongCoPhanRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int khoiLuongCoPhanId, out string message)
        {
            try
            {
                var entity = _khoiLuongCoPhanRepository.Query(c => c.KhoiLuongCoPhanId == khoiLuongCoPhanId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_khoiLuongCoPhanRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete KhoiLuongCoPhan error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
