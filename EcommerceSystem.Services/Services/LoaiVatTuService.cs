﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.LoaiVatTuModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface ILoaiVatTuService : IEntityService<LoaiVatTu>
    {
        List<LoaiVatTuModel> Search(int currentPage, int pageSize, string textSearch, int searchType, int cap, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateLoaiVatTu(LoaiVatTuModel LoaiVatTuModel, out string message);
        bool Delete(int VatTuId, out string message);
        List<LoaiVatTuModel> GetAllLoaiVatTus();
        bool CreateLoaiVatTu(LoaiVatTuModel model);
		bool ChangeStatus(int VatTuId, out string message);

    }
    public class LoaiVatTuService : EntityService<LoaiVatTu>, ILoaiVatTuService
    {
        private readonly ILoaiVatTuRepository _loaiVatTuRepository;
        public LoaiVatTuService(IUnitOfWork unitOfWork, ILoaiVatTuRepository loaiVatTuRepository)
            : base(unitOfWork, loaiVatTuRepository)
        {
            _loaiVatTuRepository = loaiVatTuRepository;
        }

        public List<LoaiVatTuModel> Search(int currentPage, int pageSize, string textSearch, int searchtype, int cap, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var loaiVatTuEntities = _loaiVatTuRepository.Search(currentPage, pageSize, textSearch, searchtype, cap, sortColumn, sortDirection, out totalPage);
				if (loaiVatTuEntities != null)
				{
					return loaiVatTuEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search LoaiVatTu error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateLoaiVatTu(LoaiVatTuModel loaiVatTuModel, out string message)
        {
            try
            {
                var loaiVatTuEntity = _loaiVatTuRepository.GetById(loaiVatTuModel.LoaiVatTuId);
				if (loaiVatTuEntity != null)
				{
					loaiVatTuEntity = loaiVatTuModel.MapToEntity(loaiVatTuEntity);
					loaiVatTuEntity.MapToModel(loaiVatTuModel);

					_loaiVatTuRepository.Update(loaiVatTuEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update LoaiVatTu error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateLoaiVatTu(LoaiVatTuModel model)
        {
            try
            {
                var createdLoaiVatTu = _loaiVatTuRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdLoaiVatTu == null)
                {
                    Log.Error("Create loaiVatTu error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create LoaiVatTu error", ex);
                return false;
            }

        }

        public bool Delete(int VatTuId, out string message)
        {
            

            try
            {
                var entity = _loaiVatTuRepository.GetById(VatTuId);
				if (entity != null)
				{
					_loaiVatTuRepository.Delete(VatTuId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete LoaiVatTu error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<LoaiVatTuModel> GetAllLoaiVatTus()
        {
            //Igrone loaiVatTu system
            return _loaiVatTuRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int VatTuId, out string message)
        {
            try
            {
                var entity = _loaiVatTuRepository.Query(c => c.LoaiVatTuId == VatTuId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_loaiVatTuRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete LoaiVatTu error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
