﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.ThanhPhamXuongModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IThanhPhamXuongService : IEntityService<ThanhPhamXuong>
    {
        List<ThanhPhamXuongModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateThanhPhamXuong(ThanhPhamXuongModel ThanhPhamXuongModel, out string message);
        bool Delete(int thanhPhamXuongId, out string message);
        List<ThanhPhamXuongModel> GetAllThanhPhamXuongs();
        bool CreateThanhPhamXuong(ThanhPhamXuongModel model);
		bool ChangeStatus(int thanhPhamXuongId, out string message);

    }
    public class ThanhPhamXuongService : EntityService<ThanhPhamXuong>, IThanhPhamXuongService
    {
        private readonly IThanhPhamXuongRepository _thanhPhamXuongRepository;
        public ThanhPhamXuongService(IUnitOfWork unitOfWork, IThanhPhamXuongRepository thanhPhamXuongRepository)
            : base(unitOfWork, thanhPhamXuongRepository)
        {
            _thanhPhamXuongRepository = thanhPhamXuongRepository;
        }

        public List<ThanhPhamXuongModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var thanhPhamXuongEntities = _thanhPhamXuongRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (thanhPhamXuongEntities != null)
				{
					return thanhPhamXuongEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search ThanhPhamXuong error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateThanhPhamXuong(ThanhPhamXuongModel thanhPhamXuongModel, out string message)
        {
            try
            {
                var thanhPhamXuongEntity = _thanhPhamXuongRepository.GetById(thanhPhamXuongModel.ThanhPhamXuongId);
				if (thanhPhamXuongEntity != null)
				{
					thanhPhamXuongEntity = thanhPhamXuongModel.MapToEntity(thanhPhamXuongEntity);
					thanhPhamXuongEntity.MapToModel(thanhPhamXuongModel);

					_thanhPhamXuongRepository.Update(thanhPhamXuongEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update ThanhPhamXuong error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateThanhPhamXuong(ThanhPhamXuongModel model)
        {
            try
            {
                var createdThanhPhamXuong = _thanhPhamXuongRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdThanhPhamXuong == null)
                {
                    Log.Error("Create thanhPhamXuong error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create ThanhPhamXuong error", ex);
                return false;
            }

        }

        public bool Delete(int thanhPhamXuongId, out string message)
        {
            

            try
            {
                var entity = _thanhPhamXuongRepository.GetById(thanhPhamXuongId);
				if (entity != null)
				{
					_thanhPhamXuongRepository.Delete(thanhPhamXuongId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete ThanhPhamXuong error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<ThanhPhamXuongModel> GetAllThanhPhamXuongs()
        {
            //Igrone thanhPhamXuong system
            return _thanhPhamXuongRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int thanhPhamXuongId, out string message)
        {
            try
            {
                var entity = _thanhPhamXuongRepository.Query(c => c.ThanhPhamXuongId == thanhPhamXuongId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_thanhPhamXuongRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete ThanhPhamXuong error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
