﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.NhomVatTuNhapRiengModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface INhomVatTuNhapRiengService : IEntityService<NhomVatTuNhapRieng>
    {
        List<NhomVatTuNhapRiengModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateNhomVatTuNhapRieng(NhomVatTuNhapRiengModel NhomVatTuNhapRiengModel, out string message);
        bool Delete(int nhomVatTuNhapRiengId, out string message);
        List<NhomVatTuNhapRiengModel> GetAllNhomVatTuNhapRiengs();
        bool CreateNhomVatTuNhapRieng(NhomVatTuNhapRiengModel model);
		bool ChangeStatus(int nhomVatTuNhapRiengId, out string message);

    }
    public class NhomVatTuNhapRiengService : EntityService<NhomVatTuNhapRieng>, INhomVatTuNhapRiengService
    {
        private readonly INhomVatTuNhapRiengRepository _nhomVatTuNhapRiengRepository;
        public NhomVatTuNhapRiengService(IUnitOfWork unitOfWork, INhomVatTuNhapRiengRepository nhomVatTuNhapRiengRepository)
            : base(unitOfWork, nhomVatTuNhapRiengRepository)
        {
            _nhomVatTuNhapRiengRepository = nhomVatTuNhapRiengRepository;
        }

        public List<NhomVatTuNhapRiengModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var nhomVatTuNhapRiengEntities = _nhomVatTuNhapRiengRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (nhomVatTuNhapRiengEntities != null)
				{
					return nhomVatTuNhapRiengEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search NhomVatTuNhapRieng error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateNhomVatTuNhapRieng(NhomVatTuNhapRiengModel nhomVatTuNhapRiengModel, out string message)
        {
            try
            {
                var nhomVatTuNhapRiengEntity = _nhomVatTuNhapRiengRepository.GetById(nhomVatTuNhapRiengModel.NhomVatTuNhapRiengId);
				if (nhomVatTuNhapRiengEntity != null)
				{
					nhomVatTuNhapRiengEntity = nhomVatTuNhapRiengModel.MapToEntity(nhomVatTuNhapRiengEntity);
					nhomVatTuNhapRiengEntity.MapToModel(nhomVatTuNhapRiengModel);

					_nhomVatTuNhapRiengRepository.Update(nhomVatTuNhapRiengEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update NhomVatTuNhapRieng error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateNhomVatTuNhapRieng(NhomVatTuNhapRiengModel model)
        {
            try
            {
                var createdNhomVatTuNhapRieng = _nhomVatTuNhapRiengRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdNhomVatTuNhapRieng == null)
                {
                    Log.Error("Create nhomVatTuNhapRieng error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create NhomVatTuNhapRieng error", ex);
                return false;
            }

        }

        public bool Delete(int nhomVatTuNhapRiengId, out string message)
        {
            

            try
            {
                var entity = _nhomVatTuNhapRiengRepository.GetById(nhomVatTuNhapRiengId);
				if (entity != null)
				{
					_nhomVatTuNhapRiengRepository.Delete(nhomVatTuNhapRiengId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete NhomVatTuNhapRieng error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<NhomVatTuNhapRiengModel> GetAllNhomVatTuNhapRiengs()
        {
            //Igrone nhomVatTuNhapRieng system
            return _nhomVatTuNhapRiengRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int nhomVatTuNhapRiengId, out string message)
        {
            try
            {
                var entity = _nhomVatTuNhapRiengRepository.Query(c => c.NhomVatTuNhapRiengId == nhomVatTuNhapRiengId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_nhomVatTuNhapRiengRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete NhomVatTuNhapRieng error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
