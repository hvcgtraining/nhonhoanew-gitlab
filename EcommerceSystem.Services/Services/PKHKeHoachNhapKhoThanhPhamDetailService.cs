﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHKeHoachNhapKhoThanhPhamDetailModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHKeHoachNhapKhoThanhPhamDetailService : IEntityService<PKHKeHoachNhapKhoThanhPhamDetail>
    {
        List<PKHKeHoachNhapKhoThanhPhamDetailModel> Search(int currentPage, int pageSize, string textSearch, int keHoachXuongId, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHKeHoachNhapKhoThanhPhamDetail(PKHKeHoachNhapKhoThanhPhamDetailModel PKHKeHoachNhapKhoThanhPhamDetailModel, out string message);
        bool Delete(int pKHKeHoachNhapKhoThanhPhamDetailId, out string message);
        List<PKHKeHoachNhapKhoThanhPhamDetailModel> GetAllPKHKeHoachNhapKhoThanhPhamDetails();
        bool CreatePKHKeHoachNhapKhoThanhPhamDetail(PKHKeHoachNhapKhoThanhPhamDetailModel model);
		bool ChangeStatus(int pKHKeHoachNhapKhoThanhPhamDetailId, out string message);

    }
    public class PKHKeHoachNhapKhoThanhPhamDetailService : EntityService<PKHKeHoachNhapKhoThanhPhamDetail>, IPKHKeHoachNhapKhoThanhPhamDetailService
    {
        private readonly IPKHKeHoachNhapKhoThanhPhamDetailRepository _pKHKeHoachNhapKhoThanhPhamDetailRepository;
        public PKHKeHoachNhapKhoThanhPhamDetailService(IUnitOfWork unitOfWork, IPKHKeHoachNhapKhoThanhPhamDetailRepository pKHKeHoachNhapKhoThanhPhamDetailRepository)
            : base(unitOfWork, pKHKeHoachNhapKhoThanhPhamDetailRepository)
        {
            _pKHKeHoachNhapKhoThanhPhamDetailRepository = pKHKeHoachNhapKhoThanhPhamDetailRepository;
        }

        public List<PKHKeHoachNhapKhoThanhPhamDetailModel> Search(int currentPage, int pageSize, string textSearch, int keHoachXuongId, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHKeHoachNhapKhoThanhPhamDetailEntities = _pKHKeHoachNhapKhoThanhPhamDetailRepository.Search(currentPage, pageSize, textSearch, keHoachXuongId, sortColumn, sortDirection, out totalPage);
				if (pKHKeHoachNhapKhoThanhPhamDetailEntities != null)
				{
					return pKHKeHoachNhapKhoThanhPhamDetailEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHKeHoachNhapKhoThanhPhamDetail error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHKeHoachNhapKhoThanhPhamDetail(PKHKeHoachNhapKhoThanhPhamDetailModel pKHKeHoachNhapKhoThanhPhamDetailModel, out string message)
        {
            try
            {
                var pKHKeHoachNhapKhoThanhPhamDetailEntity = _pKHKeHoachNhapKhoThanhPhamDetailRepository.GetById(pKHKeHoachNhapKhoThanhPhamDetailModel.PKHKeHoachNhapKhoThanhPhamDetailId);
				if (pKHKeHoachNhapKhoThanhPhamDetailEntity != null)
				{
					pKHKeHoachNhapKhoThanhPhamDetailEntity = pKHKeHoachNhapKhoThanhPhamDetailModel.MapToEntity(pKHKeHoachNhapKhoThanhPhamDetailEntity);
					pKHKeHoachNhapKhoThanhPhamDetailEntity.MapToModel(pKHKeHoachNhapKhoThanhPhamDetailModel);

					_pKHKeHoachNhapKhoThanhPhamDetailRepository.Update(pKHKeHoachNhapKhoThanhPhamDetailEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHKeHoachNhapKhoThanhPhamDetail error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHKeHoachNhapKhoThanhPhamDetail(PKHKeHoachNhapKhoThanhPhamDetailModel model)
        {
            try
            {
                var createdPKHKeHoachNhapKhoThanhPhamDetail = _pKHKeHoachNhapKhoThanhPhamDetailRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHKeHoachNhapKhoThanhPhamDetail == null)
                {
                    Log.Error("Create pKHKeHoachNhapKhoThanhPhamDetail error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHKeHoachNhapKhoThanhPhamDetail error", ex);
                return false;
            }

        }

        public bool Delete(int pKHKeHoachNhapKhoThanhPhamDetailId, out string message)
        {
            

            try
            {
                var entity = _pKHKeHoachNhapKhoThanhPhamDetailRepository.GetById(pKHKeHoachNhapKhoThanhPhamDetailId);
				if (entity != null)
				{
					_pKHKeHoachNhapKhoThanhPhamDetailRepository.Delete(pKHKeHoachNhapKhoThanhPhamDetailId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachNhapKhoThanhPhamDetail error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHKeHoachNhapKhoThanhPhamDetailModel> GetAllPKHKeHoachNhapKhoThanhPhamDetails()
        {
            //Igrone pKHKeHoachNhapKhoThanhPhamDetail system
            return _pKHKeHoachNhapKhoThanhPhamDetailRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHKeHoachNhapKhoThanhPhamDetailId, out string message)
        {
            try
            {
                var entity = _pKHKeHoachNhapKhoThanhPhamDetailRepository.Query(c => c.PKHKeHoachNhapKhoThanhPhamDetailId == pKHKeHoachNhapKhoThanhPhamDetailId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHKeHoachNhapKhoThanhPhamDetailRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachNhapKhoThanhPhamDetail error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
