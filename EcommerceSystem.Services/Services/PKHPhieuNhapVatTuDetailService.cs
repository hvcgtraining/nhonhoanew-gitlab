﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHPhieuNhapVatTuDetailModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHPhieuNhapVatTuDetailService : IEntityService<PKHPhieuNhapVatTuDetail>
    {
        List<PKHPhieuNhapVatTuDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHPhieuNhapVatTuDetail(PKHPhieuNhapVatTuDetailModel PKHPhieuNhapVatTuDetailModel, out string message);
        bool Delete(int pKHPhieuNhapVatTuDetailId, out string message);
        List<PKHPhieuNhapVatTuDetailModel> GetAllPKHPhieuNhapVatTuDetails();
        bool CreatePKHPhieuNhapVatTuDetail(PKHPhieuNhapVatTuDetailModel model);
		bool ChangeStatus(int pKHPhieuNhapVatTuDetailId, out string message);

    }
    public class PKHPhieuNhapVatTuDetailService : EntityService<PKHPhieuNhapVatTuDetail>, IPKHPhieuNhapVatTuDetailService
    {
        private readonly IPKHPhieuNhapVatTuDetailRepository _pKHPhieuNhapVatTuDetailRepository;
        public PKHPhieuNhapVatTuDetailService(IUnitOfWork unitOfWork, IPKHPhieuNhapVatTuDetailRepository pKHPhieuNhapVatTuDetailRepository)
            : base(unitOfWork, pKHPhieuNhapVatTuDetailRepository)
        {
            _pKHPhieuNhapVatTuDetailRepository = pKHPhieuNhapVatTuDetailRepository;
        }

        public List<PKHPhieuNhapVatTuDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHPhieuNhapVatTuDetailEntities = _pKHPhieuNhapVatTuDetailRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHPhieuNhapVatTuDetailEntities != null)
				{
					return pKHPhieuNhapVatTuDetailEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHPhieuNhapVatTuDetail error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHPhieuNhapVatTuDetail(PKHPhieuNhapVatTuDetailModel pKHPhieuNhapVatTuDetailModel, out string message)
        {
            try
            {
                var pKHPhieuNhapVatTuDetailEntity = _pKHPhieuNhapVatTuDetailRepository.GetById(pKHPhieuNhapVatTuDetailModel.PKHPhieuNhapVatTuDetailId);
				if (pKHPhieuNhapVatTuDetailEntity != null)
				{
					pKHPhieuNhapVatTuDetailEntity = pKHPhieuNhapVatTuDetailModel.MapToEntity(pKHPhieuNhapVatTuDetailEntity);
					pKHPhieuNhapVatTuDetailEntity.MapToModel(pKHPhieuNhapVatTuDetailModel);

					_pKHPhieuNhapVatTuDetailRepository.Update(pKHPhieuNhapVatTuDetailEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHPhieuNhapVatTuDetail error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHPhieuNhapVatTuDetail(PKHPhieuNhapVatTuDetailModel model)
        {
            try
            {
                var createdPKHPhieuNhapVatTuDetail = _pKHPhieuNhapVatTuDetailRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHPhieuNhapVatTuDetail == null)
                {
                    Log.Error("Create pKHPhieuNhapVatTuDetail error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHPhieuNhapVatTuDetail error", ex);
                return false;
            }

        }

        public bool Delete(int pKHPhieuNhapVatTuDetailId, out string message)
        {
            

            try
            {
                var entity = _pKHPhieuNhapVatTuDetailRepository.GetById(pKHPhieuNhapVatTuDetailId);
				if (entity != null)
				{
					_pKHPhieuNhapVatTuDetailRepository.Delete(pKHPhieuNhapVatTuDetailId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHPhieuNhapVatTuDetail error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHPhieuNhapVatTuDetailModel> GetAllPKHPhieuNhapVatTuDetails()
        {
            //Igrone pKHPhieuNhapVatTuDetail system
            return _pKHPhieuNhapVatTuDetailRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHPhieuNhapVatTuDetailId, out string message)
        {
            try
            {
                var entity = _pKHPhieuNhapVatTuDetailRepository.Query(c => c.PKHPhieuNhapVatTuDetailId == pKHPhieuNhapVatTuDetailId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHPhieuNhapVatTuDetailRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHPhieuNhapVatTuDetail error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
