﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.GoiDauModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IGoiDauService : IEntityService<GoiDau>
    {
        List<GoiDauModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateGoiDau(GoiDauModel GoiDauModel, out string message);
        bool Delete(int goiDauId, out string message);
        List<GoiDauModel> GetAllGoiDaus();
        bool CreateGoiDau(GoiDauModel model);
		bool ChangeStatus(int goiDauId, out string message);

    }
    public class GoiDauService : EntityService<GoiDau>, IGoiDauService
    {
        private readonly IGoiDauRepository _goiDauRepository;
        public GoiDauService(IUnitOfWork unitOfWork, IGoiDauRepository goiDauRepository)
            : base(unitOfWork, goiDauRepository)
        {
            _goiDauRepository = goiDauRepository;
        }

        public List<GoiDauModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var goiDauEntities = _goiDauRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (goiDauEntities != null)
				{
					return goiDauEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search GoiDau error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateGoiDau(GoiDauModel goiDauModel, out string message)
        {
            try
            {
                var goiDauEntity = _goiDauRepository.GetById(goiDauModel.GoiDauId);
				if (goiDauEntity != null)
				{
					goiDauEntity = goiDauModel.MapToEntity(goiDauEntity);
					goiDauEntity.MapToModel(goiDauModel);

					_goiDauRepository.Update(goiDauEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update GoiDau error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateGoiDau(GoiDauModel model)
        {
            try
            {
                var createdGoiDau = _goiDauRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdGoiDau == null)
                {
                    Log.Error("Create goiDau error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create GoiDau error", ex);
                return false;
            }

        }

        public bool Delete(int goiDauId, out string message)
        {
            

            try
            {
                var entity = _goiDauRepository.GetById(goiDauId);
				if (entity != null)
				{
					_goiDauRepository.Delete(goiDauId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete GoiDau error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<GoiDauModel> GetAllGoiDaus()
        {
            //Igrone goiDau system
            return _goiDauRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int goiDauId, out string message)
        {
            try
            {
                var entity = _goiDauRepository.Query(c => c.GoiDauId == goiDauId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_goiDauRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete GoiDau error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
