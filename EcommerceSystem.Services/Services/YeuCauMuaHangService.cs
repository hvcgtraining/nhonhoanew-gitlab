﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.YeuCauMuaHangModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IYeuCauMuaHangService : IEntityService<YeuCauMuaHang>
    {
        List<YeuCauMuaHangModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateYeuCauMuaHang(YeuCauMuaHangModel YeuCauMuaHangModel, out string message);
        void UpdateYeuCauMuaHangByPhieuMuaHangID(YeuCauMuaHangModel YeuCauMuaHangModel);
        bool Delete(int yeuCauMuaHangId, out string message);
        List<YeuCauMuaHangModel> GetAllYeuCauMuaHangs();
        List<YeuCauMuaHangModel> GetYeuCauMuaHangsByParams(int trangThai, int? phieuYeuCauMuaHangId);
        bool CreateYeuCauMuaHang(YeuCauMuaHangModel model);
		bool ChangeStatus(int yeuCauMuaHangId, out string message);

    }
    public class YeuCauMuaHangService : EntityService<YeuCauMuaHang>, IYeuCauMuaHangService
    {
        private readonly IYeuCauMuaHangRepository _yeuCauMuaHangRepository;
        public YeuCauMuaHangService(IUnitOfWork unitOfWork, IYeuCauMuaHangRepository yeuCauMuaHangRepository)
            : base(unitOfWork, yeuCauMuaHangRepository)
        {
            _yeuCauMuaHangRepository = yeuCauMuaHangRepository;
        }

        public List<YeuCauMuaHangModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var yeuCauMuaHangEntities = _yeuCauMuaHangRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (yeuCauMuaHangEntities != null)
				{
					return yeuCauMuaHangEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search YeuCauMuaHang error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateYeuCauMuaHang(YeuCauMuaHangModel yeuCauMuaHangModel, out string message)
        {
            try
            {
                var yeuCauMuaHangEntity = _yeuCauMuaHangRepository.GetById(yeuCauMuaHangModel.YeuCauMuaHangId);
				if (yeuCauMuaHangEntity != null)
				{
					yeuCauMuaHangEntity = yeuCauMuaHangModel.MapToEntity(yeuCauMuaHangEntity);
					yeuCauMuaHangEntity.MapToModel(yeuCauMuaHangModel);

					_yeuCauMuaHangRepository.Update(yeuCauMuaHangEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update YeuCauMuaHang error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }
        public void UpdateYeuCauMuaHangByPhieuMuaHangID(YeuCauMuaHangModel yeuCauMuaHangModel)
        {
            try
            {
                var yeuCauMuaHangEntity = _yeuCauMuaHangRepository.GetById(yeuCauMuaHangModel.YeuCauMuaHangId);
                if (yeuCauMuaHangEntity != null)
                {
                    yeuCauMuaHangEntity = yeuCauMuaHangModel.MapToEntity(yeuCauMuaHangEntity);
                    yeuCauMuaHangEntity.MapToModel(yeuCauMuaHangModel);

                    _yeuCauMuaHangRepository.Update(yeuCauMuaHangEntity);
                    UnitOfWork.SaveChanges();

                    return ;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return ;
        }

        public bool CreateYeuCauMuaHang(YeuCauMuaHangModel model)
        {
            try
            {
                var createdYeuCauMuaHang = _yeuCauMuaHangRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdYeuCauMuaHang == null)
                {
                    Log.Error("Create yeuCauMuaHang error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create YeuCauMuaHang error", ex);
                return false;
            }

        }

        public bool Delete(int yeuCauMuaHangId, out string message)
        {
            

            try
            {
                var entity = _yeuCauMuaHangRepository.GetById(yeuCauMuaHangId);
				if (entity != null)
				{
					_yeuCauMuaHangRepository.Delete(yeuCauMuaHangId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete YeuCauMuaHang error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<YeuCauMuaHangModel> GetAllYeuCauMuaHangs()
        {
            //Igrone yeuCauMuaHang system
            return _yeuCauMuaHangRepository.GetAll().ToList().MapToModels();
        }

        /// <summary>
        /// get all Yeucaumuahang by TrangThai and by PhieuYeuCauMuaHang
        /// </summary>
        /// <returns></returns>
        public List<YeuCauMuaHangModel> GetYeuCauMuaHangsByParams(int trangThai, int? phieuYeuCauMuaHangId)
        {
            return _yeuCauMuaHangRepository.GetAll().Where(c=>c.TrangThai == trangThai && c.PKHPhieuYeuCauMuaHangId == phieuYeuCauMuaHangId).ToList().MapToModels();
        }

        public bool ChangeStatus(int yeuCauMuaHangId, out string message)
        {
            try
            {
                var entity = _yeuCauMuaHangRepository.Query(c => c.YeuCauMuaHangId == yeuCauMuaHangId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_yeuCauMuaHangRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete YeuCauMuaHang error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
