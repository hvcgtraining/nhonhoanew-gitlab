﻿using System.Collections.Generic;
using EcommerceSystem.Models.User;
using EcommerceSystem.DataAccess;
using System.Web;
using EcommerceSystem.Models;
using System;
using System.Web.Caching;
using System.Linq;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.DataAccess.Common;
using System.Data.Entity;
using System.Web.Mvc;
using EcommerceSystem.Core;
using Microsoft.AspNet.Identity;

namespace EcommerceSystem.Services
{
    public interface IUserService : IEntityService<User>
    {
        UserModel RetrieveUser(int userId);
        List<UserModel> SearchUser(int currentPage, int pageSize, string textSearch, out int totalPage);
        bool DeleteUser(int id, out string message);
        UserCommon ValidateLogon(string userName, string password, out string msgError);
        bool IsUserExist(string userName);
        bool IsUserEmailExit(string email);
        List<string> GetRoleModuleAction(int userId, string[] roles);
        UserCommon GetUserByEmail(string email);
        List<UserCommon> GetUsersByRole(string roleCode);
        bool ChangePassword(int userId, string passwordOld, string passwordNew, out string message);
        bool ChangeStatus(int userId, out string message);
        bool UpdateUser(UserModel userModel, out string message);
        bool CreateUser(UserModel userModel, out string message);
        List<string> GetUserEmailByRoleCode (string code);
    }
    public class UserService : EntityService<User>, IUserService
    {
        private readonly IModuleActionRepository _moduleActionRepository;
        private readonly IUserRoleRepository _userRoleRepository;
        private readonly IUserRepository _userRepository;
        private const int CacheTimeoutInHours = 2;

        public UserService(IUnitOfWork unitOfWork, IUserRepository userRepository, IUserRoleRepository userRoleRepository, IModuleActionRepository moduleActionRepository)
            : base(unitOfWork, userRepository)
        {
            _userRepository = userRepository;
            _userRoleRepository = userRoleRepository;
            _moduleActionRepository = moduleActionRepository;
        }

        public UserModel RetrieveUser(int userId)
        {
            var userEntities = _userRepository.RetrieveUser(userId);
            if (userEntities != null)
            {
                return userEntities.MapToModel();
            }
            return null;
        }

        /// <summary>
        /// Search list user
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="textSearch"></param>
        /// <param name="totalPage"></param>
        /// <returns></returns>
        public List<UserModel> SearchUser(int currentPage, int pageSize, string textSearch, out int totalPage)
        {
            var userEntities = _userRepository.SearchUser(currentPage, pageSize, textSearch, out totalPage);
            if (userEntities != null)
            {
                return userEntities.MapToModels();
            }
            return null;
        }
        /// <summary>
        /// search list user by role code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public List<string> GetUserEmailByRoleCode(string code)
        {
            var users = _userRepository.FindAll(x => x.UserRoles.Any(z => z.Role.Code == code), x => x.UserRoles.Select(y => y.Role));
            return users?.Select(x => x.Email).ToList();

            //var roleUser = _userRoleRepository.GetAll().Where(c => c.Role.Code.Equals(code));
            //List<User> users = new List<User>();

            //if(roleUser != null && roleUser.Any())
            //{
            //    users = _userRepository.GetAll().Where(c => roleUser.Any(o => o.UserId == c.UserId)).ToList();
            //    if (users.Any())
            //        return users.MapToModels();
            //}
            //return null;       
        }
        public bool DeleteUser(int id, out string message)
        {
            var user = _userRepository.GetById(id);
            if (user != null)
            {
                // Delete table User
                _userRepository.Delete(id);

                // Delete table UserRole
                var userRoleEntity = _userRoleRepository.RetrieveUserRole(id);
                _userRoleRepository.Delete(userRoleEntity);

                UnitOfWork.SaveChanges();

                message = "Xóa tài khoản thành công";
                return true;
            }

            message = "Xóa tài khoản thất bại";
            return false;
        }

        public UserCommon ValidateLogon(string email, string password, out string msgError)
        {
            msgError = string.Empty;
            var result = new UserCommon();

            var user = _userRepository.GetAll().Where(c => c.Email.Equals(email)).Include(c => c.UserRoles).FirstOrDefault();
            if (user == null)
            {
                msgError = "Thông tin Email không hợp lệ.";
                result.Status = LoginResult.InvalidEmail;
            }

            if (user != null)
            {
                var passwordHasher = new CustomPasswordHasher();
                var newPass = string.Concat(user.PasswordSalt, password);

                var verify = passwordHasher.VerifyHashedPassword(user.Password, newPass);
                if (verify == PasswordVerificationResult.Success)
                {
                    if (user.IsLockedOut)
                    {
                        msgError = "Tài khoản của bạn đã bị khóa, xin vui lòng liên hệ với người quản trị.";
                        result.Status = LoginResult.IsLockedOut;
                        return result;
                    }

                    result.Status = LoginResult.Success;
                    result.Email = user.Email;
                    result.UserId = user.UserId;
                    result.UserName = user.UserName;
                    result.Avatar = user.Avatar;

                    var listIds = user.UserRoles.Select(c => c.RoleId).ToList();
                    if (listIds.Any())
                    {
                        var roleDefault = listIds.FirstOrDefault();
                        result.IsAdmin = user.IsSupperAdmin;
                        result.Roles = user.UserRoles.Select(c => c.RoleId.ToString()).ToArray();
                        result.RoleModuleActions = _moduleActionRepository.GetModuleActionByUserId(user.UserId, listIds);
                    }
                    else
                    {
                        result.RoleModuleActions = new Dictionary<string, string>();
                    }

                    //Store in cache,NoSlidingExpiration : timeout
                    HttpRuntime.Cache.Insert(user.UserId.ToString(), result.RoleModuleActions, null, DateTime.Now.AddHours(CacheTimeoutInHours),
                                             Cache.NoSlidingExpiration);
                    return result;
                }

                msgError = "Mật khẩu không hợp lệ.";
                result.Status = LoginResult.InvalidPassword;
                return result;
            }

            msgError = "Thông tin tài khoản không hợp lệ.";
            result.Status = LoginResult.Unknown;
            return result;
        }

        public bool IsUserExist(string userName)
        {
            var user = _userRepository.Query(x => x.UserName == userName).Any();
            return user;
        }

        public bool IsUserEmailExit(string email)
        {
            var user = _userRepository.Query(x => x.Email == email).Any();
            return user;
        }
        public List<string> GetRoleModuleAction(int userId, string[] roles)
        {
            return null;
        }

        public UserCommon GetUserByEmail(string email)
        {
            var result = new UserCommon();
            var user = GetAll().AsQueryable().Include(c => c.UserRoles)
                .FirstOrDefault(c => c.Email.ToLower().Equals(email.ToLower()) && (!c.IsLockedOut));

            if (user != null)
            {
                var listIds = (user.UserRoles != null) ? user.UserRoles.Select(c => c.RoleId).ToList() : new List<int>();
                var roleDefault = listIds.FirstOrDefault();
                result.IsAdmin = user.IsSupperAdmin;
                result.Roles = (user.UserRoles != null) ? user.UserRoles.Select(c => c.RoleId.ToString()).ToArray() : new string[0];

                result.UserId = user.UserId;
                result.Avatar = user.Avatar;
                result.Email = user.Email;
                result.UserName = user.UserName;
                result.RoleModuleActions = _moduleActionRepository.GetModuleActionByUserId(user.UserId, listIds);
            }

            return result;
        }

        public List<UserCommon> GetUsersByRole(string roleCode)
        {
            var users = GetAll().AsQueryable().Include(c => c.UserRoles).Include(x => x.UserRoles.Select(y => y.Role))
                            .Where(o=>o.UserRoles.Any(r=>r.Role.Code == roleCode));

            var listUsers = new List<UserCommon>();
            if (users.Any())
            {
                var listIds = users.Select(x=> new UserCommon {Email = x.Email, FirstName = x.FullName, UserId = x.UserId}).ToList();
                listUsers.AddRange(listIds);
            }

            return listUsers;
        }

        public bool ChangePassword(int userId, string passwordOld, string passwordNew, out string message)
        {
            var user = _userRepository.Find(c => c.UserId == userId);
            if (user != null)
            {
                var passwordHasher = new CustomPasswordHasher();
                var oldPass = string.Concat(user.PasswordSalt, passwordOld);

                var verify = passwordHasher.VerifyHashedPassword(user.Password, oldPass);
                if (verify == PasswordVerificationResult.Success)
                {
                    var newPass = string.Concat(user.PasswordSalt, passwordNew);
                    user.Password = (new CustomPasswordHasher()).HashPassword(newPass);
                    _userRepository.Update(user);

                    UnitOfWork.SaveChanges();

                    message = "Thay đổi mật khẩu thành công.";
                    return true;
                }
                message = "Mật khẩu cũ không đúng, vui lòng nhập lại.";
                return false;
            }

            message = "Thay đổi mật khẩu thất bại.";
            return false;
        }

        public bool ChangeStatus(int userId, out string message)
        {
            var position = _userRepository.Query(c => c.UserId == userId).FirstOrDefault();
            if (position != null)
            {
                if (position.Status)
                {
                    position.Status = false;
                }
                else
                {
                    position.Status = true;
                }

                _userRepository.Update(position);
                UnitOfWork.SaveChanges();

                message = "Cập nhật trạng thái user thành công.";
                return true;
            }

            message = "Cập nhật trạng thái user thất bại.";
            return false;
        }

        public bool UpdateUser(UserModel userModel, out string message)
        {
            var userEntity = _userRepository.GetById(userModel.UserId);
            var userRoleEntity = _userRoleRepository.RetrieveUserRole(userModel.UserId);
            if (userEntity != null)
            {
                // Update table user
                if (IsUpdatedUser(userModel))
                {
                    userEntity.Email = userModel.Email;
                    userEntity.UpdatedDate = DateTime.Now;
                    userEntity.UserName = userModel.UserName;
                    if (!string.IsNullOrEmpty(userModel.Password))
                    {
                        userEntity.Password = (new CustomPasswordHasher()).HashPassword(userModel.Password);
                    }
                    _userRepository.Update(userEntity);
                }

                //Update table UserRole
                if (userRoleEntity.RoleId != userModel.RoleId)
                {
                    userRoleEntity.RoleId = userModel.RoleId;
                    _userRoleRepository.Update(userRoleEntity);
                };

                UnitOfWork.SaveChanges();

                message = "Cập nhật thành công";
                return true;
            }
            message = "Cập nhật tài khoản thất bại.";
            return false;
        }

        public bool CreateUser(UserModel userModel, out string message)
        {
            if (!IsUserEmailExit(userModel.Email))
            {
                // Add table User
                userModel.PasswordSalt = (new CustomPasswordHasher()).HashPassword(userModel.Password);
                var password = string.Concat(userModel.PasswordSalt, userModel.Password);
                userModel.Password = (new CustomPasswordHasher()).HashPassword(password);

                userModel.CreatedDate = DateTime.Now;
                userModel.Status = true;
                var userEntity = _userRepository.Insert(userModel.MapToEntity());
                UnitOfWork.SaveChanges();

                // Add table UserRole
                var userRoleEntity = new UserRole();
                userRoleEntity.UserId = userEntity.UserId;
                userRoleEntity.RoleId = userModel.RoleId;
                userRoleEntity.CreatedDate = DateTime.Now;
                _userRoleRepository.Insert(userRoleEntity);
                UnitOfWork.SaveChanges();

                // Return
                message = "Thêm tài khoản thành công";
                return true;
            }
            message = "Email đã tồn tại";
            return false;
        }
        
        /// <summary>
        /// Check user is updated
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        private bool IsUpdatedUser(UserModel userModel)
        {
            var userEntity = _userRepository.GetById(userModel.UserId);
            if (userEntity.Email != userModel.Email)
                return true;
            if (userEntity.UserName != userModel.UserName)
                return true;
            if (userEntity.Password != userModel.Password)
                return true;
            return false;
        }
    }
}
