﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.QuanLySoNModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IQuanLySoNService : IEntityService<QuanLySoN>
    {
        List<QuanLySoNModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateQuanLySoN(QuanLySoNModel QuanLySoNModel, out string message);
        bool Delete(int quanLySoNId, out string message);
        List<QuanLySoNModel> GetAllQuanLySoNs();
        bool CreateQuanLySoN(QuanLySoNModel model);
		bool ChangeStatus(int quanLySoNId, out string message);

    }
    public class QuanLySoNService : EntityService<QuanLySoN>, IQuanLySoNService
    {
        private readonly IQuanLySoNRepository _quanLySoNRepository;
        public QuanLySoNService(IUnitOfWork unitOfWork, IQuanLySoNRepository quanLySoNRepository)
            : base(unitOfWork, quanLySoNRepository)
        {
            _quanLySoNRepository = quanLySoNRepository;
        }

        public List<QuanLySoNModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var quanLySoNEntities = _quanLySoNRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (quanLySoNEntities != null)
				{
					return quanLySoNEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search QuanLySoN error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateQuanLySoN(QuanLySoNModel quanLySoNModel, out string message)
        {
            try
            {
                var quanLySoNEntity = _quanLySoNRepository.GetById(quanLySoNModel.QuanLySoNId);
				if (quanLySoNEntity != null)
				{
					quanLySoNEntity = quanLySoNModel.MapToEntity(quanLySoNEntity);
					quanLySoNEntity.MapToModel(quanLySoNModel);

					_quanLySoNRepository.Update(quanLySoNEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update QuanLySoN error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateQuanLySoN(QuanLySoNModel model)
        {
            try
            {
                var createdQuanLySoN = _quanLySoNRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdQuanLySoN == null)
                {
                    Log.Error("Create quanLySoN error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create QuanLySoN error", ex);
                return false;
            }

        }

        public bool Delete(int quanLySoNId, out string message)
        {
            

            try
            {
                var entity = _quanLySoNRepository.GetById(quanLySoNId);
				if (entity != null)
				{
					_quanLySoNRepository.Delete(quanLySoNId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete QuanLySoN error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<QuanLySoNModel> GetAllQuanLySoNs()
        {
            //Igrone quanLySoN system
            return _quanLySoNRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int quanLySoNId, out string message)
        {
            try
            {
                var entity = _quanLySoNRepository.Query(c => c.QuanLySoNId == quanLySoNId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_quanLySoNRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete QuanLySoN error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
