﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHPhieuYeuCauCapHangDetailModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHPhieuYeuCauCapHangDetailService : IEntityService<PKHPhieuYeuCauCapHangDetail>
    {
        List<PKHPhieuYeuCauCapHangDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHPhieuYeuCauCapHangDetail(PKHPhieuYeuCauCapHangDetailModel PKHPhieuYeuCauCapHangDetailModel, out string message);
        bool Delete(int pKHPhieuYeuCauCapHangDetailId, out string message);
        List<PKHPhieuYeuCauCapHangDetailModel> GetAllPKHPhieuYeuCauCapHangDetails();
        bool CreatePKHPhieuYeuCauCapHangDetail(PKHPhieuYeuCauCapHangDetailModel model);
		bool ChangeStatus(int pKHPhieuYeuCauCapHangDetailId, out string message);

    }
    public class PKHPhieuYeuCauCapHangDetailService : EntityService<PKHPhieuYeuCauCapHangDetail>, IPKHPhieuYeuCauCapHangDetailService
    {
        private readonly IPKHPhieuYeuCauCapHangDetailRepository _pKHPhieuYeuCauCapHangDetailRepository;
        public PKHPhieuYeuCauCapHangDetailService(IUnitOfWork unitOfWork, IPKHPhieuYeuCauCapHangDetailRepository pKHPhieuYeuCauCapHangDetailRepository)
            : base(unitOfWork, pKHPhieuYeuCauCapHangDetailRepository)
        {
            _pKHPhieuYeuCauCapHangDetailRepository = pKHPhieuYeuCauCapHangDetailRepository;
        }

        public List<PKHPhieuYeuCauCapHangDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHPhieuYeuCauCapHangDetailEntities = _pKHPhieuYeuCauCapHangDetailRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHPhieuYeuCauCapHangDetailEntities != null)
				{
					return pKHPhieuYeuCauCapHangDetailEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHPhieuYeuCauCapHangDetail error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHPhieuYeuCauCapHangDetail(PKHPhieuYeuCauCapHangDetailModel pKHPhieuYeuCauCapHangDetailModel, out string message)
        {
            try
            {
                var pKHPhieuYeuCauCapHangDetailEntity = _pKHPhieuYeuCauCapHangDetailRepository.GetById(pKHPhieuYeuCauCapHangDetailModel.PKHPhieuYeuCauCapHangDetailId);
				if (pKHPhieuYeuCauCapHangDetailEntity != null)
				{
					pKHPhieuYeuCauCapHangDetailEntity = pKHPhieuYeuCauCapHangDetailModel.MapToEntity(pKHPhieuYeuCauCapHangDetailEntity);
					pKHPhieuYeuCauCapHangDetailEntity.MapToModel(pKHPhieuYeuCauCapHangDetailModel);

					_pKHPhieuYeuCauCapHangDetailRepository.Update(pKHPhieuYeuCauCapHangDetailEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHPhieuYeuCauCapHangDetail error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHPhieuYeuCauCapHangDetail(PKHPhieuYeuCauCapHangDetailModel model)
        {
            try
            {
                var createdPKHPhieuYeuCauCapHangDetail = _pKHPhieuYeuCauCapHangDetailRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHPhieuYeuCauCapHangDetail == null)
                {
                    Log.Error("Create pKHPhieuYeuCauCapHangDetail error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHPhieuYeuCauCapHangDetail error", ex);
                return false;
            }

        }

        public bool Delete(int pKHPhieuYeuCauCapHangDetailId, out string message)
        {
            

            try
            {
                var entity = _pKHPhieuYeuCauCapHangDetailRepository.GetById(pKHPhieuYeuCauCapHangDetailId);
				if (entity != null)
				{
					_pKHPhieuYeuCauCapHangDetailRepository.Delete(pKHPhieuYeuCauCapHangDetailId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHPhieuYeuCauCapHangDetail error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHPhieuYeuCauCapHangDetailModel> GetAllPKHPhieuYeuCauCapHangDetails()
        {
            //Igrone pKHPhieuYeuCauCapHangDetail system
            return _pKHPhieuYeuCauCapHangDetailRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHPhieuYeuCauCapHangDetailId, out string message)
        {
            try
            {
                var entity = _pKHPhieuYeuCauCapHangDetailRepository.Query(c => c.PKHPhieuYeuCauCapHangDetailId == pKHPhieuYeuCauCapHangDetailId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHPhieuYeuCauCapHangDetailRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHPhieuYeuCauCapHangDetail error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
