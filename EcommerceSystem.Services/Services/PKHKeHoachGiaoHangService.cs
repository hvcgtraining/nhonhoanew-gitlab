﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHKeHoachGiaoHangModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHKeHoachGiaoHangService : IEntityService<PKHKeHoachGiaoHang>
    {
        List<PKHKeHoachGiaoHangModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHKeHoachGiaoHang(PKHKeHoachGiaoHangModel PKHKeHoachGiaoHangModel, out string message);
        bool Delete(int pKHKeHoachGiaoHangId, out string message);
        List<PKHKeHoachGiaoHangModel> GetAllPKHKeHoachGiaoHangs();
        bool CreatePKHKeHoachGiaoHang(PKHKeHoachGiaoHangModel model);
		bool ChangeStatus(int pKHKeHoachGiaoHangId, out string message);

    }
    public class PKHKeHoachGiaoHangService : EntityService<PKHKeHoachGiaoHang>, IPKHKeHoachGiaoHangService
    {
        private readonly IPKHKeHoachGiaoHangRepository _pKHKeHoachGiaoHangRepository;
        public PKHKeHoachGiaoHangService(IUnitOfWork unitOfWork, IPKHKeHoachGiaoHangRepository pKHKeHoachGiaoHangRepository)
            : base(unitOfWork, pKHKeHoachGiaoHangRepository)
        {
            _pKHKeHoachGiaoHangRepository = pKHKeHoachGiaoHangRepository;
        }

        public List<PKHKeHoachGiaoHangModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHKeHoachGiaoHangEntities = _pKHKeHoachGiaoHangRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHKeHoachGiaoHangEntities != null)
				{
					return pKHKeHoachGiaoHangEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHKeHoachGiaoHang error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHKeHoachGiaoHang(PKHKeHoachGiaoHangModel pKHKeHoachGiaoHangModel, out string message)
        {
            try
            {
                var pKHKeHoachGiaoHangEntity = _pKHKeHoachGiaoHangRepository.GetById(pKHKeHoachGiaoHangModel.PKHKeHoachGiaoHangId);
				if (pKHKeHoachGiaoHangEntity != null)
				{
					pKHKeHoachGiaoHangEntity = pKHKeHoachGiaoHangModel.MapToEntity(pKHKeHoachGiaoHangEntity);
					pKHKeHoachGiaoHangEntity.MapToModel(pKHKeHoachGiaoHangModel);

					_pKHKeHoachGiaoHangRepository.Update(pKHKeHoachGiaoHangEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHKeHoachGiaoHang error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHKeHoachGiaoHang(PKHKeHoachGiaoHangModel model)
        {
            try
            {
                var createdPKHKeHoachGiaoHang = _pKHKeHoachGiaoHangRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHKeHoachGiaoHang == null)
                {
                    Log.Error("Create pKHKeHoachGiaoHang error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHKeHoachGiaoHang error", ex);
                return false;
            }

        }

        public bool Delete(int pKHKeHoachGiaoHangId, out string message)
        {
            

            try
            {
                var entity = _pKHKeHoachGiaoHangRepository.GetById(pKHKeHoachGiaoHangId);
				if (entity != null)
				{
					_pKHKeHoachGiaoHangRepository.Delete(pKHKeHoachGiaoHangId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachGiaoHang error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHKeHoachGiaoHangModel> GetAllPKHKeHoachGiaoHangs()
        {
            //Igrone pKHKeHoachGiaoHang system
            return _pKHKeHoachGiaoHangRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHKeHoachGiaoHangId, out string message)
        {
            try
            {
                var entity = _pKHKeHoachGiaoHangRepository.Query(c => c.PKHKeHoachGiaoHangId == pKHKeHoachGiaoHangId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHKeHoachGiaoHangRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachGiaoHang error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
