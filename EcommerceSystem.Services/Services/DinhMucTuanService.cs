﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.DinhMucTuanModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace EcommerceSystem.Services
{
    public interface IDinhMucTuanService : IEntityService<DinhMucTuan>
    {
        List<DinhMucTuanModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdateDinhMucTuan(DinhMucTuanModel DinhMucTuanModel, out string message);
        bool Delete(int dinhMucTuanId, out string message);
        List<DinhMucTuanModel> GetAllDinhMucTuans();
        bool CreateDinhMucTuan(DinhMucTuanModel model);
		bool ChangeStatus(int dinhMucTuanId, out string message);

        List<SelectListItem> GetListLoaiVatTu();

    }
    public class DinhMucTuanService : EntityService<DinhMucTuan>, IDinhMucTuanService
    {
        private readonly IDinhMucTuanRepository _dinhMucTuanRepository;
        private readonly ILoaiVatTuRepository _loaiVatTuRepository;

        public DinhMucTuanService(IUnitOfWork unitOfWork, IDinhMucTuanRepository dinhMucTuanRepository, ILoaiVatTuRepository loaiVatTuRepository)
            : base(unitOfWork, dinhMucTuanRepository)
        {
            _dinhMucTuanRepository = dinhMucTuanRepository;
            _loaiVatTuRepository = loaiVatTuRepository;
        }

        public List<DinhMucTuanModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var dinhMucTuanEntities = _dinhMucTuanRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (dinhMucTuanEntities != null)
				{
					return dinhMucTuanEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search DinhMucTuan error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdateDinhMucTuan(DinhMucTuanModel dinhMucTuanModel, out string message)
        {
            try
            {
                var dinhMucTuanEntity = _dinhMucTuanRepository.GetById(dinhMucTuanModel.DinhMucTuanId);
				if (dinhMucTuanEntity != null)
				{
					dinhMucTuanEntity = dinhMucTuanModel.MapToEntity(dinhMucTuanEntity);
					//dinhMucTuanEntity.MapToModel(dinhMucTuanModel);

					_dinhMucTuanRepository.Update(dinhMucTuanEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update DinhMucTuan error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreateDinhMucTuan(DinhMucTuanModel model)
        {
            try
            {
                var createdDinhMucTuan = _dinhMucTuanRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdDinhMucTuan == null)
                {
                    Log.Error("Create dinhMucTuan error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create DinhMucTuan error", ex);
                return false;
            }

        }

        public bool Delete(int dinhMucTuanId, out string message)
        {
            

            try
            {
                var entity = _dinhMucTuanRepository.GetById(dinhMucTuanId);
				if (entity != null)
				{
					_dinhMucTuanRepository.Delete(dinhMucTuanId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DinhMucTuan error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<DinhMucTuanModel> GetAllDinhMucTuans()
        {
            //Igrone dinhMucTuan system
            return _dinhMucTuanRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int dinhMucTuanId, out string message)
        {
            try
            {
                var entity = _dinhMucTuanRepository.Query(c => c.DinhMucTuanId == dinhMucTuanId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_dinhMucTuanRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete DinhMucTuan error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }

        public List<SelectListItem> GetListLoaiVatTu()
        {
            var listLoaiVatTu = _loaiVatTuRepository.GetAll().Select(x => new SelectListItem
            {
                Value = x.LoaiVatTuId.ToString(),
                Text = x.Title
            })
            .ToList();

            return listLoaiVatTu;
        }
    }
}
