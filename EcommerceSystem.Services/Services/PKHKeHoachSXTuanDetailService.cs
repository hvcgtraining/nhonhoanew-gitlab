﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHKeHoachSXTuanDetailModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHKeHoachSXTuanDetailService : IEntityService<PKHKeHoachSXTuanDetail>
    {
        List<PKHKeHoachSXTuanDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHKeHoachSXTuanDetail(PKHKeHoachSXTuanDetailModel PKHKeHoachSXTuanDetailModel, out string message);
        bool Delete(int pKHKeHoachSXTuanDetailId, out string message);
        List<PKHKeHoachSXTuanDetailModel> GetAllPKHKeHoachSXTuanDetails();
        bool CreatePKHKeHoachSXTuanDetail(PKHKeHoachSXTuanDetailModel model);
		bool ChangeStatus(int pKHKeHoachSXTuanDetailId, out string message);

    }
    public class PKHKeHoachSXTuanDetailService : EntityService<PKHKeHoachSXTuanDetail>, IPKHKeHoachSXTuanDetailService
    {
        private readonly IPKHKeHoachSXTuanDetailRepository _pKHKeHoachSXTuanDetailRepository;
        public PKHKeHoachSXTuanDetailService(IUnitOfWork unitOfWork, IPKHKeHoachSXTuanDetailRepository pKHKeHoachSXTuanDetailRepository)
            : base(unitOfWork, pKHKeHoachSXTuanDetailRepository)
        {
            _pKHKeHoachSXTuanDetailRepository = pKHKeHoachSXTuanDetailRepository;
        }

        public List<PKHKeHoachSXTuanDetailModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHKeHoachSXTuanDetailEntities = _pKHKeHoachSXTuanDetailRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHKeHoachSXTuanDetailEntities != null)
				{
					return pKHKeHoachSXTuanDetailEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHKeHoachSXTuanDetail error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHKeHoachSXTuanDetail(PKHKeHoachSXTuanDetailModel pKHKeHoachSXTuanDetailModel, out string message)
        {
            try
            {
                var pKHKeHoachSXTuanDetailEntity = _pKHKeHoachSXTuanDetailRepository.GetById(pKHKeHoachSXTuanDetailModel.PKHKeHoachSXTuanDetailId);
				if (pKHKeHoachSXTuanDetailEntity != null)
				{
					pKHKeHoachSXTuanDetailEntity = pKHKeHoachSXTuanDetailModel.MapToEntity(pKHKeHoachSXTuanDetailEntity);
					pKHKeHoachSXTuanDetailEntity.MapToModel(pKHKeHoachSXTuanDetailModel);

					_pKHKeHoachSXTuanDetailRepository.Update(pKHKeHoachSXTuanDetailEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHKeHoachSXTuanDetail error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHKeHoachSXTuanDetail(PKHKeHoachSXTuanDetailModel model)
        {
            try
            {
                var createdPKHKeHoachSXTuanDetail = _pKHKeHoachSXTuanDetailRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHKeHoachSXTuanDetail == null)
                {
                    Log.Error("Create pKHKeHoachSXTuanDetail error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHKeHoachSXTuanDetail error", ex);
                return false;
            }

        }

        public bool Delete(int pKHKeHoachSXTuanDetailId, out string message)
        {
            

            try
            {
                var entity = _pKHKeHoachSXTuanDetailRepository.GetById(pKHKeHoachSXTuanDetailId);
				if (entity != null)
				{
					_pKHKeHoachSXTuanDetailRepository.Delete(pKHKeHoachSXTuanDetailId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachSXTuanDetail error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHKeHoachSXTuanDetailModel> GetAllPKHKeHoachSXTuanDetails()
        {
            //Igrone pKHKeHoachSXTuanDetail system
            return _pKHKeHoachSXTuanDetailRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHKeHoachSXTuanDetailId, out string message)
        {
            try
            {
                var entity = _pKHKeHoachSXTuanDetailRepository.Query(c => c.PKHKeHoachSXTuanDetailId == pKHKeHoachSXTuanDetailId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHKeHoachSXTuanDetailRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachSXTuanDetail error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
