﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHKiemHoaModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHKiemHoaService : IEntityService<PKHKiemHoa>
    {
        List<PKHKiemHoaModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage);
        bool UpdatePKHKiemHoa(PKHKiemHoaModel PKHKiemHoaModel, out string message);
        bool Delete(int pKHKiemHoaId, out string message);
        List<PKHKiemHoaModel> GetAllPKHKiemHoas();
        bool CreatePKHKiemHoa(PKHKiemHoaModel model);
		bool ChangeStatus(int pKHKiemHoaId, out string message);

    }
    public class PKHKiemHoaService : EntityService<PKHKiemHoa>, IPKHKiemHoaService
    {
        private readonly IPKHKiemHoaRepository _pKHKiemHoaRepository;
        public PKHKiemHoaService(IUnitOfWork unitOfWork, IPKHKiemHoaRepository pKHKiemHoaRepository)
            : base(unitOfWork, pKHKiemHoaRepository)
        {
            _pKHKiemHoaRepository = pKHKiemHoaRepository;
        }

        public List<PKHKiemHoaModel> Search(int currentPage, int pageSize, string textSearch, string sortColumn, string sortDirection, out int totalPage)
        {
			try
            {
                var pKHKiemHoaEntities = _pKHKiemHoaRepository.Search(currentPage, pageSize, textSearch, sortColumn, sortDirection, out totalPage);
				if (pKHKiemHoaEntities != null)
				{
					return pKHKiemHoaEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHKiemHoa error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHKiemHoa(PKHKiemHoaModel pKHKiemHoaModel, out string message)
        {
            try
            {
                var pKHKiemHoaEntity = _pKHKiemHoaRepository.GetById(pKHKiemHoaModel.PKHKiemHoaId);
				if (pKHKiemHoaEntity != null)
				{
					pKHKiemHoaEntity = pKHKiemHoaModel.MapToEntity(pKHKiemHoaEntity);
					pKHKiemHoaEntity.MapToModel(pKHKiemHoaModel);

					_pKHKiemHoaRepository.Update(pKHKiemHoaEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHKiemHoa error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool CreatePKHKiemHoa(PKHKiemHoaModel model)
        {
            try
            {
                var createdPKHKiemHoa = _pKHKiemHoaRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHKiemHoa == null)
                {
                    Log.Error("Create pKHKiemHoa error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHKiemHoa error", ex);
                return false;
            }

        }

        public bool Delete(int pKHKiemHoaId, out string message)
        {
            

            try
            {
                var entity = _pKHKiemHoaRepository.GetById(pKHKiemHoaId);
				if (entity != null)
				{
					_pKHKiemHoaRepository.Delete(pKHKiemHoaId);
					UnitOfWork.SaveChanges();

					message = "Xóa bản ghi thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKiemHoa error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHKiemHoaModel> GetAllPKHKiemHoas()
        {
            //Igrone pKHKiemHoa system
            return _pKHKiemHoaRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHKiemHoaId, out string message)
        {
            try
            {
                var entity = _pKHKiemHoaRepository.Query(c => c.PKHKiemHoaId == pKHKiemHoaId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHKiemHoaRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKiemHoa error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
