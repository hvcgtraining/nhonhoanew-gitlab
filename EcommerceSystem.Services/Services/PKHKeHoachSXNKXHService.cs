﻿using EcommerceSystem.DataAccess;
using EcommerceSystem.DataAccess.Common;
using EcommerceSystem.DataAccess.Repositories;
using EcommerceSystem.Models.PKHKeHoachSXNKXHDetailModel;
using EcommerceSystem.Models.PKHKeHoachSXNKXHModel;
using EcommerceSystem.Services.AutoMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcommerceSystem.Services
{
    public interface IPKHKeHoachSXNKXHService : IEntityService<PKHKeHoachSXNKXH>
    {
        List<PKHKeHoachSXNKXHModel> Search(int currentPage, int pageSize, string textSearch, DateTime? fromDate, DateTime? toDate, string sortColumn, string sortDirection, out int totalPage, bool? status = null);
        bool UpdatePKHKeHoachSXNKXH(PKHKeHoachSXNKXHModel PKHKeHoachSXNKXHModel, out string message);
        bool UpdateSoLuongCan(long pKHKeHoachSXNKXHId, int value);
        bool Delete(long pKHKeHoachSXNKXHId, out string message);
        List<PKHKeHoachSXNKXHModel> GetAllPKHKeHoachSXNKXHs();
        bool CreatePKHKeHoachSXNKXH(PKHKeHoachSXNKXHModel model);
		bool ChangeStatus(int pKHKeHoachSXNKXHId, out string message);

    }
    public class PKHKeHoachSXNKXHService : EntityService<PKHKeHoachSXNKXH>, IPKHKeHoachSXNKXHService
    {
        private readonly IPKHKeHoachSXNKXHRepository _pKHKeHoachSXNKXHRepository;
        private readonly IPKHKeHoachSXNKXHDetailRepository _pKHKeHoachSXNKXHDetailRepository;
        public PKHKeHoachSXNKXHService(IUnitOfWork unitOfWork, IPKHKeHoachSXNKXHRepository pKHKeHoachSXNKXHRepository, IPKHKeHoachSXNKXHDetailRepository pKHKeHoachSXNKXHDetailRepository)
            : base(unitOfWork, pKHKeHoachSXNKXHRepository)
        {
            _pKHKeHoachSXNKXHRepository = pKHKeHoachSXNKXHRepository;
            _pKHKeHoachSXNKXHDetailRepository = pKHKeHoachSXNKXHDetailRepository;
        }

        public List<PKHKeHoachSXNKXHModel> Search(int currentPage, int pageSize, string textSearch, DateTime? fromDate, DateTime? toDate, string sortColumn, string sortDirection, out int totalPage, bool? status = null)
        {
			try
            {
                var pKHKeHoachSXNKXHEntities = _pKHKeHoachSXNKXHRepository.Search(currentPage, pageSize, textSearch, fromDate, toDate, sortColumn, sortDirection, out totalPage, status);
				if (pKHKeHoachSXNKXHEntities != null)
				{
					return pKHKeHoachSXNKXHEntities.MapToModels();
				}
            }
            catch (Exception ex)
            {
                Log.Error("Search PKHKeHoachSXNKXH error", ex);
            }
            totalPage = 0;
            return null;
        }

        public bool UpdatePKHKeHoachSXNKXH(PKHKeHoachSXNKXHModel pKHKeHoachSXNKXHModel, out string message)
        {
            try
            {
                var pKHKeHoachSXNKXHEntity = _pKHKeHoachSXNKXHRepository.GetById(pKHKeHoachSXNKXHModel.PKHKeHoachSXNKXHId);
				if (pKHKeHoachSXNKXHEntity != null)
				{
					pKHKeHoachSXNKXHEntity = pKHKeHoachSXNKXHModel.MapToEntity(pKHKeHoachSXNKXHEntity);
					pKHKeHoachSXNKXHEntity.MapToModel(pKHKeHoachSXNKXHModel);

					_pKHKeHoachSXNKXHRepository.Update(pKHKeHoachSXNKXHEntity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật thành công";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHKeHoachSXNKXH error", ex);
            }
            message = "Cập nhật bản ghi thất bại.";
            return false;
        }

        public bool UpdateSoLuongCan(long pKHKeHoachSXNKXHId, int value)
        {
            try
            {
                var pKHKeHoachSXNKXHEntity = _pKHKeHoachSXNKXHRepository.GetById(pKHKeHoachSXNKXHId);
                if (pKHKeHoachSXNKXHEntity != null)
                {
                    var currentValue = pKHKeHoachSXNKXHEntity.SoLuongCan.HasValue ? pKHKeHoachSXNKXHEntity.SoLuongCan.Value : 0;
                    pKHKeHoachSXNKXHEntity.SoLuongCan = currentValue + value;
                    
                    _pKHKeHoachSXNKXHRepository.Update(pKHKeHoachSXNKXHEntity);
                    UnitOfWork.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Update PKHKeHoachSXNKXH error", ex);
            }
            return false;
        }

        public bool CreatePKHKeHoachSXNKXH(PKHKeHoachSXNKXHModel model)
        {
            try
            {
                var createdPKHKeHoachSXNKXH = _pKHKeHoachSXNKXHRepository.Insert(model.MapToEntity());
                UnitOfWork.SaveChanges();
                var errorMsg = string.Empty;
                if (createdPKHKeHoachSXNKXH == null)
                {
                    Log.Error("Create pKHKeHoachSXNKXH error");
                    return false;
                }
                model.PKHKeHoachSXNKXHId = createdPKHKeHoachSXNKXH.PKHKeHoachSXNKXHId;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Create PKHKeHoachSXNKXH error", ex);
                return false;
            }

        }

        public bool Delete(long pKHKeHoachSXNKXHId, out string message)
        {
            

            try
            {
                var entity = _pKHKeHoachSXNKXHRepository.GetById(pKHKeHoachSXNKXHId);
                if (entity != null)
				{
                    var detailEntities = _pKHKeHoachSXNKXHDetailRepository.GetAll().Where(p => p.PKHKeHoachSXNKXHId == pKHKeHoachSXNKXHId);
                    if (detailEntities != null && detailEntities.Any())
                    {
                        _pKHKeHoachSXNKXHDetailRepository.DeleteMulti(detailEntities.ToList());
                        //_pKHKeHoachSXNKXHRepository.Delete(pKHKeHoachSXNKXHId);
                        //UnitOfWork.SaveChanges();

                        //message = "Xóa bản ghi thành công";
                        //return true;
                    }
                    _pKHKeHoachSXNKXHRepository.Delete(pKHKeHoachSXNKXHId);
                    UnitOfWork.SaveChanges();

                    message = "Xóa bản ghi thành công";
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachSXNKXH error", ex);
            }

            message = "Xóa bản ghi thất bại";
            return false;
        }

        public List<PKHKeHoachSXNKXHModel> GetAllPKHKeHoachSXNKXHs()
        {
            //Igrone pKHKeHoachSXNKXH system
            return _pKHKeHoachSXNKXHRepository.GetAll().ToList().MapToModels();
        }

		public bool ChangeStatus(int pKHKeHoachSXNKXHId, out string message)
        {
            try
            {
                var entity = _pKHKeHoachSXNKXHRepository.Query(c => c.PKHKeHoachSXNKXHId == pKHKeHoachSXNKXHId).FirstOrDefault();
				if (entity != null)
				{
					if (entity.Status)
					{
						entity.Status = false;
					}
					else
					{
						entity.Status = true;
					}

					_pKHKeHoachSXNKXHRepository.Update(entity);
					UnitOfWork.SaveChanges();

					message = "Cập nhật trạng thái thành công.";
					return true;
				}
            }
            catch (Exception ex)
            {
                Log.Error("Delete PKHKeHoachSXNKXH error", ex);
            }

            message = "Cập nhật trạng thái thất bại.";
            return false;
        }
    }
}
